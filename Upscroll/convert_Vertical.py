import Image
import sys
import struct

background = (255, 255, 255)
foreground = (0, 0, 0)

def p(x, y):
	if rgb.getpixel((x, y)) == foreground:
		return 1
	if rgb.getpixel((x, y)) == background:
		return 0
	print "Error at", x, y, ":", rgb.getpixel((x, y))
	sys.exit(1)


img = Image.open("Vertical_logo.png")
rgb = img.convert('RGB')
if rgb.size[1] != 8:
	print "Height error:", rgb.size[1]
	sys.exit(1)

for x in range(rgb.size[0] - 1, -1, -1):
	out = 128*p(x, 0) + 64*p(x, 1) + 32*p(x, 2) + 16*p(x, 3) + 8*p(x, 4) + 4*p(x, 5) + 2*p(x, 6) + 1*p(x, 7)
	sys.stdout.write(struct.pack('B', out))
