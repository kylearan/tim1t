import Image
import sys
import math
import struct

width = 5
height = 6

background = (255, 255, 255)
foreground = (0, 0, 0)

def p(x, y):
	if rgb.getpixel((x, y)) == foreground:
		return 1
	if rgb.getpixel((x, y)) == background:
		return 0
	print "Error at", x, y, ":", rgb.getpixel((x, y))
	sys.exit(1)


img = Image.open("Font_5x6.png")
rgb = img.convert('RGB')
if rgb.size[0] != (width + 1)*8 or rgb.size[1] != (height + 1)*8:
	print "Width error:", rgb.size[0], rgb,size[1]
	sys.exit(1)

for l in range(height - 1, -1, -1):
	for c in range(0, 27):
		c_x  = (c%8)*(width + 1)
		c_y = 4*(height + 1) + int(c/8)*(height + 1) + l
		out = 0
		out += 16*p(c_x + 0, c_y)	# ball
		out += 2*p(c_x + 1, c_y)	# P0
		out += 4*p(c_x + 2, c_y)	# M0
		out += 1*p(c_x + 3, c_y)	# P1
		out += 8*p(c_x + 4, c_y)	# M1
		sys.stdout.write(struct.pack('B', out))
