; y: line index
; x: char row index
; do dey/check for end of frame during scroller display, before logo

	lda #UP_DOTHEIGHT-1	; FIXME: Variable
	sta UpRowScanCount
	sta WSYNC
	SLEEP 10
	jmp .initRow

	; ------------------------------
	ALIGN $100
.endOfFrameBailOut
	; @29
	jmp .endOfFrame

	; ------------------------------
	; Init a new char row
	; jump here for UpRowScanCount < UP_DOTHEIGHT
.initRow
	; @13, before scroller
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3
	lda UpCharBLColor	; !3
	sta COLUPF		; !3

	; @24, during scroller display
	; Check for end of frame
	dey			; 2
	beq .endOfFrameBailOut	; 2
	
	SLEEP 7
	
	; @35, after scroller, before logo
	; Display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @49, during logo and hblank
	; Display new char row
	lda UpCharBits,x	; 4
	sta ENAM0		; 3
	lsr			; 2
	sta ENAM1		; 3
	lsr			; 2
	sta ENABL		; 3
	lda UpCharP0,x		; 4
	sta GRP0		; 3
	lda UpCharP1,x		; 4
	; ----------
	sta GRP1		; 3, @1

	SLEEP 2

	; ------------------------------
	; Display UpRowScanCount scanlines of current char row
	; @6
.wasteRow
	; check for last row scanline
	dec UpRowScanCount	; 5
	beq .endOfRow		; 2

	; @13
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3
	lda UpCharBLColor	; !3
	sta COLUPF		; !3

	; @24
	; Check for end of frame
	dey			; 2
	beq .endOfFrameBailOut	; 2

	SLEEP 7
	
	; @35, after scroller, before logo
	; Display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @49, during logo and hblank
	; Could do something for 27 cycles...
	sta WSYNC
	SLEEP 3
	jmp .wasteRow		; 3

	; ------------------------------
	; Last line of row.
	; Start new row, or set up new char if end of char is reached.
	; @12
.endOfRow
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3
	lda UpCharBLColor	; !3
	sta.w COLUPF		; !4

	; @24
	; Check for end of frame
	dey			; 2
	beq .endOfFrameBailOut	; 2

	; @28
	; decrease row index and check for end of char
	dex			; 2
	bmi .endOfChar		; 2
	
	; @32
	; set UpRowScanCount
	lda #UP_DOTHEIGHT-1	; 2
	sta UpRowScanCount	; 3
	
	; @37, after scroller, before logo
	; Display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @51
	; Could do something for 35 cycles...
	sta WSYNC
	SLEEP 10

	; @10
	jmp .initRow		; 3
	

	; ------------------------------
	; End of char: Display last row scanline and set up new char
	; @33
.endOfChar
	xxx


	; ------------------------------
	; End of frame: Display last logo line and clean up
	; @32
.endOfFrame
	; display last logo line
	SLEEP 3
	; @35, after scroller, before logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3
	; clean-up
	lda #0
	sta WSYNC
	sta PF1
	sta ENABL
	sta GRP0
	sta ENAM0
	sta GRP1
	sta ENAM1
	
	xxx
	


----------------------------------------------------------------------
		
	

; Display 10 lines of prepared font pixel data. Update COLUPF and PF1
; each line, use rest of time for precomputing the values for next line,
; especially the HMMx values
; 
; If too tight, move to ZP, saves 6 cycles
;
; Variations:
;
; - zoom: expand to double 3; 1,3,5; 1,2,3,4,5; then same to quadruple
; - do ripple zoom over letter; over pixel line
; - ripple: move expand 2,4,2 over each line
; - tech-tech per scanline

	; Handover between pixel lines
	
	; y: scanline number
	
.pixelLoop
	; @55
	lda #PFCOL		; 2
	sta COLUPF		; 3, before 60
	; @60
	lda LetterBits		; 3
	sta ENAM0		; 3
	lsr			; 2
	sta ENAM1		; 3
	lsr			; 2
	sta ENABL		; 3
	
	; ------------------------------
	sta HMOVE		; 3
	lda ParallaxLine,y	; 4
	sta PF1			; 3, before 23

	; @10
	lda LetterP0		; 3
	sta GRP0		; 3
	lda LetterP1		; 3
	sta GRP1		; 3
	lda ColorP0		; 3
	sta COLUP0		; 3

	; @28
	lda LetterBallCol	; 3
	sta COLUPF		; 3, before 38

	; @34
	lda ColorP1		; 3
	sta COLUP1		; 3

	; @40
	iny			; 2

	; leaves 13 cycles



; Variant: hmove bar, right logo

	sta WSYNC
	sta HMOVE		; 3
	
	lda #0			; 2, 0-27
	sta PF1			; 3
	lda ColorBL		; 3
	sta COLUPF		; 3
	
	lda LetterBits		; 3
	sta ENAM0		; 3
	lsr			; 2
	sta ENAM1		; 3
	lsr			; 2
	sta ENABL		; 3
	; @30
	lda LetterP0		; 3
	sta GRP0		; 3
	lda LetterP1		; 3
	sta GRP1		; 3
	
	lda Logo,x		; 4, 30-60
	sta PF1			; 3
	lda Col,x		; 4
	sta COLUPF		; 3

	lda ColorP0		; 3
	sta COLUP0		; 3
	lda ColorP1		; 3
	sta COLUP1		; 3






; Parallax 1, aligned data
	sty tmp			; 3
	tya			; 2
	and #7			; 2
	tay			; 2
	lda (),y		; 5
	sta PF1			; 3
	ldy tmp			; 3
				; =20

; Parallax 2, main loop uses TIM1024T
	dey			; 2
	bpl .noReset		; 2*
	ldy #5			; 2
.noReset
	lda (),y		; 5
	sta PF1			; 3
				; =14*

