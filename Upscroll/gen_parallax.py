#!/usr/bin/python3

import sys
import argparse
import math
import struct

width = 2	# size difference between bars
slowest = 1.0	# speed of slowest bar
factor = 2.0	# compared to speed of smallest bar

repeat = 6*width
size = repeat*factor*(1/slowest)

s = [factor*slowest, 0, 0, 0, slowest]
r0 = (size*s[0])/repeat
r4 = (size*s[4])/repeat
r_step = (r0-r4)/4.0

r3 = round(r4 + 1*r_step)
s[3] = (repeat*r3)/size

r2 = round(r4 + 2*r_step)
s[2] = (repeat*r2)/size

r1 = round(r4 + 3*r_step)
s[1] = (repeat*r1)/size


def gen_parallax():
	outfile = sys.stdout.buffer
	
	# for each frame
	for frame in range(int(size)):
		# start with "empty" PF1 array (only leftmost bit set)
		b = [128]*repeat
		# mask to or with, starting with leftmost bar at speed s[0]
		mask = 64
		# draw each bar
		for bar in range(5):
			# calc pos of bar modulo repeat
			pos = round(frame*s[bar])%repeat
			# draw bar of height (width*(5 - bar))
			for i in range(width*(5 - bar)):
				b[pos] += mask
				pos = (pos + 1)%repeat
			# next bar, move mask one bit right 
			mask /= 2
		# output frame
		for i in range(repeat):
			out = int(b[i])
#			print("%s" % bin(out))
			outfile.write(struct.pack('B', out))

		# align (optional)
#		for i in range(int(pow(2, int(math.sqrt(repeat)) + 1) - repeat)):
#			out = 0
#			print("%s" % bin(out))
#			outfile.write(struct.pack('B', out))

			
#		print("\n")


if __name__ == "__main__":
	gen_parallax()

