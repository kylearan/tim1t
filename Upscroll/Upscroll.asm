; =========================================================================
; Constants
; =========================================================================

; Script state constants

UP_LOGO_IN		= 0
UP_SCROLLER		= 1
UP_LOGO_OUT		= 2
UP_NEXT_PART		= 3


; Scroller constants

UP_SPEED_INCREASE	= 1

UP_DOTHEIGHT		= 7	; #scanlines per dot
UP_CHARGAP		= 15	; #scanlines between chars
UP_PEEKINDEX		= 8

UP_FX_NORMAL		= 128
UP_FX_PULSATING		= 129
UP_FX_TWIRL		= 130
UP_FX_SINE		= 131
UP_FX_RIPPLE		= 132
UP_FX_EOT		= 133

UP_FX_NORMAL_DUR	= 8
UP_FX_PULSATING_DUR	= 10
UP_FX_TWIRL_DUR		= 14
UP_FX_SINE_DUR		= 13

UP_FX_TWIRL_ADVANCE	= 14



; =========================================================================
; Start
; =========================================================================

UpscrollStart

	jsr UpInit
	jmp UpIncoming
	

	
; =========================================================================
; Kernel
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

UpKernel
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc UpNumFrame
	

; -------------------------------------------------------------------------

UpIncoming SUBROUTINE

	; Multiplex demo state
	lda UpDemoState
	cmp #UP_LOGO_IN
	bne .noLogoIn
	
	; Logo in
	; ------------------------------
	
	; Move
	ldx UpLogoLetterLock
	cpx #7
	bne .noNextState
	inc UpDemoState
	jmp .stateScroller
.noNextState
.moveLoop
	; increase and calc sin
	inc UpLogoLetterCounter,x
	inc UpLogoLetterCounter,x
	lda UpLogoLetterLock
	beq .firstNotTopYet
	inc UpLogoLetterCounter,x
.firstNotTopYet
	lda UpLogoLetterCounter,x
	and #127
	tay
	lda UpTwirlSin,y
	sec
	sbc #4			; normalize to 0
	; First letter?
	cpx #0			; first letter -> move in whole logo
	bne .notFirstLetter
	; Then increase related to frame counter
	sta UpTmp
	lda UpNumFrame
	asl
	cmp #220
	bcc .notTopYet
	lda #220
.notTopYet
	sta UpTmp2
	lda #220
	sec
	sbc UpTmp2
	clc
	adc UpTmp
	; First letter reached top?
	bne .noStartLocking
	inc UpStartLocking
.noStartLocking
.notFirstLetter
	sta UpLogoLetterGap,x
	; Are we in lock mode?
	lda UpStartLocking
	beq .noLockMode
	; Has letter reached 0 gap?
	lda UpLogoLetterGap,x
	bne .noLockMode
	; Is this the next letter to be locked?
	txa
	cmp UpLogoLetterLock
	bne .noLockMode
	inc UpLogoLetterLock
.noLockMode
	inx
	cpx #7
	bne .moveLoop

	; Position object
UpPositionInOutLogo
	sta WSYNC
	lda #%00000111		; quad-sized player
	sta NUSIZ0
	lda UpLogoXPos
	SLEEP 6
	sec
.div15
	sbc #15
	bcs .div15
	sta RESP0
	eor #7
	asl
	asl
	asl
	asl
	sta HMP0
	sta WSYNC
	sta HMOVE
	jmp UpWaitForVBlank


.noLogoIn
	cmp #UP_LOGO_OUT
	bne .noLogoOut

	; Logo out
	; ------------------------------
	
	dec UpLogoXPos
	bne .noNextPart
	JMB EndStart
.noNextPart
	dec UpLogoXPos

	jsr UpMoveLogoOut
		
	jmp UpPositionInOutLogo

	
.noLogoOut

.stateScroller
	; Scrolling
	; ------------------------------
	
	jsr UpAdvanceScroll
	lda UpDemoState
	cmp #UP_LOGO_OUT
	beq UpPositionInOutLogo
	
	jsr UpAdvanceFx


	; ------------------------------
	; Multiplex first effect and init objects accordingly
	; ------------------------------

UpInitFirstChar SUBROUTINE

	lda UpFxType
	cmp #UP_FX_NORMAL
	bne .notNormal

	; UP_FX_NORMAL
	; ------------------------------

	sta WSYNC
	
	; @0
	; Color
	lda UpFxColor		; 3
	sta COLUP0		; 3
	sta COLUP1		; 3
	sta UpCharBLColor	; 3

	; @12
	; Size
	lda #%00100001		; 2, Ball size 4, PF mirror
	sta CTRLPF		; 3
	lda #%00100000		; 2, Missile size 4
	sta NUSIZ0		; 3
	sta NUSIZ1		; 3
	
	SLEEP 2

	; Position objects
	; @27
	sta RESBL
	sta RESP0
	sta RESM0
	sta RESP1
	sta RESM1

.normalFinePosition
	sta HMCLR
	lda #RIGHT_8
	sta HMBL
	lda #RIGHT_4
	sta HMP0
	lda #LEFT_6
	sta HMP1
	lda #LEFT_7
	sta HMM1
	sta WSYNC
	sta HMOVE
	;SLEEP 24
	jsr UpRTS
	jsr UpRTS
	sta HMCLR
	lda #RIGHT_2
	sta HMBL
	lda #LEFT_3
	sta HMM1
	sta WSYNC
	sta HMOVE
	
	jmp UpPrepareScroll

	; ------------------------------

.notNormal
	cmp #UP_FX_PULSATING
	bne .notPulsating

	; UP_FX_PULSATING
	; UpFxData1: Animation frame counter
	; UpFxData2: Animation delta between chars
	; HMM values B, P0, M0, P1, M1 (- = right, + = left)
	;
	; 4-4-4-4-4: -10, -4, 0, +6, +10
	; 4-4-8-4-4: -8, -2, +2, +4, +8
	; 8-4-8-4-8: -4, -2, +2, +4, +8
	; 8-8-8-8-8: -1, +3, +2, +5, +4
	; ------------------------------

	; get animation state index
	lda UpFxData1		; 3
	lsr			; 2
	lsr			; 2
	and #7			; 2
	tax			; 2
	lda UpPulsatingStates,x	; 4

	sta WSYNC

	tax			; 2
	
	; @2
	; Set sizes
	lda UpPulsatingCTRLPF,x	; 4
	sta CTRLPF		; 3		
	lda UpPulsatingNUSIZ0,x	; 4
	sta NUSIZ0		; 3
	lda UpPulsatingNUSIZ1,x	; 4
	sta NUSIZ1		; 3

	; @23
	; Fine positions #1
	lda UpPulsatingHMBL_1,x	; 4

	; @27
	; Position objects
	sta RESBL		; 3
	sta RESP0		; 3
	sta RESM0		; 3
	sta RESP1		; 3
	sta RESM1		; 3

	; @42
	sta HMBL		; 3
	lda UpPulsatingHMP0,x	; 4
	sta HMP0		; 3
	lda UpPulsatingHMP1,x	; 4
	sta HMP1		; 3
	lda UpPulsatingHMM0,x	; 4
	sta HMM0		; 3
	lda UpPulsatingHMM1,x	; 4
	sta HMM1		; 3

	; @73
	; Color
	lda UpFxColor		; 3

	; @0
	sta HMOVE		; 3
	
	; @3
	ora UpPulsatingColor,x	; 4
	sta UpCharBLColor	; 3
	sta COLUP0		; 3
	sta COLUP1		; 3

	; @16
	jsr UpRTS		; 12
	; @28
	; Fine positions #2
	lda UpPulsatingHMBL_2,x	; 4
	sta HMBL		; 3
	lda #0			; 2
	sta HMP0		; 3
	sta HMP1		; 3
	sta HMM0		; 3

UpHMovePrepareScroll
	; @46
	sta WSYNC
	sta HMOVE
	
	jmp UpPrepareScroll
	
	; ------------------------------

.notPulsating
	cmp #UP_FX_TWIRL
	bne .notTwirl

	; UP_FX_TWIRL
	; ------------------------------

	; Size and Priority
	lda #%00100001		; 2, Ball size 4, PF mirror
	sta CTRLPF		; 3
	lda #%00100000		; 2, Missile size 4
	sta NUSIZ0		; 3
	sta NUSIZ1		; 3	
	; Get twirl state
	lda UpFxData1
	and #127
	tay
	cpy #63
	bcc .noOtherPriority
	lda #%00100101		; Ball size 4, Above Players, PF mirror
	sta CTRLPF
.noOtherPriority

	; Color
	lda UpFxColor		; 3
	ora UpTwirlSinCol,y	; 4
	sta UpCharBLColor	; 3
	and #$f0		; 3
	ora UpTwirlSinCol+4,y	; 4
	sta COLUP0		; 3
	and #$f0		; 3
	ora UpTwirlSinCol+12,y	; 4
	sta COLUP1		; 3

	; Position objects
	lda #4
	sta UpTmp
.twirlPosLoop
	sta WSYNC
	ldx UpTmp		; 3
	lda UpObjIndex,x	; 4
	tax			; 2
	lda UpTwirlSin,y	; 4
	sec			; 2
	sbc UpTwirlCorrection,x	; 4
	
	; @19
	sec
twirlDivide15
	sbc #15
	QBCS twirlDivide15
	sta.w RESP0,x
	eor #$07
	asl
	asl
	asl
	asl
	sta HMP0,x
	
	tya
	clc
	adc #4
	and #127
	tay
	dec UpTmp
	bpl .twirlPosLoop
	
	jmp UpHMovePrepareScroll


	; ------------------------------

.notTwirl

	; UP_FX_SINE
	; ------------------------------

	; Color
	lda UpFxColor		; 3
	sta COLUP0		; 3
	sta COLUP1		; 3
	sta UpCharBLColor	; 3

	; Size and Priority
	lda #%00100001		; 2, Ball size 4, PF mirror
	sta CTRLPF		; 3
	lda #%00100000		; 2, Missile size 4
	sta WSYNC
	; @0
	sta NUSIZ0		; 3
	sta NUSIZ1		; 3	
	; Position objects
	lda UpFxData1		; 3
	and #127		; 2
	tay			; 2
	lda UpTwirlSin,y	; 4
	lsr			; 2
	lsr
	
	; @21
	sec
sineDivide15
	sbc #15
	bcs sineDivide15
	sta RESBL
	sta RESP0
	sta RESM0
	sta RESP1
	sta RESM1

	eor #$07
	asl
	asl
	asl
	asl
	sta HMBL
	sta HMP0
	sta HMM0
	sta HMP1
	sta HMM1	
	sta WSYNC
	sta HMOVE
	
	;SLEEP 24
	jsr UpRTS
	jsr UpRTS
	jmp .normalFinePosition

	

; -------------------------------------------------------------------------

UpPrepareScroll SUBROUTINE

	; prepare first char
	ldx UpCharIndex
	stx UpCurrentChar
	lda UpText,x
	sec
	sbc #64+27		; +27 because of initial adc #27 in loop
	sta UpTmp
	ldy #0
.loop
	lda UpTmp
	clc
	adc #27
	sta UpTmp
	tax
	lda UpFont,x
	ror
	sta UpCharBits,y
	rol
	and #3
	tax
	lda UpCharP0Table,x
	sta UpCharP0,y
	lda UpCharP1Table,x
	sta UpCharP1,y
	iny
	cpy #5
	bne .loop

	; copy fx data
	ldx #0
.copyFxData
	lda UpFxData1,x
	sta UpCurrentFxData1,x
	lda UpFxData2,x
	sta UpCurrentFxData2,x
	inx
	lda UpFxType,x
	bne .copyFxData



; -------------------------------------------------------------------------
	
UpWaitForVBlank

	; Wait for end of VBLANK
	jsr UpWaitForIntim
	lda #0
	sta VBLANK



; -------------------------------------------------------------------------
; Visible area Kernels
;
; Measures:
;
; - leftmost position: @20+ 0  (@20 entering div15 -> RES @ 30)
; - BL (left) center:     +16
; - M1 (right) center:    +32
; - rightmost position:   +48
;
; -> amplitude of 0...48
; -> COLUPF before ~c26
; -> Logo gfx after c39, before c60
; -> Logo color after ~c44, before c60
;
; Char format:
; BL - P0 - M0 - P1 - M1
;
; Font byte format:
;  7    6    5    4    3    2    1    0
;  -    -    -    B   M_1  M_0  P_0  P_1
; -------------------------------------------------------------------------

UpVisible SUBROUTINE

	sta WSYNC

	; multiplex demo state
	lda UpDemoState
	cmp #UP_SCROLLER
	beq .scrollerState	; unconditional

.logoInOut
	; Display logo in/out
	lda #%00100001		; Ball size 4, PF mirror
	sta CTRLPF
	lda #%00001000
	sta REFP0

	ldx #0			; current gap+letter number
	ldy #220		; logo line to display
	sty UpTmp		; frame scanline count
	inc UpTmp
	sta WSYNC
UpDisplayGapLetterLoop
	lda UpLogoLetterGap,x
	beq .afterGap
	sta UpTmp2		; gap before next letter
.gapLoop
	sta WSYNC
	dec UpTmp
	beq .endLogoInOut
	lda #0
	sta GRP0
	dec UpTmp2
	bne .gapLoop
.afterGap
	lda UpLogoLetterSizes,x
	sta UpTmp2		; size of next letter
UpLetterLoop
	sta WSYNC
	dec UpTmp
	beq .endLogoInOut
	dey
	lda UpVertical_gfx,y
	sta GRP0
	lda UpVertical_col,y
	sta COLUP0
	dec UpTmp2
	QBNE UpLetterLoop
	inx
	bne UpDisplayGapLetterLoop	; unconditional jmp
	
.endLogoInOut
	jsr UpRTS
	lda #0
	sta GRP0
	sta REFP0
	jmp UpWasteBeforeVBLank
	
	
	
	; ------------------------------
	
.scrollerState

; y: line index
; x: char row index
; do dey/check for end of frame during scroller display, before logo

	sta WSYNC
	lda #0			; 2
	sta UpCurrentFx		; 3
	ldy #220		; 2
	lda UpRowScanline	; 3
	
	; Do HMCLR
	jsr UpRTS		; 12
	nop			; 2
	sta HMCLR
	
	; Do we have to start in a char gap?
	cmp #UP_DOTHEIGHT + 1
	bcc .startInChar
	
	; Start in gap
	sec
	sbc #UP_DOTHEIGHT
	tax
	lda #UP_DOTHEIGHT
	sta UpRowScanCount

	jmp WaitForInitRow
	
	; start in char
.startInChar
	sta WSYNC
	sta UpRowScanCount
	ldx UpRowIndex
	SLEEP 4
	jmp .initRow


UpScrollKernel
	ALIGN $100

	; ------------------------------
	; If y reaches 0, branch here @29.
	; It's here to be reachable via relative branch.
	; ------------------------------
.endOfFrameBailOut
	; @29
	jmp .endOfFrame



	; ------------------------------
	; Init a new char row
	; Works even for UpRowScanCount < UP_DOTHEIGHT
	; ------------------------------
.initRow
	; @13, before scroller
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3
	lda UpCharBLColor	; !3
	sta COLUPF		; !3

	; ##############################
	; @24, during scroller display
	; Check for end of frame
	dey			; 2
	beq .endOfFrameBailOut	; 2
	
	SLEEP 7
	
	; @35, after scroller, before logo
	; Display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @49, during logo and hblank
	; Display new char row
	lda UpCharBits,x	; 4
	sta ENAM0		; 3
	lsr			; 2
	sta ENAM1		; 3
	lsr			; 2
	sta ENABL		; 3
	lda UpCharP0,x		; 4
	sta GRP0		; 3
	lda UpCharP1,x		; 4
	; ----------
	sta GRP1		; 3, @1

	SLEEP 2

	; ------------------------------
	; Display UpRowScanCount scanlines of current char row
	; ------------------------------
	; @6
.wasteRow
	; check for last row scanline
	dec UpRowScanCount	; 5
	beq .endOfRow		; 2

.wasteRowAfterRowCheck
	; @13
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3
	lda UpCharBLColor	; !3
	sta COLUPF		; !3

	; ##############################
	; @24
	; Check for end of frame
	dey			; 2
	beq .endOfFrameBailOut	; 2

	SLEEP 7
	
	; @35, after scroller, before logo
	; Display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @49, during logo and hblank
	; Could do something for 27 cycles...
	sta WSYNC
	SLEEP 3
	jmp .wasteRow		; 3

	; ------------------------------
	; Last line of row.
	; Start new row, or set up new char if end of char is reached.
	; ------------------------------
	; @12
.endOfRow
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3
	lda UpCharBLColor	; !3
	sta.w COLUPF		; !4

	; ##############################
	; @24
	; Check for end of frame
	dey			; 2
	beq .endOfFrameBailOut	; 2

	; @28
	; decrease row index and check for end of char
	dex			; 2
	bmi .endOfChar		; 2
	
	; @32
	; set UpRowScanCount
	lda #UP_DOTHEIGHT-1	; 2, -1 b/c we skip one dec when jmping into .wasterow
	sta UpRowScanCount	; 3
	
	; @37, after scroller, before logo
	; Display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @51
	; Display new char row
	lda UpCharBits,x	; 4
	sta ENAM0		; 3
	lsr			; 2
	sta ENAM1		; 3
	lsr			; 2
	sta ENABL		; 3
	lda UpCharP0,x		; 4
	sta GRP0		; 3
	lda UpCharP1,x		; 4
	; ----------
	sta GRP1		; 3, @3
	
	; @6
	SLEEP 4
	jmp .wasteRowAfterRowCheck
	
	

	; ------------------------------
	; If y reaches 0, branch here @29.
	; It's here to be reachable via relative branch.
	; ------------------------------
.endOfFrameBailOut2
	; @29
	jmp .endOfFrame


	; ------------------------------
	; End of char: Display last row scanline and set up new char
	; ------------------------------
	; @33
.endOfChar
	; set index to next char
	ldx UpCurrentChar	; 3
	inx			; 2
	
	; @38, after scroller, before logo
	; Display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @52
	; clear objects.
	; From now on, COLUPF only needs to be updated for logo.
	lda #0			; 2
	sta GRP0		; 3
	sta GRP1		; 3
	sta ENAM0		; 3
	sta ENAM1		; 3
	sta ENABL		; 3
	
	; @69
	; Get next char
	lda UpText,x		; 4
	bpl .noNewEffect	; 2/3
	; @75
	; Increase index to current fx
	inc UpCurrentFx		; 5
	inx			; 2
	inx			; 2
	bne .newEffectAlign	; 3, unconditional

.noNewEffect
	; @0
	SLEEP 11

.newEffectAlign
	; @11
	stx UpCurrentChar	; 3
	
	; @14
	; check if we have time to display a new char
	cpy #UP_CHARGAP+1	; 2
	bcs setUpNewChar	; 2/3 

	; @18
	; Display logo for the rest of the lines, then exit
.wasteRest
	; Remove logo
	lda #0			; !2, 71 - 26
	sta.w PF1		; !4

	; ##############################
	; @24
	; Check for end of frame
	dey			; 2
	beq .endOfFrameBailOut2	; 2

	; @28
	jsr UpRTS		; 12
	
	; @40, display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @54
	sta WSYNC
	; @0
	jsr UpRTS		; 12
	SLEEP 3			; 3
	jmp .wasteRest		; 3


	; ------------------------------
	; Set up new char/effect
	; ------------------------------
	; @19
setUpNewChar
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3
	
	; ##############################

	; @24
	; Calc index into font
	; ------------------------------
	lda UpText,x		; 4
	sec			; 2
	sbc #64			; 2
	tax			; 2
	
	; @36, display logo
	dey			; 2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @52
	; Font line 1
	; ------------------------------
	lda UpFont+0,x		; 4
	ror			; 2
	sta UpCharBits+0	; 3
	rol			; 2
	and #3			; 2
	stx UpTmp		; 3
	tax			; 2
	lda UpCharP0Table,x	; 4
	sta UpCharP0+0		; 3
	lda UpCharP1Table,x	; 4
	sta UpCharP1+0		; 3
	ldx UpTmp		; 3
	
	; @11
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @16
	; Font line 2
	; ------------------------------
	lda UpFont+27,x		; 4
	ror			; 2

	; ##############################

	sta UpCharBits+1	; 3
	rol			; 2
	and #3			; 2
	stx UpTmp		; 3
	tax			; 2
	lda UpCharP0Table,x	; 4
	sta UpCharP0+1		; 3

	; @41, display logo
	dey			; 2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @57
	lda UpCharP1Table,x	; 4
	sta UpCharP1+1		; 3
	ldx UpTmp		; 3

	; @67
	jmp .bailOutGap

	
	; ------------------------------
	; If y reaches 0, branch here @29.
	; It's here to be reachable via relative branch.
	; ------------------------------
.endOfFrameBailOut3
	; @29
	jmp .endOfFrame



.bailOutGap
	; @70
	; Font line 3
	; ------------------------------
	lda UpFont+54,x		; 4
	ror			; 2
	sta UpCharBits+2	; 3
	rol			; 2
	and #3			; 2
	stx UpTmp		; 3
	tax			; 2

	; @12
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @17
	lda UpCharP0Table,x	; 4
	sta UpCharP0+2		; 3

	; ##############################
	
	lda UpCharP1Table,x	; 4
	sta UpCharP1+2		; 3
	ldx UpTmp		; 3

	; @34
	; Init UpRowScanCount here because we have some cycles to spare
	lda #UP_DOTHEIGHT	; 2
	sta UpRowScanCount	; 3

	; @39, display logo
	dey			; 2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @55
	; Font line 4
	; ------------------------------
	lda UpFont+81,x		; 4
	ror			; 2
	sta UpCharBits+3	; 3
	rol			; 2
	and #3			; 2
	stx UpTmp		; 3
	tax			; 2
	lda UpCharP0Table,x	; 4
	sta UpCharP0+3		; 3
	lda UpCharP1Table,x	; 4
	sta UpCharP1+3		; 3
	ldx UpTmp		; 3
	
	; @14
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @19
	; Font line 5
	; ------------------------------
	lda UpFont+108,x	; 4
	ror			; 2
	sta UpCharBits+4	; 3

	; ##############################

	rol			; 2
	and #3			; 2
	tax			; 2, last row so don't need to save x
	lda UpCharP0Table,x	; 4
	sta UpCharP0+4		; 3

	; @41, display logo
	dey			; 2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @57
	lda UpCharP1Table,x	; 4
	sta UpCharP1+4		; 3
	
	; @64
	; ------------------------------
	; Char is prepared. Now multiplex and set up effect.
	;
	; Size is the same for UP_FX_NORMAL, UP_FX_TWIRL and
	; UP_FX_SINE and different only for UP_FX_PULSATING.
	;
	; Color is the same for UP_FX_NORMAL and UP_FX_SINE and
	; different for UP_FX_TWIRL and UP_FX_PULSATING.
	;
	; Advance effect is not necessary for UP_FX_NORMAL.
	; ------------------------------

	; Multiplex effects
	; advance effects
	; set up objects (size, color, position)
	; adjust y, waste rest of UP_CHARGAP lines, then jmp to .initRow
	; ------------------------------
	; @64
	ldx UpCurrentFx		; 3
	lda UpFxType,x		; 4
	cmp #UP_FX_NORMAL	; 2
	beq NormalFx		; 2/3
	jmp notNormalFx		; 3

	
	
	; ------------------------------
	; UP_FX_NORMAL
	; ------------------------------
NormalFx
	; @0
	; Set up size
	lda #%00100001		; 2, Ball size 4, PF mirror
	sta CTRLPF		; 3
	lda #%00100000		; 2, Missile size 4
	sta NUSIZ0		; 3
	sta NUSIZ1		; 3

	; @13
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @18
	; Set up color
	lda UpFxColor,x		; 4
	sta UpCharBLColor	; 3

	dey			; 2
	
	; ##############################

	; @27
	; position objects
	sta RESBL		; 3
	sta RESP0		; 3
	sta RESM0		; 3
	sta RESP1		; 3
	sta RESM1		; 3

	; @42, display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @56
	lda #RIGHT_8		; 2
	sta HMBL		; 3
	lda #RIGHT_4		; 2
	sta HMP0		; 3
	lda #LEFT_6		; 2
	sta HMP1		; 3
	lda #LEFT_7		; 2
	sta HMM1		; 3

	; @0
	sta HMOVE		; 3

	; @3
	; Continue to set up color
	lda UpFxColor,x		; 4
	sta COLUP0		; 3
	sta COLUP1		; 3

	; @13
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	dey			; 2
	; prepare x because we have some cycles to spare
	ldx #UP_CHARGAP - UP_FX_NORMAL_DUR
				; 2

	; @22
	; Continue to fine-position
	lda #RIGHT_2		; 2
	
	; ##############################

	; @24
	sta HMCLR		; 3
	sta HMBL		; 3
	lda #LEFT_3		; 2
	sta HMM1		; 3

	; @35, display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3
	
	; @49
	; ------------------------------
	; Finished: Waste x lines, then jmp to .initRow.
	; Do hmove on first scanline, then hmclr.
	; Enter after logo display.
	; CAUTION: UpRowScanCount has to be initialized already!
	; ------------------------------
WaitForInitRow
	sta WSYNC
	sta.w HMOVE		; 4

	; @4
	; check for last line
	dex			; 2
	bne noInitRowYet	; 2/3
	; @8
	ldx #4			; 2
	jmp .initRow		; 3
	
noInitRowYet
	; @9
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @14
	jsr UpRTS		; 12

	; ##############################

	jsr UpRTS		; 12
	
	; @38, display logo
	dey			; 2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @54
	sta HMCLR		; 3
	jmp WaitForInitRow	; 3



	; ------------------------------
	; UP_FX_PULSATING?
	; ------------------------------
	; @2
notNormalFx
	cmp #UP_FX_PULSATING	; 2
	beq PulsatingFx	; 2/3
	jmp notPulsatingFx	; 3
	
PulsatingFx
	; @7
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @12
	; save color
	lda UpFxColor,x		; 4
	sta UpTmp		; 3

	; @19
	; Advance effect
	lda UpCurrentFxData1,x	; 4
	clc			; 2
	adc UpCurrentFxData2,x	; 4
	sta UpCurrentFxData1,x	; 4
	
	; ##############################

	; @33, display logo
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @49
	; get animation state
	lda UpCurrentFxData1,x	; 4
	lsr			; 2
	lsr			; 2
	and #7			; 2
	tax			; 2
	lda UpPulsatingStates,x	; 4
	tax			; 2

	; @67
	; Color
	lda UpTmp		; 3
	ora UpPulsatingColor,x	; 4
	sta UpCharBLColor	; 3
	sta COLUP0		; 3
	sta COLUP1		; 3

	; @7
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @12
	; Set sizes
	lda UpPulsatingCTRLPF,x	; 4
	sta CTRLPF		; 3
	lda UpPulsatingNUSIZ0,x	; 4
	sta.w NUSIZ0		; 4

	; ##############################

	; @27
	; Position objects
	sta RESBL		; 3
	sta RESP0		; 3
	sta RESM0		; 3
	sta RESP1		; 3
	sta RESM1		; 3

	; @42, display logo
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @58
	lda UpPulsatingNUSIZ1,x	; 4
	sta NUSIZ1		; 3

	; @65
	; Fine-position #1
	lda UpPulsatingHMBL_1,x	; 4
	sta HMBL		; 3
	lda UpPulsatingHMP0,x	; 4
	sta HMP0		; 3

	; @3
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @8
	lda UpPulsatingHMP1,x	; 4
	sta HMP1		; 3
	lda UpPulsatingHMM0,x	; 4
	sta HMM0		; 3
	lda UpPulsatingHMM1,x	; 4
	sta HMM1		; 3

	; ##############################

	; @29
	SLEEP 4			; 4

	; @33, display logo
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @49
	sta WSYNC
	; @0
	sta HMOVE		; 3

	; @3
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @8
	; Fine-position #2
	lda UpPulsatingHMBL_2,x	; 4

	ldx #UP_CHARGAP - UP_FX_PULSATING_DUR
				; 2

	jsr UpRTS		; 12
	
	; ##############################

	; @26
	sta HMBL		; 3
	lda #0			; 2
	sta HMP0		; 3
	sta HMP1		; 3
	sta HMM0		; 3
	
	; @40, display logo
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @56
	jmp WaitForInitRow



	; ------------------------------
	; UP_FX_TWIRL ?
	; ------------------------------
	; @9
notPulsatingFx
	cmp #UP_FX_TWIRL	; 2
	beq TwirlFx		; 2/3
	jmp notTwirlFx		; 3
	
TwirlFx
	; @14
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3
	
	; @19
	; Set up size
	lda #%00100001		; 2, Ball size 4, PF mirror
	sta CTRLPF		; 3

	; ##############################

	lda #%00100000		; 2, Missile size 4
	sta NUSIZ0		; 3
	sta.w NUSIZ1		; 4

	; @33, display logo
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @49
	; Get base color
	lda UpFxColor,x		; 4
	sta UpCharBLColor	; 3

	; @56
	; Advance effect
	lda UpCurrentFxData1,x	; 4
	clc			; 2
	adc #UP_FX_TWIRL_ADVANCE; 2
	and #127		; 2
	sta UpCurrentFxData1,x	; 4

	; @70
	; Get twirl state and set up color
	tax			; 2, sin index
	nop			; 2

	; @74
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @3
	lda UpCharBLColor	; 3
	ora UpTwirlSinCol,x	; 4
	sta UpCharBLColor	; 3
	and #$f0		; 2
	ora UpTwirlSinCol+4,x	; 4
	sta COLUP0		; 3

	; ##############################

	and #$f0		; 2
	ora UpTwirlSinCol+12,x	; 4
	sta COLUP1		; 3
	
	nop			; 2

	; ------------------------------

	; Position objects

	; @33, display logo
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3
	
	; @49
	; Check priority b/c we have some cycles to spare
	cpx #63			; 2
	bcc noOtherPriority	; 2/3
	lda #%00100101		; 2, Ball size 4, Above Players, PF mirror
	sta CTRLPF		; 3	
noOtherPriority
	
	; Ball
	sta WSYNC
	
	; @0
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @5
	dey			; 2
	lda UpTwirlSin,x	; 4

	; waste some bytes to align QBCS later
	jmp .wasteBytes1	; 3
.wasteBytes1
	SLEEP 7
	
	; ##############################
	
	; @21
	sec			; 2
UpBallDivide15
	sbc #15			; 2
	QBCS UpBallDivide15	; 2/3
	sta RESBL		; 3
	; Amplitude 40:
	; @30 earliest
	; @40 latest

	sta UpTmp		; 3
	
	; @43, display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @57
	lda UpTmp		; 3
	eor #7			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	sta HMBL		; 3

	; ------------------------------

	; @73
	; P0
	sta WSYNC

	; @0
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @5
	txa			; 2
	adc #3			; 2, 3+carry=4
	and #127		; 2
	tax			; 2
	dey			; 2

	; @15
	lda UpTwirlSin,x	; 4
	sec			; 2
	sbc #1			; 2

	; ##############################
	
	; @23
	; carry already set from sbc
UpP0Divide15
	sbc #15			; 2
	QBCS UpP0Divide15	; 2/3
	sta RESP0		; 3
	; Amplitude 40:
	; @30 earliest
	; @40 latest

	sta UpTmp		; 3
	
	; @43, display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @57
	lda UpTmp		; 3
	eor #7			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	sta HMP0		; 3

	; ------------------------------

	; @73
	; M0
	sta WSYNC

	; @0
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @5
	txa			; 2
	adc #3			; 2, 3+carry=4
	and #127		; 2
	tax			; 2
	dey			; 2

	; @15
	lda UpTwirlSin,x	; 4
	SLEEP 2

	; ##############################
	
	; @21
	sec			; 2
UpM0Divide15
	sbc #15			; 2
	QBCS UpM0Divide15	; 2/3
	sta RESM0		; 3
	; Amplitude 40:
	; @30 earliest
	; @40 latest

	sta UpTmp		; 3
	
	; @43, display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @57
	lda UpTmp		; 3
	eor #7			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	sta HMM0		; 3

	; ------------------------------

	; @73
	; P1
	sta WSYNC

	; @0
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @5
	txa			; 2
	adc #3			; 2, 3+carry=4
	and #127		; 2
	tax			; 2
	dey			; 2

	; @15
	lda UpTwirlSin,x	; 4
	sec			; 2
	sbc #1			; 2

	; ##############################
	
	; @23, carry set already from sbc
UpP1Divide15
	sbc #15			; 2
	QBCS UpP1Divide15	; 2/3
	sta RESP1		; 3
	; Amplitude 40:
	; @30 earliest
	; @40 latest

	sta UpTmp		; 3
	
	; @43, display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @57
	lda UpTmp		; 3
	eor #7			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	sta HMP1		; 3

	; ------------------------------

	; @73
	; M1
	sta WSYNC

	; @0
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @5
	txa			; 2
	adc #3			; 2, 3+carry=4
	and #127		; 2
	tax			; 2
	dey			; 2

	; @15
	lda UpTwirlSin,x	; 4
	SLEEP 2

	; ##############################
	
	; @21
	sec			; 2
UpM1Divide15
	sbc #15			; 2
	QBCS UpM1Divide15	; 2/3
	sta RESM1		; 3
	; Amplitude 40:
	; @30 earliest
	; @40 latest

	sta UpTmp		; 3
	
	; @43, display logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @57
	lda UpTmp		; 3
	eor #7			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	sta HMM1		; 3

	; @73
	
	sta WSYNC
	
	; @0
	; Remove logo
	lda #0			; !2, 71 - 26
	sta PF1			; !3

	; @5
	ldx #UP_CHARGAP - UP_FX_TWIRL_DUR
	jsr UpRTS
	jsr UpRTS
	SLEEP 3
				; 2
	
	; ##############################
	
	; @34, display logo
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3
	
	; @50
	jmp WaitForInitRow



	; ------------------------------
	; UP_FX_SINE?
	; ------------------------------
	; @16
notTwirlFx
	; Remove logo
	ldx #0			; !2, 71 - 26
	stx PF1			; !3

	; @21
	ldx UpCurrentFx		; 3

	; ##############################

	; @24
	; Size
	lda #%00100001		; 2, Ball size 4, PF mirror
	sta CTRLPF		; 3
	lda #%00100000		; 2, Missile size 4
	sta NUSIZ0		; 3
	sta NUSIZ1		; 3

	; @37, display logo
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @53
	; Advance effect and get position	
	lda UpCurrentFxData1,x		; 4
	clc			; 2
	adc #64			; 2
	and #127		; 2
	sta UpCurrentFxData1,x	; 4
	tax			; 2
	lda UpTwirlSin,x	; 4
	lsr			; 2
	lsr			; 2

	; @1
	; Remove logo
	ldx #0			; !2, 71 - 26
	stx PF1			; !3
	
	; @6
	SLEEP 10

	; ##############################

	; @16
	sec
UpSineDivide15
	sbc #15			; 2
	QBCS UpSineDivide15	; 2/3
	sta RESBL		; 3
	sta RESP0		; 3
	sta RESM0		; 3
	sta RESP1		; 3
	sta RESM1		; 3
	tax			; 2
	
	; Amplitude 20:
	; @39 earliest
	; @44 latest
	; At the moment: max amp 10!

	; @44, display logo
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @60
	txa			; 2
	eor #7			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	asl			; 2

	; sync again
	sta WSYNC
	
	; @0
	; Remove logo
	ldx #0			; !2, 71 - 26
	stx PF1			; !3

	; @5
	sta HMBL		; 3
	sta HMP0		; 3
	sta HMM0		; 3
	sta HMP1		; 3
	sta HMM1		; 3
	
	; @20
	; Color
	ldx UpCurrentFx		; 3
	lda UpFxColor,x		; 4
	sta COLUP0		; 3
	sta COLUP1		; 3
	sta UpCharBLColor	; 3

	; ##############################

	; @36
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @52
	sta WSYNC
	sta HMOVE
	
	; @3
	; Remove logo
	ldx #0			; !2, 71 - 26
	stx PF1			; !3
	
	; @8
	; SLEEP 24
	jsr UpRTS
	jsr UpRTS
	
	; ##############################

	; @32
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @48

	sta HMCLR		; 3
	lda #RIGHT_8		; 2
	sta HMBL		; 3
	lda #RIGHT_4		; 2
	sta HMP0		; 3
	lda #LEFT_6		; 2
	sta HMP1		; 3
	lda #LEFT_7		; 2
	sta HMM1		; 3
	
	; @71	
	sta WSYNC
	sta HMOVE
	
	; @3
	; Remove logo
	ldx #0			; !2, 71 - 26
	stx PF1			; !3
	
	; @8
	; SLEEP 24
	jsr UpRTS
	jsr UpRTS
	
	; ##############################

	; @32
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @48
	sta HMCLR		; 3
	lda #RIGHT_2		; 2
	sta HMBL		; 3
	lda #LEFT_3		; 2
	sta HMM1		; 3

	; @61	
	sta WSYNC
	sta HMOVE		; 3
	
	; @3
	; Remove logo
	ldx #0			; !2, 71 - 26
	stx PF1			; !3
	
	; @8
	; SLEEP 24
	jsr UpRTS
	jsr UpRTS
	
	; ##############################

	; @32
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @48
	lda #RIGHT_8		; 2
	sta HMBL		; 3
	sta HMP0		; 3
	sta HMM0		; 3
	sta HMP1		; 3
	sta HMM1		; 3

	; @65
	sta WSYNC
	sta HMOVE		; 3
	
	; @3
	; Remove logo
	ldx #0			; !2, 71 - 26
	stx PF1			; !3
	
	; @8
	; SLEEP 24
	jsr UpRTS
	jsr UpRTS
	
	; ##############################

	; @32
	dey			; !2
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3

	; @48
	lda #RIGHT_7		; 2
	sta HMBL		; 3
	sta HMP0		; 3
	sta HMM0		; 3
	sta HMP1		; 3
	sta HMM1		; 3

	; @65
	ldx #UP_CHARGAP - UP_FX_SINE_DUR
				; 2

	; @67
	jmp WaitForInitRow

	

	; ------------------------------
	; End of frame: Display last logo line and clean up
	; ------------------------------
	; @32
.endOfFrame
	; display last logo line
	SLEEP 3
	; @35, after scroller, before logo
	lda UpVertical_gfx,y	; !4, 39-60
	sta PF1			; !3
	lda UpVertical_col,y	; !4, 44-60
	sta COLUPF		; !3
	; clean-up
	lda #0
	sta WSYNC
	sta PF1
	sta ENABL
	sta GRP0
	sta ENAM0
	sta GRP1
	sta ENAM1
	
UpWasteBeforeVBLank
	WASTE 4



; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

UpDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	JSB MusicPlayer

	jsr UpWaitForIntim

	jmp UpKernel



; =========================================================================
; Subroutines
; =========================================================================


; -------------------------------------------------------------------------
; Advance scroller one scanline.
; -------------------------------------------------------------------------

UpDoScroll
	; Advance lines, rows, and characters
	dec UpRowScanline
	beq .newRow
	rts
.newRow
	lda #UP_DOTHEIGHT
	sta UpRowScanline
	dec UpRowIndex
	bmi .newChar
	rts
.newChar
	inc UpCharIndex

	; advance effect once
	lda UpFxType
	cmp #UP_FX_TWIRL
	bne .noTwirlOnce
	lda UpFxData1
	clc
	adc #UP_FX_TWIRL_ADVANCE
	sta UpFxData1
	jmp .doCheckRemoveFx
.noTwirlOnce
	cmp #UP_FX_SINE
	bne .noSineOnce
	lda UpFxData1
	clc
	adc #64
	sta UpFxData1
	jmp .doCheckRemoveFx
.noSineOnce
	cmp #UP_FX_PULSATING
	bne .noPulsatingOnce
	lda UpFxData1
	clc
	adc UpFxData2
	sta UpFxData1

.noPulsatingOnce

.doCheckRemoveFx

	; Check for effect to remove
	ldx UpCharIndex
	lda UpText,x
	bpl .noEffect

	; remove effect from queue
	inc UpCharIndex
	inc UpCharIndex
	ldx #255
.removeFirstFx
	inx
	lda UpFxColor+1,x
	sta UpFxColor,x
	lda UpFxData1+1,x
	sta UpFxData1,x
	lda UpFxData2+1,x
	sta UpFxData2,x
	lda UpFxType+1,x
	sta UpFxType,x
	bne .removeFirstFx
	
	; HACK
	lda UpFxType
	cmp #UP_FX_TWIRL
	bne .noHackTwirl
	lda UpFxData1
	clc
	adc #UP_FX_TWIRL_ADVANCE
	sta UpFxData1
	jmp .afterHack
.noHackTwirl
	cmp #UP_FX_SINE
	bne .noHackSine
	lda UpFxData1
	clc
	adc #64
	sta UpFxData1
	jmp .afterHack
.noHackSine
	cmp #UP_FX_PULSATING
	bne .noHackPulsating
	lda UpFxData1
	clc
	adc UpFxData2
	sta UpFxData1

.noHackPulsating

.afterHack

.noEffect

	; Advance peek index and check for effect to add
	inc UpPeekIndex
	ldx UpPeekIndex
	lda UpText,x
	bpl .noPeekFx
	
	; Next state?
	cmp #UP_FX_EOT
	bne .noNextState
	inc UpDemoState
	lda #0
	sta UpNumFrame
	sta UpScrollSpeedLo
	sta UpScrollAddLo
	sta UpScrollAddHi
	lda #1
	sta UpScrollSpeedHi
.noNextState

	; Add new effect to queue
	; Set y to tail of queue
	ldy #255
.findFxTail
	iny
	lda UpFxType,y
	bne .findFxTail
	
	; set up new effect type
	lda UpText,x
	cmp #UP_FX_RIPPLE
	bne .noRipple
	lda #UP_FX_PULSATING
	sta UpFxType,y
	lda #0
	sta UpFxData1,y
	lda #4
	sta UpFxData2,y
	bne .doColor		; conditional jmp
.noRipple
	sta UpFxType,y
	lda #0
	sta UpFxData1,y
	sta UpFxData2,y

.doColor
	; color
	inx
	lda UpText,x
	sta UpFxColor,y	
	
	; mark end
	lda #0
	sta UpFxType+1,y
	; new peek counter
	inx
	stx UpPeekIndex

.noPeekFx
	
	; Start new char
	lda #4
	sta UpRowIndex
	lda #UP_DOTHEIGHT + UP_CHARGAP	; initial gap before char
	sta UpRowScanline
	
	
.noNewChar
	
.noNewRow

	rts



; =========================================================================
; Tables and small subroutines
; =========================================================================

; Scrolltext
; ------------------------------

	ALIGN $100
UpText
	dc.b "@@@@@"
	dc.b "BIG@"

	dc.b UP_FX_PULSATING, $60
	dc.b "RESPECT@"
	dc.b UP_FX_NORMAL, $20
	dc.b "TO@"

	dc.b UP_FX_TWIRL, $d0
	dc.b "NOICE@"
	dc.b UP_FX_SINE, $70
	dc.b "DSS@"
	dc.b UP_FX_RIPPLE, $c0
	dc.b "TRSI@"
	dc.b UP_FX_TWIRL, $50
	dc.b "TRILOBIT@"
	dc.b UP_FX_SINE, $c0
	dc.b "JAC@"
	dc.b UP_FX_PULSATING, $80
	dc.b "XAYAX@"
	dc.b UP_FX_TWIRL, $60
	dc.b "ATARIAGE@"
	
	dc.b UP_FX_SINE, $00
	dc.b "LFT@"
	
	dc.b UP_FX_RIPPLE, $30
	dc.b "STILL@"
	dc.b UP_FX_TWIRL, $90
	dc.b "FAIRLIGHT@"
	dc.b UP_FX_SINE, $40
	dc.b "DEAD@ROMAN@"
	dc.b UP_FX_PULSATING, $60
	dc.b "ASD@"
	dc.b UP_FX_TWIRL, $70
	dc.b "OUTRACKS@"
	dc.b UP_FX_SINE, $00
	dc.b "CONSPIRACY@"
	
	dc.b UP_FX_NORMAL, $20
	dc.b "@AND@ALL@THE@REST"
	dc.b "@@@@@@@@@@@@"

	dc.b UP_FX_EOT

	echo "Upscroll text length: ", * - UpText



; Font
; 27 chars (@=" ", A-Z) of height 6
; line 1: offset 0
; line 2: ofset 27
; line 3: ...
; ------------------------------

	ALIGN $100
UpFont
	include "font_5x5.asm"

; P0/P1 table for font char conversion to GRP0/1
; ------------------------------

UpCharP0Table
	dc.b 0, 0, %11110000, %11110000
	
UpCharP1Table
	dc.b 0, %11110000, 0, %11110000


; Table to map loop index to twirl object
; ------------------------------
UpObjIndex
	dc.b 3, 1, 2, 0, 4

; Table for position correction: P0/1 needs to be shifted left by 1
UpTwirlCorrection
	dc.b 1, 1, 0, 0, 0



; UP_FX_PULSATING animation tables
; ------------------------------

	; UP_FX_PULSATING
	; HMM values B, P0, M0, P1, M1 (- = right, + = left)
	;
	; 4-4-4-4-4: -10, -4, 0, +6, +10
	; 4-4-8-4-4: -8, -2, +2, +4, +8
	; 8-4-8-4-8: -4, -2, +2, +4, +8
	; 8-8-8-8-8: -1, +3, +2, +5, +4
	; ------------------------------

UpPulsatingStates
	dc.b 1, 2, 3, 2, 1, 0, 0, 0

UpPulsatingColor
	dc.b $08, $0a, $0c, $0e
	
UpPulsatingCTRLPF
	dc.b %00100001, %00100001, %00110001, %00110001
	
UpPulsatingNUSIZ0
	dc.b %00100000, %00110000, %00110000, %00110101
	
UpPulsatingNUSIZ1
	dc.b %00100000, %00100000, %00110000, %00110101

UpPulsatingHMBL_1
	dc.b RIGHT_5, RIGHT_4, RIGHT_4, RIGHT_1
UpPulsatingHMBL_2
	dc.b RIGHT_5, RIGHT_4, 0, 0

UpPulsatingHMP0
	dc.b RIGHT_4, RIGHT_2, RIGHT_2, LEFT_3

UpPulsatingHMM0
	dc.b 0, LEFT_2, LEFT_2, LEFT_2

UpPulsatingHMP1
	dc.b LEFT_6, LEFT_4, LEFT_4, LEFT_5

	; Apply two times
UpPulsatingHMM1
	dc.b LEFT_5, LEFT_4, LEFT_4, LEFT_2


	; Flashing colors for normal and sine fx
	
UpColorCycle
	dc.b $0e, $0e, $0e, $0c, $0a, $08, $0a, $0c



; ------------------------------
; Do scrolling
; ------------------------------

UpAdvanceScroll SUBROUTINE	
	lda UpScrollSpeedLo
	clc
	adc UpScrollAddLo
	sta UpScrollSpeedLo
	lda UpScrollSpeedHi
	adc UpScrollAddHi
	bne .notZero
	lda #1
.notZero
	sta UpScrollSpeedHi
	
	lda UpNumFrame
	and #3
	bne .noAddSpeed
	lda UpScrollAddLo
	clc
	adc #UP_SPEED_INCREASE
	sta UpScrollAddLo
	lda UpScrollAddHi
	adc #0
	sta UpScrollAddHi
.noAddSpeed

.scrollLoop
	jsr UpDoScroll
	dec UpScrollSpeedHi
	bne .scrollLoop
	rts
	


; Vertical gfx
; ------------------------------

; Logo
	ALIGN $100
UpVertical_gfx
	include "vertical_gfx.asm"


; Let logo fall out
; ------------------------------

UpMoveLogoOut SUBROUTINE

	; Move
	lda UpScrollAddLo
	clc
	adc #40
	sta UpScrollAddLo
	lda UpScrollAddHi
	adc #0
	sta UpScrollAddHi

	lda UpScrollSpeedLo
	clc
	adc UpScrollAddLo
	sta UpScrollSpeedLo
	lda UpScrollSpeedHi
	adc UpScrollAddHi
	sta UpScrollSpeedHi
	
	ldx #6
.fallOutLoop
	sta UpLogoLetterGap,x
	dex
	bpl .fallOutLoop
	rts
	

; Colors
	ALIGN $100
UpVertical_col
	dc.b 0, 0
	dc.b $c4, $c4, $c4, $c4, $c4, $a4, $c4, $a4
	dc.b $a4, $a4, $a4, $a4, $a4, $a6, $a4, $a6
	dc.b $a6, $a6, $a6, $a6, $a6, $a6, $a6, $a6
	dc.b $a6, $88, $a6, $88, $88, $88, $88, $88
	dc.b $88, $86, $88, $86, $86, $86, $86, $86
	dc.b $86, $86, $86, $86, $86, $66, $86, $66
	dc.b $66, $66, $66, $66, $66, $66, $66, $66
	dc.b $66, $66, $66, $66, $66, $66, $66, $66
	dc.b $66, $66, $66, $66, $66, $66, $66, $66
	dc.b $66, $66, $66, $66, $66, $68, $66, $68
	dc.b $68, $46, $68, $46, $46, $46, $46, $46
	dc.b $46, $48, $46, $48, $48, $48, $48, $48
	dc.b $48, $48, $48, $48, $48, $28, $48, $28
	dc.b $28, $28, $28, $28, $28, $28, $28, $28
	dc.b $28, $48, $28, $48, $48, $48, $48, $48
	dc.b $48, $48, $48, $48, $48, $46, $48, $46
	dc.b $46, $46, $46, $46, $46, $68, $46, $68
	dc.b $68, $66, $68, $66, $66, $66, $66, $66
	dc.b $66, $66, $66, $66, $66, $66, $66, $66
	dc.b $66, $66, $66, $66, $66, $66, $66, $66
	dc.b $66, $66, $66, $66, $66, $66, $66, $66
	dc.b $66, $86, $66, $86, $86, $86, $86, $86
	dc.b $86, $86, $86, $86, $86, $88, $86, $88
	dc.b $88, $88, $88, $88, $88, $a6, $88, $a6
	dc.b $a6, $a6, $a6, $a6, $a6, $a6, $a6, $a6
	dc.b $a6, $a4, $a6, $a4, $a4, $a4, $a4, $a4
	dc.b $a4, $c4, $a4, $c4, $c4, $c4, $c4, $c4
	dc.b $c4, $c4, $c4, $c4


; -------------------------------------------------------------------------
; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

UpWaitForIntim SUBROUTINE
	lda INTIM
	bne UpWaitForIntim
	sta WSYNC

UpRTS
	rts

; Logo letter size and pos table
; ------------------------------

UpLogoLetterSizes
	dc.b 29, 31, 33, 32, 33, 30, 30

	echo "Cave at ", *


; Twirl sin table
; ------------------------------

	ALIGN $100
UpTwirlSin
	dc.b $04, $04, $04, $04, $04, $05, $05, $05
	dc.b $05, $06, $06, $07, $07, $08, $08, $09
	dc.b $0a, $0a, $0b, $0c, $0c, $0d, $0e, $0f
	dc.b $10, $11, $11, $12, $13, $14, $15, $16
	dc.b $17, $18, $19, $1a, $1b, $1c, $1d, $1d
	dc.b $1e, $1f, $20, $21, $22, $22, $23, $24
	dc.b $24, $25, $26, $26, $27, $27, $28, $28
	dc.b $29, $29, $29, $29, $2a, $2a, $2a, $2a
	dc.b $2a, $2a, $2a, $2a, $2a, $29, $29, $29
	dc.b $29, $28, $28, $27, $27, $26, $26, $25
	dc.b $24, $24, $23, $22, $22, $21, $20, $1f
	dc.b $1e, $1d, $1d, $1c, $1b, $1a, $19, $18
	dc.b $17, $16, $15, $14, $13, $12, $11, $11
	dc.b $10, $0f, $0e, $0d, $0c, $0c, $0b, $0a
	dc.b $0a, $09, $08, $08, $07, $07, $06, $06
	dc.b $05, $05, $05, $05, $04, $04, $04, $04


; Inits
; ------------------------------

UpInit SUBROUTINE
	
	; Demo state
	lda #0
	sta UpNumFrame
	sta UpDemoState

	sta PF0
	sta PF1
	sta PF2
	sta COLUBK

	sta UpCharIndex

	sta UpFxType+1

	sta UpLogoLetterLock
	sta UpStartLocking

	; Scoller state
	sta UpScrollSpeedLo
	sta UpScrollSpeedHi
	sta UpScrollAddHi
	lda #100
	sta UpScrollAddLo
	
	lda #4
	sta UpRowIndex
	lda #UP_DOTHEIGHT
	sta UpRowScanline
	
	; effects state
	lda #UP_PEEKINDEX
	sta UpPeekIndex
	lda #UP_FX_NORMAL
	sta UpFxType
	lda #$20
	sta UpFxColor
	
	; logo in states
	lda #111
	sta UpLogoXPos
	
	lda #0
	ldx #6
.initLetterCounters
	sta UpLogoLetterCounter,x
	clc
	adc #25
	dex
	bpl .initLetterCounters

	rts



; Advannce FX
; ------------------------------

UpAdvanceFx SUBROUTINE

	ldx #0
.loop
	lda UpFxType,x
	beq .endAdvance
	cmp #UP_FX_PULSATING
	bne .notPulsating
	inc UpFxData1,x
	jmp .nextAdvance
.notPulsating
	cmp #UP_FX_TWIRL
	bne .notTwirl
	dec UpFxData1,x
	jmp .nextAdvance
.notTwirl
	cmp #UP_FX_SINE
	bne .noSine
	inc UpFxData1,x
	inc UpFxData1,x
	inc UpFxData1,x
.noSine
	; Do for sine and normal
	lda UpNumFrame
	lsr
	lsr
	lsr
	and #7
	tay
	lda UpFxColor,x
	and #$f0
	ora UpColorCycle,y
	sta UpFxColor,x

.nextAdvance
	inx
	bne .loop	; unconditional
	
.endAdvance
	rts



	ALIGN $100
UpTwirlSinCol
	dc.b $06, $08, $0a, $0c, $0e, $0e, $0e, $0e
	dc.b $0e, $0e, $0e, $0e, $0e, $0e, $0e, $0e
	dc.b $0e, $0e, $0e, $0e, $0e, $0e, $0e, $0e
	dc.b $0e, $0e, $0e, $0e, $0e, $0e, $0e, $0e
	dc.b $0e, $0e, $0e, $0e, $0e, $0e, $0e, $0e
	dc.b $0e, $0e, $0e, $0e, $0e, $0e, $0e, $0e
	dc.b $0e, $0e, $0e, $0e, $0e, $0e, $0e, $0e
	dc.b $0e, $0e, $0e, $0e, $0e, $0e, $0e, $0e
	dc.b $0c, $0a, $08, $06, $04, $02, $00, $00
	dc.b $00, $00, $00, $00, $00, $00, $00, $00
	dc.b $00, $00, $00, $00, $00, $00, $00, $00
	dc.b $00, $00, $00, $00, $00, $00, $00, $00
	dc.b $00, $00, $00, $00, $00, $00, $00, $00
	dc.b $00, $00, $00, $00, $00, $00, $00, $00
	dc.b $00, $00, $00, $00, $00, $00, $00, $00
	dc.b $00, $00, $00, $00, $00, $02, $02, $04
	
	dc.b $06, $08, $0a, $0c, $0e, $0e, $0e, $0e
	dc.b $0e, $0e, $0e, $0e, $0e, $0e, $0e, $0e
	
	echo "Cave at ", *
