; -------------------------------------------------------------------------
; Get y pos of board from sine table and store into ChessTmp
; -------------------------------------------------------------------------

ChessGetBoardYPos SUBROUTINE
	ldx ChessNumFrame
	lda ChessSinTable,x
	sta ChessTmp
	RTB



; -------------------------------------------------------------------------
; Above board: Calc pos and sizes of board, textbox and empty lines
; -------------------------------------------------------------------------

ChessCalcFromAbove SUBROUTINE

	; Hack
	lda ChessBoardYPos
	sec
	sbc #8
	sta ChessBoardYPos
	
	; 255 means to lines to prepare, otherwise #lines
	lda #255
	sta ChessLinesToPrepare

	; visible at all?
	lda ChessBoardYPos
	cmp #219
	bcc .visible
	; not visible
	lda #220
	sta ChessBoardYPos
	lda #0
	sta ChessTextDisplayHeight
	sta ChessBoardAfterLines
	jmp .doPrepare

.visible
	; big enough?
	lda ChessBoardBackDist
	lsr
	sta ChessBoardDisplayHeight	; in case height is not abnormal (might get changed later)
	cmp #4
	bcs .bigEnough
	; too small
	lda #5
.bigEnough
	; too large?
	cmp #35
	bcc .notTooLarge
	lda #35
.notTooLarge
	sta ChessLinesToPrepare
	
	; Textbox visible?
	lda ChessBoardYPos
	clc
	adc ChessBoardBackDist	; = end of board
	bcs .notVisible
	cmp #219
	bcc .boxVisible
	; Box not visible
	; shrink board, hide textbox
.notVisible
	lda #218
	sec
	sbc ChessBoardYPos
	lsr
	sta ChessBoardDisplayHeight
	lda #0
	sta ChessTextDisplayHeight
	sta ChessBoardAfterLines
	jmp .doPrepare
	
.boxVisible
	; Box completely visible?
	sta ChessTmp
	lda #220
	sec
	sbc ChessTmp
	cmp #28
	bcc .notOver
	; Textbox fully visible
	lda #26
	sta ChessTextDisplayHeight
	; calc after line
	lda #217
	sec
	sbc ChessBoardYPos
	sbc ChessBoardDisplayHeight
	sbc ChessBoardDisplayHeight
	sbc #26		; text box
	sta ChessBoardAfterLines
	jmp .doPrepare

.notOver
	; Textbox partially visible
	; Hack: hide it
	lda #218
	sec
	sbc ChessBoardYPos
	sbc ChessBoardDisplayHeight
	sbc ChessBoardDisplayHeight
	sta ChessBoardAfterLines
	lda #0
	sta ChessTextDisplayHeight

;	sta ChessTextDisplayHeight
;	lda #0
;	sta ChessBoardAfterLines
	
.doPrepare
	lda #CHESS_MODE_ABOVE
	sta ChessBoardMode

	; prevent negative gap
	lda ChessBoardAfterLines
	bpl .noHack
	lda #0
	sta ChessBoardAfterLines
.noHack

	RTB



; -------------------------------------------------------------------------
; Below board: Calc pos and sizes of board, textbox and empty lines
; -------------------------------------------------------------------------
	
ChessCalcFromBelow SUBROUTINE

	; 255 means to lines to prepare, otherwise #lines
	lda #255
	sta ChessLinesToPrepare

	; big enough?
	lda ChessBoardBackDist
	lsr
	sta ChessBoardDisplayHeight	; in case height is not abnormal (might get changed later)
	cmp #4
	bcs .bigEnough
	; too small
	lda #5
.bigEnough
	; too large?
	cmp #35
	bcc .notTooLarge
	lda #35
.notTooLarge
	sta ChessLinesToPrepare

	; lower gap
	lda #87
	clc
	adc ChessBoardBackDist
	sta ChessBoardAfterLines
	; hack
	lda ChessBoardYPos
	and #3
	cmp #2
	beq .two
	cmp #3
	bne .noHack
	inc ChessBoardAfterLines
	bne .noHack
.two
	dec ChessBoardAfterLines

.noHack
	
	lda #CHESS_MODE_BELOW
	sta ChessBoardMode
	RTB



; -------------------------------------------------------------------------
; Credit text pointers
; -------------------------------------------------------------------------

ChessSetCreditPtr SUBROUTINE
	ldx ChessDemoState
	lda ChessGfxHi,x
	sta Chess48_1+1
	sta Chess48_2+1
	sta Chess48_3+1
	sta Chess48_4+1
	sta Chess48_5+1
	sta Chess48_6+1
	
	clc
	lda ChessGfxLo,x
	sta Chess48_1
	adc #12
	sta Chess48_2
	adc #12
	sta Chess48_3
	adc #12
	sta Chess48_4
	adc #12
	sta Chess48_5
	adc #12
	sta Chess48_6
	
	RTB
	
ChessGfxLo
	dc.b <Code_1, <Kylearan_1, <Music_1, <KK_1
	dc.b <Player_1, <KK_1, <Design_1, <Kylearan_1
	
ChessGfxHi
	dc.b >Code_1, >Kylearan_1, >Music_1, >KK_1
	dc.b >Player_1, >KK_1, >Design_1, >Kylearan_1



; -------------------------------------------------------------------------
; Sine table for bouncing
; -------------------------------------------------------------------------

	
ChessSinTable
	dc.b $de, $da, $d6, $d2, $ce, $ca, $c6, $c2
	dc.b $be, $ba, $b7, $b3, $af, $ab, $a7, $a3
	dc.b $9f, $9c, $98, $94, $90, $8d, $89, $85
	dc.b $82, $7e, $7b, $77, $74, $70, $6d, $69
	dc.b $66, $63, $5f, $5c, $59, $56, $53, $50
	dc.b $4d, $4a, $47, $44, $41, $3e, $3c, $39
	dc.b $36, $34, $31, $2f, $2c, $2a, $28, $25
	dc.b $23, $21, $1f, $1d, $1b, $19, $17, $16
	dc.b $14, $12, $11, $0f, $0e, $0d, $0b, $0a
	dc.b $09, $08, $07, $06, $05, $04, $04, $03
	dc.b $02, $02, $01, $01, $01, $00, $00, $00

	dc.b $00, $00, $00, $01, $01, $02, $02, $03
	dc.b $04, $05, $07, $08, $09, $0b, $0d, $0f
	dc.b $11, $13, $15, $18, $1a, $1d, $20, $23
	dc.b $26, $29, $2c, $30, $33, $37, $3b, $3f
	dc.b $43, $47, $4c, $50, $55, $5a, $5f, $64

	dc.b $64, $5f, $5a, $55, $50, $4c, $47, $43
	dc.b $3f, $3b, $37, $33, $30, $2c, $29, $26
	dc.b $23, $20, $1d, $1a, $18, $15, $13, $11
	dc.b $0f, $0d, $0b, $09, $08, $07, $05, $04
	dc.b $03, $02, $02, $01, $01, $00, $00, $00

	dc.b $00, $00, $00, $00, $01, $01, $01, $02
	dc.b $02, $03, $04, $04, $05, $06, $07, $08
	dc.b $09, $0a, $0b, $0d, $0e, $0f, $11, $12
	dc.b $14, $16, $17, $19, $1b, $1d, $1f, $21
	dc.b $23, $25, $28, $2a, $2c, $2f, $31, $34
	dc.b $36, $39, $3c, $3e, $41, $44, $47, $4a
	dc.b $4d, $50, $53, $56, $59, $5c, $5f, $63
	dc.b $66, $69, $6d, $70, $74, $77, $7b, $7e
	dc.b $82, $85, $89, $8d, $90, $94, $98, $9c
	dc.b $9f, $a3, $a7, $ab, $af, $b3, $b7, $ba
	dc.b $be, $c2, $c6, $ca, $ce, $d2, $d6, $da
