; Screen layout: PF mirrored, score mode.
; Left 8 pixels blank, for HMOVE bars.
;
; Lanes:
; Max lane width: 13*4 = 52
; Min lane width: 6*4 = 24
; Lane width delta: 7*4 = 28
;
; Max slope: 7 (due to hmove)
; Min chessboard height: 5 (lane width 52, 45, 38, 31, 24)
; for smaller heights, simply use max 7 and let fade colors do the rest
;
; World/Screen:
; Back moves at twice the speed of front.
; Max lanes visible: 3 (one in middle, two moving in/out)
; Kernel: 2LK, screen height: 210
; Lane front height: 24 (12 text + 2*6 setup)
; Back distance between lanes: 69 (210/3 - 1)
; Front distance between lanes: 138
;
; Left side: M1/P1, right side: P0/M0
; M1: -6*4 = -24 ->  8*4 =  32
; P1:  5*4 =  20 -> 12*4 =  48
; P0: 26*4 = 104 -> 33*4 = 132
; M1: 30*4 = 120 -> 44*4 = 176
; add +1 for missiles (b/c of RESMx)
;
; Start positions narrow:  33, 48, 104, 121
; visual correction:       31, 48, 104, 123
; Start positions wide:   137, 20, 132,  17


; =========================================================================
; Constants
; =========================================================================

;CHESS_SCRHEIGHT		= 210
;CHESS_SCRMIDDLE		= CHESS_SCRHEIGHT/2
;CHESS_BACKDIST		= 69	; 220/3 + 1
;CHESS_FRONTDIST		= 2*CHESS_BACKDIST

CHESS_MODE_ABOVE	= 1
CHESS_MODE_BELOW	= 2

; Script state constants

CHESS_NEXT_PART		= 200



; =========================================================================
; Start
; =========================================================================

ChessStart

	; inits
	; ------------------------------
	
	; gfx
	lda #$00
	sta PF0
	sta PF1
	sta PF2
;	sta ENAM0
;	sta ENAM1
;	sta ENABL

	sta ChessNumFrame
	sta ChessDemoState

	lda #%00000011		; ball 1, score mode, mirror
	sta CTRLPF
	
			
	jmp ChessIncoming

	
	
; =========================================================================
; Kernel
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

ChessKernel
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc ChessNumFrame
	lda ChessNumFrame
	ldx ChessDemoState
	cmp ChessScriptDurations,x 
	bne .noNextState
	inc ChessDemoState
	lda #0
	sta ChessNumFrame
	lda ChessDemoState
	cmp #8
	bne .noNextState
	JMB UpscrollStart
.noNextState

	JSB ChessSetCreditPtr

; -------------------------------------------------------------------------

ChessIncoming SUBROUTINE

	lda #0
	sta COLUP0
	sta COLUP1
	sta COLUPF
	sta COLUBK

; -------------------------------------------------------------------------

	; prepare board
	JSB ChessGetBoardYPos

	; Set text color
	lda ChessTmp
	cmp #166
	bcs .black
	cmp #134
	bcc .white
	; fade
	lda #166
	sec
	sbc ChessTmp
	lsr
	sta ChessTextColor
	sec
	sbc #$0a
	bcc .noUnderflow
	lda #$04
.noUnderflow
	sta ChessTextColor2
	jmp .afterColor
	
.white
	lda #$0e
	sta ChessTextColor
	lda #$04
	sta ChessTextColor2
	bne .afterColor
	
.black
	lda #0
	sta ChessTextColor
	sta ChessTextColor2

.afterColor

	; Board from above or below?
	lda ChessTmp
	sta ChessBoardYPos
	cmp #110
	bcc .smallerThan110

	; from above
	; ----------------------------
	sec
	sbc #110
	sta ChessBoardFrontDist	; front distance from center
	lsr
	sta ChessBoardBackDist	; back distance from center, and 2*board height
	
	JSB ChessCalcFromAbove
	ldx ChessLinesToPrepare
	bmi .noPrepare
	jsr ChessPrepareAbove
.noPrepare
	
	jmp .afterPrepare
	


.smallerThan110
	; from below
	; ----------------------------
	lda #110
	sec
	sbc ChessBoardYPos
	sta ChessBoardFrontDist	; front distance from center
	lsr
	sta ChessBoardBackDist	; back distance from center

	JSB ChessCalcFromBelow
	ldx ChessLinesToPrepare
	bmi .noPrepare2
	jsr ChessPrepareBelow
.noPrepare2
	

.afterPrepare
	
; -------------------------------------------------------------------------
	
	
	jsr ChessWaitForIntim
	lda #0
	sta VBLANK


; -------------------------------------------------------------------------
; Visible area
; -------------------------------------------------------------------------

ChessVisible SUBROUTINE

	sta WSYNC

	; Upper gap
	ldx ChessBoardYPos
	beq .noUpperGap
.wasteUpper
	sta WSYNC
	dex
	bne .wasteUpper	
.noUpperGap

	; multiplex board state
	lda ChessBoardMode
	cmp #CHESS_MODE_ABOVE
	bne .fromBelow

	; from above
	; ----------------------------
	; Show board
	jsr ChessAboveBoard

.showTextBox
	; Show Textbox
	ldx ChessTextDisplayHeight
	beq .noTextBox
	jsr ChessShowTextBox
	ldx #6
.wasteAfterBox
	sta WSYNC
	dex
	bne .wasteAfterBox
	sta WSYNC
	sta HMOVE
	lda ChessTextColor2
	sta COLUBK
	sta WSYNC
	lda #0
	sta COLUBK
.noTextBox

.showLowerGap
	; Lower gap
	ldx ChessBoardAfterLines
	beq .noLowerGap
.wasteLower
	sta WSYNC
	dex
	bne .wasteLower
.noLowerGap
	jmp .afterDisplay
	


.fromBelow
	; from below
	; ----------------------------
	
	; Show Textbox
	jsr ChessShowTextBox
	sta WSYNC
	sta HMOVE
	lda #0
	sta COLUBK

	; show board
	jsr ChessBelowBoard

	jmp .showLowerGap
	

.afterDisplay
	sta WSYNC
	
	
; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

ChessDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	JSB MusicPlayer

	jsr ChessWaitForIntim

	jmp ChessKernel


	
; =========================================================================
; Subroutines
; =========================================================================

; -------------------------------------------------------------------------
; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

ChessWaitForIntim SUBROUTINE
	lda INTIM
	bne ChessWaitForIntim
	sta WSYNC
ChessRTS
	rts



; -------------------------------------------------------------------------
; Set up and show text box.
; Returns after text and before board setup.
; -------------------------------------------------------------------------

ChessShowTextBox SUBROUTINE

	sta WSYNC
	sta HMOVE
	lda ChessTextColor
	sta COLUBK
	
	lda #11
	sta Chess48Height
	lda ChessTextDisplayHeight
	bne .doDisplay
	beq .return
	
;	cmp #3
;	bcs .displayText
;	tax
;.noText
;	sta WSYNC
;	sta HMOVE
;	lda #$00
;	sta COLUBK
;	dex
;	bne .noText
;	jmp .return
	
.displayText
	cmp #15
	bcc .displayPartialText
	lda #11
	sta Chess48Height
	bne .doDisplay

.displayPartialText
	sec
	sbc #3
	sta Chess48Height

.doDisplay
	
	sta WSYNC
	; ----------
	sta HMOVE
	lda #$00
	sta COLUBK
	lda #%00000011	; three copies close
	sta NUSIZ0
	sta NUSIZ1
	lda #1
	sta VDELP0
	sta VDELP1

	nop
	
	lda #RIGHT_2
	sta HMP0
	lda #RIGHT_1
	sta HMP1
	
	sta RESP0	; @36
	sta RESP1

	lda ChessTextColor
	sta COLUP0
	sta COLUP1

	sta WSYNC
	sta HMOVE

	sta WSYNC
	sta WSYNC
	sta WSYNC
	
Chess48Loop
	ldy Chess48Height
	lda (Chess48_1),y

	sta WSYNC

	sta GRP0
	lda (Chess48_2),y
	sta GRP1
	lda (Chess48_3),y
	sta GRP0
	lda (Chess48_6),y
	sta ChessTmp
	lax (Chess48_5),y
	lda (Chess48_4),y
	ldy ChessTmp
	sta.w GRP1		; @41
	stx GRP0
	sty GRP1
	sta GRP0
		
	dec Chess48Height
	QBPL Chess48Loop

	lda #0
	sta VDELP0
	sta VDELP1
	sta GRP0
	sta GRP1
	sta COLUP0
	sta COLUP1

.return
	rts



; -------------------------------------------------------------------------
; Fine-position object number in x at position in a.
; HMOVE must be done by caller.
; -------------------------------------------------------------------------

ChessPositionObject SUBROUTINE
	sec
	sta WSYNC
ChessPosDivide15
	sbc #15
	QBCS ChessPosDivide15
	eor #$07
	asl
	asl
	asl
	asl
	sta.wx HMP0,x
	sta RESP0,x
	rts



; -------------------------------------------------------------------------
; Prepare board for below.
; x: board height
; -------------------------------------------------------------------------

ChessPrepareBelow SUBROUTINE

	stx ChessLines
	txa
	asl
	sta ChessLineCounter
	
	; Prepare counters
	lda #0
	sta ChessCounterHi
	sta ChessCounterLo
	lda ChessCounterTableHi,x
	sta ChessAddHi
	lda ChessCounterTableLo,x
	sta ChessAddLo

	
	; TODO: Use while loop, not for, to avoid first sty
	; TODO: Reverse loop, and use pha
	
	; calc new hi and outer delta values
	lda ChessCounterHi
	asl
	sta ChessTmp2
	ldx #0		; loop counter
	ldy ChessCounterHi
.prepareLoop
	; new counter hi
	lda ChessCounterLo	; 3
	clc			; 2
	adc ChessAddLo		; 3
	sta ChessCounterLo	; 3
	tya			; 2
	adc ChessAddHi		; 3
	tay			; 2
	sta ChessDeltaBuffer,x	; 4
	inx			; 2

	; new outer delta based on double inner counter + correction
	; first, double counter
	asl			; 2
	bit ChessCounterLo	; 3
	bpl .noInc		; 2
	ora #1			; 2
.noInc
	sta ChessTmp		; 3
	; then, calc delta
	sec			; 2
	sbc ChessTmp2		; 3
	sta ChessDeltaBuffer,x	; 4
	lda ChessTmp		; 3
	sta ChessTmp2		; 3, new old counter

	; next
	inx			; 2
	cpx ChessLineCounter	; 3
	bne .prepareLoop	; 3

				; =62
	
	
	; Restore counter for kernel
	lda #0
	sta ChessCounterHi
	sta ChessCounterLo

	rts



; -------------------------------------------------------------------------
; Prepare board for above
; x: board height
; -------------------------------------------------------------------------

ChessPrepareAbove SUBROUTINE

	stx ChessLines
	txa
	asl
	sta ChessLineCounter

	; Prepare counters
	lda #27
	sta ChessCounterHi
	lda #128	; !!! IF <128, REMOVE INX BELOW !!!
	sta ChessCounterLo
	lda ChessCounterTableHi,x
	sta ChessAddHi
	lda ChessCounterTableLo,x
	sta ChessAddLo
	
	; TODO: Use while loop, not for, to avoid first sty
	; TODO: Reverse loop, and use pha
	
	; calc new hi and outer delta values
	lda ChessCounterHi
	asl
	tax
	inx		; !!! ONLY IF LO >= 128 !!!
	stx ChessTmp2
	ldx #0		; loop counter
	ldy ChessCounterHi
.prepareLoop
	; new counter hi
	lda ChessCounterLo	; 3
	sec			; 2
	sbc ChessAddLo		; 3
	sta ChessCounterLo	; 3
	tya			; 2
	sbc ChessAddHi		; 3
	tay			; 2
	sta ChessDeltaBuffer,x	; 4
	inx			; 2

	; new outer delta based on double inner counter + correction
	; first, double counter
	asl			; 2
	bit ChessCounterLo	; 3
	bpl .noInc		; 2
	ora #1			; 2
.noInc
	; then, calc delta
	sta ChessTmp		; 3
	lda ChessTmp2		; 3, old counter
	sec			; 2
	sbc ChessTmp		; 3
	sta ChessDeltaBuffer,x	; 4
	lda ChessTmp		; 3
	sta ChessTmp2		; 3, new old counter

	; next
	inx			; 2
	cpx ChessLineCounter	; 3
	bne .prepareLoop	; 3

				; =62
	
	
	; Restore counter for kernel
	lda #27
	sta ChessCounterHi
	lda #128
	sta ChessCounterLo

	rts
	
	
	
; -------------------------------------------------------------------------
; Display board from below
; -------------------------------------------------------------------------

ChessBelowBoard SUBROUTINE

	lda #%00110000
	sta NUSIZ0
	sta NUSIZ1
	; Position objects
	; M1
	lda #137
	ldx #3
	jsr ChessPositionObject
	; P1
	lda #20
	ldx #1
	jsr ChessPositionObject
	; P0
	lda #132
	ldx #0
	jsr ChessPositionObject
	; M0
	lda #17
	ldx #2
	jsr ChessPositionObject
	sta WSYNC
	sta HMOVE
	
	lda #$ff
	sta GRP0
	sta GRP1
	
	lda #0
	sta ChessCurrentCol1
	sta ChessCurrentCol2

	lda ChessBoardDisplayHeight
	bne .boardVisible
	sta WSYNC
	sta WSYNC		; ??? how many???
	jmp .afterDisplay

.boardVisible
	sta ChessLineCounter

	tsx
	stx ChessOldSP
	ldx #ChessDeltaBuffer-1
	txs
	
	sta WSYNC
	SLEEP 24
	sta HMCLR
	
	ldx ChessCounterHi
	jmp .aligned
	
	echo "Code-align at ", *

	ALIGN $100
.aligned



	sta WSYNC
	
	; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	; CAUTION: x changes between lines. Maybe use offset -1
	; if PFx and other values don't match visually?
	; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	; x is current inner counter, = gfx line index
	; stack yields new inner counter and outer delta
.loop
	; Line 1
	; ----------
	; @0
	sta HMOVE		; 3
	
	; @3
	lda ChessPF0,x		; 4
	sta PF0			; 3
	lda ChessCurrentCol1	; 3
	sta COLUBK		; 3
	lda ChessPF1,x		; 4
	sta PF1			; 3
	lda ChessPF2,x		; 4
	sta PF2			; 3
	
	; @30
	sta HMCLR		; 3

	; @33
	; calc inner delta
	pla			; 4, new counter hi
	stx ChessTmp		; 3, new old counter

	; @40
	ldy ChessCurrentCol2	; 3
	sty COLUBK		; 3, @41...@56

	; @46
	tax			; 2, new counter hi
	sec			; 2
	sbc ChessTmp		; 3
	sta ChessInnerDelta	; 3, delta

	; @56
	
	SLEEP 9

	; @65
	; Prepare colors
	lda ChessColorsAbove_1,x ;4
	sta ChessCurrentCol1	; 3
	ldy ChessColorsAbove_2,x ;4
		
	; Line 2
	; ----------
	; @0
	sta HMOVE		; 3
	
	; @3
	sta COLUBK		; 3
	sta COLUP1		; 3
	sty COLUP0		; 3
	sty ChessCurrentCol2	; 3
	
	; @15
	; ENAM
	lda ChessENAMTable,x	; 4
	sta ENAM1		; 3
	sta ENAM0		; 3

	; @25
	; inner lines
	ldy ChessInnerDelta	; 3
	lda ChessHMRightTable,y	; 4
	sta HMP1		; 3
	lda ChessHMLeftTable,y	; 4
	sta HMP0		; 3

	; @42
	lda ChessCurrentCol2	; 3
	sta COLUBK		; 3, @41...@56
	
	; @48
	; outer lines
	pla			; 4, outer delta
	tay			; 2
	lda ChessHMRightTable,y	; 4
	sta HMM1		; 3
	lda ChessHMLeftTable,y	; 4
	sta HMM0		; 3
	
	; @68
	dec ChessLineCounter	; 5
	bne .loop		; 3
	
	
	
	; restore stack
	ldx ChessOldSP
	txs

.afterDisplay
	; clean up gfx
	lda #0
	sta COLUBK
	sta PF0
	sta PF1
	sta ENAM0
	sta ENAM1
	sta GRP0
	sta GRP1
	sta GRP0
	sta GRP1
	sta PF2

	rts




; -------------------------------------------------------------------------
; Display board from above
; -------------------------------------------------------------------------

ChessAboveBoard SUBROUTINE

	lda #%00110000
	sta NUSIZ0
	sta NUSIZ1

	; Position objects
	; M1
	lda #31
	ldx #3
	jsr ChessPositionObject
	; P1
	lda #48
	ldx #1
	jsr ChessPositionObject
	; P0
	lda #104
	ldx #0
	jsr ChessPositionObject
	; M0
	lda #123
	ldx #2
	jsr ChessPositionObject
	sta WSYNC
	sta HMOVE
	
	; Set up vars
	lda #0
	sta ChessCurrentCol1
	sta ChessCurrentCol2

	lda #$ff
	sta GRP0
	sta GRP1

	tsx
	stx ChessOldSP
	ldx #ChessDeltaBuffer-1
	txs

	lda ChessBoardDisplayHeight
	bne .boardVisible
	sta WSYNC
	sta WSYNC		; ??? how many???
	jmp .afterDisplay

.boardVisible
	sta ChessLineCounter

	
	sta WSYNC
	SLEEP 24
	sta HMCLR
	
	ldx ChessCounterHi
	jmp .aligned
	
	echo "Code-align at ", *

	ALIGN $100
.aligned



	sta WSYNC
	
	; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	; CAUTION: x changes between lines. Maybe use offset -1
	; if PFx and other values don't match visually?
	; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	; x is current inner counter, = gfx line index
	; stack yields new inner counter and outer delta
.loop
	; Line 1
	; ----------
	; @0
	sta HMOVE		; 3
	
	; @3
	lda ChessPF0,x		; 4
	sta PF0			; 3
	lda ChessCurrentCol1	; 3
	sta COLUBK		; 3
	lda ChessPF1,x		; 4
	sta PF1			; 3
	lda ChessPF2,x		; 4
	sta PF2			; 3
	
	; @30
	sta HMCLR		; 3

	; @33
	; calc inner delta
	pla			; 4, new counter hi
	sta ChessTmp		; 3

	; @40
	lda ChessCurrentCol2	; 3
	sta COLUBK		; 3, @41...@56

	; @46
	txa			; 2
	ldx ChessTmp		; 3
	sec			; 2
	sbc ChessTmp		; 3
	sta ChessInnerDelta	; 3, delta

	; @59
	
	SLEEP 6

	; @65
	; Prepare colors
	lda ChessColorsBelow_1,x ;4
	sta ChessCurrentCol1	; 3
	ldy ChessColorsBelow_2,x ;4
		
	; Line 2
	; ----------
	; @0
	sta HMOVE		; 3
	
	; @3
	sta COLUBK		; 3
	sta COLUP1		; 3
	sty COLUP0		; 3
	sty ChessCurrentCol2	; 3
	
	; @15
	; ENAM
	lda ChessENAMTable,x	; 4
	sta ENAM1		; 3
	sta ENAM0		; 3

	; @25
	; inner lines
	ldy ChessInnerDelta	; 3
	lda ChessHMLeftTable,y	; 4
	sta HMP1		; 3
	lda ChessHMRightTable,y	; 4
	sta HMP0		; 3

	; @42
	lda ChessCurrentCol2	; 3
	sta COLUBK		; 3, @41...@56
	
	; @48
	; outer lines
	pla			; 4, outer delta
	tay			; 2
	lda ChessHMLeftTable,y	; 4
	sta HMM1		; 3
	lda ChessHMRightTable,y	; 4
	sta HMM0		; 3
	
	; @68
	dec ChessLineCounter	; 5
	bne .loop		; 3
	
	
	

.afterDisplay
	; clean up gfx
	lda #0
	sta COLUBK
	sta PF0
	sta PF1
	sta ENAM0
	sta ENAM1
	sta GRP0
	sta GRP1
	sta GRP0
	sta GRP1
	sta PF2

	; restore stack
	ldx ChessOldSP
	txs

	rts
	


; =========================================================================
; Tables
; =========================================================================

; Script event durations
; ------------------------------

ChessScriptDurations
	dc.b 255, 255, 255, 255, 255, 255


; Gfx
; ------------------------------

	ALIGN $100
	include Code_gfx.asm
	include Design_gfx.asm
	include KK_gfx.asm

	; PF0: 4...7
ChessPF0
	; 0
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000

	; 1
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000

	; 2
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000

	; 3
	dc.b %00010000
	dc.b %00010000
	dc.b %00010000
	dc.b %00110000

	; 4
	dc.b %01110000
	dc.b %01110000
	dc.b %01110000
	dc.b %11110000

	; 5
	dc.b %11110000
	dc.b %11110000
	dc.b %11110000
	dc.b %11110000

	; 6
	dc.b %11110000
	dc.b %11110000
	dc.b %11110000
	dc.b %11110000

ChessHMLeftTable
	dc.b NO_MOTION
	dc.b LEFT_1, LEFT_2, LEFT_3, LEFT_4, LEFT_5, LEFT_6, LEFT_7
	dc.b LEFT_7, LEFT_7, LEFT_7, LEFT_7

	echo "Cave 1:", *

	ALIGN $100
	include Kylearan_gfx.asm
	include Music_gfx.asm
	include Player_gfx.asm

	; PF1: 7...0
ChessPF1
	; 0
	dc.b %00011111
	dc.b %00011111
	dc.b %00011111
	dc.b %00011111

	; 1
	dc.b %00001111
	dc.b %00001111
	dc.b %00001111
	dc.b %00001111

	; 2
	dc.b %00000111
	dc.b %00000111
	dc.b %00000111
	dc.b %00000111

	; 3
	dc.b %00000011
	dc.b %00000011
	dc.b %00000011
	dc.b %00000011

	; 4
	dc.b %00000001
	dc.b %00000001
	dc.b %00000001
	dc.b %00000001

	; 5
	dc.b %10000000
	dc.b %11000000
	dc.b %11000000
	dc.b %11000000

	; 6
	dc.b %11100000
	dc.b %11100000
	dc.b %11100000
	dc.b %11110000

ChessHMRightTable
	dc.b NO_MOTION
	dc.b RIGHT_1, RIGHT_2, RIGHT_3, RIGHT_4, RIGHT_5, RIGHT_6, RIGHT_7
	dc.b RIGHT_7, RIGHT_7, RIGHT_7, RIGHT_7
	
	echo "Cave 2:", *


	ALIGN $100
	; PF2: 0...7
ChessPF2
	; 0
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	
	; 1
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	
	; 2
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	
	; 3
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	
	; 4
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	
	; 5
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	dc.b %11111111
	
	; 6
	dc.b %11111110
	dc.b %11111110
	dc.b %11111110
	dc.b %11111110

ChessENAMTable
	ds.b 11, 0
	ds.b 17, 2

; Colors for view from above

ChessColorsAbove_1
	dc.b $de, $de, $de, $de
        dc.b $de, $da, $de, $da
        dc.b $da, $d8, $da, $d8
	dc.b $d8, $d6
	
        dc.b $a8, $a6
        dc.b $a6, $a4, $a6, $a4
        dc.b $a4, $a2
        
        dc.b $d4, $d2
        dc.b $d2, $d0, $d2, $d0

	dc.b $d0, $d0

ChessColorsAbove_2
	dc.b $ae, $ae, $ae, $ae
        dc.b $ae, $aa, $ae, $aa
        dc.b $aa, $a8, $aa, $a8
	dc.b $a8, $a6
	
        dc.b $d8, $d6
        dc.b $d6, $d4, $d6, $d4
        dc.b $d4, $d2
        
        dc.b $a4, $a2
        dc.b $a2, $a0, $a2, $a0

	dc.b $a0, $a0

; Colors for view from below

ChessColorsBelow_1
	dc.b $6e, $6e, $6e, $6e
        dc.b $6e, $6a, $6e, $6a
        dc.b $6a, $68, $6a, $68
        dc.b $68, $66
        
        dc.b $48, $46
        dc.b $46, $44, $46, $44
        dc.b $44, $42
        
        dc.b $64, $62
        dc.b $62, $60, $62, $60

ChessColorsBelow_2
	dc.b $4e, $4e, $4e, $4e
        dc.b $4e, $2a, $4e, $2a
        dc.b $2a, $48, $2a, $48
        dc.b $48, $46
        
        dc.b $68, $66
        dc.b $66, $64, $66, $64
        dc.b $64, $62
        
        dc.b $44, $42
        dc.b $42, $40, $42, $40


; Counter tables

ChessCounterTableHi:

	 dc.b 0, 28, 14, 9, 7, 5, 4, 4
	 dc.b 3, 3, 2, 2, 2, 2, 2, 1
	 dc.b 1, 1, 1, 1, 1, 1, 1, 1
	 dc.b 1, 1, 1, 1, 1, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0, 0, 0
	 dc.b 0, 0, 0, 0, 0, 0

ChessCounterTableLo:

	 dc.b 1, 0, 0, 85, 0, 153, 170, 0
	 dc.b 128, 28, 204, 139, 85, 39, 0, 221
	 dc.b 192, 165, 142, 121, 102, 85, 69, 55
	 dc.b 42, 30, 19, 9, 0, 247, 238, 231
	 dc.b 224, 217, 210, 204, 199, 193, 188, 183
	 dc.b 179, 174, 170, 166, 162, 159, 155, 152
	 dc.b 149, 146, 143, 140, 137, 135, 132, 130
	 dc.b 128, 125, 123, 121, 119, 117, 115, 113
	 dc.b 112, 110, 108, 106, 105, 103, 102, 100
	 dc.b 99, 98, 96, 95, 94, 93, 91, 90
	 dc.b 89, 88, 87, 86, 85, 84, 83, 82
	 dc.b 81, 80, 79, 78, 77, 77, 76, 75
	 dc.b 74, 73, 73, 72, 71, 70, 70, 69
	 dc.b 68, 68, 67, 66, 66, 65
	 
	echo "Cave 5:", *
