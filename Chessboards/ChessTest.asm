.loop
	; @0
	sta HMOVE		; 3
	lda PF0Data,x		; 4
	sta PF0			; 3
	lda PF1Data,x		; 4
	sta PF1			; 3
	lda PF2Data,x		; 4
	sta PF2			; 3
	lda P0ColData,x		; 4
	sta COLUP0		; 3
	lda P0ColData,x		; 4
	sta COLUP0		; 3



	; @0
	(sta HMOVE)
	
.afterHmoveJmpTarget

	; @3
	lda #
	sta COLUP0
	lda #
	sta COLUP1
	lda #
	sta PF0
	lda #
	sta PF1
	lda #
	sta PF2
	
	; @28

	lda DoCycleLo
	adc CycleStepLo
	sta DoCycleLo
	lda DoCycleHi
	adc #0
	sta DoCycleHi

	jmp (DoCycle)
	
	; @50
	
	lda DoHMove
	adc HMoveStep
	sta DoHMove
	bcc .noHmove
	

.hmove	
	xxx + 3 cycles
	dec lines
	bpl .afterHmoveJmpTarget
	
.noHmove
	xxx
	dec lines
	bpl .loop
