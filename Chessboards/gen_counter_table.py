import sys
import os
import math

lo = []
hi = []
delta = 28.0

for c in range(110):
	if c is 0:
		hi.append(0)
		lo.append(1)
	else:
		hi.append(math.trunc(delta/c))
		lo.append(256*((delta/c) - math.trunc(delta/c)))

# hi
print("\tALIGN $100")
print("ChessCounterTableHi:")
for i in range(110):
	out = hi[i]
	if i%8 == 0:
		sys.stdout.write("\n\t dc.b ")
	sys.stdout.write("%d" % out)
	if i%8 != 7 and i != 109:
		sys.stdout.write(", ")
	
# lo
print("\n\n\tALIGN $100")
print("ChessCounterTableLo:")
for i in range(110):
	out = lo[i]
	if i%8 == 0:
		sys.stdout.write("\n\t dc.b ")
	sys.stdout.write("%d" % out)
	if i%8 != 7 and i != 109:
		sys.stdout.write(", ")

		
