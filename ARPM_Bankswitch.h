    IFNCONST _ARPM_BANKSWITCH_
_ARPM_BANKSWITCH_ EQU 1         ; Limit one copy of the header in compilation.

;=============================================================================
; ARPM_Bankswitch.h
;
;   Target System:      Atari 2600
;   Target Assembler:   DASM
; 
; Revision History:
;    Feb-19-2011 - Robert Mundschau - Initial Revision.
;
;-----------------------------------------------------------------------------
;
; This file defines bankswitching routines as macros to help simplify
; creating bankswitched Atari 2600 projects.  The macros in this file support
; the popular F8, F6 and F4 bankswitching schemes.  There is no reason an
; industrious person could not add support for other schemes as well.
;
; A general attempt was made to include conditional code in the macros to
; not execute the bankswitch code if the target label is in the current
; bank.  That way you can use the bankswitching macros in place of normal
; JMP and JSR instructions and freely move your code around.  The macros will
; automatically choose the fastest code for you.
;
; Macros defined:
; ---------------
;
;   BS_START_BANK <bank_num>  - Declare a new bank and insert common code.
;   BS_END_BANK   <bank_num>  - End a bank image and report space remaining.
;
;   JMB    <label>   - JuMp Bank switched. (Replaces JMP instruction)
;   JSB    <label>   - Jump to Subroutine Bank switched (Replaces JSR)
;   RTB    <label>   - ReTurn from subroutine Bank switched. (Replaces RTS)
;
;
; Required Configuration Labels:
; ------------------------------
;   To use the macros in this file you must define the following
;   labels in your source code.  You will get better results if you 
;   define the required labels before you invoke any macros.
;
;   BS_Scheme  = The bankswitching scheme for your project. At present
;                there is support for the F4, F6, and F8 schemes.  Use
;                the table below to choose your scheme and set this label
;                to $F4, $F6, or $F8 to compile your selection.
;
;   BS_Begin   = Set this label to point at the powerup start routine
;                for your program.  The routine may be in any of your banks.
;                On powerup, the common bankswitching code will automatically
;                determine the current bank and jump to your routine in
;                whichever bank it is located.
;
; Optional Configuration Labels:
; ------------------------------
;   BS_BRK_Vector0  = By default the macros in this file will set the BRK
;   BS_BRK_Vector1    instruction vector to point at the _BS_PowerupEntry 
;   BS_BRK_Vector2    label which bankswitches to your label BS_Begin.
;   BS_BRK_Vector3    You can override this default behavior on a per bank
;   BS_BRK_Vector4    basis by defining the label of the corresponding bank
;   BS_BRK_Vector5    to point at a target routine within that bank.
;   BS_BRK_Vector6    
;   BS_BRK_Vector7    
;
;
;   BS_DEBUG = Defining this label to any value enables the module debug
;              mode.  In debug mode the compile process will be more
;              verbose, and more error checking may be done.
;
;-----------------------------------------------------------------------------
; BS_Scheme:
;
; The F4, F6, and F8 bankswitching schemes use a range of addresses near 
; the end of each bank to switch between banks.  These addresses are 
; called hotspots. A CPU read of a hotspot address will switch to the
; corresponding bank.
;
; We calculate the starting address of the bank switching hotspot
; addresses from the user defined label BS_Scheme.
; 
; The table below shows the hotspots and number of banks for the F4, F6, and F8
; bankswitching schemes.  If BS_Scheme is undefined the module defaults to the
; standard 4K ROM format with no bankswitching. In that mode the BS_START_BANK
; and BS_END_BANK macros are disabled.
;
;               | BS_Scheme label
;       Hotspot | is  ==>> $F4  $F6  $F8  undefined      
;       Address |          32K  16K   8K   4K == Total ROM Bytes
;       --------+----------------------------- 
;         $1FF4 = bank_num   0    x    x    x    x = No bank switch
;         $1FF5 = bank_num   1    x    x    x        occurs.
;         $1FF6 = bank_num   2    0    x    x
;         $1FF7 = bank_num   3    1    x    x    0-7 = Switch to that bank.
;         $1FF8 = bank_num   4    2    0    x
;         $1FF9 = bank_num   5    3    1    x
;         $1FFA = bank_num   6    x    x    x
;         $1FFB = bank_num   7    x    x    x
;       -------------------------------------
;       Number of banks =    8    4    2    1
;
; Note: The 6507 processor in the Atari 2600 has an instruction prefetch
;       feature that will read the next instruction opcode while processing
;       the previous one.  For that reason you must not put an instruction 
;       (i.e., JMP, RTS, BRK) as the last thing in ROM before a hotspot address.
;       If you do the processor will prefetch from the hotspot and cause an
;       unexpected bank switch.  It is better to use the address just below
;       the first hotspot for data, or nothing at all. 
;
; Example:
; BS_Scheme EQU $F6     ; Set the bankswitch scheme to 4 banks (16K total)
;
;--------------------------------------------------------------------------
;
; This code validates BS_Scheme and sets up the number of banks and the 
; bankswitching hotspots.
;  
    IFCONST BS_Scheme
        IF [BS_Scheme == $F4]
_BS_HotSpots_   EQU $1FF4
_BS_NumBanks_   EQU 8
        ELSE
            IF [BS_Scheme == $F6]
_BS_HotSpots_   EQU $1FF6
_BS_NumBanks_   EQU 4
            ELSE
                IF [BS_Scheme == $F8]
_BS_HotSpots_   EQU $1FF8
_BS_NumBanks_   EQU 2
                ELSE
            echo "ERROR: ARPM_Bankswitch.h: BS_SCHEME = $", BS_Scheme
            echo "       Please define BS_Scheme to equal $F8, $F6 or $F4."
            echo "       Defaulting to no bankswitching, 4K ROM mode."        
_BS_HotSpots_   EQU $1FFC
_BS_NumBanks_   EQU 1
                ENDIF
            ENDIF
        ENDIF
    ELSE
        echo "REPORT: ARPM_Bankswitch.h, The required user label BS_Scheme is undefined."
        echo "        Defaulting to no bankswitching, 4K ROM mode."
_BS_HotSpots_   EQU $1FFC
_BS_NumBanks_   EQU 1
    ENDIF

    IFCONST BS_DEBUG
        IFCONST BS_Scheme
            ECHO "DEBUG: ARPM_Bankswitch.h: BS_Scheme     =", BS_Scheme
        ELSE
            ECHO "DEBUG: ARPM_Bankswitch.h: BS_Scheme is UNDEFINED!"
        ENDIF
        ECHO "DEBUG: ARPM_Bankswitch.h: _BS_HotSpots_ =", [_BS_HotSpots_]d
        ECHO "DEBUG: ARPM_Bankswitch.h: _BS_NumBanks_ =", [_BS_NumBanks_]d
    ENDIF

;==========================================================================
; Macro: BS_START_BANK <0-7>
; --------------------------
; All code that you write following the BS_START_BANK macro will be 
; located in the declared bank.  When you are done adding code to a bank
; you must call the BS_END_BANK macro (below) to finish off the bank.
;
; This macro:
; - Creates an intialized segment with the name BANK_<num>. Use SEG BANK_<num>
;   in your source code to switch between banks as needed.
; - Sets origin and rorigin of the new segment to start the specified memory bank.
;   - The origin sets the starting address for the code in the 
;     ROM chip. 
;   - The rorigin is the virtual starting address of the bank in
;     in the memory space of the VCS.  All labels created within
;     the segment BANK_<num> will encode the bank number into the
;     3 MSB of the label's address value.  The encoded bank number is
;     used in the bankswitching macros to switch to the correct bank.  
;     Therefore, if your code dynamically creates address labels, you must
;     encode the bank number in the address to get correct behavior from
;     the macros in this file. 
; - This macro inserts the common bank switching code and powerup routine
;   hook at the top of the bank.
; - NOTE: USe this macro only once per bank. Use SEG BANK_<num> to 
;---------------------------------------------------------------------------
    MAC     BS_START_BANK   
    
    IF [{1} >= _BS_NumBanks_]
        ECHO "ERROR: BS_START_BANK: Invalid bank number ", [{1}]d, " for BS_Scheme = ", BS_Scheme
    ELSE
_BS_BANK_{1}_ EQU 1
        
        SEG     BANK_{1}

        ORG     [ {1} * $1000 ] + $1000
        RORG    [ {1} * $2000 ] + $1000        
        
    ; This is a block of common bank switching code positioned at the start
    ; of every declared bank.  The routine occupies the same relative
    ; addresses within each bank, so execution of the code continues smoothly
    ; during bankswitches. 
    ;
    ; We use ifnconst constructs to ensure that entry point labels to this
    ; common code are created only once and then shared between all the banks.
        IFNCONST _BS_PowerupEntry_
_BS_PowerupEntry_           ; This label is declared once and shared in all banks.
                            ; Program execution begins at this relative label in
                            ; every bank.
        ENDIF
        ldx #$FF                ; Initialize SP
        txs
        
        lda #>[BS_Begin - 1]    ; Push the start code address onto the stack and then
        pha                     ; fall through to execute the equivalent of an RTB
        lda #<[BS_Begin - 1]    ; macro call.  
        pha                     ;
    
        IFNCONST _BS_RTS_
_BS_RTS_                    ; This label is declared once and shared in all banks.
                            ; We jump to this label to return from  a cross-bank
                            ; subroutine call. 
        ENDIF
  
        tsx                     ; The target bank is encoded in bits 5-7 of the MSB
        lda 2,x                 ; of the target address on the stack.
   
        ; TODO: We could add an optional check here that would bypass the bankswitch
        ;       if we are already in the right bank.  This option would speed up
        ;       in-bank subroutine returns, but slow down cross-bank returns.

  
        lsr                     ; NOTE: We can perform this calculation at compile
        lsr                     ;       time and store it on the stack along with
        lsr                     ;       the return address, but that would make in-bank
        lsr                     ;       calls (using JSR) and cross-bank calls (using JSB)
        lsr                     ;       incompatible with RTB. I decided to keep 
                                ;       things flexible rather than speed optimal.
        
        tax                     ; 0-7 index to F4 bankswitching hotspots.
    
        IFNCONST _BS_JMP_
_BS_JMP_                    ; This label is declared once and shared in all banks.
                            ; This label marks the code to jump to a new bank.
        ENDIF

        lda _BS_HotSpots_,x     ; Switch to desired bank. Since this common code exists
                                ; at the same offset in each bank, the system stays in 
                                ; this routine when the switch occurs.

        rts                     ; Go to target routine now that we are in the right
                                ; bank.
    ENDIF

    ENDM


;-------------------------------------------------------------------- 
; BS_END_BANK <0-7>:
; Use this macro to "bookend" the current bank.  No more code can be
; compiled for the bank once this macro is executed.
; Each usage of BS_START_BANK must be paired with a BS_END_BANK in order
; to map the reset vectors at the end of each bank to the startup code at
; the top of the bank.
;
; The ordering of the banks in the source does not need to be
; from 0 to 7, but it sure makes the code easier to understand.
; - The macro will report space remaining in the memory bank
;   - NOTE: If your program uses every byte of ROM up to the address below
;           the first hotspot address, then the last byte of ROM used must
;           contain data and not a processor instruction. This is because 
;           the 6507 processor will prefetch the next instruction which in
;           this case is the hotspot causing an unwanted bankswitch.The
;           macro prints a warning when this special case happens.
; - The macro inserts the required interupt vectors at the end of the 
;   bank.  These vectors point at the powerup routine defined in the
;   BS_START_BANK macro.
; - NOTE: If you want to use the BRK instruction within a bank, you
;   will need to define the corresponding BS_BRK_VECTORx user label.
;----------------------------------------------------------------------
    MAC    BS_END_BANK


    IFNCONST _BS_BANK_{1}_
        ECHO "ERROR: BS_END_BANK ",[{1}]d," has no matching call to BS_START_BANK!"
    ELSE
        SEG BANK_{1}

_BS_SPACE_LEFT SET [[_BS_HotSpots_ & $1FFF] - [* & $1FFF]] 

        ECHO "REPORT: BS_END_BANK: ", [_BS_SPACE_LEFT]d , "bytes of ROM space left in bank ",[{1}]d)
        
        IF [ _BS_SPACE_LEFT < 0]
            ECHO "ERROR: BS_END_BANK: ROM bank overflowed! Move code to another bank."
        ENDIF
        IF [ _BS_SPACE_LEFT == 0]
            ECHO "WARNING: BS_END_BANK: No ROM left. Confirm last byte of your program ROM is data"
            ECHO "         and not code, or unplanned backswitches will occur!"
        ENDIF
    
        ; Skip the remaining unused ROM bytes in this bank and define the 
        ; interrupt vectors at the very end of the bank.
        ORG     [ {1} * $1000 ] + $1FFC
        RORG    [ {1} * $2000 ] + $1FFC
    
        .word    _BS_PowerupEntry_      ; Reset Vector.
    
        IFCONST BS_BRK_Vector{1}
            .word   BS_BRK_Vector{1}     ; User defined BRK Vector.
        ELSE 
            .word   _BS_PowerupEntry_    ; Default BRK Vector.
        ENDIF
    ENDIF
    
    ENDM
    
    
;====================================================
; Macro JSB - Jump to subroutine in any bank.
;
; Use this macro in place of the standard JSR instruction
; to branch to a subroutine in any bank.  If the target routine 
; is in the current bank, then the macro resolves to a normal
; JSR instruction.
;
; OUTPUT REGISTER STATUS: (No corruption if the target is in the same bank!)
; A: Corrupted
; X: The number of the bank containing the target routine.
; Y: Unchanged
; Stack: Four bytes used to perform the jump. Two bytes remain
;        used until the return operation is performed.
;    
;----------------------------------------------------
    MAC JSB
    
    ; The bank number is encoded in the 3 MSB of any label or bank address
    ; created by the compiler from its own program counter (*).
    IF [[{1} & $E000] == [* & $E000]]
        jsr {1}                     ; We are already in the right bank!
    ELSE    
        lda #>[.return_point-1]     ; We are NOT in the right bank!
        pha                         ; Push the return address onto the stack first.
        lda #<[.return_point-1]
        pha
        
        lda #>[{1}-1]               ; Now push the target address onto the stack.
        pha
        lda #<[{1}-1]
        pha
        ldx #[>[{1}-1]]>>5          ; Calculate the target bank.

        jmp    _BS_JMP_             ; Jump to the common bankswitching routine.
    ENDIF 
.return_point

    ENDM
    
;================================================================
; Macro JMB - Jump to code in any bank.
;
; Use this macro in place of the standard JMP instruction
; to jump to code located in any bank.  If the target address
; is in the current bank, the macro will resolve to a normal
; JMP instruction.
;
; OUTPUT REGISTER STATUS: (No corruption if the target is in the same bank!)
; A: Corrupted
; X: The number of the bank containing the target routine.
; Y: Unchanged
; Stack: Two bytes used to perform the jump.
; 
;-----------------------------------------------------------------
    MAC JMB
    
    ; The bank number is encoded in the 3 MSB of any label or bank address.
    IF [[{1} & $E000] == [* & $E000]]
        jmp {1}                 ; We are already in the right bank!
    ELSE    
        lda #>[{1}-1]           ; We are NOT in the right bank!
        pha                     ; Push the target address onto the stack.
        lda #<[{1}-1]           
        pha
        ldx #[>[{1}-1]]>>5     ; Calculate the target bank.

        jmp _BS_JMP_            ; Jump to the common bankswitching routine.
    ENDIF
    
    ENDM
    
    
;=================================================================
; Macro RTB - Return from a subroutine to the caller in any bank.
;
; Use this macro in place of the standard RTS instruction
; to return from a cross-bank subroutine call made with the 
; macro JSB.  Note RTB is compatible with JSR, so you can jump
; to a subroutine using JSR or JSB, and then return safely
; using RTB in both cases.  RTB is slower than RTS, but you can 
; only use RTS if all calls to the subroutine originate in the same
; bank as the subroutine.   
;
; TODO: At some point we may alter the macro to automatically select
;       the faster RTS instruction when all calls to a subroutine are
;       made from within the same bank as the subroutine.  Such a
;       feature would require all subroutines to have a single entry
;       point which often is not the case for highly optimized 
;       assembly code.
;
; OUTPUT REGISTER STATUS:
; A: The number of the bank containing the return address.
; X: The number of the bank containing the return address.
; Y: Unchanged
; Stack: Two bytes are popped from the stack containing the
;        return address.
; 
;-------------------------------------------------------
    MAC RTB
    
    jmp _BS_RTS_            ; Jump to the common bankswitching routine.
    
    ENDM 

  
;=======================================================================
; End of ARPM_Bankswitch.h
;=======================================================================
    ENDIF
