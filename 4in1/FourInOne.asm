; =========================================================================
; Constants
; =========================================================================

; Script state constants

FOUR_FIRST_LABEL	= 0
FOUR_MINI_SLIDE_IN	= 1
FOUR_MINI_DELAY1	= 2
FOUR_PLASMA_BARS	= 3
FOUR_MINI_DELAY2	= 4
FOUR_SINE_WAVES		= 5
FOUR_MINI_DELAY3	= 6
FOUR_RASTER_SPLITS	= 7
FOUR_MINI_DELAY4	= 8
FOUR_MINI_SLIDE_OUT	= 9
FOUR_SECOND_LABEL	= 10
FOUR_FOUR_SLIDE_IN	= 11
FOUR_FOUR_1		= 12
FOUR_FOUR_2		= 13
FOUR_FOUR_3		= 14
FOUR_FOUR_4		= 15
FOUR_FOUR_SLIDE_OUT	= 16
FOUR_NEXT_PART		= 17


; Colors

FOUR_LABEL_BG		= $50
FOUR_TOPLABEL_BG	= $62
FOUR_TOPLABEL_FG	= $2e
FOUR_BOTTOMLABEL_BG	= $2e
FOUR_BOTTOMLABEL_FG	= $62



; =========================================================================
; Start
; =========================================================================

FourStart

	; inits
	; ------------------------------
	
	; gfx
	lda #%00000001
	sta CTRLPF
	
	; Demo state
	lda #0
	sta FourNumFrame
	sta FourDemoState
	
	sta FourRSIndex1
	lda #16
	sta FourRSIndex2
	lda #32
	sta FourRSIndex3
	lda #48
	sta FourRSIndex4
	
	lda #0
	sta FourPBLine1
	sta FourPBLine2
	sta FourCurrentHMove0
	sta FourCurrentHMove1
	sta FourNextHMove0
	sta FourNextHMove1
	sta FourCurrLineLo
	sta FourCurrLineHi

	sta FourCurrentSWLine1
	lda #64
	sta FourCurrentSWLine2

	lda #1
	sta FourAddHi
	sta FourAddLo
	sta FourTwisterDir
	
	jmp FourIncoming
	
	
; =========================================================================
; Kernel
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

FourKernel
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc FourNumFrame
	lda FourNumFrame
	ldx FourDemoState
	cmp FourScriptDurations,x 
	bne .noNextState
	inc FourDemoState
	lda FourDemoState
	cmp #FOUR_NEXT_PART
	bne .noNextPart
	JMB ChessStart
.noNextPart
	lda #0
	sta FourNumFrame
.noNextState
	

; -------------------------------------------------------------------------

FourIncoming

	; Advance effects
	JSB FourAdvancePlasmaBars
	JSB FourAdvanceRasterSplits
	JSB FourAdvanceSineWaves
	
	; Init current top effect
	ldx FourDemoState
	lda FourScriptInitLo,x
	sta FourKernelPtr
	lda FourScriptInitHi,x
	sta FourKernelPtr+1
	lda #>(.afterInit-1)
	pha
	lda #<(.afterInit-1)
	pha
	jmp (FourKernelPtr)
.afterInit

	; Set pointer to current top effect
	ldx FourDemoState
	lda FourScriptKernelLo,x
	sta FourKernelPtr
	lda FourScriptKernelHi,x
	sta FourKernelPtr+1

	; Wait for end of VBLANK
	jsr FourWaitForIntim
	lda #0
	sta VBLANK

	; Jump to current top effect
	jmp (FourKernelPtr)



; -------------------------------------------------------------------------
; Visible area Kernels
; -------------------------------------------------------------------------

; -------------------------------------------------------------------------
; Watch 3 get 1 free Kernel
; -------------------------------------------------------------------------

Watch3Get1Init SUBROUTINE
	sta WSYNC
	; ----------
	lda #0
	sta GRP0
	sta GRP1
	sta ENAM0
	sta ENAM1
	sta ENABL
	lda #%00000011	; three copies close
	sta NUSIZ0
	sta NUSIZ1
	lda #RIGHT_2
	sta HMP0
	lda #RIGHT_1
	sta.w HMP1
	
	sta RESP0	; @36
	sta RESP1

	lda #%00000001	; Mirror mode
	sta CTRLPF
	lda #1
	sta VDELP0
	sta VDELP1
	lda #0
	sta PF0
	sta PF1
	lda #%11111111
	sta PF2
	lda #FOUR_LABEL_BG
	sta COLUBK
	sta COLUPF
	; ----------
	sta HMOVE

	rts

; -------------------------------------------------------------------------

Watch3Get1 SUBROUTINE

	ldx #53
	jsr FourWaste

	; Label top
	lda FourNumFrame
	cmp #15
	bcc .doFade
	cmp #FOUR_FIRST_LABEL_DUR - 15
	bcc .noFadeOut
	lda #FOUR_FIRST_LABEL_DUR
	sbc FourNumFrame
	bcs .doFade
.noFadeOut
	lda #15
.doFade
	sta FourFadeState
	tax
	lda FourLabelBackFade,x
	
	sta WSYNC
	sta COLUPF

	; Very first display of background does not need fade in
	lda FourDemoState
	cmp #FOUR_FIRST_LABEL
	bne .notFirst
	lda FourNumFrame
	cmp #15
	bcs .notFirst
	lda #$62
	sta COLUPF
	lda FourLabelForeFirstFade,x
	sta COLUP0
	sta COLUP1
	bne .next	; unconditional jmp
.notFirst
	lda FourLabelForeFade,x
	sta COLUP0
	sta COLUP1
.next

	ldx #5
	jsr FourWaste
	
	; Watch 3
	lda #<Watch3_1
	ldx #>Watch3_1
	ldy #10
	jsr FourPrepare48	
	jsr FourDisplay48

	ldx #4
	jsr FourWaste
	
	; classic/boring
	lda FourNumFrame
	asl
	asl
	cmp #32
	bcs .classic
	lda #<boring_1
	ldx #>boring_1
	bne .boring
.classic
	lda #<classic_1
	ldx #>classic_1
.boring
	ldy #12
	jsr FourPrepare48	
	jsr FourDisplay48

	ldx #4
	jsr FourWaste
	
	; effects
	lda #<effects_1
	ldx #>effects_1
	ldy #10
	jsr FourPrepare48	
	jsr FourDisplay48

	ldx #10
	jsr FourWaste

	; Show Get 1 free?
	lda FourDemoState
	cmp #FOUR_SECOND_LABEL
	beq .showGet1
	lda #FOUR_LABEL_BG
	sta COLUPF
	ldx #49
	jsr FourWaste
	jmp .end
	
.showGet1
	; Label bottom
	ldx FourFadeState
	lda FourLabelForeFade,x
	sta COLUPF
	lda FourLabelBackFade,x
	sta COLUP0
	sta COLUP1
	ldx #5
	jsr FourWaste
	
	; Get 1
	lda #<Get1_1
	ldx #>Get1_1
	ldy #10
	jsr FourPrepare48	
	jsr FourDisplay48

	ldx #4
	jsr FourWaste
	
	; free
	lda FourNumFrame
	asl
	asl
	asl
	bmi .doFree
	ldx FourFadeState
	lda FourLabelForeFade,x
	sta COLUP0
	sta COLUP1
.doFree
	lda #<free_1
	ldx #>free_1
	ldy #10
	jsr FourPrepare48	
	jsr FourDisplay48
	
	ldx #10
	jsr FourWaste
	lda #FOUR_LABEL_BG
	sta COLUPF

.end	
	ldx #54
	jsr FourWaste

	jmp FourDoVBlank



; -------------------------------------------------------------------------
; Mini delay Kernel
; -------------------------------------------------------------------------

MiniDelayInit SUBROUTINE
	lda #0
	sta COLUBK
	rts

; -------------------------------------------------------------------------

MiniDelay SUBROUTINE
	ldx #111
	jsr FourWaste
	
	lda #FOUR_LABEL_BG
	sta COLUBK
	
	ldx #116
	jsr FourWaste

	jmp FourDoVBlank
	
	

; -------------------------------------------------------------------------
; Mini slide in Kernel
; -------------------------------------------------------------------------

MiniSlideInOutInit SUBROUTINE

	lda #0
	sta COLUBK
	sta GRP0
	sta GRP1
	sta PF0
	sta PF1
	sta PF2
	rts

; -------------------------------------------------------------------------

MiniSlideIn SUBROUTINE

	lda FourNumFrame
	asl
	asl
	sta FourTmp
	tax
.top
	sta WSYNC
	dex
	bpl .top
	
	lda #FOUR_LABEL_BG
	sta COLUBK
	lda #226
	sec
	sbc FourTmp
	tax
.bottom
	sta WSYNC
	dex
	bne .bottom
	
	jmp FourDoVBlank


; -------------------------------------------------------------------------
; Mini slide Out Kernel
; -------------------------------------------------------------------------

MiniSlideOut SUBROUTINE

	lda #FOUR_MINI_SLIDE_DUR
	sec	
	sbc FourNumFrame
	asl
	asl
	sta FourTmp
	tax
.top
	sta WSYNC
	dex
	bpl .top
	
	lda #FOUR_LABEL_BG
	sta COLUBK
	lda #226
	sec
	sbc FourTmp
	tax
.bottom
	sta WSYNC
	dex
	bne .bottom
	
	jmp FourDoVBlank



; -------------------------------------------------------------------------
; Raster Splits Kernel
; -------------------------------------------------------------------------

RasterSplitsInit SUBROUTINE
	
	; position players
	sta WSYNC
	lda #%00110101		; Missile size 8, double-sized player
	sta NUSIZ0
	sta NUSIZ1
	lda #%11111111
	sta GRP0
	sta GRP1
	lda #0
	sta ENAM0
	sta ENAM1
	sta COLUPF
	sta COLUP0
	sta COLUP1
	lda #LEFT_3
	sta HMP0
	lda #RIGHT_4
	
	sta RESP0		; @40
	sta RESP1

	sta HMP1

	lda #>FourRSColors
	sta FourRSPtr1+1
	sta FourRSPtr2+1
	sta FourRSPtr3+1
	sta FourRSPtr4+1
	lda #0
	sta COLUBK
	sta VDELP0
	sta VDELP1

	sta WSYNC
	sta HMOVE

	rts
	
; -------------------------------------------------------------------------
	
RasterSplits SUBROUTINE
	sta WSYNC
	lda #0
	sta PF0
	lda #%00000000
	sta PF1
	lda #%11111111
	sta PF2

	ldy #96-1
	
.loop
	sta WSYNC
	lda (FourRSPtr1),y
	sta COLUPF
	lda (FourRSPtr2),y
	sta COLUP0
	lda (FourRSPtr3),y
	sta COLUP1
	lda (FourRSPtr4),y
	
	; Sleep 20
	jsr FourRTS
	SLEEP 8
	sta COLUPF

	dey
	bpl .loop
.afterLoop

	lda #0
	sta COLUPF
	sta COLUP0
	sta COLUP1
	sta GRP0
	sta GRP1
	
	jsr FourInit48
	lda #<RasterSplits_1
	ldx #>RasterSplits_1
	ldy #31
	jsr FourPrepare48	

	ldx #7
	jsr FourWaste
	
	
	; lower half

MiniLowerHalf
	lda #FOUR_LABEL_BG
	sta COLUBK
	
	; calc y position
	lda #FOUR_MINI_DUR
	sec
	sbc FourNumFrame
	lsr
	lsr
	lsr
	sta FourTmp2
	clc
	adc #22
	tax
	jsr FourWaste

	; Set fade color
	lda FourNumFrame
	cmp #15
	bcc .doFade
	cmp #FOUR_MINI_DUR - 15
	bcc .noFadeOut
	lda #FOUR_MINI_DUR
	sbc FourNumFrame
	bcs .doFade
.noFadeOut
	lda #15
.doFade
	tax
	lda FourLabelForeFade,x
	sta COLUP0
	sta COLUP1

	jsr FourDisplay48

	lda #62
	sec
	sbc FourTmp2
	tax
	; Hack
	lda FourDemoState
	cmp #FOUR_PLASMA_BARS
	bne .noHack
	inx
.noHack
	jsr FourWaste

	jmp FourDoVBlank




; -------------------------------------------------------------------------
; Sine Waves Kernel
; -------------------------------------------------------------------------

SineWavesInit SUBROUTINE
	
	; position players
	sta WSYNC
	lda #%00110000		; Missile size 8
	sta NUSIZ0
	sta NUSIZ1
	lda #%11111111
	sta GRP0
	lda #0
	sta GRP1
	sta COLUP0
	sta COLUP1
	lda #$62
	sta COLUPF
	lda #3
	sta ENAM0
	sta ENAM1
	lda #RIGHT_3
	
	nop
	sta RESM0
	sta RESP0
	sta RESM1

	sta HMM0
	lda #LEFT_2
	sta HMP0
	lda #LEFT_7
	sta HMM1

	sta WSYNC
	sta HMOVE
	
	lda #0
	sta COLUBK
	
	; Sleep 17
	jsr FourRTS
	SLEEP 5

	lda #0
	sta HMP0
	lda FourCurrentHMove0
	sta HMM0
	lda FourCurrentHMove1
	sta HMM1
	lda FourNextHMove0
	sta FourCurrentHMove0
	lda FourNextHMove1
	sta FourCurrentHMove1
	sta WSYNC
	sta HMOVE
	rts
	
; -------------------------------------------------------------------------
	
SineWaves SUBROUTINE
	sta WSYNC
	lda #0
	sta PF0
	lda #%00000001
	sta PF1
	lda #%11111111
	sta PF2
	sta HMCLR
	ldy #0
.loop
	lda FourLineData,y
	sta HMM0
	asl
	asl
	asl
	asl
	sta HMM1
	sta WSYNC
	sta HMOVE
	sta WSYNC
	iny
	cpy #48
	bne .loop

	lda #0
	sta COLUPF
	sta COLUP0
	sta COLUP1
	
	jsr FourInit48
	lda #<SineWaves_1
	ldx #>SineWaves_1
	ldy #31
	jsr FourPrepare48	

	ldx #8
	jsr FourWaste
	
	; lower half
	jmp MiniLowerHalf
	


; -------------------------------------------------------------------------
; Plasma Bar Kernel
; -------------------------------------------------------------------------

PlasmaBarsInit SUBROUTINE
	lda #0
	sta COLUBK
	rts


PlasmaBars SUBROUTINE
	sta WSYNC
	lda #0
	sta COLUPF
	sta PF0
	lda #%00000001
	sta PF1
	lda #%11111111
	sta PF2
	
	lda #95
	sta FourBCurrentLine
	lda FourPBLine1
	sta FourPBCurrentLine
	ldy FourPBLine2
	
	sta WSYNC

.loop
	inc FourPBCurrentLine	; 5
	ldx FourPBCurrentLine	; 3
	iny			; 2
	lda FourPBSine1,x	; 4
	adc FourPBSine2,y	; 4
	tax			; 2
	lda FourPBColors,x	; 4
	sta COLUPF		; 3

	sta WSYNC		; 3
	dec FourBCurrentLine	; 5
	bpl .loop		; 3

	lda #0
	sta COLUPF
	
	jsr FourInit48
	lda #<PlasmaBars_1
	ldx #>PlasmaBars_1
	ldy #30
	jsr FourPrepare48	

	ldx #8
	jsr FourWaste
	
	; lower half
	jmp MiniLowerHalf
	
	
	
; -------------------------------------------------------------------------
; Four Minis slide in/out Kernel
; -------------------------------------------------------------------------

FourFourSlideInit SUBROUTINE

	lda #0
	sta COLUBK
	sta GRP0
	sta GRP1
	sta COLUPF
	lda #%11000000
	sta PF0
	lda #%11111111
	sta PF1
	lda #%01111111
	sta PF2
	rts

; -------------------------------------------------------------------------

FourFourSlide SUBROUTINE

	JSB SupportFourFourSlide
	sta WSYNC
	jmp FourDoVBlank
	


; -------------------------------------------------------------------------
; All Four Kernel
; -------------------------------------------------------------------------

AllFourInit SUBROUTINE
	; position players
	sta WSYNC
	; ----------
	lda #%00110101		; Missile size 8, double-sized player
	sta NUSIZ0
	sta NUSIZ1
	lda #%00110001		; Ball size 8, reflected PF
	sta CTRLPF
	lda #%11111111
	sta GRP0
	sta GRP1
	lda #0
	sta COLUP0
	ldx #$00

	; @28
	sta RESM0
	sta RESBL
	sta RESM1

	sta COLUP1
	stx COLUPF
	
	lda #RIGHT_4
	sta HMM0
	lda #LEFT_1
	sta HMBL

	; @53
	sta RESP0
	sta RESP1
	
	lda #LEFT_6
	sta HMM1
	
	lda #LEFT_4
	sta HMP0
	lda #RIGHT_3
	sta HMP1

	lda #3
	; ----------
	sta HMOVE

	sta ENAM0
	sta ENAM1
	sta ENABL
	lda #0
	sta COLUBK
	sta VDELP0
	sta VDELP1

	lda #>FourRSColors
	sta FourRSPtr1+1
	sta FourRSPtr2+1
	sta FourRSPtr3+1
	sta FourRSPtr4+1

	sta HMCLR
	
	lda FourNextHMove0
	sta FourCurrentHMove0
	sta HMM0
	lda FourNextHMove1
	sta FourCurrentHMove1
	sta HMM1

	sta WSYNC
	sta HMOVE

	rts

; -------------------------------------------------------------------------

AllFour SUBROUTINE

	lda #%00110000
	sta PF0
	lda #%00000000
	sta PF1
	lda #%10000000
	sta PF2

	; prepare stack
	tsx
	stx FourTmp
	ldx #FourLineData - 1
	txs

	sta HMCLR

	; 2LK kernel
	; start split writes at pixel 66
	; PF before 84, P0 before 102, P1 before 118, PF before 134
	;
	; cycles:
	; Start at 45
	; PF before 50, P0 before 56, P1 before 62, PF before 68
	
	sta WSYNC
	lda #$62
	sta COLUBK
	SLEEP 6

	ldy #96-1
	jmp .loop
	ALIGN $100
.loop
	; ---------- Line 1 ----------

	; Sine waves
	; @16
	pla			; 4
	sta HMM0		; 3
	asl			; 2
	asl			; 2
	asl			; 2
	asl			; 2
	sta HMM1		; 3

	; Raster split	
	; @35
	lax (FourRSPtr1),y	; 5
	lda (FourRSPtr2),y	; 5
	sta COLUP0		; 3, @45
	stx COLUBK		; 3, @48
	lda (FourRSPtr3),y	; 5
	sta COLUP1		; 3, @56
	lda (FourRSPtr4),y	; 5
	sta COLUBK		; 3, @64

	; Cleanup
	; @67
	lda #$62		; 2
	SLEEP 3			; 3
	sta.w COLUBK		; 4

	; ----------
	sta HMOVE		; 3, @0

	lda #0			; 2
	sta COLUP0		; 3
	sta COLUP1		; 3
	
	; @11
	dey			; 2
	SLEEP 4			; 6

	; ---------- Line 2 ----------
	
	; @17
	; Sine waves: Don't do HMOVE
	SLEEP 18		; 18

	; Raster split	
	; @35
	lax (FourRSPtr1),y	; 5
	lda (FourRSPtr2),y	; 5
	sta COLUP0		; 3, @45
	stx COLUBK		; 3, @48
	lda (FourRSPtr3),y	; 5
	sta COLUP1		; 3, @56
	lda (FourRSPtr4),y	; 5
	sta COLUBK		; 3, @64
				; = 32

	; Cleanup
	; @67
	lda #$62		; 2
	nop			; 2
	sta.w COLUBK		; 4
	lda #0			; 2
	; ----------
	sta COLUP0		; 3, @1
	sta COLUP1		; 3
	
	; @7
	SLEEP 5			; 6
	dey			; 2
	bpl .loop		; 3

	lda #0
	sta COLUBK
	sta ENAM0
	sta ENAM1
	sta ENABL
	sta GRP0
	sta GRP1

	; Restore stack
	ldx FourTmp
	txs

; -------------------------------------------------------------------------

FourGap SUBROUTINE

	lda #23*76/64 + 1
	sta TIM64T

	; Calc twister
	JSB FourAdvanceTwister

	; Plasma and Twister init
PlasmaTwisterInit SUBROUTINE
	
	; position players
	sta WSYNC
	lda #%00000000
	sta NUSIZ0
	sta NUSIZ1
	sta COLUP0
	sta COLUP1
	sta ENAM0
	sta ENAM1
	sta ENABL
	lda #%11001111
	sta PF0
	lda #%11111111
	sta PF1
	lda #%01111111
	sta PF2
	
	lda #RIGHT_3
	sta HMP0
	lda #RIGHT_2
	sta HMP1

	SLEEP 6
	
	sta RESP0
	sta RESP1

	; Wait for end of gap
.waitForEnd
	lda INTIM
	bne .waitForEnd

; -------------------------------------------------------------------------

PlasmaTwister SUBROUTINE

	; prepare stack
	sta WSYNC
	sta HMOVE

	; SLEEP 29
	jsr FourRTS
	jsr FourRTS
	SLEEP 5
	
	tsx
	stx FourTmp
	ldx #FourLineData - 1
	txs

	lda #95
	sta FourBCurrentLine
	lda FourPBLine1
	sta FourPBCurrentLine
	ldy FourPBLine2
	lda #48
	sta FourCurrentLine
	
	;SLEEP 29

.loop
	; ---------- Line 1 ----------
	; @60
	
	; Plasma Bars
	inc FourPBCurrentLine	; 5
	ldx FourPBCurrentLine	; 3
	iny			; 2
	lda FourPBSine1,x	; 4
	adc FourPBSine2,y	; 4
	tax			; 2
	lda FourPBColors,x	; 4
	sta COLUPF		; 3
				; = 27
	; Twister
	; @11
	
	pla			; 4
	sta FourTmp2		; 3
	tax			; 2
	lda twister_gfx_1,x	; 4
	sta GRP0		; 3
	lda twister_gfx_2,x	; 4
	sta GRP1		; 3
	lda TwisterColors,x	; 4
	sta COLUP0		; 3
	sta COLUP1		; 3
				; = 33

	; @44
	lda #$c0		; 2
	sta COLUPF		; 3
	
	; @49
	SLEEP 11		; 11
	
	; ---------- Line 2 ----------
	; @60
	
	; Plasma Bars
	inc FourPBCurrentLine	; 5
	ldx FourPBCurrentLine	; 3
	iny			; 2
	lda FourPBSine1,x	; 4
	adc FourPBSine2,y	; 4
	tax			; 2
	lda FourPBColors,x	; 4
	sta COLUPF		; 3
				; = 27

	; Twister
	; @11

	ldx FourTmp2		; 3
	lda twister_gfx_1,x	; 4
	sta GRP0		; 3
	lda twister_gfx_2,x	; 4
	sta GRP1		; 3
	lda TwisterColors,x	; 4
	sta COLUP0		; 3
	sta COLUP1		; 3
				; = 27

	; @38
	SLEEP 6			; 6

	; @44
	lda #$c0		; 2
	sta COLUPF		; 3
	
	; @49
	SLEEP 2			; 2
	dec FourCurrentLine	; 5
	bne .loop		; 4! (page boundary)
	
	; restore stack
	ldx FourTmp
	txs

; -------------------------------------------------------------------------

	; prepare small free

FourSmallFree SUBROUTINE
	lda #LEFT_4 ;RIGHT_8
	sta HMP0
	sta HMP1
	lda #0
	sta COLUPF
	
	sta HMOVE	; @0

	sta GRP0
	sta GRP1
	ldx #$66
	lda FourNumFrame
	asl
	asl
	asl
	bmi .showFree
	ldx #0
.showFree
	stx COLUP0
	stx COLUP1
	lda #%00000001
	sta NUSIZ0
	sta NUSIZ1
	
	; display small free
	ldx #6
	ldy #0
.displaySmallFree
	sta WSYNC
	lda smallfree_1,x
	sta GRP0
	lda smallfree_2,x
	sta GRP1
	lda smallfree_3,x
	; @18
	; SLEEP 41
	jsr FourRTS
	jsr FourRTS
	jsr FourRTS
	SLEEP 5
	sta GRP0
	sty GRP1
	dex
	bpl .displaySmallFree

	lda #0
	sta GRP0
	sta GRP1

	jmp FourDoVBlank



; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

FourDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	JSB MusicPlayer

	jsr FourWaitForIntim

	jmp FourKernel


; =========================================================================
; Subroutines
; =========================================================================

; -------------------------------------------------------------------------
; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

FourWaitForIntim SUBROUTINE
	lda INTIM
	bne FourWaitForIntim
	sta WSYNC

FourRTS
	rts



; -------------------------------------------------------------------------
; Waste x scanlines
; -------------------------------------------------------------------------

FourWaste

.loop
	sta WSYNC
	dex
	bne .loop

	rts


	
; -------------------------------------------------------------------------
; Prepare pointers for 48 pixel sprites.
; IN:
; a: lo ptr to first byte
; x: hi ptr to first byte
; y: height of sprite
; -------------------------------------------------------------------------

FourPrepare48 SUBROUTINE
	sta FourTmp
	lda #<Four48_1
	sta FourKernelPtr
	lda #>Four48_1
	sta FourKernelPtr + 1
	sty FourTmp2
	dey
	sty FourCurrentLine	; for display current later
	ldy #0

.loop
	; Store ptr
	lda FourTmp
	sta (FourKernelPtr),y
	iny
	txa
	sta (FourKernelPtr),y
	iny
	; advance ptr
	lda FourTmp
	clc
	adc FourTmp2
	sta FourTmp
	bcc .noHi
	inx
.noHi
	cpy #12
	bne .loop

	rts



; -------------------------------------------------------------------------
; Position sprites for 48pix display and clear PF.
; Used in mini effect screens.
; -------------------------------------------------------------------------

FourInit48 SUBROUTINE
	sta WSYNC
	; ----------
	lda #0
	sta GRP0
	sta GRP1
	sta ENAM0
	sta ENAM1
	sta ENABL
	lda #%00000011	; three copies close
	sta NUSIZ0
	sta NUSIZ1
	lda #RIGHT_2
	sta HMP0
	lda #RIGHT_1
	sta.w HMP1
	
	sta RESP0	; @36
	sta RESP1

	lda #1
	sta VDELP0
	sta VDELP1
	lda #0
	sta PF0
	sta PF1
	sta PF2

	sta WSYNC
	sta HMOVE

	rts
	
	

; -------------------------------------------------------------------------
; Display centered 48 pixel sprite.
; Pointers are in Four48_x
; Height-1 is in FourCurrentLine
; -------------------------------------------------------------------------

FourDisplay48 SUBROUTINE

.loop
	ldy FourCurrentLine
	lda (Four48_1),y

	sta WSYNC

	sta GRP0
	lda (Four48_2),y
	sta GRP1
	lda (Four48_3),y
	sta GRP0
	lda (Four48_6),y
	sta FourTmp
	lax (Four48_5),y
	lda (Four48_4),y
	ldy FourTmp
	sta.w GRP1		; @41
	stx GRP0
	sty GRP1
	sta GRP0
	dec FourCurrentLine
	bpl .loop

	lda #0
	sta GRP0
	sta GRP1
	sta GRP0
	rts



	
; =========================================================================
; Tables
; =========================================================================

; Script event durations
; ------------------------------

FOUR_FIRST_LABEL_DUR	= 250
FOUR_MINI_SLIDE_DUR	= 28
FOUR_MINI_DUR		= 200
FOUR_SECOND_LABEL_DUR	= 250
FOUR_FOUR_SLIDE_IN_DUR	= 100

FourScriptDurations
	dc.b FOUR_FIRST_LABEL_DUR	;	= 0
	dc.b FOUR_MINI_SLIDE_DUR	;	= 1
	dc.b 25		; FOUR_MINI_DELAY1	= 2
	dc.b FOUR_MINI_DUR	;	= 3
	dc.b 25		; FOUR_MINI_DELAY2	= 4
	dc.b FOUR_MINI_DUR	;	= 5
	dc.b 25		; FOUR_MINI_DELAY3	= 6
	dc.b FOUR_MINI_DUR	; 	= 7
	dc.b 25		; FOUR_MINI_DELAY4	= 8
	dc.b FOUR_MINI_SLIDE_DUR	;	= 9
	dc.b FOUR_SECOND_LABEL_DUR	;	= 10
	dc.b FOUR_FOUR_SLIDE_IN_DUR	;	= 11
	dc.b 255	;FOUR_FOUR_1		= 12
	dc.b 255	;FOUR_FOUR_2		= 13
	dc.b 255	;FOUR_FOUR_3		= 14
	dc.b 255	;FOUR_FOUR_4		= 15
	dc.b FOUR_FOUR_SLIDE_IN_DUR	;	= 16


; Parts init and kernel pointers
; ------------------------------
	
FourScriptKernelLo
	dc.b <Watch3Get1
	dc.b <MiniSlideIn
	dc.b <MiniDelay
	dc.b <PlasmaBars
	dc.b <MiniDelay
	dc.b <SineWaves
	dc.b <MiniDelay
	dc.b <RasterSplits
	dc.b <MiniDelay
	dc.b <MiniSlideOut
	dc.b <Watch3Get1
	dc.b <FourFourSlide
	dc.b <AllFour
	dc.b <AllFour
	dc.b <AllFour
	dc.b <AllFour
	dc.b <FourFourSlide
	
FourScriptKernelHi
	dc.b >Watch3Get1
	dc.b >MiniSlideIn
	dc.b >MiniDelay
	dc.b >PlasmaBars
	dc.b >MiniDelay
	dc.b >SineWaves
	dc.b >MiniDelay
	dc.b >RasterSplits
	dc.b >MiniDelay
	dc.b >MiniSlideOut
	dc.b >Watch3Get1
	dc.b >FourFourSlide
	dc.b >AllFour
	dc.b >AllFour
	dc.b >AllFour
	dc.b >AllFour
	dc.b >FourFourSlide

FourScriptInitLo
	dc.b <Watch3Get1Init
	dc.b <MiniSlideInOutInit
	dc.b <MiniDelayInit
	dc.b <PlasmaBarsInit
	dc.b <MiniDelayInit
	dc.b <SineWavesInit
	dc.b <MiniDelayInit
	dc.b <RasterSplitsInit
	dc.b <MiniDelayInit
	dc.b <MiniSlideInOutInit
	dc.b <Watch3Get1Init
	dc.b <FourFourSlideInit
	dc.b <AllFourInit
	dc.b <AllFourInit
	dc.b <AllFourInit
	dc.b <AllFourInit
	dc.b <FourFourSlideInit
	
FourScriptInitHi
	dc.b >Watch3Get1Init
	dc.b >MiniSlideInOutInit
	dc.b >MiniDelayInit
	dc.b >PlasmaBarsInit
	dc.b >MiniDelayInit
	dc.b >SineWavesInit
	dc.b >MiniDelayInit
	dc.b >RasterSplitsInit
	dc.b >MiniDelayInit
	dc.b >MiniSlideInOutInit
	dc.b >Watch3Get1Init
	dc.b >FourFourSlideInit
	dc.b >AllFourInit
	dc.b >AllFourInit
	dc.b >AllFourInit
	dc.b >AllFourInit
	dc.b >FourFourSlideInit


; Four label fade/flash colors
; ------------------------------

FourLabelBackFade
	dc.b $50, $50, $50, $30, $30, $30, $32, $32
	dc.b $20, $20, $42, $42, $40, $62, $62, $62

FourLabelForeFade
	dc.b $50, $50, $50, $52, $52, $32, $32, $34
	dc.b $36, $36, $38, $3a, $2a, $2c, $2c, $2e

	; From red to white, for very first fade when red back is already there
FourLabelForeFirstFade
	dc.b $60, $60, $60, $60, $62, $64, $42, $44
	dc.b $44, $46, $46, $48, $4a, $2a, $2c, $2e

; small blinking free gfx
; ------------------------------

smallfree_1
	dc.b %11000011
	dc.b %11000011
	dc.b %11110011
	dc.b %11110011
	dc.b %11000011
	dc.b %11111011
	dc.b %11111011


smallfree_2
	dc.b %00110111
	dc.b %01100111
	dc.b %11000110
	dc.b %11100111
	dc.b %00110110
	dc.b %11100111
	dc.b %11100111


smallfree_3
	dc.b %11011111
	dc.b %11011111
	dc.b %00011000
	dc.b %10011110
	dc.b %00011000
	dc.b %11011111
	dc.b %11011111



; Effects data
; ------------------------------

TwisterColors
	dc.b $76, $78, $76, $78, $78, $7a, $78, $7a
	dc.b $7a, $7c, $7a, $7c, $7c, $7e, $7c, $7e
	dc.b $7e, $7c, $7e, $7c, $7c, $7a, $7c, $7a
	dc.b $7a, $78, $7a, $78, $78, $76, $78, $76

	ALIGN $100
FourPBColors
	dc.b $80, $82, $80, $82, $82, $84, $82, $84
	dc.b $84, $86, $84, $86, $86, $88, $86, $88
	dc.b $88, $8a, $88, $8a, $8a, $8c, $8a, $8c
	dc.b $8c, $8e, $8c, $8e, $8e, $ee, $8e, $ee
	dc.b $ee, $8e, $ee, $8e, $8e, $8c, $8e, $8c
	dc.b $8c, $8a, $8c, $8a, $8a, $88, $8a, $88
	dc.b $88, $86, $88, $86, $86, $84, $86, $84
	dc.b $84, $82, $84, $82, $82, $80, $82, $80

	dc.b $80, $82, $80, $82, $82, $84, $82, $84
	dc.b $84, $86, $84, $86, $86, $88, $86, $88
	dc.b $88, $8a, $88, $8a, $8a, $8c, $8a, $8c
	dc.b $8c, $8e, $8c, $8e, $8e, $ee, $8e, $ee
	dc.b $ee, $8e, $ee, $8e, $8e, $8c, $8e, $8c
	dc.b $8c, $8a, $8c, $8a, $8a, $88, $8a, $88
	dc.b $88, $86, $88, $86, $86, $84, $86, $84
	dc.b $84, $82, $84, $82, $82, $80, $82, $80

	dc.b $b0, $b2, $b0, $b2, $b2, $b4, $b2, $b4
	dc.b $b4, $b6, $b4, $b6, $b6, $b8, $b6, $b8
	dc.b $b8, $ba, $b8, $ba, $ba, $bc, $ba, $bc
	dc.b $bc, $be, $bc, $be, $be, $ee, $be, $ee
	dc.b $ee, $be, $ee, $be, $be, $bc, $be, $bc
	dc.b $bc, $ba, $bc, $ba, $ba, $b8, $ba, $b8
	dc.b $b8, $b6, $b8, $b6, $b6, $b4, $b6, $b4
	dc.b $b4, $b2, $b4, $b2, $b2, $b0, $b2, $b0

	dc.b $b0, $b2, $b0, $b2, $b2, $b4, $b2, $b4
	dc.b $b4, $b6, $b4, $b6, $b6, $b8, $b6, $b8
	dc.b $b8, $ba, $b8, $ba, $ba, $bc, $ba, $bc
	dc.b $bc, $be, $bc, $be, $be, $ee, $be, $ee
	dc.b $ee, $be, $ee, $be, $be, $bc, $be, $bc
	dc.b $bc, $ba, $bc, $ba, $ba, $b8, $ba, $b8
	dc.b $b8, $b6, $b8, $b6, $b6, $b4, $b6, $b4
	dc.b $b4, $b2, $b4, $b2, $b2, $b0, $b2, $b0

FourPBSine1
	include PB_Sine1_192_63.asm
	include PB_Sine1_64_31.asm

FourPBSine2
	include PB_Sine2_100_63.asm
	include PB_Sine2_56_31.asm
	include PB_Sine2_100_63.asm

FourRSColors
	dc.b $20, $22, $20, $22, $22, $24, $22, $24
	dc.b $24, $26, $24, $26, $26, $28, $26, $28
	dc.b $28, $2a, $28, $2a, $2a, $2c, $2a, $2c
	dc.b $2c, $2e, $2c, $2e, $2e, $ee, $2e, $ee
	dc.b $ee, $2e, $ee, $2e, $2e, $2c, $2e, $2c
	dc.b $2c, $2a, $2c, $2a, $2a, $28, $2a, $28
	dc.b $28, $26, $28, $26, $26, $24, $26, $24
	dc.b $24, $22, $24, $22, $22, $20, $22, $20

	dc.b $30, $32, $30, $32, $32, $34, $32, $34
	dc.b $34, $36, $34, $36, $36, $38, $36, $38
	dc.b $38, $3a, $38, $3a, $3a, $3c, $3a, $3c
	dc.b $3c, $3e, $3c, $3e, $3e, $ee, $3e, $ee
	dc.b $ee, $3e, $ee, $3e, $3e, $3c, $3e, $3c
	dc.b $3c, $3a, $3c, $3a, $3a, $38, $3a, $38
	dc.b $38, $36, $38, $36, $36, $34, $36, $34
	dc.b $34, $32, $34, $32, $32, $30, $32, $30

	dc.b $20, $22, $20, $22, $22, $24, $22, $24
	dc.b $24, $26, $24, $26, $26, $28, $26, $28
	dc.b $28, $2a, $28, $2a, $2a, $2c, $2a, $2c
	dc.b $2c, $2e, $2c, $2e, $2e, $ee, $2e, $ee
	dc.b $ee, $2e, $ee, $2e, $2e, $2c, $2e, $2c
	dc.b $2c, $2a, $2c, $2a, $2a, $28, $2a, $28
	dc.b $28, $26, $28, $26, $26, $24, $26, $24
	dc.b $24, $22, $24, $22, $22, $20, $22, $20

	dc.b $30, $32, $30, $32, $32, $34, $32, $34
	dc.b $34, $36, $34, $36, $36, $38, $36, $38
	dc.b $38, $3a, $38, $3a, $3a, $3c, $3a, $3c
	dc.b $3c, $3e, $3c, $3e, $3e, $ee, $3e, $ee
	dc.b $ee, $3e, $ee, $3e, $3e, $3c, $3e, $3c
	dc.b $3c, $3a, $3c, $3a, $3a, $38, $3a, $38
	dc.b $38, $36, $38, $36, $36, $34, $36, $34
	dc.b $34, $32, $34, $32, $32, $30, $32, $30

TwisterGfx
	include twister_gfx.asm


; Text graphics
; ------------------------------

	include Watch3_gfx.asm
	include classic_gfx.asm
	include boring_gfx.asm
	include effects_gfx.asm
	include Get1_gfx.asm
	include free_gfx.asm

	include RasterSplits_gfx.asm
	include SineWaves_gfx.asm
	include PlasmaBars_gfx.asm

