#!/usr/bin/python3

import sys
import argparse
import math
import struct

def gen_pb_colors():
	outfile = sys.stdout.buffer
	
	for c in range(5*16, 6*16, 2):
		for i in range(0, 4):
			outfile.write(struct.pack('B', round(c)))
	for c in range(6*16-2, 5*16-2, -2):
		for i in range(0, 4):
			outfile.write(struct.pack('B', round(c)))
	for c in range(6*16, 7*16, 2):
		for i in range(0, 4):
			outfile.write(struct.pack('B', round(c)))
	for c in range(7*16-2, 6*16-2, -2):
		for i in range(0, 4):
			outfile.write(struct.pack('B', round(c)))


if __name__ == "__main__":
	gen_pb_colors()

