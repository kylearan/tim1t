classic_1
	 dc.b 0, 124, 254, 198, 192, 192, 198, 254
	 dc.b 124, 0, 0, 0

classic_2
	 dc.b 0, 231, 239, 236, 239, 231, 224, 231
	 dc.b 231, 224, 224, 0

classic_3
	 dc.b 0, 239, 239, 96, 227, 239, 110, 239
	 dc.b 195, 0, 0, 0

classic_4
	 dc.b 0, 199, 247, 112, 243, 199, 6, 247
	 dc.b 243, 0, 0, 0

classic_5
	 dc.b 0, 231, 247, 55, 247, 231, 7, 247
	 dc.b 247, 0, 7, 7

classic_6
	 dc.b 0, 62, 127, 99, 96, 96, 99, 127
	 dc.b 62, 0, 0, 0

