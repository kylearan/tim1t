import Image
import sys
import os
import math

height = 64

background = (255, 255, 255)
foreground = (0, 0, 0)

img = Image.new("RGB", (16, height), "white")

for l in range(0, height):
	a = (l*2*math.pi)/height
	x1 = int(8+7.5*math.sin(a))
	x2 = int(8+7.5*math.sin(a + math.pi/2))
	x3 = int(8+7.5*math.sin(a + math.pi))
	x4 = int(8+7.5*math.sin(a + 3*math.pi/2))
	
	if x1 < x2:
		for x in range(x1, x2, 2):
			img.putpixel((x, l), (0, 0, 0))
	if x2 < x3:
		for x in range(x2, x3, 1):
			img.putpixel((x, l), (0, 0, 0))
#		img.putpixel((x2, l), (0, 0, 0))
#		img.putpixel((x3, l), (0, 0, 0))
	if x3 < x4:
		for x in range(x3, x4, 2):
			img.putpixel((x, l), (0, 0, 0))
	if x4 < x1:
		for x in range(x4, x1, 1):
			img.putpixel((x, l), (0, 0, 0))
#		img.putpixel((x4, l), (0, 0, 0))
#		img.putpixel((x1, l), (0, 0, 0))


img.save("twister_gfx.png")
