; =========================================================================
; Subroutines
; =========================================================================

; -------------------------------------------------------------------------
; Advance twister parameters
; -------------------------------------------------------------------------

FourAdvanceTwister SUBROUTINE
	
	lda FourNumFrame
	and #$7f
	tax
	lda TwisterSine,x
	sta FourCurrLineHi
	
	lda FourAddHi
	beq .hiZero
	lda #8
	bne .doAdder
.hiZero
	lda #4

.doAdder
	sta FourTmp

	; change twist
	lda FourTwisterDir
	bne .addDir
	
	; decrease adder
	lda FourAddLo
	sec
	sbc FourTmp
	sta FourAddLo
	lda FourAddHi
	sbc #0
	sta FourAddHi	
	bpl .advanceTwister
	; switch direction
	lda #0
	sta FourAddHi
	lda #1
	sta FourAddLo
	sta FourTwisterDir
	bne .advanceTwister	; unconditional
	
.addDir
	; increase adder
	lda FourAddLo
	clc
	adc FourTmp
	sta FourAddLo
	lda FourAddHi
	adc #0
	sta FourAddHi
	cmp #3
	bne .advanceTwister
	; switch direction
	lda #0
	sta FourTwisterDir
	lda #2
	sta FourAddHi
	lda #255
	sta FourAddLo

.advanceTwister
	lda FourCurrLineLo
	sta FourTmp
	lda FourCurrLineHi
	sta FourTmp2
	
	ldy #47
	clc
.loop
	lda FourTmp		; 3
	adc FourAddLo		; 3
	sta FourTmp		; 3
	lda FourTmp2		; 3
	adc FourAddHi		; 3
	sta FourTmp2		; 3
	and #31			; 2
	sta FourLineData,y	; 4

	dey			; 2
	bpl .loop		; 3

	RTB



; -------------------------------------------------------------------------
; Advance sine wave parameters
; -------------------------------------------------------------------------

FourAdvanceSineWaves SUBROUTINE
	tsx
	stx FourCurrentLine

	inc FourCurrentSWLine1
	dec FourCurrentSWLine2

	lda #0
	sta FourTmp2
	lda FourCurrentSWLine1
	tax
	clc
	adc #48
	sta FourLoopEnd
	ldy FourCurrentSWLine2
.loop
	lda SWSine1,x		; 4
	adc SWSine2,y		; 4
	and #$f0		; 2
	sta FourTmp		; 3
	lda SWSine1,x		; 4
	adc SWSine3,y		; 4
	lsr			; 2
	lsr			; 2
	lsr			; 2
	lsr			; 2
	ora FourTmp		; 3
	txs			; 2
	ldx FourTmp2		; 3
	sta FourLineData,x	; 4
	inc FourTmp2		; 5
	tsx			; 2
	iny			; 2
	inx			; 2
	cpx FourLoopEnd		; 3
	bne .loop		; 3

	ldx FourCurrentLine
	txs
	
	; correct starting pos
	lda FourLineData
	and #$f0
	clc
	adc FourNextHMove0
	and #$f0
	sta FourNextHMove0
	lda FourLineData
	asl
	asl
	asl
	asl
	clc
	adc FourNextHMove1
	and #$f0
	sta FourNextHMove1
	
	RTB
	
	

; -------------------------------------------------------------------------
; Advance raster splits parameters
; -------------------------------------------------------------------------

FourAdvanceRasterSplits SUBROUTINE
	inc FourRSIndex1
	inc FourRSIndex2
	inc FourRSIndex3
	inc FourRSIndex4
	ldx FourRSIndex1
	lda FourRSParabola,x
	sta FourRSPtr1
	ldx FourRSIndex2
	lda FourRSParabola,x
	sta FourRSPtr2
	ldx FourRSIndex3
	lda FourRSParabola,x
	sta FourRSPtr3
	ldx FourRSIndex4
	lda FourRSParabola,x
	sta FourRSPtr4
	RTB



; -------------------------------------------------------------------------
; Advance plasma bar parameters
; -------------------------------------------------------------------------

FourAdvancePlasmaBars SUBROUTINE
	lda FourPBLine1
	clc
	adc #1
	sta FourPBLine1
	lda FourPBLine2
	sec
	sbc #1
	sta FourPBLine2
	
	RTB



; -------------------------------------------------------------------------
; Four Slide kernel
; -------------------------------------------------------------------------

SupportFourFourSlide SUBROUTINE
	lda FourDemoState
	cmp #FOUR_FOUR_SLIDE_OUT
	bne .slideIn
	lda #FOUR_FOUR_SLIDE_IN_DUR
	sec
	sbc FourNumFrame
	sta FourNumFrame	
.slideIn

	lda #28		; floor(227/8)
	sec
	sbc FourNumFrame
	bcs .noUnderflow
	lda #0
	sta FourTmp
	beq .slideComplete
.noUnderflow
	asl
	asl
	asl
	sta FourTmp

	sta WSYNC
	cmp #3 - 1
	bcs .ge1
	lda #3
	sbc FourTmp
	tax
	bpl .upperGap

.ge1
	cmp #2 + 96 - 1
	bcs .ge2
	lda #2 + 96
	sbc FourTmp
	tax
	bpl .upperMinis
	
.ge2
	cmp #2 + 96 + 24 - 1
	bcs .ge3
	lda #2 + 96 + 24
	sbc FourTmp
	tax
	bpl .middleGap

.ge3
	cmp #2 + 96 + 24 + 97 - 1
	bcs .ge4
	lda #2 + 96 + 24 + 97
	sbc FourTmp
	tax
	bpl .lowerMinis

.ge4
	lda #2 + 96 + 24 + 97 + 8
	sbc FourTmp
	tax
	bpl .lowerGap

.slideComplete
	ldx #1
.upperGap
	WASTEX


	ldx #96
.upperMinis
	lda #$02
	sta COLUPF
	WASTEX

	ldx #24
.middleGap
	lda #0
	sta COLUPF
	WASTEX

	ldx #97
.lowerMinis
	dex
	lda #$02
	sta COLUPF
	WASTEX

	ldx #8
.lowerGap
	lda #0
	sta COLUPF
	WASTEX
	
	ldx FourTmp
	beq .end
	dex
	beq .end
.oldBackground
	lda FourDemoState
	cmp #FOUR_FOUR_SLIDE_IN
	beq .slideIn3
	lda #0
	beq .slideOut	; unconditional jmp
.slideIn3
	lda #FOUR_LABEL_BG
.slideOut
	sta COLUBK
	sta COLUPF
	WASTEX

.end
	lda FourDemoState
	cmp #FOUR_FOUR_SLIDE_OUT
	bne .slideIn2
	lda #FOUR_FOUR_SLIDE_IN_DUR
	sec
	sbc FourNumFrame
	sta FourNumFrame	
.slideIn2

	RTB



; =========================================================================
; Tables
; =========================================================================

FourRSParabola
	include RSSine.asm
	include RSParabola.asm

TwisterSine
	include TwisterSine_128_31.asm
	
	ALIGN $100
SWSine1
	include SWSine_32_1.asm
	include SWSine_32_1.asm
	include SWSine_32_1.asm
	include SWSine_32_1.asm
	include SWSine_64_1.asm
	include SWSine_64_1.asm

	ALIGN $100
SWSine2
	include SWSine_96_1.asm
	include SWSine_32_1.asm
	include SWSine_48_1.asm
	include SWSine_32_1.asm
	include SWSine_48_1.asm

	ALIGN $100
SWSine3
	include SWSine_64_1.asm
	include SWSine_32_1.asm
	include SWSine_16_1.asm
	include SWSine_64_1.asm
	include SWSine_32_1.asm
	include SWSine_48_1.asm
