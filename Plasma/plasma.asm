; =========================================================================
; Constants
; =========================================================================

PLSM_COLOR_LAST_PART	= $fe
PLSM_COLOR_NEXT_PART	= $2e

PLSM_FRAME_COLOR	= $b2

PLASMA_WIDTH		= 9
PLASMA_HEIGHT		= 11
RT_SPREAD_0		= 2
RT_SPREAD_1		= 5
SIN_SPREAD_Y		= 2

; Script state constants

PLSM_BEGIN		= 0
PLSM_FRAMES_SLIDEIN	= 1
PLSM_SQUARES_IN		= 2
PLSM_PLASMA_BLOCKS1	= 3
PLSM_PLASMA_BLOCKS2	= 4
PLSM_PLASMA_DELAY1	= 5
PLSM_PLASMA_I4_FIRST1	= 6
PLSM_PLASMA_I4_FIRST2	= 7
PLSM_PLASMA_DELAY2	= 8
PLSM_PLASMA_I31		= 9
PLSM_PLASMA_I32		= 10
PLSM_PLASMA_DELAY3	= 11
PLSM_PLASMA_I4_SECOND1	= 12
PLSM_PLASMA_I4_SECOND2	= 13
PLSM_SQUARES_OUT	= 14
PLSM_END_DELAY		= 15
PLSM_HANDOVER		= 16

; =========================================================================
; Start
; =========================================================================

PlasmaStart SUBROUTINE

; Init for this specific demo part.

	; Demo state
	lda #0
	sta PlsmNumFrame
	sta PlsmDemoState
	; init missile/ball registers
	lda #%00110000	; non-mirrored PF, ball size 8
	sta CTRLPF
	; optional, safe inits
	lda #0
	sta VDELP0
	sta VDELP1
	sta GRP1
	sta ENABL

	lda #0
	sta PaletteOffset
	lda #120
	sta PlasmaCnt0
	lda #70
	sta PlasmaCnt1
	
	jmp PlasmaIncoming
	
	
; =========================================================================
; Kernel
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

PlasmaKernel
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc PlsmNumFrame
	lda PlsmNumFrame
	ldx PlsmDemoState
	cmp PlsmScriptDurations,x 
	bne .noNextState
	inc PlsmDemoState
	lda #0
	sta PlsmNumFrame
	lda PlsmDemoState
	cmp #PLSM_HANDOVER
	bne .noNextPart
	JMB MorpherStart
.noNextPart
	cmp #PLSM_PLASMA_I4_FIRST1
	beq .incPlasma
	cmp #PLSM_PLASMA_I31
	beq .incPlasma
	cmp #PLSM_PLASMA_I4_SECOND1
	bne .noNextState
.incPlasma	
	lda PaletteOffset
	clc
	adc #64
	cmp #192	; last palette same as first
	bne .noPaletteReset
	lda #0
.noPaletteReset
	sta PaletteOffset
	lda PlasmaCnt1
	adc #37
	sta PlasmaCnt1
.noNextState
	
	; Time to compute the last 6 rows of plasma, or play music,
	; depending on state
	lda PlsmDemoState
	cmp #PLSM_FRAMES_SLIDEIN
	bne .doCalcPlasma
	JSB MusicPlayer
	jmp .afterCalcPlasma

.doCalcPlasma
	lda #1
	ldx #6
	jsr CalcPlasma
.afterCalcPlasma
	
PlasmaIncoming
	jsr PlasmaWaitForIntim
	lda #0
	sta VBLANK

; -------------------------------------------------------------------------
; Visible area: 228 lines
; -------------------------------------------------------------------------

; Upper screen frame
; ------------------------------

	lda PlsmDemoState
	cmp #PLSM_BEGIN
	bne .noBegin
.begin
	lda #PLSM_COLOR_LAST_PART
	sta COLUBK
	ldx #199
	jsr WasteLines
	
	lda #28*76/64 + 1
	sta TIM64T
	
	JSB MusicPlayer
	
	jsr PlasmaWaitForIntim
	
	jmp PlasmaDoVBlank
	
.noBegin
	cmp #PLSM_FRAMES_SLIDEIN
	bne UpperFrame
	
; Frames slide-in
; ------------------------------

SlideIn SUBROUTINE
	; Let upper and lower frame slide in and back to their final positions.
	; 228 scanlines means each frame has to cover 114 lines at max. To make
	; this routine easier, let this be 113 = $71 lines.
	
	; Compute height of upper frame
	lda PlsmNumFrame
	asl
	clc
	adc #93
	tax
	lda #113
	sec
	sbc Sin128,x
	sta PlsmTmp
	; Display upper frame
	tax

	; Determine and set color of middle part
	ldy #PLSM_COLOR_LAST_PART
	lda PlsmNumFrame
	cmp #48
	bcc .storeColor
	ldy #0
.storeColor
	jsr WasteLines
	sty COLUBK

	; Compute and display middle part
	lda #114	; 228/2, half of visible scanlines
	sec
	sbc PlsmTmp
	asl
	tax
	dex
	jsr WasteLines
	lda #PLSM_FRAME_COLOR
	sta COLUBK

	; display lower frame
	ldx PlsmTmp
	jsr WasteLines
	
	; check for end of part
	lda PlsmNumFrame
	cmp #84	
	bne .noNextEvent
	inc PlsmDemoState
	lda #0
	sta PlsmNumFrame
.noNextEvent
	sta WSYNC
	jmp PlasmaDoVBlank

; Display upper frame
; ------------------------------

UpperFrame SUBROUTINE
;	lda #44*76/64 + 1
;	sta TIM64T

	lda #8*76/64 + 1
	sta TIM64T
	JSB MusicPlayer
.waitAfterMusic
	lda INTIM
	bne .waitAfterMusic
	sta WSYNC

	lda #36*76/64 + 1
	sta TIM64T

	lda PlsmDemoState
	cmp #PLSM_END_DELAY
	beq .noSquares
	cmp #PLSM_SQUARES_IN
	bcc .noSquares
	cmp #PLSM_SQUARES_OUT + 1
	bcc .squares
	
	; No squares: Waste lines
.noSquares
	ldx #20; 28
	jsr WasteLines
	jmp .afterSquares

; Display upper Squares
; ------------------------------

.squares
	; squares setup
	lda #PLSM_FRAME_COLOR - 2
	sta COLUP0
	lda #PLSM_FRAME_COLOR + 2
	sta COLUP1
	sta HMCLR
	jsr DetermineNusiz
	sta PlsmTmp
	; waste some lines to center squares
	ldx #2;	10
	jsr WasteLines
	; init position sides (missiles)
	sta NUSIZ0
	sta NUSIZ1
	SLEEP 9
	sta RESM0
	sta RESM1
	lda #NO_MO_74
	sta HMM0
	lda #LEFT74_2
	sta HMM1
	;SLEEP 30	; for a cycle 74 HMOVE
	jsr PlasmaRts
	jsr PlasmaRts
	SLEEP 6
	sta HMOVE

	; top
	sta WSYNC
	;SLEEP 25
	jsr PlasmaRts
	jsr PlasmaRts	
	sta.w RESP0
	
	sta WSYNC
	lda #%11111100
	sta GRP0

	; sides
	sta WSYNC
	lda #0
	sta GRP0
	lda #%00000010
	sta ENAM0
	sta ENAM1
	ldx #11
	jsr WasteLines

	; bottom
	;SLEEP 15	; 4 cycles wasted already by dex/bne above
	jsr PlasmaRts
	SLEEP 3
	STA RESP1
	
	sta WSYNC
	lda #0
	sta ENAM0
	sta ENAM1
	lda #%11111100
	sta GRP1
	
	sta WSYNC
	lda #0
	sta GRP1

.afterSquares
	ldx #15		; waste lines to center squares
	jsr WasteLines
	lda #0
	sta COLUBK
	sta COLUPF
	sta COLUP0
	sta COLUP1
	
.upperFrame
	jsr PlasmaWaitForIntim


; ------------------------------
; Display plasma
; ------------------------------
	
DisplayPlasma SUBROUTINE
	lda PlsmDemoState
	cmp #PLSM_PLASMA_BLOCKS1
	bcc .noPlasma
	cmp #PLSM_PLASMA_I4_SECOND2 + 1
	bcs .noPlasma
	sec
	sbc #PLSM_PLASMA_BLOCKS1
	tax
	lda .plasmaTargetsLo,x
	sta Sine0
	lda .plasmaTargetsHi,x
	sta Sine1
	jmp (Sine0)
	
.plasmaTargetsLo
	dc.b <.blocks, <.blocks, <.noPlasma
	dc.b <.i4First, <.i4First, <.noPlasma
	dc.b <.i3, <.i3, <.noPlasma
	dc.b <.i4Second, <.i4Second

.plasmaTargetsHi
	dc.b >.blocks, >.blocks, >.noPlasma
	dc.b >.i4First, >.i4First, >.noPlasma
	dc.b >.i3, >.i3, >.noPlasma
	dc.b >.i4Second, >.i4Second

; No plasma
; ------------------------------
	
	; Demo state says no plasma yet, so waste lines
.noPlasma
	ldx #134
	jsr WasteLines
	jmp .afterPlasma

; Blocks plasma
; ------------------------------

.blocks
	jsr InitPlasma
	; calc jmp point for slide-in
	lda #<.blockJsrs
	sta Sine0
	lda #>.blockJsrs
	sta Sine1
	lda PlsmDemoState
	cmp #PLSM_PLASMA_BLOCKS1
	bne .blocksNoSlideIn
	lda PlsmNumFrame
	cmp #51
	bcs .blocksNoSlide
	asl
	tay
	lda Sin128+83,y
	jmp .calcJmp

.blocksNoSlideIn
	; check for slide out
	lda #200
	sec
	sbc PlsmNumFrame
	cmp #51
	bcs .blocksNoSlide
	asl
	tay
	lda Sin128+83,y
	jmp .calcJmp
.blocksNoSlide
	lda #0
.calcJmp
	jsr CalcPlasmaJmp	
.blocksJmpIn
	ldx #5
	jsr WasteLines
	lda #%0000010
	sta VBLANK
	; SLEEP 50
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	SLEEP 2
	jmp (Sine0)
.blockJsrs
	include "plasmajsrs_blocks.asm"
	jsr SwitchOffPlasma
	lda RtSine
	clc
	adc #4
	tax
	jsr WasteLines
	jmp .afterPlasma
	
; First interleaved 4 plasma
; ------------------------------

.i4First
	jsr InitPlasma

	; calc jmp point for slide-in
	lda #<PlasmaJsrsI4
	sta Sine0
	lda #>PlasmaJsrsI4
	sta Sine1
	lda #>(.i4FirstReturn-1)
	pha
	lda #<(.i4FirstReturn-1)
	pha	
	lda PlsmDemoState
	cmp #PLSM_PLASMA_I4_FIRST1
	bne .i4FirstNoSlideIn
	lda PlsmNumFrame
	cmp #59
	bcs .i4FirstNoSlide
	asl
	tay
	lda Sin128+67,y
	jmp .i4FirstcalcJmp

.i4FirstNoSlideIn
	; check for slide out
	lda #250
	sec
	sbc PlsmNumFrame
	cmp #59
	bcs .i4FirstNoSlide
	asl
	tay
	lda Sin128+67,y
	jmp .i4FirstcalcJmp
.i4FirstNoSlide
	lda #0
.i4FirstcalcJmp
	jsr CalcPlasmaJmp	
.i4FirstJmpIn
	sta WSYNC
	lda #%0000010
	sta VBLANK
	; SLEEP 60
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	jmp (Sine0)
	
.i4FirstReturn
	jsr SwitchOffPlasma
	ldx RtSine
	inx
	jsr WasteLines
	jmp .afterPlasma

; Interleaved 3 plasma
; ------------------------------

.i3
	jsr InitPlasma
	; calc jmp point for slide-in
	lda #<.i3Jsrs
	sta Sine0
	lda #>.i3Jsrs
	sta Sine1
	lda PlsmDemoState
	cmp #PLSM_PLASMA_I31
	bne .i3NoSlideIn
	lda PlsmNumFrame
	cmp #51
	bcs .i3NoSlide
	asl
	tay
	lda Sin128+97,y
	jmp .i3CalcJmp

.i3NoSlideIn
	; check for slide out
	lda #250
	sec
	sbc PlsmNumFrame
	cmp #51
	bcs .i3NoSlide
	asl
	tay
	lda Sin128+97,y
	jmp .i3CalcJmp
.i3NoSlide
	lda #0
.i3CalcJmp
	jsr CalcPlasmaJmp	
.i3JmpIn
	ldx #11
	jsr WasteLines
	lda #%0000010
	sta VBLANK
	;SLEEP 50
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	SLEEP 2
	jmp (Sine0)
.i3Jsrs
	include "plasmajsrs_interleaved3.asm"
	jsr SwitchOffPlasma
	lda RtSine
	clc
	adc #11
	tax
	jsr WasteLines
	jmp .afterPlasma

; Second interleaved 4 plasma
; ------------------------------

.i4Second
	jsr InitPlasma
	; calc jmp point for slide-in
	lda #<PlasmaJsrsI4
	sta Sine0
	lda #>PlasmaJsrsI4
	sta Sine1
	lda #>(.i4SecondReturn-1)
	pha
	lda #<(.i4SecondReturn-1)
	pha	
	lda PlsmDemoState
	cmp #PLSM_PLASMA_I4_SECOND1
	bne .i4SecondNoSlideIn
	lda PlsmNumFrame
	cmp #59
	bcs .i4SecondNoSlide
	asl
	tay
	lda Sin128+67,y
	jmp .i4SecondcalcJmp

.i4SecondNoSlideIn
	; check for slide out
	lda #253
	sec
	sbc PlsmNumFrame
	cmp #59
	bcs .i4SecondNoSlide
	asl
	tay
	lda Sin128+67,y
	jmp .i4SecondcalcJmp
.i4SecondNoSlide
	lda #0
.i4SecondcalcJmp
	jsr CalcPlasmaJmp	
.i4SecondJmpIn
	sta WSYNC
	lda #%0000010
	sta VBLANK
	;SLEEP 60
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	jmp (Sine0)
.i4SecondReturn
	jsr SwitchOffPlasma
	ldx RtSine
	inx
	jsr WasteLines
	
.afterPlasma
	

; Lower screen frame
; ------------------------------

LowerFrame SUBROUTINE

	lda #48*76/64 + 1
	sta TIM64T

	ldx #8		; Black gap between plasma area and lower frame
	jsr WasteLines
	lda #PLSM_FRAME_COLOR
	sta COLUBK

	lda PlsmDemoState
	cmp #PLSM_SQUARES_IN
	bcc .noSquares
	cmp #PLSM_SQUARES_OUT + 1
	bcc .squares
	
.noSquares
	jmp .afterSquares

; Display lower Squares
; ------------------------------

.squares
	; squares setup
	lda #PLSM_FRAME_COLOR - 2
	sta COLUP0
	lda #PLSM_FRAME_COLOR + 2
	sta COLUP1
	sta HMCLR
	jsr DetermineNusiz
	sta PlsmTmp
	
	; waste some lines to center squares
	ldx #10
	jsr WasteLines
	
	; init position sides (missiles)
	lda PlsmTmp
	sta NUSIZ0
	sta NUSIZ1
	;SLEEP 31
	jsr PlasmaRts
	jsr PlasmaRts
	SLEEP 7
	sta RESM0
	sta RESM1
	lda #NO_MO_74
	sta HMM0
	lda #LEFT74_2
	sta HMM1
	SLEEP 5		; for a cycle 74 HMOVE
	sta HMOVE

	; top
	sta WSYNC
	; SLEEP 50
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	SLEEP 2
	sta RESP0
	
	sta WSYNC
	lda #%11111100
	sta GRP0

	; sides
	sta WSYNC
	lda #0
	sta GRP0
	lda #%00000010
	sta ENAM0
	sta ENAM1
	ldx #11
	jsr WasteLines

	; bottom
	;SLEEP 40
	jsr PlasmaRts
	jsr PlasmaRts
	jsr PlasmaRts
	SLEEP 4
	STA RESP1
	
	sta WSYNC
	lda #0
	sta ENAM0
	sta ENAM1
	lda #%11111100
	sta GRP1
	
	sta WSYNC
	lda #0
	sta GRP1

.afterSquares
	jsr PlasmaWaitForIntim


; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

PlasmaDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T

	; That's enough time for computing 5 rows of plasma
	lda #0
	ldx #5
	jsr CalcPlasma
	
.OverscanLoop
	lda INTIM
	bne .OverscanLoop

	jmp PlasmaKernel

	
; =========================================================================
; Subroutines
; =========================================================================

; Waste X scanlines.
; -------------------------------------------------------------------------

WasteLines
	sta WSYNC
	dex
	bne WasteLines
PlasmaRts
	rts
	

; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

PlasmaWaitForIntim
	lda INTIM
	bne PlasmaWaitForIntim
	sta WSYNC
	rts
	

; Calc jmp into plasma jsrs.
; IN:
; acc: Number of lines to skip.
; Sine0/Sine0+1: Address of first jsr.
; OUT:
; Rtsine: Number of lines to skip afterwards.
; Sine0/Sine0+1: Jmp vector
; -------------------------------------------------------------------------

CalcPlasmaJmp SUBROUTINE
	sta RtSine
	; mul 3 (size of jsr xxxx)
	ldx #3
	lda Sine0
.mulLoop
	clc
	adc RtSine
	bcc .noOverflow
	inc Sine1
.noOverflow
	dex
	bne .mulLoop
	sta Sine0
	rts

	
; Init plasma display.
; -------------------------------------------------------------------------

InitPlasma SUBROUTINE
	; position missiles and set PF
	sta HMCLR
	sta WSYNC
	lda #%00110010	; missile size 8, two copied medium
	sta NUSIZ0
	sta NUSIZ1
	lda #%00110000
	sta PF0
	lda #%00000000
	sta PF1
	lda #%00000011
	sta PF2
	lda #0
	sta GRP0
	SLEEP 9
	sta RESM0
	sta RESM1
	; hmove and enable missiles
	lda #$10
	sta HMM1
	sta WSYNC
	sta HMOVE
	lda #%00000010
	sta ENAM0
	sta ENAM1
	rts


PlasmaJsrsI4 SUBROUTINE
	include "plasmajsrs_interleaved4.asm"
	rts
	
	
; Switch off plasma display.
; -------------------------------------------------------------------------

SwitchOffPlasma SUBROUTINE
	lda #0
	sta COLUBK
	sta PF0
	sta PF2
	sta ENAM0
	sta ENAM1	
	sta VBLANK	; re-enable display
	rts
	
	
; Determine number of squares according to demo state.
; Out: NUSIZx value in acc
; Trashes X
; -------------------------------------------------------------------------

DetermineNusiz SUBROUTINE
	lda PlsmDemoState
	cmp #PLSM_SQUARES_IN
	bne .noIn
	; Number is determines by frame counter; 16 frames per square
	lda PlsmNumFrame
	lsr
	lsr
	lsr
	lsr
	tax
	lda .nusizValuesIn,x
	rts
	
.noIn	cmp #PLSM_SQUARES_OUT
	bne .noOut
	; Number is determines by frame counter; 16 frames per square
	lda PlsmNumFrame
	lsr
	lsr
	lsr
	lsr
	tax
	lda .nusizValuesOut,x
	rts
	
.noOut	lda #%00000011
	SLEEP 12	; compensate for instructions in other branches
	rts
	
.nusizValuesIn
	dc.b %00000000, %00000001, %00000011, %00000011
.nusizValuesOut
	dc.b %00000011, %00000001, %00000000, %00000000


	
; Calculate parts of plasma cell values.
; In:
; Acc: 0 = Start, 1 = Continue
; X: Number of rows to calc this pass
; -------------------------------------------------------------------------

CalcPlasma SUBROUTINE
	stx RowsLeft
	tsx
	stx PlsmTmp
	cmp #0
	beq .newStart

	;Restore vars from last pass to continue
	ldx CurrentCell
	txs
	jmp .yLoop
		
.newStart
	;Init vars for new calc
	ldx #CellMem + PLASMA_WIDTH*PLASMA_HEIGHT - 1
	txs
	
	inc PlasmaCnt0
	dec PlasmaCnt1
	
	lda #0
	sta Sine0
	sta Sine1
	sta RtSine
	sta Color
	
	lda #PLASMA_HEIGHT - 1
	sta YPos
	
.yLoop
	lda PlasmaCnt0
	adc Sine0
	tax
	lda PlasmaCnt1
	adc Sine1
	tay
	clc
	lda Sin128,x
	adc Sin64,y
	sta LineOffset

	lda Sine0
	adc #RT_SPREAD_0
	sta Sine0
	lda Sine1
	adc #RT_SPREAD_1
	sta Sine1
	
	ldx #PLASMA_WIDTH-1
.xLoop
	lda SinOffsets,x
	adc LineOffset
	adc RtSine
	tay
	txa
	adc Sin64,y
	adc PlasmaCnt0
	adc Color
	and #$3f
	adc PaletteOffset
	tay
	lda ColorTable,y
	pha
	dex
	bpl .xLoop
	
	lda RtSine
	adc #SIN_SPREAD_Y
	sta RtSine
	inc Color
	dec YPos
	dec RowsLeft
	bne .yLoop
	
	; Store vars for potential next pass
	tsx
	stx CurrentCell
	
	; Restore stack pointer
	ldx PlsmTmp
	txs

	; If this was last pass, do color correction for VBLANK
	lda YPos
	cmp #$ff
	beq .correctColors
	rts
	
.correctColors
	; 5th and 9th cell of each row have bit 1 set or unset
	; so that it can be used to turn VBLANK on or off.
	ldx #%11111101
	ldy #0
	tya
	clc
.correctLoop
	tay
	lda CellMem+4,y
	sax CellMem+4,y
	lda CellMem+8,y
	ora #%00000010
	sta CellMem+8,y
	tya
	adc #9
	cmp #9*11
	bne .correctLoop	
	rts


; Subroutines to display individual plasma rows.
; Each subroutine expects to start at the beginning of a scanline.
; Needs 70 cycles so that a jsr to the next line completes a full scanline.
; 5th address must have bit 1 cleared, 9th address must have bit 1 set for
; VBLANK to work.
; -------------------------------------------------------------------------

	include "plasmarows.asm"

; =========================================================================
; Tables
; =========================================================================

; Script event durations
; ------------------------------

PlsmScriptDurations
	dc.b 20		; PLSM_BEGIN
	dc.b 255	; PLSM_FRAMES_SLIDEIN
	dc.b 5*16	; PLSM_SQUARES_IN
	dc.b 200	; PLSM_PLASMA_BLOCKS1
	dc.b 200	; PLSM_PLASMA_BLOCKS2
	dc.b 30		; PLSM_PLASMA_DELAY1
	dc.b 250	; PLSM_PLASMA_I4_FIRST1
	dc.b 250	; PLSM_PLASMA_I4_FIRST2
	dc.b 30		; PLSM_PLASMA_DELAY2
	dc.b 250	; PLSM_PLASMA_I31
	dc.b 250	; PLSM_PLASMA_I32
	dc.b 30		; PLSM_PLASMA_DELAY3
	dc.b 254	; PLSM_PLASMA_I4_SECOND1
	dc.b 254	; PLSM_PLASMA_I4_SECOND2
	dc.b 3*16	; PLSM_SQUARES_OUT
	dc.b 50		; PLSM_END_DELAY
	dc.b 255	; PLSM_HANDOVER


; Sine tables
; ------------------------------

SinOffsets
	dc.b 0, 3, 6, 9, 12, 15, 18, 21, 24, 27
	dc.b 30, 33, 36, 39, 42, 45, 48, 51, 54, 57
	dc.b 60, 63, 66, 69, 72, 75, 78, 81, 84, 87
	dc.b 90, 93, 96, 99, 102, 105, 108, 111, 114, 117

	ALIGN $100
Sin64
	include "sin64.asm"
Sin128
	include "sin128.asm"

; Color tables
; ------------------------------

	ALIGN $100
ColorTable
	; Similar to Rastersplits/Coine
	dc.b $64, $64, $66, $66
	dc.b $46, $46, $48, $48, $2a, $2a, $2c, $2c
	dc.b $3c, $3c, $3e, $3e, $0e, $0e, $ee, $ee
	dc.b $7e, $7e, $7c, $7c, $be, $be, $bc, $bc
	dc.b $ba, $ba, $b8, $b8, $de, $de, $dc, $dc
	dc.b $da, $da, $d8, $d8, $ae, $ae, $ac, $ac
	dc.b $aa, $aa, $a8, $a8, $8e, $8e, $8c, $8c
	dc.b $8a, $8a, $88, $88, $86, $86, $84, $84
	dc.b $82, $82, $80, $80

	dc.b $60, $60, $60, $60, $60, $62, $60, $62
	dc.b $62, $44, $62, $44, $44, $46, $44, $46
	dc.b $46, $48, $46, $48, $48, $2a, $48, $2a
	dc.b $2a, $2e, $2a, $2e, $2e, $0e, $2c, $0e
	dc.b $2c, $4a, $2c, $4a, $4a, $68, $4a, $68
	dc.b $68, $66, $68, $66, $66, $66, $66, $66
	dc.b $66, $86, $66, $86, $86, $a4, $86, $a4
	dc.b $a4, $c2, $a4, $c2, $c2, $c0, $c2, $c0


	dc.b $90, $92, $90, $92, $92, $94, $92, $94
	dc.b $94, $76, $94, $76, $76, $78, $76, $78
	dc.b $78, $78, $78, $78, $78, $5c, $78, $5c
	dc.b $5c, $5e, $5c, $5e
	dc.b $5e, $0e, $5e, $0e, $0e, $5e, $0e, $5e
	dc.b $5e, $5a, $5e, $5a, $5a, $38, $5a, $38
	dc.b $38, $36, $38, $36, $36, $24, $36, $24
	dc.b $24, $22, $24, $22, $22, $40, $22, $40
	dc.b $40, $60, $40, $60

	;dc.b $60, $60, $60, $60, $60, $62, $60, $62
	;dc.b $62, $44, $62, $44, $44, $46, $44, $46
	;dc.b $46, $48, $46, $48, $48, $2a, $48, $2a
	;dc.b $2a, $2e, $2a, $2e
	;dc.b $2c, $4a, $2c, $4a, $4a, $68, $4a, $68
	;dc.b $68, $66, $68, $66, $66, $66, $66, $66
	;dc.b $66, $86, $66, $86, $86, $a4, $86, $a4
	;dc.b $a4, $c2, $a4, $c2

	; Pink/Blue/Violet
	;dc.b $60, $62, $64, $64, $66, $68, $68, $6a
	;dc.b $6c, $6e, $ee, $6e, $6c, $6a, $6a, $68
	;dc.b $66, $64, $62, $62, $60
	;dc.b $d0, $d2, $d2, $d4, $d6, $d6, $d8, $da
	;dc.b $dc, $de, $ee, $de, $dc, $da, $da, $d8
	;dc.b $d6, $d4, $d2, $d2, $d0
	;dc.b $50, $52, $54, $54, $56, $58, $58, $5a
	;dc.b $5c, $5e, $ee, $5e, $5c, $5a, $5a, $58
	;dc.b $56, $54, $54, $52, $50, $50

	
	;; Cyan
	;dc.b $70, $70, $72, $72, $74, $74, $76, $76
	;dc.b $78, $78, $7a, $7a, $7c, $7c, $7e, $7e
	;dc.b $0e, $0e, $7e, $7e, $7c, $7c, $7a, $7a
	;dc.b $78, $78, $76, $76, $74, $74, $72, $72
	;; Orange
	;dc.b $20, $20, $22, $22, $24, $24, $26, $26
	;dc.b $28, $28, $2a, $2a, $2c, $2c, $2e, $2e
	;dc.b $ee, $ee, $2e, $2e, $2c, $2c, $2a, $2a
	;dc.b $28, $28, $26, $26, $24, $24, $22, $22

