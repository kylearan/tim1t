PlasmaRow1
	lda $82
	sta COLUBK
	lda $83
	sta COLUPF
	lda $84
	sta COLUP0
	lda $85
	sta COLUP1
	lda $86
	ldx $87
	ldy $88
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $89
	sta COLUP1
	lda $8a
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow2
	lda $8b
	sta COLUBK
	lda $8c
	sta COLUPF
	lda $8d
	sta COLUP0
	lda $8e
	sta COLUP1
	lda $8f
	ldx $90
	ldy $91
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $92
	sta COLUP1
	lda $93
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow3
	lda $94
	sta COLUBK
	lda $95
	sta COLUPF
	lda $96
	sta COLUP0
	lda $97
	sta COLUP1
	lda $98
	ldx $99
	ldy $9a
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $9b
	sta COLUP1
	lda $9c
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow4
	lda $9d
	sta COLUBK
	lda $9e
	sta COLUPF
	lda $9f
	sta COLUP0
	lda $a0
	sta COLUP1
	lda $a1
	ldx $a2
	ldy $a3
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $a4
	sta COLUP1
	lda $a5
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow5
	lda $a6
	sta COLUBK
	lda $a7
	sta COLUPF
	lda $a8
	sta COLUP0
	lda $a9
	sta COLUP1
	lda $aa
	ldx $ab
	ldy $ac
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $ad
	sta COLUP1
	lda $ae
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow6
	lda $af
	sta COLUBK
	lda $b0
	sta COLUPF
	lda $b1
	sta COLUP0
	lda $b2
	sta COLUP1
	lda $b3
	ldx $b4
	ldy $b5
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $b6
	sta COLUP1
	lda $b7
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow7
	lda $b8
	sta COLUBK
	lda $b9
	sta COLUPF
	lda $ba
	sta COLUP0
	lda $bb
	sta COLUP1
	lda $bc
	ldx $bd
	ldy $be
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $bf
	sta COLUP1
	lda $c0
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow8
	lda $c1
	sta COLUBK
	lda $c2
	sta COLUPF
	lda $c3
	sta COLUP0
	lda $c4
	sta COLUP1
	lda $c5
	ldx $c6
	ldy $c7
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $c8
	sta COLUP1
	lda $c9
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow9
	lda $ca
	sta COLUBK
	lda $cb
	sta COLUPF
	lda $cc
	sta COLUP0
	lda $cd
	sta COLUP1
	lda $ce
	ldx $cf
	ldy $d0
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $d1
	sta COLUP1
	lda $d2
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow10
	lda $d3
	sta COLUBK
	lda $d4
	sta COLUPF
	lda $d5
	sta COLUP0
	lda $d6
	sta COLUP1
	lda $d7
	ldx $d8
	ldy $d9
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $da
	sta COLUP1
	lda $db
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

PlasmaRow11
	lda $dc
	sta COLUBK
	lda $dd
	sta COLUPF
	lda $de
	sta COLUP0
	lda $df
	sta COLUP1
	lda $e0
	ldx $e1
	ldy $e2
	sta VBLANK
	sta COLUBK
	stx COLUPF
	sty COLUP0
	lda $e3
	sta COLUP1
	lda $e4
	sta COLUBK
	sta VBLANK
	SLEEP 4
	rts

