#!/usr/bin/python3

import sys

ROWS = 11
CELLADDR = 0x82

addr = CELLADDR

def hexaddr():
	global addr
	haddr = "$"+hex(addr)[2:]
	addr += 1
	return haddr

for col in range(ROWS):
	print("PlasmaRow%d" % (col + 1))
	print("\
	lda " + hexaddr() + "\n\
	sta COLUBK\n\
	lda " + hexaddr() + "\n\
	sta COLUPF\n\
	lda " + hexaddr() + "\n\
	sta COLUP0\n\
	lda " + hexaddr() + "\n\
	sta COLUP1\n\
	lda " + hexaddr() + "\n\
	ldx " + hexaddr() + "\n\
	ldy " + hexaddr() + "\n\
	sta VBLANK\n\
	sta COLUBK\n\
	stx COLUPF\n\
	sty COLUP0\n\
	lda " + hexaddr() + "\n\
	sta COLUP1\n\
	lda " + hexaddr() + "\n\
	sta COLUBK\n\
	sta VBLANK\n\
	SLEEP 4\n\
	rts\n\
")
