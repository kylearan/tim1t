; TODO: FIXME:
; Fix: Put Music into most plasma parts!
; Fix Beginning: Shutters should reveal CLUSTER earlier, not plate first
; Fix jerkiness in upscroller (start in gap or in char doesn't advance once)

	processor 6502
	include vcs.h
	include macro.h
	include HMxx.h

; Bankswitching setup
BS_DEBUG  = 1             ; Enable bankswitching debug statements during compilation.
BS_Scheme = $F4           ; Select the 32K ROM $F4 bankswitching scheme.
	include ARPM_Bankswitch.h


	incdir "Intro"
	incdir "Starfield"
	incdir "Plasma"
	incdir "4in1"
	incdir "Upscroll"
	incdir "Chessboards"
	incdir "End"

	incdir "Music"


; =========================================================================
; Bank 0
; =========================================================================

	BS_START_BANK 0

BS_Begin
	CLEAN_START

	JMB ZoomerStart
;	JMB BeginningStart
;	JMB BackZoomerStart
;	JMB IntroStart
;	JMB StarfieldStart
;	JMB PlasmaStart
;	JMB FourStart
;	JMB UpscrollStart
;	JMB ChessStart
;	JMB EndStart


	include "Intro.asm"
	
	include "Starfield_projection.asm"

	BS_END_BANK 0

; =========================================================================
; Bank 1
; =========================================================================

	BS_START_BANK 1

	include "Starfield.asm"

	include "End.asm"

	include "Chessboards_support.asm"

	BS_END_BANK 1

; =========================================================================
; Bank 2
; =========================================================================

	BS_START_BANK 2

	include "plasma.asm"

	BS_END_BANK 2

; =========================================================================
; Bank 3
; =========================================================================

	BS_START_BANK 3

	include "Upscroll.asm"

	BS_END_BANK 3

; =========================================================================
; Bank 4
; =========================================================================

	BS_START_BANK 4

	include "Beginning.asm"

	include "Zoomer.asm"
	
	BS_END_BANK 4

; =========================================================================
; Bank 5
; =========================================================================

	BS_START_BANK 5

	include "FourInOne.asm"

	BS_END_BANK 5

; =========================================================================
; Bank 6
; =========================================================================

	BS_START_BANK 6

	include "FourInOne_support.asm"
	
	include "Chessboards.asm"

	BS_END_BANK 6

; =========================================================================
; Bank 7: Music
; =========================================================================

	BS_START_BANK 7

	include "player.asm"

;MusicPlayer
	;ldx #14
	;lda #0
;.trash
	;sta WSYNC
	;sta $ed,x
	;dex
	;bpl .trash
	
	;RTB

	BS_END_BANK 7

; =========================================================================
; Vars and RAM definitions
; =========================================================================

	include "vars.asm"
