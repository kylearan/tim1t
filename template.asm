	processor 6502
	include vcs.h
	include macro.h
	include HMxx.h

; =========================================================================
; Constants
; =========================================================================


; Script state constants
	SEG.U STATE_LABELS
	ORG 0


; =========================================================================
; Vars
; =========================================================================

	SEG.U VARS
	ORG $80
NumFrame	ds 1	; Frame counter
DemoState	ds 1	; Current demo/effect state

	; CellMem must start at $82 for auto-generated plasma row display .asm

	echo "----",($100 - *) , "bytes of RAM left"

	SEG CODE
	org $F000

	
; =========================================================================
; Start
; =========================================================================

Start

; Init that is only needed for the stand-alone version.
; Has to be removed / moved to Main demo ROM.

	CLEAN_START

	; optional, safe inits
	lda #0
	sta VDELP0
	sta VDELP1
	sta GRP1
	sta ENABL

; Init for this specific demo part.

	; Demo state
	lda #0
	sta NumFrame
	sta DemoState

	
; =========================================================================
; Kernal
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

Kernal SUBROUTINE
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc NumFrame
	lda NumFrame
	ldx DemoState
	cmp XXXScriptDurations,x 
	bne .noNextState
	inc DemoState
	lda #0
	sta NumFrame
.noNextState
	
	; VBLANK CODE GOES HERE
	jsr WaitForIntim
	lda #0
	sta VBLANK

; -------------------------------------------------------------------------
; Visible area: 228 lines
; -------------------------------------------------------------------------

	; VISIBLE CODE GOES HERE
	
; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

DoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T

	; VBLANK CODE GOES HERE
	
.OverscanLoop
	lda INTIM
	bne .OverscanLoop

	jmp Kernal

	
; =========================================================================
; Subroutines
; =========================================================================

; Waste X scanlines.
; -------------------------------------------------------------------------

WasteLines
	sta WSYNC
	dex
	bne WasteLines
	rts
	

; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

WaitForIntim
	lda INTIM
	bne WaitForIntim
	sta WSYNC
	rts
	

; =========================================================================
; Tables
; =========================================================================

; Script event durations
; ------------------------------

XXXScriptDurations
	dc.b 50		; PLSM_BEGIN


; =========================================================================
; Adresses
; =========================================================================

	echo "----",($10000 - *) , "bytes of ROM left"

 	org $FFFA
	.word	Start
	.word	Start
	.word	Start
