; section SongSeq0
;  
;  
;  
;  var ns0=0x04,ns1;				// 00xx 0xxx   Number-Size player/missle 0/1
;  								//		 ^^^	- Player-Missile number & Player size (See table below)
;  								//		 000		  0  One copy              (X.........)
;  								//		 001		  1  Two copies - close    (X.X.......)
;  								//		 010		  2  Two copies - medium   (X...X.....)
;  								//		 011		  3  Three copies - close  (X.X.X.....)
;  								//		 100		  4  Two copies - wide     (X.......X.)
;  								//		 101		  5  Double sized player   (XX........)
;  								//		 110		  6  Three copies - medium (X...X...X.)
;  								//		 111		  7  Quad sized player     (XXXX......)
;  								//	 ^^			- Missile Size  (0..3 = 1,2,4,8 pixels width)
;  
;  var cp0=0x06,cp1,cpf,cbg;		// xxxx xxx-   Color-Luminance Player 0/1, Playfield, Background
;  var ctpf=0x0A;					// --xx -xxx   Control Playfield, Ball, Collisions
;  								//		   ^	- Playfield Reflection     (0=Normal, 1=Mirror right half)
;  								//		  ^		- Playfield Color          (0=Normal, 1=Score Mode, only if Bit2=0)
;  								//		 ^		- Playfield/Ball Priority  (0=Normal, 1=Above Players/Missiles)
;  								//	 ^^			- Ball size                (0..3 = 1,2,4,8 pixels width)
;  
;  var rep0=0x0B, rep1;			// ---- x---   Reflection Player 0/1
;  var pf0=0x0D, pf1, pf2;			// xxxx ----   Playfield Register Byte 0
;  								// xxxx xxxx   Playfield Register Byte 1
;  								// xxxx xxxx   Playfield Register Byte 2
;  var rp0=0x10,rp1,rm0,rm1,rb;	// ---- ----   Reset Player 0/1, Missile 0/1, Ball
;  
;  var gp0=0x1B,gp1;				// xxxx xxxx   Graphics Register Player 0/1
;  var em0=0x1D,em1,eb;			// ---- --x-   Graphics Enable Missle 0/1, Ball
;  var hp0=0x20,hp1,hm0,hm1,hb;	// xxxx ----   Horizontal Motion Player 0/1, Missile 0/1, Ball
;  var vdp0=0x25,vdp1,vdb;			// ---- ---x   Vertical Delay Player 0/1, Ball
;  var rmp0=0x28,rmp1;				// ---- --x-   Reset Missle 0/1 to Player 0/1
;  var hmove=0x2A;					// ---- ----   Apply Horizontal Motion
;  var hmclr=0x2B;					// ---- ----   Clear Horizontal Move Registers
;  
;  var gp0h=0x11B,gp1h;			// xxxx xxxx   Graphics Register Player 0/1
;  var rmp0h=0x128,rmp1h;			// ---- --x-   Reset Missle 0/1 to Player 0/1
;  var pf0h=0x10D, pf1h, pf2h;		// xxxx ----   Playfield Register Byte 0
;  var cp0h=0x106,cp1h,cpfh,cbgh;	// xxxx xxx-   Color-Luminance Player 0/1, Playfield, Background
;  var hp0h=0x120,hp1h,hm0h,hm1h,hbh;	// xxxx ----   Horizontal Motion Player 0/1, Missile 0/1, Ball
;  
;  var AUDC0=0x15,AUDC1;				// ---- xxxx   Audio Control 0/1
;  var AUDF0=0x17,AUDF1;				// ---x xxxx   Audio Frequency 0/1
;  var AUDV0=0x19,AUDV1;				// ---- xxxx   Audio Volume 0/1
;  
;  var inpt0=0x38;					// x--- ----   read pot port
;  var inpt1=0x39;					// x--- ----   read pot port
;  var inpt2=0x3A;					// x--- ----   read pot port
;  var inpt3=0x3B;					// x--- ----   read pot port
;  var inpt4=0x3C;					// x--- ----   read input / P0 Fire (0 active)
;  var inpt5=0x3D;					// x--- ----   read input / P1 Fire (0 active)
;    
;  var swcha=0x280;				// xxxx xxxx   Port A
;  								//         ^	- P1 Up		(0 active)
;  								//        ^		- P1 Down	(0 active)
;  								//       ^		- P1 Left	(0 active)
;  								//      ^		- P1 Right	(0 active)
;  								//    ^			- P0 Up		(0 active)
;  								//   ^			- P0 Down	(0 active)
;  								//  ^			- P0 Left	(0 active)
;  								// ^			- P0 Right	(0 active)
;  
;  var swchb=0x282;				// xx-- x-xx   Port B
;  								//         ^	- Reset Button          (0=Pressed)
;  								//		  ^		- Select Button         (0=Pressed)
;  								//      ^		- Color Switch          (0=B/W, 1=Color) (Always 0 for SECAM)
;  								//  ^			- P0 Difficulty Switch  (0=Beginner (B), 1=Advanced (A))
;  								// ^			- P1 Difficulty Switch  (0=Beginner (B), 1=Advanced (A))
;  
;  
;  
;  
;  // ---------------- $8x ---------------- volatile - temps used by frame & synth
;  
;  // general temps
;  var TempVars+4=0x80;
;  var PlayerVars+4=0x81;
;  var tmp3=0x82;
;  var tmp4=0x83;	// _TempVars+0
;  var tmp5=0x84;	// _TempVars+2
;  var tmp6=0x85;	// _ptrC
;  var tmp7=0x86;	// _ptrD
;  var tmp8=0x87;	// count
;  
;  var TempVars+0=0x88;
;  var TempVars+2=0x8A;
;  var ptrC=0x8C;
;  var ptrD=0x8E;
;  
;  var _TempVars+0=0x83;	// tmp4
;  var _TempVars+2=0x84;	// tmp5
;  var _ptrC=0x85;	// tmp6
;  var _ptrD=0x86;	// tmp7
;  var count=0x87;	// tmp8
;  
;  
;  
;  // big synth temps (+TempVars+4 PlayerVars+4 tmp3 tmp4 tmp5 TempVars+0! TempVars+2!)
;  //var tia0v=0x85;
;  //var tia0w=0x86;
;  //var tia0f=0x87;
;  //var tia1v=0x8C;
;  //var tia1w=0x8D;
;  //var tia1f=0x8E;
;  //var synflags=0x8F;
;  
;  
;  
;  // ---------------- $9x ---------------- reserved for fx
;  
;  var gfx0=0x90;
;  var col0=0x92;
;  var gfx1=0x94;
;  var col1=0x96;
;  var map =0x98;
;  var plx =0x9A, ply, plf, plj;	// 9A 9B 9C 9D
;  // plf:
;  //	0x01	- player left
;  //	0x02	- player move
;  //	0x04	- cheat joy pressed
;  //	0x08
;  //	0x10
;  //	0x20
;  //	0x40
;  //	0x80
;  //
;  
;  
;  // ---------------- $Ax ---------------- reserved for fx
;  
;  // framebuffer
;  var frambuff=0xA0;	// $Ax, $Bx, $Cx, $Dx
;  // A0 A1 A2 A3 A4 A5 A6 A7 A8 A9 AA AB AC AD AE AF
;  // B0 B1 B2 B3 B4 B5 B6 B7 B8 B9 BA BB BC BD BE BF
;  // C0 C1 C2 C3 C4 C5 C6 C7 C8 C9 CA CB CC CD CE CF
;  // D0 D1 D2 D3 D4 D5 D6 D7 D8 D9 DA DB DC DD DE DF
;  
;  
;  // --- down ---->
;  // gfx tile			| 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |
;  // gfx source:		  11   10    9    8    7    6    5    4    3    2    1
;  // position src:    11   10   9    8    7    6    5    4    3    2    1    0
;  // refl bit:		b3   b2   b1   b0   a7   a6   a5   a4   a3   a2   a1   a0
;  
;  
;  
;  //  0  1  2  3  4  5  6  7  8  9 10
;  // A0 A1 A2 A3 A4 A5 A6 A7 A8 A9 AA
;  // AB AC AD AE AF B0 B1 B2 B3 B4 B5
;  // B6 B7 B8 B9 BA BB BC BD BE BF C0
;  // C1 C2 C3 C4 C5 C6 C7 C8 C9 CA CB
;  // CC CD CE CF D0 D1 D2 D3 D4 D5 D6 D7 D8 D9 DA DB DC DD DE DF
;  
;  
;  //									 0  1  2  3  4  5  6  7  8  9 10
;  var Pos0  = 0xA0;		// [ 0..10] A0 A1 A2 A3 A4 A5 A6 A7 A8 A9 AA
;  var Pos1  = 0xAB;		// [ 0..10] AB AC AD AE AF B0 B1 B2 B3 B4 B5
;  var Gfx0  = 0xB6;		// [ 0..10] B6 B7 B8 B9 BA BB BC BD BE BF C0
;  var Gfx1  = 0xC1;		// [ 0..10] C1 C2 C3 C4 C5 C6 C7 C8 C9 CA CB
;  // unused: CC CD CE CF D0 D1 D2 D3 D4 D5 D6 D7 D8 D9 DA DB DC DD DE DF
;  var dbg0 = 0xDA, dbg1, dbg2, dbg3, dbg4, dbg5;
;  var trap = 0xD9;
;  
;  var txtp = 0xA0;	// 24 regsF
;  var txtc = 0xB8;	//  2 regs
;  
;  
;  // ---------------- $Bx ---------------- reserved for fx
;  
;  
;  // ---------------- $Cx ---------------- reserved for fx
;  
;  // ---------------- $Dx ---------------- reserved for fx
;  
;  
;  // ---------------- $Ex ---------------- persistent game & synth state
;  
;  // counters
;  var frame=0xE0, subframe, smallframe, midframe;	// $E0, $E1, $E2, $E3
;  var subframe_hi=0x1E1;
;  
;  // big synth counters
;  //var PlayerVars+1=0xE4, PlayerVars+3, songpos_sub, chanpos0, chanpos1, chanpos2, chanpos3;	// $E4 ... $EA
;  
;  // mini synth counters
;  var PlayerVars+1=0xE4, PlayerVars+3, PlayerVars+2, PlayerVars+0;	// E4 E5 E6 E7
;  
;  
;  
;  // ---------------- $Fx ---------------- reserved stack
;  
;  
;  
;  
;  // segmentation:
;  //
;  // $8x	- fx			text buffer 0..7, playfield buffers
;  // $9x	- volatile		text buffer 8..11, color ptr, temps
;  // $Ax	- fx			framebuffer, text colors, tunnel counter
;  // $Bx	- fx			framebuffer 
;  // $Cx	- fx			framebuffer 
;  // $Dx	- fx			framebuffer
;  // $Ex	- persistent	frame counter, fx counters, synth counters
;  // $Fx	- stack			stack
;  
;  data SongSeq0 {
SongSeq0
    .byte     #$00
;  	0x00 0x00 0x00 0x00 0x10 0x20 0x30 0x40 0x10 0x20 0x30 0x40 0x10 0x20 0x30 0x40 0x50 0x60 0x70 0x80 0x50 0x60 0x70 0x80 0x90 0x90 0x90 0x90 0x90 0x90 0xA0 0xB0 0xC0 0xC0 0xC0 0xC0 0xC0 0xC0 0xC0 0xC0 0xC0 0xC0 0xC0 0xC0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xD0 0xD0 0xD0 0xE0 0xF0 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$10
    .byte     #$20
    .byte     #$30
    .byte     #$40
    .byte     #$10
    .byte     #$20
    .byte     #$30
    .byte     #$40
    .byte     #$10
    .byte     #$20
    .byte     #$30
    .byte     #$40
    .byte     #$50
    .byte     #$60
    .byte     #$70
    .byte     #$80
    .byte     #$50
    .byte     #$60
    .byte     #$70
    .byte     #$80
    .byte     #$90
    .byte     #$90
    .byte     #$90
    .byte     #$90
    .byte     #$90
    .byte     #$90
    .byte     #$A0
    .byte     #$B0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$C0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$D0
    .byte     #$D0
    .byte     #$D0
    .byte     #$E0
    .byte     #$F0
;  }
;  data SongSeq1 {
; section SongSeq1
SongSeq1
    .byte     #$01
;  	0x01 0x01 0x11 0x21 0x01 0x01 0x11 0x21 0x01 0x01 0x11 0x21 0x01 0x01 0x11 0x21 0x01 0x01 0x11 0x21 0x01 0x01 0x11 0x21 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x31 0x41 0x51 0x61 0x31 0x41 0x51 0x61 0x31 0x41 0x51 0x61 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0x71 0x81 0x91 0xA1 0xB1 
    .byte     #$01
    .byte     #$11
    .byte     #$21
    .byte     #$01
    .byte     #$01
    .byte     #$11
    .byte     #$21
    .byte     #$01
    .byte     #$01
    .byte     #$11
    .byte     #$21
    .byte     #$01
    .byte     #$01
    .byte     #$11
    .byte     #$21
    .byte     #$01
    .byte     #$01
    .byte     #$11
    .byte     #$21
    .byte     #$01
    .byte     #$01
    .byte     #$11
    .byte     #$21
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$31
    .byte     #$41
    .byte     #$51
    .byte     #$61
    .byte     #$31
    .byte     #$41
    .byte     #$51
    .byte     #$61
    .byte     #$31
    .byte     #$41
    .byte     #$51
    .byte     #$61
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$71
    .byte     #$81
    .byte     #$91
    .byte     #$A1
    .byte     #$B1
;  }
;  data SongSeq2 {
; section SongSeq2
SongSeq2
    .byte     #$00
;  	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0xC1 0xD1 0xD1 0xE1 0xD1 0xD1 0xD1 0xE1 0xF1 0xF1 0xF1 0xF1 0x02 0x02 0x02 0x12 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x32 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$C1
    .byte     #$D1
    .byte     #$D1
    .byte     #$E1
    .byte     #$D1
    .byte     #$D1
    .byte     #$D1
    .byte     #$E1
    .byte     #$F1
    .byte     #$F1
    .byte     #$F1
    .byte     #$F1
    .byte     #$02
    .byte     #$02
    .byte     #$02
    .byte     #$12
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$32
;  }
;  data SongSeq3 {
; section SongSeq3
SongSeq3
    .byte     #$42
;  	0x42 0x52 0x42 0x52 0x42 0x52 0x42 0x52 0x62 0x62 0x62 0x72 0x62 0x62 0x62 0x72 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x62 0x82 0x92 0xF1 0xF1 0xA2 0xB2 0xC2 0xD2 0xA2 0xB2 0xE2 0xF2 0xA2 0xB2 0xC2 0xD2 0xA2 0xB2 0xE2 0xF2 0xA2 0xB2 0xC2 0xD2 0xA2 0xB2 0xE2 0xF2 0x03 0x13 0x23 0x33 0x03 0x13 0x23 0x33 0x43 0x53 0x63 0x73 0x43 0x83 0x93 0xA3 0x43 0x53 0x63 0x73 0x43 0x83 0x93 0xA3 0x01 0x11 0xB3 0xC3 0x01 0x11 0xB3 0xC3 0x43 0x53 0x63 0x73 0x43 0x83 0x93 0xA3 0x43 0x53 0x63 0x73 0x43 0x83 0x93 0xA3 0x01 0x11 0xB3 0xC3 0x01 0x11 0xB3 0xC3 0xD3 
    .byte     #$52
    .byte     #$42
    .byte     #$52
    .byte     #$42
    .byte     #$52
    .byte     #$42
    .byte     #$52
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$72
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$72
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$62
    .byte     #$82
    .byte     #$92
    .byte     #$F1
    .byte     #$F1
    .byte     #$A2
    .byte     #$B2
    .byte     #$C2
    .byte     #$D2
    .byte     #$A2
    .byte     #$B2
    .byte     #$E2
    .byte     #$F2
    .byte     #$A2
    .byte     #$B2
    .byte     #$C2
    .byte     #$D2
    .byte     #$A2
    .byte     #$B2
    .byte     #$E2
    .byte     #$F2
    .byte     #$A2
    .byte     #$B2
    .byte     #$C2
    .byte     #$D2
    .byte     #$A2
    .byte     #$B2
    .byte     #$E2
    .byte     #$F2
    .byte     #$03
    .byte     #$13
    .byte     #$23
    .byte     #$33
    .byte     #$03
    .byte     #$13
    .byte     #$23
    .byte     #$33
    .byte     #$43
    .byte     #$53
    .byte     #$63
    .byte     #$73
    .byte     #$43
    .byte     #$83
    .byte     #$93
    .byte     #$A3
    .byte     #$43
    .byte     #$53
    .byte     #$63
    .byte     #$73
    .byte     #$43
    .byte     #$83
    .byte     #$93
    .byte     #$A3
    .byte     #$01
    .byte     #$11
    .byte     #$B3
    .byte     #$C3
    .byte     #$01
    .byte     #$11
    .byte     #$B3
    .byte     #$C3
    .byte     #$43
    .byte     #$53
    .byte     #$63
    .byte     #$73
    .byte     #$43
    .byte     #$83
    .byte     #$93
    .byte     #$A3
    .byte     #$43
    .byte     #$53
    .byte     #$63
    .byte     #$73
    .byte     #$43
    .byte     #$83
    .byte     #$93
    .byte     #$A3
    .byte     #$01
    .byte     #$11
    .byte     #$B3
    .byte     #$C3
    .byte     #$01
    .byte     #$11
    .byte     #$B3
    .byte     #$C3
    .byte     #$D3
;  }
;  data SongPatA {
; section SongPatA
	 ALIGN 256,100
SongPatA
;  	align 256
;  	/* pattern  0 */	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern  1 */	0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
;  	/* pattern  2 */	0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
;  	/* pattern  3 */	0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
;  	/* pattern  4 */	0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 0x07 
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
;  	/* pattern  5 */	0x07 0x07 0x07 0x07 0x00 0x00 0x00 0x07 0x07 0x07 0x07 0x07 0x00 0x00 0x00 0x07 
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$07
;  	/* pattern  6 */	0x07 0x07 0x07 0x07 0x00 0x00 0x00 0x07 0x07 0x07 0x07 0x07 0x00 0x00 0x00 0x07 
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$07
;  	/* pattern  7 */	0x07 0x07 0x07 0x07 0x00 0x00 0x00 0x07 0x07 0x07 0x07 0x07 0x00 0x00 0x00 0x07 
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$07
;  	/* pattern  8 */	0x07 0x07 0x07 0x07 0x00 0x00 0x00 0x07 0x07 0x07 0x07 0x07 0x00 0x00 0x00 0x07 
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$07
;  	/* pattern  9 */	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern 10 */	0x0E 0x00 0x00 0x00 0x0E 0x00 0x00 0x00 0x0E 0x00 0x00 0x00 0x0E 0x00 0x00 0x00 
    .byte     #$0E
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$0E
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$0E
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$0E
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern 11 */	0x0E 0x00 0x0E 0x00 0x0E 0x00 0x0E 0x00 0x0E 0x0E 0x0E 0x0E 0x0E 0x0E 0x0E 0x0E 
    .byte     #$0E
    .byte     #$00
    .byte     #$0E
    .byte     #$00
    .byte     #$0E
    .byte     #$00
    .byte     #$0E
    .byte     #$00
    .byte     #$0E
    .byte     #$0E
    .byte     #$0E
    .byte     #$0E
    .byte     #$0E
    .byte     #$0E
    .byte     #$0E
    .byte     #$0E
;  	/* pattern 12 */	0x0E 0x00 0x07 0x00 0x07 0x00 0x07 0x00 0x07 0x00 0x07 0x00 0x07 0x00 0x07 0x00 
    .byte     #$0E
    .byte     #$00
    .byte     #$07
    .byte     #$00
    .byte     #$07
    .byte     #$00
    .byte     #$07
    .byte     #$00
    .byte     #$07
    .byte     #$00
    .byte     #$07
    .byte     #$00
    .byte     #$07
    .byte     #$00
    .byte     #$07
    .byte     #$00
;  	/* pattern 13 */	0x0E 0x00 0x07 0x07 0x15 0x00 0x07 0x07 0x0E 0x00 0x07 0x07 0x15 0x00 0x07 0x07 
    .byte     #$0E
    .byte     #$00
    .byte     #$07
    .byte     #$07
    .byte     #$15
    .byte     #$00
    .byte     #$07
    .byte     #$07
    .byte     #$0E
    .byte     #$00
    .byte     #$07
    .byte     #$07
    .byte     #$15
    .byte     #$00
    .byte     #$07
    .byte     #$07
;  	/* pattern 14 */	0x0E 0x07 0x15 0x07 0x15 0x07 0x07 0x07 0x0E 0x15 0x07 0x0E 0x15 0x07 0x15 0x07 
    .byte     #$0E
    .byte     #$07
    .byte     #$15
    .byte     #$07
    .byte     #$15
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$0E
    .byte     #$15
    .byte     #$07
    .byte     #$0E
    .byte     #$15
    .byte     #$07
    .byte     #$15
    .byte     #$07
;  	/* pattern 15 */	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern 16 */	0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
;  	/* pattern 17 */	0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
;  	/* pattern 18 */	0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
;  	/* pattern 19 */	0x2A 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 
    .byte     #$2A
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
;  	/* pattern 20 */	0x2A 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 
    .byte     #$2A
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
;  	/* pattern 21 */	0x2A 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 
    .byte     #$2A
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
;  	/* pattern 22 */	0x2A 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 0x30 
    .byte     #$2A
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
    .byte     #$30
;  	/* pattern 23 */	0x2A 0x00 0x2A 0x00 0x2A 0x30 0x00 0x2A 0x00 0x2A 0x00 0x2A 0x2A 0x30 0x2A 0x30 
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$30
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$2A
    .byte     #$30
    .byte     #$2A
    .byte     #$30
;  	/* pattern 24 */	0x2A 0x00 0x2A 0x00 0x2A 0x30 0x00 0x2A 0x00 0x2A 0x00 0x2A 0x2A 0x30 0x2A 0x30 
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$30
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$2A
    .byte     #$30
    .byte     #$2A
    .byte     #$30
;  	/* pattern 25 */	0x2A 0x00 0x2A 0x00 0x2A 0x30 0x00 0x2A 0x00 0x2A 0x00 0x2A 0x2A 0x30 0x2A 0x30 
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$30
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$2A
    .byte     #$30
    .byte     #$2A
    .byte     #$30
;  	/* pattern 26 */	0x2A 0x00 0x2A 0x00 0x2A 0x30 0x00 0x2A 0x00 0x2A 0x00 0x2A 0x2A 0x2A 0x2A 0x2A 
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$30
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$00
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
;  	/* pattern 27 */	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern 28 */	0x00 0x00 0x00 0x00 0x37 0x3D 0x00 0x00 0x00 0x00 0x00 0x00 0x37 0x3D 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$37
    .byte     #$3D
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$37
    .byte     #$3D
    .byte     #$00
    .byte     #$00
;  	/* pattern 29 */	0x00 0x00 0x00 0x00 0x37 0x3D 0x00 0x00 0x00 0x00 0x00 0x00 0x37 0x3D 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$37
    .byte     #$3D
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$37
    .byte     #$3D
    .byte     #$00
    .byte     #$00
;  	/* pattern 30 */	0x00 0x00 0x00 0x00 0x37 0x3D 0x00 0x00 0x00 0x00 0x37 0x3D 0x00 0x00 0x37 0x37 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$37
    .byte     #$3D
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$37
    .byte     #$3D
    .byte     #$00
    .byte     #$00
    .byte     #$37
    .byte     #$37
;  	/* pattern 31 */	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern 32 */	0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
;  	/* pattern 33 */	0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 0x44 
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
    .byte     #$44
;  	/* pattern 34 */	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern 35 */	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern 36 */	0x1C 0x4B 0x52 0x1D 0x4C 0x53 0x1E 0x4D 0x54 0x1F 0x1C 0x4B 0x52 0x1D 0x4C 0x53 
    .byte     #$1C
    .byte     #$4B
    .byte     #$52
    .byte     #$1D
    .byte     #$4C
    .byte     #$53
    .byte     #$1E
    .byte     #$4D
    .byte     #$54
    .byte     #$1F
    .byte     #$1C
    .byte     #$4B
    .byte     #$52
    .byte     #$1D
    .byte     #$4C
    .byte     #$53
;  	/* pattern 37 */	0x1E 0x4D 0x54 0x1F 0x1C 0x4B 0x52 0x1D 0x4C 0x53 0x1E 0x4D 0x54 0x1F 0x1C 0x4B 
    .byte     #$1E
    .byte     #$4D
    .byte     #$54
    .byte     #$1F
    .byte     #$1C
    .byte     #$4B
    .byte     #$52
    .byte     #$1D
    .byte     #$4C
    .byte     #$53
    .byte     #$1E
    .byte     #$4D
    .byte     #$54
    .byte     #$1F
    .byte     #$1C
    .byte     #$4B
;  	/* pattern 38 */	0x5B 0x00 0x5B 0x62 0x00 0x00 0x00 0x5B 0x62 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$5B
    .byte     #$00
    .byte     #$5B
    .byte     #$62
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$5B
    .byte     #$62
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern 39 */	0x5B 0x00 0x5B 0x62 0x00 0x5B 0x5B 0x5B 0x62 0x00 0x00 0x00 0x00 0x00 0x5B 0x5B 
    .byte     #$5B
    .byte     #$00
    .byte     #$5B
    .byte     #$62
    .byte     #$00
    .byte     #$5B
    .byte     #$5B
    .byte     #$5B
    .byte     #$62
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$5B
    .byte     #$5B
;  	/* pattern 40 */	0x69 0x6F 0x75 0x7C 0x82 0x88 0x8F 0x95 0x9B 0xA2 0xA8 0xAE 0xB5 0xBB 0x69 0x69 
    .byte     #$69
    .byte     #$6F
    .byte     #$75
    .byte     #$7C
    .byte     #$82
    .byte     #$88
    .byte     #$8F
    .byte     #$95
    .byte     #$9B
    .byte     #$A2
    .byte     #$A8
    .byte     #$AE
    .byte     #$B5
    .byte     #$BB
    .byte     #$69
    .byte     #$69
;  	/* pattern 41 */	0x69 0x69 0x69 0x69 0x69 0x69 0x69 0x69 0x69 0x69 0x69 0x69 0x69 0x69 0x69 0x69 
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
    .byte     #$69
;  	/* pattern 42 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x5B 0x62 0x61 0x60 0x5B 0x62 0x61 0x60 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
;  	/* pattern 43 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x5B 0x62 0x61 0x60 0x5B 0x62 0x61 0x60 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
;  	/* pattern 44 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x5B 0x62 0x61 0x60 0x5B 0x62 0x61 0x60 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
;  	/* pattern 45 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
;  	/* pattern 46 */	0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x61 0x60 0x5B 0x62 0x5B 0x62 0x61 0x60 
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
;  	/* pattern 47 */	0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
;  	/* pattern 48 */	0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
;  	/* pattern 49 */	0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
;  	/* pattern 50 */	0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
;  	/* pattern 51 */	0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 0xC2 
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
    .byte     #$C2
;  	/* pattern 52 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
;  	/* pattern 53 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x61 0x60 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
;  	/* pattern 54 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
;  	/* pattern 55 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x61 0x60 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
;  	/* pattern 56 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x61 0x60 0x5F 0x62 0x5B 0x62 0x61 0x60 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
;  	/* pattern 57 */	0x5B 0x62 0x61 0x60 0x5F 0x62 0x61 0x60 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 
    .byte     #$5B
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5F
    .byte     #$62
    .byte     #$61
    .byte     #$60
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
;  	/* pattern 58 */	0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 0x5B 0x62 
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
    .byte     #$5B
    .byte     #$62
;  	/* pattern 59 */	0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
;  	/* pattern 60 */	0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 0x1C 0x23 
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
    .byte     #$1C
    .byte     #$23
;  	/* pattern 61 */	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  }
;  data SongPatB {
; section SongPatB
	 ALIGN 256,100
SongPatB
;  	align 256
;  	/* pattern  0*/0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	/* pattern  1*/0x01 0x01 0x02 0x02 0x03 0x03 0x04 0x04 0x05 0x05 0x06 0x06 0x07 0x07 0x08 0x08 
    .byte     #$01
    .byte     #$01
    .byte     #$02
    .byte     #$02
    .byte     #$03
    .byte     #$03
    .byte     #$04
    .byte     #$04
    .byte     #$05
    .byte     #$05
    .byte     #$06
    .byte     #$06
    .byte     #$07
    .byte     #$07
    .byte     #$08
    .byte     #$08
;  	/* pattern  2*/0x09 0x09 0x0A 0x0A 0x0B 0x0B 0x0C 0x0C 0x0D 0x0D 0x0E 0x0E 0x0F 0x0F 0x10 0x10 
    .byte     #$09
    .byte     #$09
    .byte     #$0A
    .byte     #$0A
    .byte     #$0B
    .byte     #$0B
    .byte     #$0C
    .byte     #$0C
    .byte     #$0D
    .byte     #$0D
    .byte     #$0E
    .byte     #$0E
    .byte     #$0F
    .byte     #$0F
    .byte     #$10
    .byte     #$10
;  	/* pattern  3*/0x10 0x10 0x0F 0x0F 0x0E 0x0E 0x0D 0x0D 0x0C 0x0C 0x0B 0x0B 0x0A 0x0A 0x09 0x09 
    .byte     #$10
    .byte     #$10
    .byte     #$0F
    .byte     #$0F
    .byte     #$0E
    .byte     #$0E
    .byte     #$0D
    .byte     #$0D
    .byte     #$0C
    .byte     #$0C
    .byte     #$0B
    .byte     #$0B
    .byte     #$0A
    .byte     #$0A
    .byte     #$09
    .byte     #$09
;  	/* pattern  4*/0x08 0x08 0x07 0x07 0x06 0x06 0x05 0x05 0x04 0x04 0x03 0x03 0x02 0x02 0x01 0x01 
    .byte     #$08
    .byte     #$08
    .byte     #$07
    .byte     #$07
    .byte     #$06
    .byte     #$06
    .byte     #$05
    .byte     #$05
    .byte     #$04
    .byte     #$04
    .byte     #$03
    .byte     #$03
    .byte     #$02
    .byte     #$02
    .byte     #$01
    .byte     #$01
;  	/* pattern  5*/0x01 0x01 0x02 0x02 0x02 0x02 0x02 0x04 0x05 0x05 0x06 0x06 0x06 0x06 0x06 0x08 
    .byte     #$01
    .byte     #$01
    .byte     #$02
    .byte     #$02
    .byte     #$02
    .byte     #$02
    .byte     #$02
    .byte     #$04
    .byte     #$05
    .byte     #$05
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$08
;  	/* pattern  6*/0x09 0x09 0x0A 0x0A 0x0A 0x0A 0x0A 0x0C 0x0D 0x0D 0x0E 0x0E 0x0E 0x0E 0x0E 0x10 
    .byte     #$09
    .byte     #$09
    .byte     #$0A
    .byte     #$0A
    .byte     #$0A
    .byte     #$0A
    .byte     #$0A
    .byte     #$0C
    .byte     #$0D
    .byte     #$0D
    .byte     #$0E
    .byte     #$0E
    .byte     #$0E
    .byte     #$0E
    .byte     #$0E
    .byte     #$10
;  	/* pattern  7*/0x10 0x10 0x0F 0x0F 0x0F 0x0F 0x0F 0x0D 0x0C 0x0C 0x0B 0x0B 0x0B 0x0B 0x0B 0x09 
    .byte     #$10
    .byte     #$10
    .byte     #$0F
    .byte     #$0F
    .byte     #$0F
    .byte     #$0F
    .byte     #$0F
    .byte     #$0D
    .byte     #$0C
    .byte     #$0C
    .byte     #$0B
    .byte     #$0B
    .byte     #$0B
    .byte     #$0B
    .byte     #$0B
    .byte     #$09
;  	/* pattern  8*/0x08 0x08 0x07 0x07 0x07 0x07 0x07 0x05 0x04 0x04 0x03 0x03 0x03 0x03 0x03 0x01 
    .byte     #$08
    .byte     #$08
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$05
    .byte     #$04
    .byte     #$04
    .byte     #$03
    .byte     #$03
    .byte     #$03
    .byte     #$03
    .byte     #$03
    .byte     #$01
;  	/* pattern  9*/0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
;  	/* pattern 10*/0x83 0x11 0x11 0x11 0x83 0x11 0x11 0x11 0x83 0x11 0x11 0x11 0x83 0x11 0x11 0x11 
    .byte     #$83
    .byte     #$11
    .byte     #$11
    .byte     #$11
    .byte     #$83
    .byte     #$11
    .byte     #$11
    .byte     #$11
    .byte     #$83
    .byte     #$11
    .byte     #$11
    .byte     #$11
    .byte     #$83
    .byte     #$11
    .byte     #$11
    .byte     #$11
;  	/* pattern 11*/0x83 0x11 0x83 0x11 0x83 0x11 0x83 0x11 0x83 0x83 0x83 0x83 0x83 0x83 0x83 0x83 
    .byte     #$83
    .byte     #$11
    .byte     #$83
    .byte     #$11
    .byte     #$83
    .byte     #$11
    .byte     #$83
    .byte     #$11
    .byte     #$83
    .byte     #$83
    .byte     #$83
    .byte     #$83
    .byte     #$83
    .byte     #$83
    .byte     #$83
    .byte     #$83
;  	/* pattern 12*/0x83 0x12 0x13 0x13 0x14 0x14 0x0E 0x0E 0x14 0x14 0x13 0x13 0x14 0x14 0x0E 0x0E 
    .byte     #$83
    .byte     #$12
    .byte     #$13
    .byte     #$13
    .byte     #$14
    .byte     #$14
    .byte     #$0E
    .byte     #$0E
    .byte     #$14
    .byte     #$14
    .byte     #$13
    .byte     #$13
    .byte     #$14
    .byte     #$14
    .byte     #$0E
    .byte     #$0E
;  	/* pattern 13*/0x83 0x12 0x0D 0x0D 0x88 0x15 0x0D 0x0D 0x83 0x12 0x0D 0x0D 0x88 0x15 0x0D 0x0D 
    .byte     #$83
    .byte     #$12
    .byte     #$0D
    .byte     #$0D
    .byte     #$88
    .byte     #$15
    .byte     #$0D
    .byte     #$0D
    .byte     #$83
    .byte     #$12
    .byte     #$0D
    .byte     #$0D
    .byte     #$88
    .byte     #$15
    .byte     #$0D
    .byte     #$0D
;  	/* pattern 14*/0x83 0x08 0x88 0x0D 0x88 0x08 0x0D 0x0D 0x83 0x88 0x0D 0x83 0x88 0x08 0x88 0x0D 
    .byte     #$83
    .byte     #$08
    .byte     #$88
    .byte     #$0D
    .byte     #$88
    .byte     #$08
    .byte     #$0D
    .byte     #$0D
    .byte     #$83
    .byte     #$88
    .byte     #$0D
    .byte     #$83
    .byte     #$88
    .byte     #$08
    .byte     #$88
    .byte     #$0D
;  	/* pattern 15*/0x0D 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 
    .byte     #$0D
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
;  	/* pattern 16*/0x17 0x17 0x18 0x18 0x19 0x19 0x1A 0x1A 0x17 0x17 0x18 0x18 0x19 0x19 0x1A 0x1A 
    .byte     #$17
    .byte     #$17
    .byte     #$18
    .byte     #$18
    .byte     #$19
    .byte     #$19
    .byte     #$1A
    .byte     #$1A
    .byte     #$17
    .byte     #$17
    .byte     #$18
    .byte     #$18
    .byte     #$19
    .byte     #$19
    .byte     #$1A
    .byte     #$1A
;  	/* pattern 17*/0x1B 0x1B 0x1C 0x1C 0x19 0x19 0x1A 0x1A 0x1B 0x1B 0x1C 0x1C 0x19 0x19 0x1A 0x1A 
    .byte     #$1B
    .byte     #$1B
    .byte     #$1C
    .byte     #$1C
    .byte     #$19
    .byte     #$19
    .byte     #$1A
    .byte     #$1A
    .byte     #$1B
    .byte     #$1B
    .byte     #$1C
    .byte     #$1C
    .byte     #$19
    .byte     #$19
    .byte     #$1A
    .byte     #$1A
;  	/* pattern 18*/0x17 0x17 0x1D 0x1D 0x19 0x19 0x1A 0x1A 0x17 0x17 0x1D 0x1D 0x1A 0x1A 0x19 0x19 
    .byte     #$17
    .byte     #$17
    .byte     #$1D
    .byte     #$1D
    .byte     #$19
    .byte     #$19
    .byte     #$1A
    .byte     #$1A
    .byte     #$17
    .byte     #$17
    .byte     #$1D
    .byte     #$1D
    .byte     #$1A
    .byte     #$1A
    .byte     #$19
    .byte     #$19
;  	/* pattern 19*/0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
;  	/* pattern 20*/0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
;  	/* pattern 21*/0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x20 
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
;  	/* pattern 22*/0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x21 
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
;  	/* pattern 23*/0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x22 0x22 0x1E 0x1E 0x1E 0x22 0x22 0x1E 0x1E 
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$22
    .byte     #$22
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$22
    .byte     #$22
    .byte     #$1E
    .byte     #$1E
;  	/* pattern 24*/0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x1F 0x23 0x23 0x1F 0x1F 0x1F 0x23 0x23 0x1F 0x1F 
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$23
    .byte     #$23
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
    .byte     #$23
    .byte     #$23
    .byte     #$1F
    .byte     #$1F
;  	/* pattern 25*/0x20 0x20 0x20 0x20 0x20 0x20 0x20 0x24 0x24 0x20 0x20 0x20 0x24 0x24 0x20 0x20 
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$24
    .byte     #$24
    .byte     #$20
    .byte     #$20
    .byte     #$20
    .byte     #$24
    .byte     #$24
    .byte     #$20
    .byte     #$20
;  	/* pattern 26*/0x21 0x21 0x21 0x21 0x21 0x21 0x21 0x25 0x25 0x21 0x21 0x21 0x25 0x1F 0x1F 0x23 
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$25
    .byte     #$25
    .byte     #$21
    .byte     #$21
    .byte     #$21
    .byte     #$25
    .byte     #$1F
    .byte     #$1F
    .byte     #$23
;  	/* pattern 27*/0x23 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 
    .byte     #$23
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
;  	/* pattern 28*/0x00 0x00 0x00 0x00 0x88 0x88 0x06 0x06 0x26 0x26 0x26 0x26 0x88 0x88 0x06 0x06 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$88
    .byte     #$88
    .byte     #$06
    .byte     #$06
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$88
    .byte     #$88
    .byte     #$06
    .byte     #$06
;  	/* pattern 29*/0x26 0x26 0x26 0x26 0x88 0x88 0x06 0x06 0x26 0x26 0x26 0x26 0x88 0x88 0x06 0x06 
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$88
    .byte     #$88
    .byte     #$06
    .byte     #$06
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$88
    .byte     #$88
    .byte     #$06
    .byte     #$06
;  	/* pattern 30*/0x26 0x26 0x26 0x26 0x88 0x88 0x06 0x06 0x26 0x26 0x88 0x88 0x06 0x26 0x88 0x88 
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$88
    .byte     #$88
    .byte     #$06
    .byte     #$06
    .byte     #$26
    .byte     #$26
    .byte     #$88
    .byte     #$88
    .byte     #$06
    .byte     #$26
    .byte     #$88
    .byte     #$88
;  	/* pattern 31*/0x26 0x26 0x26 0x26 0x26 0x26 0x26 0x26 0x26 0x26 0x26 0x26 0x26 0x26 0x26 0x26 
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
    .byte     #$26
;  	/* pattern 32*/0x01 0x01 0x02 0x02 0x03 0x03 0x04 0x04 0x05 0x05 0x06 0x06 0x07 0x07 0x08 0x08 
    .byte     #$01
    .byte     #$01
    .byte     #$02
    .byte     #$02
    .byte     #$03
    .byte     #$03
    .byte     #$04
    .byte     #$04
    .byte     #$05
    .byte     #$05
    .byte     #$06
    .byte     #$06
    .byte     #$07
    .byte     #$07
    .byte     #$08
    .byte     #$08
;  	/* pattern 33*/0x09 0x09 0x0A 0x0A 0x0B 0x0B 0x0C 0x0C 0x0D 0x0D 0x0E 0x0E 0x0F 0x0F 0x10 0x10 
    .byte     #$09
    .byte     #$09
    .byte     #$0A
    .byte     #$0A
    .byte     #$0B
    .byte     #$0B
    .byte     #$0C
    .byte     #$0C
    .byte     #$0D
    .byte     #$0D
    .byte     #$0E
    .byte     #$0E
    .byte     #$0F
    .byte     #$0F
    .byte     #$10
    .byte     #$10
;  	/* pattern 34*/0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
    .byte     #$10
;  	/* pattern 35*/0x10 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 
    .byte     #$10
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
;  	/* pattern 36*/0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
;  	/* pattern 37*/0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 0x18 
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$18
;  	/* pattern 38*/0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
;  	/* pattern 39*/0x1E 0x1E 0x1E 0x1E 0x1E 0x27 0x28 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x1E 0x28 
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$27
    .byte     #$28
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$1E
    .byte     #$28
;  	/* pattern 40*/0x88 0x88 0x88 0x88 0x88 0x88 0x88 0x88 0x88 0x88 0x88 0x88 0x88 0x88 0x01 0x01 
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$88
    .byte     #$01
    .byte     #$01
;  	/* pattern 41*/0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 0x01 
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$01
;  	/* pattern 42*/0x22 0x22 0x22 0x22 0x22 0x22 0x29 0x29 0x2A 0x2A 0x2A 0x2A 0x24 0x24 0x24 0x24 
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$29
    .byte     #$29
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
;  	/* pattern 43*/0x23 0x23 0x23 0x23 0x23 0x23 0x2A 0x2A 0x29 0x29 0x29 0x29 0x24 0x24 0x24 0x24 
    .byte     #$23
    .byte     #$23
    .byte     #$23
    .byte     #$23
    .byte     #$23
    .byte     #$23
    .byte     #$2A
    .byte     #$2A
    .byte     #$29
    .byte     #$29
    .byte     #$29
    .byte     #$29
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
;  	/* pattern 44*/0x22 0x22 0x22 0x22 0x22 0x22 0x29 0x29 0x2A 0x2A 0x2A 0x2A 0x2B 0x2B 0x2B 0x2B 
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$29
    .byte     #$29
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2B
    .byte     #$2B
    .byte     #$2B
    .byte     #$2B
;  	/* pattern 45*/0x24 0x24 0x24 0x24 0x24 0x24 0x2C 0x2C 0x2A 0x2A 0x29 0x29 0x24 0x24 0x29 0x29 
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$2C
    .byte     #$2C
    .byte     #$2A
    .byte     #$2A
    .byte     #$29
    .byte     #$29
    .byte     #$24
    .byte     #$24
    .byte     #$29
    .byte     #$29
;  	/* pattern 46*/0x2B 0x2B 0x2A 0x2A 0x22 0x22 0x2C 0x2C 0x2C 0x2C 0x29 0x29 0x24 0x24 0x24 0x24 
    .byte     #$2B
    .byte     #$2B
    .byte     #$2A
    .byte     #$2A
    .byte     #$22
    .byte     #$22
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$29
    .byte     #$29
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
;  	/* pattern 47*/0x2C 0x2C 0x2A 0x2A 0x29 0x29 0x2C 0x2C 0x2A 0x2A 0x29 0x29 0x24 0x24 0x25 0x25 
    .byte     #$2C
    .byte     #$2C
    .byte     #$2A
    .byte     #$2A
    .byte     #$29
    .byte     #$29
    .byte     #$2C
    .byte     #$2C
    .byte     #$2A
    .byte     #$2A
    .byte     #$29
    .byte     #$29
    .byte     #$24
    .byte     #$24
    .byte     #$25
    .byte     #$25
;  	/* pattern 48*/0x22 0x2A 0x2B 0x2A 0x22 0x2A 0x2B 0x2A 0x22 0x2A 0x2B 0x2A 0x22 0x2A 0x2B 0x2A 
    .byte     #$22
    .byte     #$2A
    .byte     #$2B
    .byte     #$2A
    .byte     #$22
    .byte     #$2A
    .byte     #$2B
    .byte     #$2A
    .byte     #$22
    .byte     #$2A
    .byte     #$2B
    .byte     #$2A
    .byte     #$22
    .byte     #$2A
    .byte     #$2B
    .byte     #$2A
;  	/* pattern 49*/0x23 0x22 0x2A 0x22 0x23 0x22 0x2A 0x22 0x23 0x22 0x2A 0x2B 0x2C 0x2A 0x29 0x24 
    .byte     #$23
    .byte     #$22
    .byte     #$2A
    .byte     #$22
    .byte     #$23
    .byte     #$22
    .byte     #$2A
    .byte     #$22
    .byte     #$23
    .byte     #$22
    .byte     #$2A
    .byte     #$2B
    .byte     #$2C
    .byte     #$2A
    .byte     #$29
    .byte     #$24
;  	/* pattern 50*/0x24 0x29 0x2C 0x29 0x24 0x29 0x2C 0x29 0x24 0x29 0x2C 0x29 0x24 0x29 0x2C 0x29 
    .byte     #$24
    .byte     #$29
    .byte     #$2C
    .byte     #$29
    .byte     #$24
    .byte     #$29
    .byte     #$2C
    .byte     #$29
    .byte     #$24
    .byte     #$29
    .byte     #$2C
    .byte     #$29
    .byte     #$24
    .byte     #$29
    .byte     #$2C
    .byte     #$29
;  	/* pattern 51*/0x25 0x24 0x29 0x24 0x25 0x24 0x29 0x24 0x23 0x22 0x2A 0x22 0x24 0x29 0x2C 0x29 
    .byte     #$25
    .byte     #$24
    .byte     #$29
    .byte     #$24
    .byte     #$25
    .byte     #$24
    .byte     #$29
    .byte     #$24
    .byte     #$23
    .byte     #$22
    .byte     #$2A
    .byte     #$22
    .byte     #$24
    .byte     #$29
    .byte     #$2C
    .byte     #$29
;  	/* pattern 52*/0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x22 0x25 0x25 0x22 0x22 0x2A 0x2A 
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$22
    .byte     #$25
    .byte     #$25
    .byte     #$22
    .byte     #$22
    .byte     #$2A
    .byte     #$2A
;  	/* pattern 53*/0x2C 0x2C 0x2C 0x2C 0x2C 0x2C 0x2A 0x2A 0x2A 0x2A 0x2A 0x2A 0x29 0x29 0x29 0x29 
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$29
    .byte     #$29
    .byte     #$29
    .byte     #$29
;  	/* pattern 54*/0x24 0x24 0x24 0x24 0x24 0x24 0x24 0x24 0x24 0x24 0x27 0x27 0x24 0x24 0x29 0x29 
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$27
    .byte     #$27
    .byte     #$24
    .byte     #$24
    .byte     #$29
    .byte     #$29
;  	/* pattern 55*/0x2A 0x2A 0x2A 0x2A 0x2A 0x2A 0x29 0x29 0x29 0x29 0x29 0x29 0x24 0x24 0x24 0x24 
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$29
    .byte     #$29
    .byte     #$29
    .byte     #$29
    .byte     #$29
    .byte     #$29
    .byte     #$24
    .byte     #$24
    .byte     #$24
    .byte     #$24
;  	/* pattern 56*/0x2C 0x2C 0x2C 0x2C 0x2C 0x2C 0x2A 0x2A 0x2A 0x2A 0x2A 0x2A 0x2B 0x2B 0x2B 0x2B 
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2A
    .byte     #$2B
    .byte     #$2B
    .byte     #$2B
    .byte     #$2B
;  	/* pattern 57*/0x2C 0x2C 0x2C 0x2C 0x2C 0x2C 0x2C 0x2C 0x2B 0x2B 0x2A 0x2A 0x29 0x29 0x24 0x24 
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2C
    .byte     #$2B
    .byte     #$2B
    .byte     #$2A
    .byte     #$2A
    .byte     #$29
    .byte     #$29
    .byte     #$24
    .byte     #$24
;  	/* pattern 58*/0x2C 0x2C 0x2A 0x2A 0x29 0x29 0x24 0x24 0x2A 0x2A 0x29 0x29 0x24 0x24 0x25 0x25 
    .byte     #$2C
    .byte     #$2C
    .byte     #$2A
    .byte     #$2A
    .byte     #$29
    .byte     #$29
    .byte     #$24
    .byte     #$24
    .byte     #$2A
    .byte     #$2A
    .byte     #$29
    .byte     #$29
    .byte     #$24
    .byte     #$24
    .byte     #$25
    .byte     #$25
;  	/* pattern 59*/0x17 0x17 0x1D 0x1D 0x19 0x19 0x1A 0x1A 0x17 0x17 0x1D 0x1D 0x19 0x19 0x1A 0x1A 
    .byte     #$17
    .byte     #$17
    .byte     #$1D
    .byte     #$1D
    .byte     #$19
    .byte     #$19
    .byte     #$1A
    .byte     #$1A
    .byte     #$17
    .byte     #$17
    .byte     #$1D
    .byte     #$1D
    .byte     #$19
    .byte     #$19
    .byte     #$1A
    .byte     #$1A
;  	/* pattern 60*/0x2B 0x2B 0x2D 0x2D 0x19 0x19 0x1A 0x1A 0x2B 0x2B 0x2D 0x2D 0x1A 0x1A 0x19 0x19 
    .byte     #$2B
    .byte     #$2B
    .byte     #$2D
    .byte     #$2D
    .byte     #$19
    .byte     #$19
    .byte     #$1A
    .byte     #$1A
    .byte     #$2B
    .byte     #$2B
    .byte     #$2D
    .byte     #$2D
    .byte     #$1A
    .byte     #$1A
    .byte     #$19
    .byte     #$19
;  	/* pattern 61*/0x19 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 0x16 
    .byte     #$19
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
;  }
;  data SongScaleFreq {
; section SongScaleFreq
	 ALIGN 256,100
SongScaleFreq
;  	align 256
;  	 0 31 29 27 26 24 22 20 19 17 15 14 12 10  8  7 
    .byte     #$00
    .byte     #$1F
    .byte     #$1D
    .byte     #$1B
    .byte     #$1A
    .byte     #$18
    .byte     #$16
    .byte     #$14
    .byte     #$13
    .byte     #$11
    .byte     #$0F
    .byte     #$0E
    .byte     #$0C
    .byte     #$0A
    .byte     #$08
    .byte     #$07
;  	 5  0 10  6  4 23  0  3  7  6  2  4  9  8 15 19 
    .byte     #$05
    .byte     #$00
    .byte     #$0A
    .byte     #$06
    .byte     #$04
    .byte     #$17
    .byte     #$00
    .byte     #$03
    .byte     #$07
    .byte     #$06
    .byte     #$02
    .byte     #$04
    .byte     #$09
    .byte     #$08
    .byte     #$0F
    .byte     #$13
;  	17 20  7  9  8 21  0 11 27  6 13 10  5 11 
    .byte     #$11
    .byte     #$14
    .byte     #$07
    .byte     #$09
    .byte     #$08
    .byte     #$15
    .byte     #$00
    .byte     #$0B
    .byte     #$1B
    .byte     #$06
    .byte     #$0D
    .byte     #$0A
    .byte     #$05
    .byte     #$0B
;  }
;  data SongScaleWave {
; section SongScaleWave
	 ALIGN 256,100
SongScaleWave
;  	align 256
;  	 0  8  8  8  8  8  8  8  8  8  8  8  8  8  8  8 
    .byte     #$00
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$08
;  	 8  3  3  8  8  8  1  6  6  1  6  6  6  6  7  7 
    .byte     #$08
    .byte     #$03
    .byte     #$03
    .byte     #$08
    .byte     #$08
    .byte     #$08
    .byte     #$01
    .byte     #$06
    .byte     #$06
    .byte     #$01
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$07
    .byte     #$07
;  	 7  7  7  7  7  1  8  7  1  7  1  1  7  6 
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$01
    .byte     #$08
    .byte     #$07
    .byte     #$01
    .byte     #$07
    .byte     #$01
    .byte     #$01
    .byte     #$07
    .byte     #$06
;  }
;  data SongEnvVol {
; section SongEnvVol
	 ALIGN 256,100
SongEnvVol
;  	align 256
;  	 0  0  0  0  0  0  0 11  0  0  0  0  0  0 15 15 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$0B
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$0F
    .byte     #$0F
;  	15 14 12  0  0 15 15 15 15 15 15  0  2  7  3  6 
    .byte     #$0F
    .byte     #$0E
    .byte     #$0C
    .byte     #$00
    .byte     #$00
    .byte     #$0F
    .byte     #$0F
    .byte     #$0F
    .byte     #$0F
    .byte     #$0F
    .byte     #$0F
    .byte     #$00
    .byte     #$02
    .byte     #$07
    .byte     #$03
    .byte     #$06
;  	 5  8  6  9  7 10  8  6  5  3 15 14 13 12 11 10 
    .byte     #$05
    .byte     #$08
    .byte     #$06
    .byte     #$09
    .byte     #$07
    .byte     #$0A
    .byte     #$08
    .byte     #$06
    .byte     #$05
    .byte     #$03
    .byte     #$0F
    .byte     #$0E
    .byte     #$0D
    .byte     #$0C
    .byte     #$0B
    .byte     #$0A
;  	 9  9  8  7  6  6  8 14 14 12 12 11 11  9  7  7 
    .byte     #$09
    .byte     #$09
    .byte     #$08
    .byte     #$07
    .byte     #$06
    .byte     #$06
    .byte     #$08
    .byte     #$0E
    .byte     #$0E
    .byte     #$0C
    .byte     #$0C
    .byte     #$0B
    .byte     #$0B
    .byte     #$09
    .byte     #$07
    .byte     #$07
;  	 0  0  0  0 13  0  0  0  0  0  0  9  7 10  2  7 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$0D
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$09
    .byte     #$07
    .byte     #$0A
    .byte     #$02
    .byte     #$07
;  	 3  6  5  8  6  9  7 10  2  7  3 12 10  8  7  6 
    .byte     #$03
    .byte     #$06
    .byte     #$05
    .byte     #$08
    .byte     #$06
    .byte     #$09
    .byte     #$07
    .byte     #$0A
    .byte     #$02
    .byte     #$07
    .byte     #$03
    .byte     #$0C
    .byte     #$0A
    .byte     #$08
    .byte     #$07
    .byte     #$06
;  	 7  6  5  6  7  6  5  6  7  6  6  6  6  6  6  6 
    .byte     #$07
    .byte     #$06
    .byte     #$05
    .byte     #$06
    .byte     #$07
    .byte     #$06
    .byte     #$05
    .byte     #$06
    .byte     #$07
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
;  	 6  6  6  6  6  6  6  6  6  6  6  6  6  6  6  6 
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
;  	 6  6  6  6  6  6  6  6  6  6  6  6  6  6  6  6 
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
;  	 6  6  6  6  6  6  6  6  6  6  6  6  6  6  6  6 
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
;  	 6  6  6  6  6  6  6  6  6  6  6  6  6  6  6  6 
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
;  	 6  6  6  6  6  6  6  6  6  6  6  6  6  6  6  6 
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$06
;  	 6  6 11  7  4  2  1  0  0 
    .byte     #$06
    .byte     #$06
    .byte     #$0B
    .byte     #$07
    .byte     #$04
    .byte     #$02
    .byte     #$01
    .byte     #$00
    .byte     #$00
;  }
;  data SongEnvFreq {
; section SongEnvFreq
	 ALIGN 256,100
SongEnvFreq
;  	align 256
;  	 0  0  0  0  0  0  0  0  0  0  0  0  0  0  3 10 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$03
    .byte     #$0A
;  	26 14 12 10 10  5  6  7 10 14 18 23  0  0  0  0 
    .byte     #$1A
    .byte     #$0E
    .byte     #$0C
    .byte     #$0A
    .byte     #$0A
    .byte     #$05
    .byte     #$06
    .byte     #$07
    .byte     #$0A
    .byte     #$0E
    .byte     #$12
    .byte     #$17
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	 0  0  0  0  0  0  0  6  6  8  8  9  9 11 14 14 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$06
    .byte     #$06
    .byte     #$08
    .byte     #$08
    .byte     #$09
    .byte     #$09
    .byte     #$0B
    .byte     #$0E
    .byte     #$0E
;  	22 22 22 22  0  0  0  0  0  0  0  0  0  0  0  0 
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  	 0  0  0  0  0  0  0  0  0  0  0  0  1  1  1  2 
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$01
    .byte     #$01
    .byte     #$01
    .byte     #$02
;  	 2  3  3  3  4  4  5  5  5  6  6  6  7  7  7  8 
    .byte     #$02
    .byte     #$03
    .byte     #$03
    .byte     #$03
    .byte     #$04
    .byte     #$04
    .byte     #$05
    .byte     #$05
    .byte     #$05
    .byte     #$06
    .byte     #$06
    .byte     #$06
    .byte     #$07
    .byte     #$07
    .byte     #$07
    .byte     #$08
;  	 8  8  9  9 10 10 10 11 11 12 12 12 13 13 13 14 
    .byte     #$08
    .byte     #$08
    .byte     #$09
    .byte     #$09
    .byte     #$0A
    .byte     #$0A
    .byte     #$0A
    .byte     #$0B
    .byte     #$0B
    .byte     #$0C
    .byte     #$0C
    .byte     #$0C
    .byte     #$0D
    .byte     #$0D
    .byte     #$0D
    .byte     #$0E
;  	14 14 15 15 15 16 16 17 17 17 18 18 19 19 19 20 
    .byte     #$0E
    .byte     #$0E
    .byte     #$0F
    .byte     #$0F
    .byte     #$0F
    .byte     #$10
    .byte     #$10
    .byte     #$11
    .byte     #$11
    .byte     #$11
    .byte     #$12
    .byte     #$12
    .byte     #$13
    .byte     #$13
    .byte     #$13
    .byte     #$14
;  	20 20 21 21 21 22 22 22 23 23 24 24 24 25 25 26 
    .byte     #$14
    .byte     #$14
    .byte     #$15
    .byte     #$15
    .byte     #$15
    .byte     #$16
    .byte     #$16
    .byte     #$16
    .byte     #$17
    .byte     #$17
    .byte     #$18
    .byte     #$18
    .byte     #$18
    .byte     #$19
    .byte     #$19
    .byte     #$1A
;  	26 26 27 27 27 28 28 28 29 29 29 30 30 31 31 31 
    .byte     #$1A
    .byte     #$1A
    .byte     #$1B
    .byte     #$1B
    .byte     #$1B
    .byte     #$1C
    .byte     #$1C
    .byte     #$1C
    .byte     #$1D
    .byte     #$1D
    .byte     #$1D
    .byte     #$1E
    .byte     #$1E
    .byte     #$1F
    .byte     #$1F
    .byte     #$1F
;  	31 31  0  0  0  0  0  0  0 
    .byte     #$1F
    .byte     #$1F
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
    .byte     #$00
;  }
;  inline song_seq_wrap { x?116 >={ x=0 } }
; section main
;  main {
MusicPlayer SUBROUTINE
main
    LDX     PlayerVars+3
;  	x=PlayerVars+3
;  	a=SongSeq0,x
    LDA     SongSeq0,X
;  	player_prep_pointers
;  	player_fetch_volume
	; inlined player_prep_pointers
    TAY
    AND     #$F0
    STA     TempVars+0
    TYA
    AND     #$0F
    CLc
    ADC     #>[SongPatA]
    STA     TempVars+0+1
    LDY     PlayerVars+1
    LDA     (TempVars+0),Y
    STA     TempVars+2
	; end of inlined player_prep_pointers
;  	!={
	; inlined player_fetch_volume
    LDY     PlayerVars+2
    LDA     #>[SongEnvVol]
    STA     TempVars+2+1
    LDA     (TempVars+2),Y
	; end of inlined player_fetch_volume
    BEQ     __label_11
;  		TempVars+4=a
    STA     TempVars+4
;  		player_fetch_freq_wave AUDF0=a AUDC0=x
	; inlined player_fetch_freq_wave
    LDA     TempVars+0+1
    SEc
    SBC     #>[SongPatA]
    CLc
    ADC     #>[SongPatB]
    STA     TempVars+0+1
    LDY     PlayerVars+1
    LDA     (TempVars+0),Y
    TAX
	BPL	__I11__label_5
    LDY     PlayerVars+2
    LDA     #>[SongEnvFreq]
    STA     TempVars+2+1
    LDA     (TempVars+2),Y
	JMP	__I11__label_6
__I11__label_5
    LDA     SongScaleFreq,X
    TAY
    LDA     SongScaleWave,X
    TAX
    TYA
__I11__label_6
	; end of inlined player_fetch_freq_wave
    STA     AUDF0
    STX     AUDC0
;  	} else {
    JMP     __label_12
__label_11
;  		// channel 1 play through
    LDX     PlayerVars+3
;  		x=PlayerVars+3
;  		a=SongSeq1,x
    LDA     SongSeq1,X
;  		player_prep_pointers
;  		player_fetch_volume		TempVars+4=a
	; inlined player_prep_pointers
    TAY
    AND     #$F0
    STA     TempVars+0
    TYA
    AND     #$0F
    CLc
    ADC     #>[SongPatA]
    STA     TempVars+0+1
    LDY     PlayerVars+1
    LDA     (TempVars+0),Y
    STA     TempVars+2
	; end of inlined player_prep_pointers
	; inlined player_fetch_volume
    LDY     PlayerVars+2
    LDA     #>[SongEnvVol]
    STA     TempVars+2+1
    LDA     (TempVars+2),Y
	; end of inlined player_fetch_volume
    STA     TempVars+4
;  		player_fetch_freq_wave	AUDF0=a AUDC0=x
	; inlined player_fetch_freq_wave
    LDA     TempVars+0+1
    SEc
    SBC     #>[SongPatA]
    CLc
    ADC     #>[SongPatB]
    STA     TempVars+0+1
    LDY     PlayerVars+1
    LDA     (TempVars+0),Y
    TAX
	BPL	__I14__label_5
    LDY     PlayerVars+2
    LDA     #>[SongEnvFreq]
    STA     TempVars+2+1
    LDA     (TempVars+2),Y
	JMP	__I14__label_6
__I14__label_5
    LDA     SongScaleFreq,X
    TAY
    LDA     SongScaleWave,X
    TAX
    TYA
__I14__label_6
	; end of inlined player_fetch_freq_wave
    STA     AUDF0
    STX     AUDC0
;  	}
;  	mod_volume
__label_12
;  	AUDV0=a
	; inlined mod_volume
    LDA     PlayerVars+0
    EOR     #$0F
    STA     PlayerVars+4
    LDA     #$00
    LSR     TempVars+4
	BCC	__I15__label_7
    CLc
    ADC     PlayerVars+4
__I15__label_7
    ASL     PlayerVars+4
    LSR     TempVars+4
	BCC	__I15__label_8
    CLc
    ADC     PlayerVars+4
__I15__label_8
    ASL     PlayerVars+4
    LSR     TempVars+4
	BCC	__I15__label_9
    CLc
    ADC     PlayerVars+4
__I15__label_9
    ASL     PlayerVars+4
    LSR     TempVars+4
	BCC	__I15__label_10
    CLc
    ADC     PlayerVars+4
__I15__label_10
    LSR
    LSR
    LSR
    LSR
    ADC     #$00
	; end of inlined mod_volume
    STA     AUDV0
;  
;  	x=PlayerVars+3
    LDX     PlayerVars+3
;  	a=SongSeq2,x
    LDA     SongSeq2,X
;  	player_prep_pointers
;  	player_fetch_volume
	; inlined player_prep_pointers
    TAY
    AND     #$F0
    STA     TempVars+0
    TYA
    AND     #$0F
    CLc
    ADC     #>[SongPatA]
    STA     TempVars+0+1
    LDY     PlayerVars+1
    LDA     (TempVars+0),Y
    STA     TempVars+2
	; end of inlined player_prep_pointers
;  	!={
	; inlined player_fetch_volume
    LDY     PlayerVars+2
    LDA     #>[SongEnvVol]
    STA     TempVars+2+1
    LDA     (TempVars+2),Y
	; end of inlined player_fetch_volume
    BEQ     __label_13
;  		TempVars+4=a
    STA     TempVars+4
;  		player_fetch_freq_wave	AUDF1=a AUDC1=x
	; inlined player_fetch_freq_wave
    LDA     TempVars+0+1
    SEc
    SBC     #>[SongPatA]
    CLc
    ADC     #>[SongPatB]
    STA     TempVars+0+1
    LDY     PlayerVars+1
    LDA     (TempVars+0),Y
    TAX
	BPL	__I18__label_5
    LDY     PlayerVars+2
    LDA     #>[SongEnvFreq]
    STA     TempVars+2+1
    LDA     (TempVars+2),Y
	JMP	__I18__label_6
__I18__label_5
    LDA     SongScaleFreq,X
    TAY
    LDA     SongScaleWave,X
    TAX
    TYA
__I18__label_6
	; end of inlined player_fetch_freq_wave
    STA     AUDF1
    STX     AUDC1
;  	} else {
    JMP     __label_14
__label_13
;  		// channel 3 play through
    LDX     PlayerVars+3
;  		x=PlayerVars+3
;  		a=SongSeq3,x
    LDA     SongSeq3,X
;  		player_prep_pointers
;  		player_fetch_volume		TempVars+4=a
	; inlined player_prep_pointers
    TAY
    AND     #$F0
    STA     TempVars+0
    TYA
    AND     #$0F
    CLc
    ADC     #>[SongPatA]
    STA     TempVars+0+1
    LDY     PlayerVars+1
    LDA     (TempVars+0),Y
    STA     TempVars+2
	; end of inlined player_prep_pointers
	; inlined player_fetch_volume
    LDY     PlayerVars+2
    LDA     #>[SongEnvVol]
    STA     TempVars+2+1
    LDA     (TempVars+2),Y
	; end of inlined player_fetch_volume
    STA     TempVars+4
;  		player_fetch_freq_wave	AUDF1=a AUDC1=x
	; inlined player_fetch_freq_wave
    LDA     TempVars+0+1
    SEc
    SBC     #>[SongPatA]
    CLc
    ADC     #>[SongPatB]
    STA     TempVars+0+1
    LDY     PlayerVars+1
    LDA     (TempVars+0),Y
    TAX
	BPL	__I21__label_5
    LDY     PlayerVars+2
    LDA     #>[SongEnvFreq]
    STA     TempVars+2+1
    LDA     (TempVars+2),Y
	JMP	__I21__label_6
__I21__label_5
    LDA     SongScaleFreq,X
    TAY
    LDA     SongScaleWave,X
    TAX
    TYA
__I21__label_6
	; end of inlined player_fetch_freq_wave
    STA     AUDF1
    STX     AUDC1
;  	}
;  	mod_volume
__label_14
;  	AUDV1=a
	; inlined mod_volume
    LDA     PlayerVars+0
    EOR     #$0F
    STA     PlayerVars+4
    LDA     #$00
    LSR     TempVars+4
	BCC	__I22__label_7
    CLc
    ADC     PlayerVars+4
__I22__label_7
    ASL     PlayerVars+4
    LSR     TempVars+4
	BCC	__I22__label_8
    CLc
    ADC     PlayerVars+4
__I22__label_8
    ASL     PlayerVars+4
    LSR     TempVars+4
	BCC	__I22__label_9
    CLc
    ADC     PlayerVars+4
__I22__label_9
    ASL     PlayerVars+4
    LSR     TempVars+4
	BCC	__I22__label_10
    CLc
    ADC     PlayerVars+4
__I22__label_10
    LSR
    LSR
    LSR
    LSR
    ADC     #$00
	; end of inlined mod_volume
    STA     AUDV1
;  
;  	// next tick/step/pattern
;  	x=PlayerVars+2 x++
    LDX     PlayerVars+2
    INX
;  	a=PlayerVars+1 a&3
    LDA     PlayerVars+1
    AND     #$03
;  	//== { x?6 } else { x?5 }
;  	x?5 !={ PlayerVars+2=x }
    CPX     #$05
    BEQ     __label_15
    STX     PlayerVars+2
;  	else {
    JMP     __label_16
__label_15
;  		PlayerVars+2=x=0
    LDX     #$00
    STX     PlayerVars+2
;  		a=PlayerVars+1 c- a+1 a&0x0F PlayerVars+1=a
    LDA     PlayerVars+1
    CLc
    ADC     #$01
    AND     #$0F
    STA     PlayerVars+1
;  		=={
    BNE     __label_17
;  			x=PlayerVars+3 x++ song_seq_wrap PlayerVars+3=x
    LDX     PlayerVars+3
    INX
	; inlined song_seq_wrap
    CPX     #$74
	BCC	__I23__label_3
    LDX     #$00
__I23__label_3
	; end of inlined song_seq_wrap
    STX     PlayerVars+3
;  		}
;  	}
__label_17
;  }
__label_16
;  
	RTB
