# TIM1T, by Cluster and DMA #

My first attempt at becoming a productive member of the scene.
Atari VCS 2600, no extra RAM (128 bytes only), no framebuffer, 32k ROM.
90% party version; sadly, the credits part is far from finished.

Code and design:  Kylearan/Cluster (andre.wichmann@gmx.de)
Music and player: KK/DMA (kk@devkk.net)



### Zooming square: ###

Since one goal for this demo was to focus on flow and do proper
transitions, I wanted an opening that doesn't simply throw the first
effect into the face of the viewer, but a slower one with a build-up
instead. So I came up with the idea of a square that flies by, spots
the viewer, quickly hides, resurfaces again and then zooms in. All I
needed for that is a routine that can prepare a horizontal, hi-res line
with an arbitrary length starting at any position.

Is there *any* system where drawing an arbitrary horizontal line is
more complicated than on the VCS 2600? You have to combine hi-res
sprites for the start and the end of the line with lo-res playfield for
the middle, while of course the playfield registers are of different
sizes (4 and 8 bits) and different orientations (draw bits 4..7, 7...0,
0...7). Ugh, what a mess...


### Intro screen: ###

A simple intro screen, and yet the only kernel in this demo that
needed to be run in zeropage because I needed the extra cycles from
self-modifying code. The black, vertical lines are actually the ball I
had to reuse each scanline, which made things a bit tedious to set up.

The rectangles zooming back are a re-use of the line-drawing routine
of course.


### Logo build-up: ###

A rather simple effect (Blocks moving in from above and below build
up the logo of the demo) required a somewhat complex kernel because of
the lack of a framebuffer. Speed or RAM is not the problem here, but
correct hand-overs between different parts of the kernel while keeping
the total scanline count constant. The kernel first displays the falling
squares, then the top line of the logo that gets currently built from
RAM, then the lines of the logo that are finished already from ROM,
then the bottom line that gets currently built, then the rising squares.

Coding was more time-consuming than hard, with lots of details to
take care of and fiddling around. Doing everything for transitions!


### 3D Starfield: ###

Since seeing my first (2D) starfield on the C64 in 1987, I like
starfields a lot - them being oldschool or not. They just look nice. 2D
starfields have been don before on the VCS (e.g. 2600 by Vantage) and
they are easy anyway, but I haven't seen a 3D starfield before. Can't be
so hard, as that's not exactly rocket science, right?

With no framebuffer and only 128 bytes of RAM, I hit some unexpected
obstacles on the way. First, the kernel. Arbitrary object positioning
with no empty scanlines inbetween, plus setting star color and playfield
had a consequence: The name of the demo had not only to be a palindrome,
but the left and right side of the word had to be an exact graphical
mirror as well - no time for an asynchronous playfield. Oh, and hooray
for the illegal ISC opcode!

Second, having no framebuffer means I have to sort the list of stars
each frame to be able to display them. Here I take advantage of the fact
that the list is partially sorted already.

Third, I cannot display two stars on the same y position, so they
flicker, er, twinkle, in that case. No problem though - that's what
stars do, right? :-)

Last but not least, RAM. Each star has x, y and z coordinates,
screen x and y coordinates, plus two indices in lists I have to maintain
for sorting. With some creative data structures and a little bit of
faking, I'm able to display 34 stars. RAM is the bottleneck here, so the
projecting and sorting routines are not very optimized for speed -
although I use a lot of tables for the 2D projection (sue me - this was
never meant to be a 4K demo!).


### Plasma: ###

Until now, plasma on the VCS either has either ugly gaps between
lines (e.g., Liquid Candy/Noice) or doesn't allow arbitrary colours and
positions (Stella Lives!/Tjoppen, although his approach is really
clever!). My goal was to give up on fullscreen in exchange for
improvement on both fronts.

The result is a solid 9x11 plasma with arbitrary colours - 9 wide
because my kernel can "only" display 9 arbitrary colors per scanline,
and 11 high because available RAM doesn't allow for more. The first
plasma displays these capabilities before the other plasmas use
interleaving lines for better visual quality.

Actually, I lied a little bit - some colors are not completely
arbitrary. If you pay close attention, the middle column appears
slightly darker and the rightmost column slightly lighter sometimes.
Colors on these columns must have bit 1 cleared/set respectively -
experienced VCS coders can make an educated guess why. ;-)


### Four in one: ###

The four individual effects used here are simple, but it was a nice
little challenge to combine kernels to make two at a time happen side by
side. That meant I have to pre-calculate data for two of the effects
per frame: For the sine waves and for the twister in this case, while
plasma bars and raster splits are calculated in the kernel on the fly.
Twister pre-calc needs 19 scanlines, so that explaines the size of the
vertical gap between the upper and lower effects.

Very simple effects used by classic VCS 2600 4K demos (hi Tjoppen,
JAC!, and others! :-)), but I wanted to have a relaxed and fun part that
plays with the audience a bit.


### Chessboard credits: ###

I ran out of time here unfortunately. The idea was to have several
perspective chessboards rotating in and out of the screen in a big sine
movement, with up to three boards on screen at the same time. The code
base is there, but that's something for the 100% version.


### Credits upscroller: ###

The biggest Kernel (code-wise, without data or init code) in the
whole demo. Having the Cluster logo only on one side means I have to set
PF1 and COLUPF twice per scanline at specific cycle intervals, and do
everything else inbetween. The kernel seamlessly and cycle-exact
switches between different modes: Displaying one row of font pixels;
setting up the next row of pixels; setting up the next char; setting
up the next effect.

I like the twirling effect. However, half-way through coding this
part, I saw "We are all connected" by Fairlight/Offence/Prosonix. Ah
well, different machine, so...

The Transitions in and out were not very difficult, but add to the
flow IMHO.


### End part: ###

Simple and calm ending. Not much to say here.