#!/usr/bin/python3

import sys
import math
import struct

out = sys.stdout.buffer
	
# 160 should be >max star x position
for i in range(0, 160):
	value = int((159-i)/15)+1
	out.write(struct.pack('B', value))
