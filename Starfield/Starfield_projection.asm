; =========================================================================
; Subtoutines
; =========================================================================

; Advance all star's z positions, and project all stars' 3D coords into 2D.
; If z reaches 0 or either the resulting x or y coordinate falls outside the
; display, a new star is created.

AdvanceStars
	ldx #SF_NUM_STARS-1
.loop
	; advance z coord
	; test star's x-sign bit (bit 7 of z coord)
	lda StarZ,x
	bmi .xSignSet
	dec StarZ,x
	bne .noNewStar
	; new star
	; rnd
	lda SFSeed
	beq .doEor
	asl
	beq .noEor ;if the input was $80, skip the EOR
	bcc .noEor
.doEor
	eor #$1d
.noEor
	sta SFSeed

	sta StarXY,x
	lda #127 - 20
	sta StarZ,x
	bne .projectStar
.xSignSet
	dec StarZ,x
	lda StarZ,x
	cmp #%10000000
	bne .noNewStar
	; new star

	; rnd
	lda SFSeed
	beq .doEor2
	asl
	beq .noEor2 ;if the input was $80, skip the EOR
	bcc .noEor2
.doEor2
	eor #$1d
.noEor2
	sta SFSeed

	sta StarXY,x
	lda #255 - 20	
	sta StarZ,x
.noNewStar

	; project coords to 2D
.projectStar
	; x
	; calc pointer to project table and store it into SFTmp1/2
	lda StarXY,x
	and #%00001111
	tay
	lda CoordPtrsLo,y
	sta SFTmp
	lda CoordPtrsHi,y
	sta SFTmp2
	; project x
	lda StarZ,x
	and #%01111111
	tay
	lda (SFTmp),y
	; still onscreen? -> table entry is 255 for too large x values
	bpl .noNewStar2
	; generate new star
.newStar
	; rnd 2
	lda SFSeed2
	beq .doEor3
	asl
	beq .noEor3 ;if the input was $80, skip the EOR
	bcc .noEor3
.doEor3
	eor #$1d
.noEor3
	sta SFSeed2

	sta StarXY,x
	lda StarZ,x
	bmi .xSignSet2
	lda #127 - 20	
	sta StarZ,x
	bne .projectStar
.xSignSet2
	lda #255 - 20	
	sta StarZ,x
	bne .projectStar

.noNewStar2
	; check for left or right quadrant (x-sign = bit 7 of z)
	ldy StarZ,x
	bmi .xSignSet3
	clc
	adc #SF_XRES/2 + 9	; +9 to avoid hmove bar
	sta StarScrX,x
	bne .projectY
.xSignSet3
	sta SFTmp
	lda #SF_XRES/2 + 9	; +9 to avoid hmove bar
	sec
	sbc SFTmp
	sta StarScrX,x
	
.projectY
	; y
	; calc pointer to project table and store it into SFTmp1/2
	lda StarXY,x
	lsr
	lsr
	lsr
	lsr
	tay
	lda CoordPtrsLo,y
	sta SFTmp
	lda CoordPtrsHi,y
	sta SFTmp2
	; project y
	lda StarZ,x
	and #%01111111
	tay
	lda (SFTmp),y
	; still onscreen?
	cmp #SF_YRES/2
	bcs .newStar
	sta StarScrY,x
	
	; loop
	dex
	bmi .exit
	jmp .loop
.exit

	RTB
	

; =========================================================================
; Tables
; =========================================================================

; Projection precalculation tables

StarZTables
 	include "star_z_tables.asm"
