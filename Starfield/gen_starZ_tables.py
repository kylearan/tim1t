import math

max_scrx = 140/2
max_x = 15
max_z = 127
dist = 256

print("CoordPtrsLo")
for i in range(0, max_x + 1):
	if i%4 == 0:
		print("\tdc.b ", end="")
	print("<StarCoords%d" % i, end="")
	if i%4 == 3:
		print()
	else:
		print(", ", end="")

print("CoordPtrsHi")
for i in range(0, max_x + 1):
	if i%4 == 0:
		print("\tdc.b ", end="")
	print(">StarCoords%d" % i, end="")
	if i%4 == 3:
		print()
	else:
		print(", ", end="")

print("\n\tALIGN $100")

for x in range(0, max_x + 1):
	print("\nStarCoords%d" % x)
	for z in range(0, max_z + 1):
		if z == 0:
			out = 255
		else:
			out = int(round(dist*x/z))
			if out >= max_scrx:
				out = 255
		if z%8 == 0:
			print("\tdc.b ", end="")
		if z%8 == 7:
			print(out)
		else:
			print("%d, " % out, end="")

