; Each star's data is packed into 4 bytes, in seperate tables.
;
; StarXY:
;	7...4: World y coordinate
;	3...0: World x coordinate
; StarZ:
;	7: 1 if x coordinate is negative, 0 if positive
;	6...0: World z coordinate
; StarScrX:
;	Screen x coordinate, used as index into HMoveTable and DelayTable
; StarScrY:
;	Screen y coordinate, equals double scanline, is sort key
;
; There is also a list of indices to the stars used for y sorting, plus a
; "sanitized" list which is strictly monotonously ordered, so overall 6 bytes are
; used per star. The sanitized list ends with a dummy star y value of 255.
;
; Kernel uses M0 and M1 (alternating) to display stars.
; Each star is two scanlines high.
; Each two scanlines,
;	- PF1 and PF2 have to be written to.
;	- COLM0/1 has to be written to
;	- HMOVE for HMM0/1 has to be executed
;	- New HMOVE and delay values have to be computed
;	- Index to next star has to be advanced
;	- RESM0/1 has to be executed after respective delay
;	- Line counter has to be increased and testet for loop
;
; POSITION LIMITS
;	- X can range between 0 and 149, although the first 8 pixels the star
;	gets displayed only half (b/c of hmove bars)
;
;;;;;;;;;;
; !!! CAUTION:
; - CurrentLine counts down from HALF_HEIGHT-1, so Y values of stars and their
; order have to reflect this!
;;;;;;;;;;
;
; OPTIMIZATION:
; - do a set/clear V flag instead of SLEEP 6, avoiding the bit test later
; and thus gaining an additional 9 pixels to the right


; =========================================================================
; Constants
; =========================================================================

SF_NUM_STARS	= 17
SF_XRES		= 140		; star x coord resolution
SF_YRES		= 100		; star y coord resolution
SF_HALF_HEIGHT	= SF_YRES/2	; number of double lines of half of height


; Script state constants
SF_STARS_IN	= 0
SF_LOGO_1	= 1
SF_LOGO_2	= 2
SF_STARS_OUT	= 3
SF_FADE_OUT	= 4

	

; =========================================================================
; Start
; =========================================================================

StarfieldStart

; optional, safe inits
	lda #0
	sta VDELP0
	sta VDELP1
	sta GRP0
	sta GRP1
	sta ENABL

; Init stars

	ldx #SF_NUM_STARS
.initStars
	lda InitSortingArray,x
	sta SortingArray,x
	lda InitStarXY,x
	sta StarXY,x
	lda InitStarZ,x
	sta StarZ,x
	dex
	bpl .initStars	
	; Initialize head and tail of star display list
	lda #255
	sta StarScrY+SF_NUM_STARS
	lda #SF_NUM_STARS
	sta StarArray
	sta StarArray+SF_NUM_STARS+1

	lda #93
	sta SFSeed
	lda #5
	sta SFSeed2
	
	; Demo state
	lda #0
	sta NumFrame
	sta DemoState

	; Missiles and PF
	lda #%00000101
	sta CTRLPF
	lda #0
	sta NUSIZ0
	sta NUSIZ1
	sta PF0
	lda #$de
	sta COLUPF
	
	jmp StarfieldIncoming


; =========================================================================
; Kernel
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

StarfieldKernel SUBROUTINE
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc NumFrame
	lda NumFrame
	ldx DemoState
	cmp SFScriptDurations,x 
	bne .noNextState
	inc DemoState
	lda DemoState
	cmp #SF_FADE_OUT + 1
	bne .noEndSF
	JMB PlasmaStart
.noEndSF
	lda #0
	sta NumFrame
.noNextState


	jsr SortStars
	jsr SortStars
StarfieldIncoming
	jsr SortStars
	jsr SortStars
		
	jsr CreateDisplayList
	
	jsr StarfieldWaitForIntim
	lda #0
	sta VBLANK

; -------------------------------------------------------------------------
; Visible area: 228 lines
; -------------------------------------------------------------------------

	sta WSYNC
	lda #$ec
	sta COLUBK
	sta WSYNC
	lda #0
	sta COLUBK
	
	lda DemoState
	cmp #SF_FADE_OUT
	bne DisplayUpperStarfield

; Display Logo alone and fade background to white
	lda NumFrame
	lsr
	lsr
	and #%00001111
	sta COLUBK
	cmp #$0a
	bcc .noNewLogoColor
	sta COLUPF
.noNewLogoColor
	
DisplayLogo SUBROUTINE
	ldy #SF_HALF_HEIGHT - 1
.upperLoop
	sta WSYNC
	lda SF_Upper_PF1,y
	sta PF1	
	lda SF_Upper_PF2,y
	sta PF2	
	sta WSYNC
	dey
	bpl .upperLoop
	iny
.lowerLoop
	sta WSYNC
	lda SF_Lower_PF1,y
	sta PF1
	lda SF_Lower_PF2,y
	sta PF2
	sta WSYNC
	iny
	cpy #SF_HALF_HEIGHT
	bne .lowerLoop
	sta WSYNC
	lda #21*76/64 + 1
	sta TIM64T
	jmp AfterStarfield
	

; Upper half
; -------------------------------------------------------------------------

DisplayUpperStarfield SUBROUTINE

	lda #SF_HALF_HEIGHT - 1
	sta CurrentLine		; 1 line = 2 scanlines
	lda #0
	sta CurrentStarIndex
	sta COLUP0
	sta COLUP1

.displayM0
	sta WSYNC		; 3

	; M0, first scanline
	; ----------------------------------------	
	; @0
	; Set registers 
	sta HMOVE		; 3
	lda #%00000010		; 2
	sta ENAM0		; 3
	lda #0			; 2
	sta ENAM1		; 3
	ldy CurrentLine		; 3
	lda SF_Upper_PF1,y		; 4
	sta PF1			; 3
	lda SF_Upper_PF2,y		; 4
	sta PF2			; 3
				; = 30

	; @30
	; Check if new star has to be displayed for M1 (next doubleline)
	ldx CurrentStarIndex	; 3
	ldy StarArray+1,x	; 4
	lda CurrentLine		; 3
	cmp StarScrY,y		; 4
	bne .noStarM0		; 2/3
	inc CurrentStarIndex	; 5
				; = 21/22 (51/52)

	; @51
	; Set color
	lda StarZ,y		; 4
	and #%01111111		; 2
	tax			; 2
	lda StarColors,x	; 4
	sta COLUP1		; 3
				; = 15 (66)
				
	; @66
	; Get x position values
	ldx StarScrX,y		; 4
	lda SF_HMoveTable,x	; 4
	sta HMM1		; 3
	; M0, second scanline @1
	; ----------------------------------------
	ldy SF_DelayTable,x	; 1+4
	; next line
	dec CurrentLine		; 5
				; = 10
	
	; @10
	SLEEP 6	; compensate for deleted color lsrs
	; Delay=1 will end at cycle 20
.delayM0
	dey			; 2
	bne .delayM0		; 2/3
	sta RESM1		; 3

.displayM1
	sta WSYNC		; 3

	; M1, first scanline
	; ----------------------------------------	
	; @0
	; Set registers
	sta HMOVE		; 3
	lda #%00000010		; 2
	sta ENAM1		; 3
	lda #0			; 2
	sta ENAM0		; 3
	ldy CurrentLine		; 3
	lda SF_Upper_PF1,y		; 4
	sta PF1			; 3
	lda SF_Upper_PF2,y		; 4
	sta PF2			; 3
				; = 30

	; @30
	; Check if new star has to be displayed for M1 (next doubleline)
	ldx CurrentStarIndex	; 3
	ldy StarArray+1,x	; 4
	lda CurrentLine		; 3
	cmp StarScrY,y		; 4
	bne .noStarM1		; 2/3
	inc CurrentStarIndex	; 5
				; = 21/22 (51/52)

	; @51
	; Set color
	lda StarZ,y		; 4
	and #%01111111		; 2
	tax			; 2
	lda StarColors,x	; 4
	sta COLUP0		; 3
				; = 15 (66)
				
	; @66
	; Get x position values
	ldx StarScrX,y		; 4
	lda SF_HMoveTable,x	; 4
	sta HMM0		; 3
	; M1, second scanline @1
	; ----------------------------------------
	ldy SF_DelayTable,x	; 1+4
	; next line
	dec CurrentLine		; 5
				; = 10
	
	; @10
	bmi .afterDisplay	; 2/3
	SLEEP 4
	; Delay=1 will end at cycle 20
.delayM1
	dey			; 2
	bne .delayM1		; 2/3
	sta RESM0		; 3

	; loop
	jmp .displayM0		; 5 +3 (WSYNC)

.noStarM0
	lda #0			; 2
	sta HMM1		; 3
	sta COLUP1		; 3
	sta WSYNC		; 3
	sta RESM1		; 3
	dec CurrentLine		; 5
	jmp .displayM1		; 5
	
.noStarM1
	lda #0			; 2
	sta HMM0		; 3
	sta COLUP0		; 3
	sta WSYNC		; 3
	sta RESM0		; 3
	dec CurrentLine		; 5
	bmi .afterDisplay	; 2/3
	jmp .displayM0		; 5 +3 (WSYNC)

.afterDisplay

	lda #122*76/64 + 1
	sta TIM64T


; Lower half
; -------------------------------------------------------------------------

DisplayLowerStarfield SUBROUTINE

	lda #0
	sta COLUP0
	sta COLUP1
	sta CurrentLine		; 1 line = 2 scanlines
	; CurrentStarIndex needs to be decreased by one, but instead we're
	; point the base of the index one less in the kernel.

	; REMOVEME?
;	jmp .displayM0
;	ALIGN $100

.displayM0
	sta WSYNC		; 3

	; M0, first scanline
	; ----------------------------------------	
	; @0
	; Set registers 
	sta HMOVE		; 3
	lda #%00000010		; 2
	sta ENAM0		; 3
	lda #0			; 2
	sta ENAM1		; 3
	ldy CurrentLine		; 3
	lda SF_Lower_PF1,y		; 4
	sta PF1			; 3
	lda SF_Lower_PF2,y		; 4
	sta PF2			; 3
				; = 30

	; @30
	; Check if new star has to be displayed for M1 (next doubleline)
	ldx CurrentStarIndex	; 3
	ldy StarArray,x		; 4
	lda CurrentLine		; 3
	cmp StarScrY,y		; 4
	bne .noStarM0		; 2/3
	dec CurrentStarIndex	; 5
				; = 21/22 (51/52)

	; @51
	; Set color
	lda StarZ,y		; 4
	and #%01111111		; 2
	tax			; 2
	lda StarColors,x	; 4
	sta COLUP1		; 3
				; = 15 (66)
				
	; @66
	; Get x position values
	ldx StarScrX,y		; 4
	lda SF_Reverse_HMoveTable,x	; 4
	sta HMM1		; 3
	; M0, second scanline @1
	; ----------------------------------------
	ldy SF_Reverse_DelayTable,x	; 1+4
	; next line
	inc CurrentLine		; 5
				; = 10
	
	; @10
	SLEEP 6	; compensate for deleted color lsrs
	; Delay=1 will end at cycle 20
.delayM0
	dey			; 2
	bne .delayM0		; 2/3
	sta RESM1		; 3

.displayM1
	sta WSYNC		; 3

	; M1, first scanline
	; ----------------------------------------	
	; @0
	; Set registers
	sta HMOVE		; 3
	lda #%00000010		; 2
	sta ENAM1		; 3
	lda #0			; 2
	sta ENAM0		; 3
	ldy CurrentLine		; 3
	lda SF_Lower_PF1,y		; 4
	sta PF1			; 3
	lda SF_Lower_PF2,y		; 4
	sta PF2			; 3
				; = 30

	; @30
	; Check if new star has to be displayed for M1 (next doubleline)
	ldx CurrentStarIndex	; 3
	ldy StarArray,x		; 4
	lda CurrentLine		; 3
	cmp StarScrY,y		; 4
	bne .noStarM1		; 2/3
	dec CurrentStarIndex	; 5
				; = 21/22 (51/52)

	; @51
	; Set color
	lda StarZ,y		; 4
	and #%01111111		; 2
	tax			; 2
	lda StarColors,x	; 4
	sta COLUP0		; 3
				; = 15 (66)
				
	; @66
	; Get x position values
	ldx StarScrX,y		; 4
	lda SF_Reverse_HMoveTable,x	; 4
	sta HMM0		; 3
	; M1, second scanline @1
	; ----------------------------------------
	ldy SF_Reverse_DelayTable,x	; 1+4
	; next line
	lda #SF_HALF_HEIGHT	; 2
	isb CurrentLine		; 5
	bcc .afterDisplay	; 2/3
	nop			; 2
				; = 9/10
	
	; @16
	; Delay=1 will end at cycle 20
.delayM1
	dey			; 2
	bne .delayM1		; 2/3
	sta RESM0		; 3

	; loop
	jmp .displayM0		; 5 +3 (WSYNC)

.noStarM0
	lda #0			; 2
	sta HMM1		; 3
	sta COLUP1		; 3
	sta WSYNC		; 3
	sta RESM1		; 3
	inc CurrentLine		; 5
	jmp .displayM1		; 5
	
.noStarM1
	lda #0			; 2
	sta HMM0		; 3
	sta COLUP0		; 3
	sta WSYNC		; 3
	sta RESM0		; 3
	inc CurrentLine		; 5
	lda CurrentLine		; 3
	cmp #SF_HALF_HEIGHT
	bcs .afterDisplay	; 2/3
	jmp .displayM0		; 5 +3 (WSYNC)


; after display
; -------------------------------------------------------------------------

.afterDisplay

	sta WSYNC
	sta WSYNC
	lda #0
	sta ENAM0
	sta ENAM1
	sta PF1
	sta PF2
	
; After display, process stars
; -------------------------------------------------------------------------

AfterStarfield
	
	JSB MusicPlayer

	jsr StarfieldWaitForIntim
	
	sta WSYNC
	sta WSYNC
	lda #$ec
	sta COLUBK
	sta WSYNC
	lda #0
	sta COLUBK

	
; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

StarfieldDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	JSB AdvanceStars

; Stars fade in
SFStarsFadeIn SUBROUTINE
	lda DemoState
	cmp #SF_STARS_IN
	bne .noStarsFadeIn
	lda #127
	sec
	sbc NumFrame
	lsr
	lsr
	lsr
	tax
	beq .noStarsFadeIn
.loop
	lda StarZ,x
	ora #%01111111
	sta StarZ,x
	dex
	bpl .loop
.noStarsFadeIn


; Stars fade out
SFStarsFadeOut SUBROUTINE
	lda DemoState
	cmp #SF_STARS_OUT
	bne .noStarsFadeOut
	lda NumFrame
	bpl .noLogoFade
	lda #128
.noLogoFade	
	lsr
	lsr
	lsr
	tax
	beq .noStarsFadeOut
.loop
	lda StarZ,x
	ora #%01111111
	sta StarZ,x
	dex
	bpl .loop
.noStarsFadeOut

	jsr StarfieldWaitForIntim

	jmp StarfieldKernel

	
; =========================================================================
; Subroutines
; =========================================================================


; Gradually sort the list of stars according to y value.
; Use a bubblesort-like algorithm with a fixed amount of passes.	
; -------------------------------------------------------------------------

SortStars SUBROUTINE
	ldx #SF_NUM_STARS-1
.loop
	; get y pos at index
	ldy SortingArray,x
	lda StarScrY,y
	sta SFTmp
	; compare to y pos at index-1
	ldy SortingArray-1,x
	lda StarScrY,y
	cmp SFTmp
	bcs .noSwap
	lda SortingArray,x
	sta SortingArray-1,x
	sty SortingArray,x
.noSwap
	dex
	bne .loop
	rts


; Create a list of stars monotonously sorted by y out of the gradually
; sorted list. Leave out any stars not sorted or with same y value as
; another star.	
; -------------------------------------------------------------------------

CreateDisplayList SUBROUTINE
	ldx SortingArray	; first entry is known already
	stx StarArray+1		; +1 to skip dummy 255 star
	lda StarScrY,x
	sta SFTmp		; current largest y value
	ldx #1
	stx CurrentStarIndex	; index into StarArray
.loop
	stx CurrentSortingIndex	; index into SortingArray
	ldy SortingArray,x
	lda StarScrY,y
	cmp SFTmp
	bcs .noAdd
	sta SFTmp		; new largest y
	; add star
	ldx CurrentStarIndex
	sty StarArray+1,x
	inc CurrentStarIndex
.noAdd
	; advance to next star in SortingArray
	ldx CurrentSortingIndex
	inx
	cpx #SF_NUM_STARS
	bne .loop
	rts
	


; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

StarfieldWaitForIntim
	lda INTIM
	bne StarfieldWaitForIntim
	sta WSYNC
	rts
	

; =========================================================================
; Tables
; =========================================================================

; Script event durations
; ------------------------------

SFScriptDurations
	dc.b 127	; SF_STARS_IN
	dc.b 255	; SF_LOGO_1
	dc.b 250	; SF_LOGO_2
	dc.b 150	; SF_STARS_OUT
	dc.b 64		; SF_FADE_OUT


; Star tables
; ------------------------------

; Star init tables

InitSortingArray
	dc.b 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
InitStarXY
	dc.b 201, 155, 185, 19, 208, 170, 213, 139
	dc.b 187, 11, 151, 134, 222, 148, 203, 59
	dc.b 249, 76, 79, 89, 82, 173, 211, 103
	
InitStarZ
	dc.b 128+91, 111, 128+37, 110, 128+108, 71, 128+20, 11
	dc.b 128+85, 22, 128+7, 47, 128+99, 77, 128+68, 71
	dc.b 128+61, 112, 128+50, 39, 128+63, 123, 128+16, 8


; Logo playfield graphics

	ALIGN $100
SF_Upper_PF1
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %11111100
	dc.b %11111100
	dc.b %11111100
	dc.b %11111100
	dc.b %11111100
	dc.b %11111100

	ds SF_HALF_HEIGHT-11
	
SF_Upper_PF2
	dc.b %10110011
	dc.b %10110011
	dc.b %11110011
	dc.b %11110011
	dc.b %11110011
	dc.b %01110011
	dc.b %01110011
	dc.b %01110011
	dc.b %00110011
	dc.b %00110011
	dc.b %00110011
	
	ds SF_HALF_HEIGHT-11

SF_Lower_PF1
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000

	ds SF_HALF_HEIGHT-8

SF_Lower_PF2
	dc.b %10110011
	dc.b %00110011
	dc.b %00110011
	dc.b %00110011
	dc.b %00110011
	dc.b %00110011
	dc.b %00110011
	dc.b %00110011
	dc.b %00110011
	dc.b %00110011

	ds SF_HALF_HEIGHT-8

; Star color tables according to distance
	ALIGN $100
StarColors
	include "star_color_table.asm"

; Cycle delay and hmove tables for per coordinate

	ALIGN $100
SF_HMoveTable
	include "hmove_table.asm"
	
	ALIGN $100
SF_DelayTable
	include "delay_table.asm"

SF_Reverse_HMoveTable
	include "reverse_hmove_table.asm"

SF_Reverse_DelayTable
	include "reverse_delay_table.asm"
