import math

max_x = 15
max_z = 127
dist = 256
screen_width = 72

screen = [0 for i in range(0, (max_x + 1)*screen_width)]

for x in range(0, max_x + 1):
	for z in range(max_z, 1, -1):
		out = int(round(dist*x/z))
		if out < screen_width:
			screen[x*screen_width + out] += 1
	
for x in range(0, max_x + 1):
	for i in range(0, screen_width):
		pos = x*screen_width + i
		if screen[pos] == 0:
			print(".", end="")
		elif screen[pos] < 10:
			print(screen[pos], end="")
		else:
			print("*", end="")
	print("\n")


