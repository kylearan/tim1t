#!/usr/bin/python3

import sys
import math
import struct

out = sys.stdout.buffer

cols = [14, 14, 14, 14, 12, 12, 10, 10, 8, 6, 6, 4, 2, 2, 0, 0]
	
for i in range(0, 127):
	value = cols[(i&127)>>3]
	out.write(struct.pack('B', value))
