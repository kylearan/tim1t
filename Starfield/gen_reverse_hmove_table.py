#!/usr/bin/python3

import sys
import math
import struct

hv = [5*16, 4*16, 3*16, 2*16, 1*16, 0*16, 15*16, 14*16, 13*16, 12*16, 11*16, 10*16, 9*16, 8*16, 6*16]
out = sys.stdout.buffer
	
# 160 should be >max star x position
for i in range(0, 160):
	value = hv[((159-i) + 14)%15]
	out.write(struct.pack('B', value))
