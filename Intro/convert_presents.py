import Image
import sys

background = (255, 255, 255)
foreground = (0, 0, 0)

def p(x, y):
	if rgb.getpixel((x, y)) == foreground:
		return 1
	if rgb.getpixel((x, y)) == background:
		return 0
	print "Error at", x, y, ":", rgb.getpixel((x, y))
	sys.exit(1)


img = Image.open("presents.png")
rgb = img.convert('RGB')
if rgb.size[0] != 48:
	print "Width error:", rgb.size[0]
	sys.exit(1)

offset = 255
for s in range(0, 6):
	offset += rgb.size[1]
	if offset > 255:
		offset = 0
		print("\tALIGN $100")
	sys.stdout.write("presents_%d" % (s + 1))
	for i in range(0, rgb.size[1]):
		y = rgb.size[1] - i - 1
		o=s*8
		out = 128*p(o, y) + 64*p(o + 1, y) + 32*p(o + 2, y) + 16*p(o + 3, y) + 8*p(o + 4, y) + 4*p(o + 5, y) + 2*p(o + 6, y) + 1*p(o + 7, y)
		if i%8 == 0:
			sys.stdout.write("\n\t dc.b ")
		sys.stdout.write("%d" % out)
		if i%8 != 7 and i != rgb.size[1] - 1:
			sys.stdout.write(", ")
	sys.stdout.write("\n\n")

