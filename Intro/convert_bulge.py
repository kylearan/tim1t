import Image
import sys

background = (255, 255, 255)
foreground = (0, 0, 0)

def p(x, y):
	if rgb.getpixel((x, y)) == foreground:
		return 1
	if rgb.getpixel((x, y)) == background:
		return 0
	print "Error at", x, y, ":", rgb.getpixel((x, y))
	sys.exit(1)


img = Image.open("bulge.png")
rgb = img.convert('RGB')
if rgb.size[0] != 40:
	print "Width error:", rgb.size[0]
	sys.exit(1)

offset = 255
for s in range(0, 3):
	offset += rgb.size[1]
	if offset > 255:
		offset = 0
		print("\tALIGN $100")
	sys.stdout.write("bulge_%d" % (s + 1))
	
	for i in range(0, rgb.size[1]):
		if s is 0:
			out = 16*p(0, i) + 32*p(1, i) + 64*p(2, i) + 128*p(3, i)
		if s is 1:
			out = 128*p(4, i) + 64*p(5, i) + 32*p(6, i) + 16*p(7, i) + 8*p(8, i) + 4*p(9, i) + 2*p(10, i) + 1*p(11, i)
		if s is 2:
			out = 1*p(12, i) + 2*p(13, i) + 4*p(14, i) + 8*p(15, i) + 16*p(16, i) + 32*p(17, i) + 64*p(18, i) + 128*p(19, i)
		if i%8 == 0:
			sys.stdout.write("\n\t dc.b ")
		sys.stdout.write("%d" % out)
		if i%8 != 7 and i != rgb.size[1] - 1:
			sys.stdout.write(", ")
	sys.stdout.write("\n\n")

