; =========================================================================
; Constants
; =========================================================================

; Script state constants
ZOOM_PAUSE_1		= 0
ZOOM_FIRST_BOX		= 1
ZOOM_PAUSE_2		= 2
ZOOM_SECOND_BOX		= 3
ZOOM_PAUSE_3		= 4
ZOOM_THIRD_BOX		= 5
ZOOM_PAUSE_4		= 6
ZOOM_ZOOMING_BOX	= 7
ZOOM_FADE_BACK		= 8
ZOOM_PAUSE_5		= 9
ZOOM_NEXT_PART		= 10

ZOOM_FIRST_STAY		= 80	; #frames first box will stay between zooms
ZOOM_SECOND_STAY	= 60
ZOOM_THIRD_STAY		= 20

BZOOM_WAIT		= 50	; Delay until backzoomer
BZOOM_DURATION		= BZOOM_WAIT + 53	; Duration of backzoomer part

MORPHER_DURATION	= 80

MORPH_STARTX		= 0
MORPH_STARTWIDTH	= 160
MORPH_STARTY		= 44
MORPH_STARTHEIGHT	= 184

MORPH_ENDX		= 48
MORPH_ENDWIDTH		= 64
MORPH_ENDY		= 53
MORPH_ENDHEIGHT		= 70

MORPH_ADDXLO		= 128
MORPH_ADDXHI		= 1
MORPH_SUBWIDTHLO	= 0
MORPH_SUBWIDTHHI	= 3
MORPH_ADDYLO		= 72
MORPH_ADDYHI		= 0
MORPH_SUBHEIGHTLO	= 10
MORPH_SUBHEIGHTHI	= 2



; =========================================================================
; Start
; =========================================================================

ZoomerStart

	; inits
	; ------------------------------
	
	; gfx
	lda #0
	sta CTRLPF
	sta COLUBK
	sta NUSIZ0
	sta NUSIZ1
	sta CTRLPF
	sta ENAM0
	sta ENAM1
	sta ENABL
	sta VDELP0
	sta VDELP1
	
	ldx #14
.resetMusic
	sta TempVars,x
	dex
	bpl .resetMusic
	
	; Demo state
	sta ZoomerNumFrame
	sta ZoomerDemoState

	jmp ZoomerIncoming
	
	
; =========================================================================
; Kernel
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

ZoomerKernel
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

ZoomerIncoming

	; Script counter
	inc ZoomerNumFrame
	lda ZoomerNumFrame
	ldx ZoomerDemoState
	cmp ZoomerScriptDurations,x 
	bne .noNextState
	inc ZoomerDemoState
	lda ZoomerDemoState
	cmp #ZOOM_NEXT_PART
	bne .noNextPart
	JMB BeginningStart

.noNextPart
	lda #0
	sta ZoomerNumFrame
.noNextState

; -------------------------------------------------------------------------

	lda ZoomerDemoState
	cmp #ZOOM_FIRST_BOX
	bne ZoomerNoFirst

ZoomerFirstBox SUBROUTINE
	
	; First box, slowly peeking
	; Box x
	lda ZoomerNumFrame
	lsr
	lsr
	lsr
	lsr
	clc
	adc #30
	sta LineXPos
	; Box Y
	lda ZoomerNumFrame
	lsr
	lsr
	lsr
	lsr
	lsr
	clc
	adc #30
	sta BoxY
	
	; Check state
	lda ZoomerNumFrame
	cmp #32
	bcs .notZoomIn
	; Zoom in
	jmp ZoomerCalcSize
	
.notZoomIn
	cmp #ZOOM_FIRST_STAY+32
	bcc ZoomerCorrectX
	; Zoom out
	sec
	sbc #ZOOM_FIRST_STAY+32
	sta ZoomerTmp
	lda #32
	sec
	sbc ZoomerTmp
	; Rest as in zoom in

; -------------------------------------------------------------------------

ZoomerCalcSize
	sta ZoomerTmp
	; size
	clc
	adc #2
	lsr
	sta LineWidth
	asl
	sta BoxHeight
	; color
	lda ZoomerTmp
	lsr
	lsr
	clc
	adc #$90
	sta COLUPF
	sta COLUP0
	sta COLUP1

; -------------------------------------------------------------------------

	; correct x and y for size
ZoomerCorrectX
	lda LineWidth
	lsr
	sta LineTmp
	lda LineXPos
	sec
	sbc LineTmp
	sta LineXPos
	lda BoxY
	sec
	sbc LineTmp
	sta BoxY
	jmp ZoomerCalcBoxParams

; -------------------------------------------------------------------------

ZoomerNoFirst
	cmp #ZOOM_SECOND_BOX
	bne ZoomerNoSecond

ZoomerSecondBox SUBROUTINE
	
	; Second box, moving smaller and faster to the lower right
	; Box x
	lda ZoomerNumFrame
	clc
	adc #25
	sta LineXPos
	; Box Y
	lda ZoomerNumFrame
	lsr
	sta ZoomerTmp
	asl
	asl
	sec
	sbc ZoomerTmp
	clc
	adc #20
	sta BoxY
	
	; Check state
	lda ZoomerNumFrame
	cmp #16
	bcs .notZoomIn
	; Zoom in
	jmp ZoomerCalcSize
		
.notZoomIn
	cmp #ZOOM_SECOND_STAY+16
	bcc ZoomerCorrectX
	; Zoom out
	sec
	sbc #ZOOM_SECOND_STAY+16
	sta ZoomerTmp
	lda #16
	sec
	sbc ZoomerTmp
	; Rest as in zoom in
	jmp ZoomerCalcSize

; -------------------------------------------------------------------------

ZoomerNoSecond
	cmp #ZOOM_THIRD_BOX
	bne ZoomerNoThird

ZoomerThirdBox SUBROUTINE
	
	; Third box, quickly taking a static peek before ZOOMING IN
	; Box x
	lda #110
	sta LineXPos
	; Box Y
	lda #160
	sta BoxY
	
	; Check state
	lda ZoomerNumFrame
	cmp #24
	bcs .notZoomIn
	; Zoom in
	jmp ZoomerCalcSize
		
.notZoomIn
	cmp #ZOOM_THIRD_STAY+24
	bcc ZoomerCorrectX
	; Zoom out
	sec
	sbc #ZOOM_THIRD_STAY+24
	sta ZoomerTmp
	lda #24
	sec
	sbc ZoomerTmp
	; Rest as in zoom in
	jmp ZoomerCalcSize
	

; -------------------------------------------------------------------------

ZoomerNoThird
	cmp #ZOOM_ZOOMING_BOX
	bne ZoomerNoZooming

	; Zooming in at parabola speed
	lda ZoomerNumFrame
	tax
	lda ZoomerParabola,x
	; xpos
	sta ZoomerTmp
	lda #106
	sec
	sbc ZoomerTmp
	sta LineXPos
	; width
	lda ZoomerTmp
	lsr
	sec		; +1 for minimum 1
	adc ZoomerTmp
	sta LineWidth
	; ypos
	lda #150
	sec
	sbc ZoomerTmp
	sbc ZoomerTmp
	bcs .noUnderflow
	lda #0
.noUnderflow
	sta BoxY
	; height
	lda ZoomerTmp
	asl
	sec		; +1 for minimum 1
	adc ZoomerTmp
	bcc .noOverflow
	lda #225
.noOverflow
	cmp #225
	bcc .noMax
	lda #225
.noMax
	sta BoxHeight
	; color
	lda ZoomerTmp
	lsr
	lsr
	lsr
	clc
	adc #$90
	sta COLUPF
	sta COLUP0
	sta COLUP1
	jmp ZoomerCalcBoxParams

; -------------------------------------------------------------------------

ZoomerNoZooming
	cmp #ZOOM_FADE_BACK
	bne .noFadeBack
	lda #32
	sec
	sbc ZoomerNumFrame
	lsr
	clc
	adc #$90
	sta COLUBK
	jmp .fadeBack 
	
	; hide box
.noFadeBack
	lda #0
.fadeBack
	sta COLUPF
	sta COLUP0
	sta COLUP1
	lda #1
	sta LineXPos
	sta LineWidth
	sta BoxY
	sta BoxHeight
	
; -------------------------------------------------------------------------

ZoomerCalcBoxParams
	; prepare box parameters
	jsr FillLine
	; position p0/1 for box
	lda LineXPos
	ldx #0
	jsr ZoomerPositionObject
	sta WSYNC
	lda #0
	sta HMP0
	lda s1Pos
	inx
	jsr ZoomerPositionObject
	
	
; -------------------------------------------------------------------------


	jsr ZoomerWaitForIntim
	lda #0
	sta VBLANK


; -------------------------------------------------------------------------
; Visible area: 228 lines
; -------------------------------------------------------------------------

DisplayBox SUBROUTINE

	ldx BoxY
	beq .noWasteTop
.wasteTop
	sta WSYNC
	dex
	bne .wasteTop
.noWasteTop
	
	jsr ZoomerDisplayBox
	
	lda #226
	sec
	sbc BoxY
	sbc BoxHeight
	tax
	beq .noWasteBottom
.wasteBottom
	sta WSYNC
	dex
	bne .wasteBottom
.noWasteBottom
	

; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

ZoomerDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	JSB MusicPlayer

	jsr ZoomerWaitForIntim

	jmp ZoomerKernel



; =========================================================================
; Backzoomer: Transition between Cluster and TIM1T build-up
; =========================================================================

BackZoomerStart SUBROUTINE

	; inits
	; ------------------------------
	
	; gfx
	lda #0
	sta CTRLPF
	sta NUSIZ0
	sta NUSIZ1
	sta ENAM0
	sta ENAM1
	sta ENABL
	; Demo state
	sta ZoomerNumFrame
	sta ZoomerDemoState

	jsr ZoomerClearGrpPf
	jmp BackZoomerIncoming

	
	
; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

BackZoomerKernel SUBROUTINE
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc ZoomerNumFrame
	lda ZoomerNumFrame
	cmp #BZOOM_DURATION
	bne .noNextPart
	JMB IntroStart
.noNextPart

; -------------------------------------------------------------------------

BackZoomerIncoming
	; Zooming out at parabola speed, 2 halves (top and bottom)
	lda ZoomerNumFrame
	cmp #BZOOM_WAIT
	bcs .doBackZoom

	; Delay: Hide squares
	ldx #0
	stx BZoomerWaitState
	jmp .afterBoxParams
	
.doBackZoom
	ldx #1
	stx BZoomerWaitState

	; Upper square
	lda #0
	sta BZoomerBackCol
	lda #BZOOM_WAIT + 63
	sec
	sbc ZoomerNumFrame
	bcs .noUnderflowX
	lda #0
.noUnderflowX
	tax
	lda ZoomerParabola,x
	sta BoxHeight
	lsr
	sta ZoomerTmp
	
	; xpos
	lda #102
	sec
	sbc BoxHeight
	lsr
	sta LineXPos
	
	; width
	lda BoxHeight
	sec		; +1 for minimum 1
	adc ZoomerTmp
	sta LineWidth
	
	; color
	lda #$90
	sta COLUPF
	sta COLUP0
	sta COLUP1

.doBoxParams
	; prepare box parameters and save them
	jsr FillLine
	lda LineXPos
	sta LineXPos_2
	lda s0
	sta s0_2
	lda s1
	sta s1_2
	lda s1Pos
	sta s1Pos_2
	lda p0
	sta p0_2
	lda p1
	sta p1_2
	lda p2
	sta p2_2
	lda p3
	sta p3_2
	lda p4
	sta p4_2
	lda p5
	sta p5_2
	
	; Lower square
	; xpos
	lda #160
	sec
	sbc LineXPos
	sbc LineWidth
	sta LineXPos
	jsr FillLine

	; position p0/1 for upper box
	lda LineXPos_2
	ldx #0
	jsr ZoomerPositionObject
	sta WSYNC
	lda #0
	sta HMP0
	lda s1Pos_2
	inx
	jsr ZoomerPositionObject
	
	; Prepare color of top/bottom line
	lda ZoomerNumFrame
	sec
	sbc #BZOOM_WAIT
	bcs .noWaitPhase
	lda #0
.noWaitPhase
	lsr
	lsr
	clc
	adc #$90
	sta BZoomerLineCol

	
; -------------------------------------------------------------------------

.afterBoxParams
	jsr ZoomerWaitForIntim
	lda #0
	sta VBLANK


; -------------------------------------------------------------------------
; Visible area: 228 lines
; -------------------------------------------------------------------------

BackZoomerDisplayBox SUBROUTINE

	lda BZoomerWaitState
	bne .showZooming
	
	; Wait state: Blue screen
	lda #$90
	sta COLUBK
	WASTE 227
	jmp BackZoomerDoVBlank
	
	; Zooming state: Display squares
.showZooming
	; top line
	sta WSYNC
	lda BZoomerLineCol
	sta COLUBK
	sta WSYNC
	lda BZoomerBackCol
	sta COLUBK
	sta WSYNC

	; top square
	ldx BoxHeight
.displayBox
	sta WSYNC
	lda s0_2
	sta GRP0
	lda s1_2
	sta GRP1
	lda p0_2
	sta PF0
	lda p1_2
	sta PF1
	lda p2_2
	sta PF2
	lda p3_2
	sta PF0
	lda p4_2
	sta PF1
	SLEEP 4
	lda p5_2
	sta PF2
	dex
	bne .displayBox

	sta WSYNC
	jsr ZoomerClearGrpPf
	
	lda #105
	jsr ZoomerWasteMinusBoxLines
	sta WSYNC

	; bottom square
	lda #106
	jsr ZoomerWasteMinusBoxLines

	; hack for stable scanline count
	lda ZoomerNumFrame
	cmp #BZOOM_WAIT+26
	bcc .noHack
	sta WSYNC
.noHack
	
	; position p0/1 for lower square
	lda LineXPos
	ldx #0
	jsr ZoomerPositionObject
	sta WSYNC
	lda #0
	sta HMP0
	lda s1Pos
	inx
	jsr ZoomerPositionObject

	; display square
	jsr ZoomerDisplayBox
	
	; bottom line	
	sta WSYNC
	lda BZoomerLineCol
	sta COLUBK
	sta WSYNC
	lda #0
	sta COLUBK


; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

BackZoomerDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	JSB MusicPlayer

	jsr ZoomerWaitForIntim

	jmp BackZoomerKernel



; =========================================================================
; Morpher: Transition between Plasma and 4in1
; =========================================================================

MorpherStart SUBROUTINE

	; inits
	; ------------------------------
	
	; gfx
	lda #0
	sta CTRLPF
	sta NUSIZ0
	sta NUSIZ1
	sta ENAM0
	sta ENAM1
	sta ENABL

	jsr ZoomerClearGrpPf
	
	; Demo state
	sta ZoomerNumFrame
	sta ZoomerDemoState

	; Box init
	lda #0
	sta MorphBoxXLo
	sta MorphBoxYLo
	sta MorphBoxWidthLo
	sta MorphBoxHeightLo

	sta MorphBoxXHi
	lda #160
	sta MorphBoxWidthHi
	lda #44
	sta MorphBoxYHi
	lda #140
	sta MorphBoxHeightHi
	
	jmp MorpherIncoming

	
	
; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

MorpherKernel SUBROUTINE
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc ZoomerNumFrame
	lda ZoomerNumFrame
	cmp #MORPHER_DURATION
	bne .noNextPart
	JMB FourStart
.noNextPart

; -------------------------------------------------------------------------

MorpherIncoming

	; Advance morph box
	lda MorphBoxXHi
	cmp #MORPH_ENDX
	beq .xEnd
	lda MorphBoxXLo
	clc
	adc #MORPH_ADDXLO
	sta MorphBoxXLo
	lda MorphBoxXHi
	adc #MORPH_ADDXHI
	sta MorphBoxXHi
.xEnd	

	lda MorphBoxYHi
	cmp #MORPH_ENDY
	beq .yEnd
	lda MorphBoxYLo
	clc
	adc #MORPH_ADDYLO
	sta MorphBoxYLo
	lda MorphBoxYHi
	adc #MORPH_ADDYHI
	sta MorphBoxYHi
.yEnd	

	lda MorphBoxWidthHi
	cmp #MORPH_ENDWIDTH
	beq .endWidth
	lda MorphBoxWidthLo
	sec
	sbc #MORPH_SUBWIDTHLO
	sta MorphBoxWidthLo
	lda MorphBoxWidthHi
	sbc #MORPH_SUBWIDTHHI	
	sta MorphBoxWidthHi
.endWidth

	lda MorphBoxHeightHi
	cmp #MORPH_ENDHEIGHT
	beq .endHeight
	lda MorphBoxHeightLo
	sec
	sbc #MORPH_SUBHEIGHTLO
	sta MorphBoxHeightLo
	lda MorphBoxHeightHi
	sbc #MORPH_SUBHEIGHTHI	
	sta MorphBoxHeightHi
.endHeight
	
	 ; prepare draw
	lda MorphBoxXHi
	sta LineXPos
	lda MorphBoxWidthHi
	sta LineWidth
	jsr FillLine 

	; set up objects
	lda MorphBoxXHi
	ldx #0
	jsr ZoomerPositionObject
	sta WSYNC
	lda #0
	sta HMP0
	lda s1Pos
	inx
	jsr ZoomerPositionObject
	
	; and colors
	lda ZoomerNumFrame
	cmp #31
	bcc .smaller64
	lda #31
.smaller64
	lsr
	tax
	lda MorphBlueToGreen,x
	sta COLUBK
	lda MorphBlackToRed,x
	sta COLUPF
	sta COLUP0
	sta COLUP1
	
	
; -------------------------------------------------------------------------

	jsr ZoomerWaitForIntim
	lda #0
	sta VBLANK


; -------------------------------------------------------------------------
; Visible area: 228 lines
; -------------------------------------------------------------------------

MorpherDisplayBox SUBROUTINE

	ldx MorphBoxYHi
.wasteTop
	sta WSYNC
	dex
	bne .wasteTop
	
	lda MorphBoxHeightHi
	sta BoxHeight
	jsr ZoomerDisplayBox
	
	lda #226
	sec
	sbc MorphBoxYHi
	sbc MorphBoxHeightHi
	tax
.wasteBottom
	sta WSYNC
	dex
	bne .wasteBottom



; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

MorpherDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	JSB MusicPlayer

	jsr ZoomerWaitForIntim

	jmp MorpherKernel



; Gradient from blue plasma bg to green 4in1 bg
; -------------------------------------------------------------------------

MorphBlueToGreen
	dc.b $b2, $b2, $92, $92, $92, $72, $72, $72
	dc.b $72, $70, $70, $70, $52, $50, $50, $50

MorphBlackToRed
	dc.b $00, $00, $00, $02, $02, $02, $02, $02
	dc.b $02, $02, $02, $62, $62, $62, $62, $62



; =========================================================================
; Subroutines
; =========================================================================

; -------------------------------------------------------------------------
; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

ZoomerWaitForIntim SUBROUTINE
	lda INTIM
	bne ZoomerWaitForIntim
	sta WSYNC
	rts



; -------------------------------------------------------------------------
; Display box
; -------------------------------------------------------------------------

ZoomerDisplayBox
	ldx BoxHeight
.displayBox
	sta WSYNC
	lda s0
	sta GRP0
	lda s1
	sta GRP1
	lda p0
	sta PF0
	lda p1
	sta PF1
	lda p2
	sta PF2
	lda p3
	sta PF0
	lda p4
	sta PF1
	SLEEP 4
	lda p5
	sta PF2
	dex
	bne .displayBox

	sta WSYNC

; -------------------------------------------------------------------------
; Clear gfx registers
; -------------------------------------------------------------------------

ZoomerClearGrpPf SUBROUTINE	
	lda #0
	sta GRP0
	sta GRP1
	sta PF0
	sta PF1
	sta PF2
	rts



; -------------------------------------------------------------------------
; Waste a-Boxheight lines
; -------------------------------------------------------------------------

ZoomerWasteMinusBoxLines SUBROUTINE
	sec
	sbc BoxHeight
	tax
.wasteTop
	sta WSYNC
	dex
	bpl .wasteTop

	rts



; -------------------------------------------------------------------------
; Fine-position player number in x at position in a.
; Don't mess with HMxx values immediately thereafter.
; -------------------------------------------------------------------------

ZoomerPositionObject SUBROUTINE
	sta WSYNC
	SLEEP 13
	sec
.divide15
	sbc #15
	bcs .divide15
	sta RESP0,x
	eor #$07
	asl
	asl
	asl
	asl
	sta HMP0,x
	sta WSYNC
	sta HMOVE
	rts



; -------------------------------------------------------------------------
; Prepare paramteres for a horizontal line.
; in:
; LineXPos: 0...159
; LineWidth: 1..160
;
; out:
; s0: first sprite
; s1: last sprite
; p0...p5: PF bytes
;
; uses:
; LineTmp, b0...b5
; -------------------------------------------------------------------------

FillLine SUBROUTINE

	; init bx
	; ------------------------------
	lda #0
	sta b0
	sta b1
	sta b2
	sta b3
	sta b4
	
	; construct s0 (0...3 bits set)
	; ------------------------------
	lda #4
	sec
	sbc LineXPos
	and #%00000011
	cmp LineWidth
	bcc .noSmallWidth
	lda LineWidth
.noSmallWidth	
	sta s0Width
	tax
	lda sValues,x
	sta s0

	; construct b0-4
	; ------------------------------
	
	; find first bit to set in bx
	
	; snap to 4pix boundary
	lda LineXPos
	clc
	adc #3
	; convert to 4pix pos
	lsr
	lsr

	; move index to correct byte
	ldx #0		; index to bx
.findbx
	cmp #8
	bcc .foundbx
	inx
	sec
	sbc #8
	bcs .findbx
.foundbx
	; now x holds index to bx, and a holds offset to bit
	
	; move bitmask to correct positon
	tay
	lda #0
	sec
.findbit
	rol
	dey
	bpl .findbit
	; now a has bit set at first position, so can use as set mask
	sta mask
	
	; calc rest of 4pix width to fill: Account for LineXPos hires
	lda LineWidth
	sec
	sbc s0Width
	lsr
	lsr
	beq .no4pFill
	tay
	
	; fill rest of bits in first bx
	lda #0
.fillFirst
	ora mask
	dey
	beq .endFill
	clc			; needed?
	rol mask
	bcc .fillFirst
	sta b0,x
	inx
	
	; fill full bx bytes
	tya
	ldy #%11111111
.fillFull
	cmp #8
	bcc .endFillFull	
	sty b0,x
	inx
	sec
	sbc #8
	bcs .fillFull
.endFillFull
	; now a holds rest of 4pix bits to fill
	
	; fill rest of bits in last bx
	tay
	beq .noRestFill
	lda #0
.fillLast
	sec
	rol
	dey
	bne .fillLast

.endFill
	sta b0,x

.noRestFill

.no4pFill

	; construct s1
	; ------------------------------

	lda LineWidth
	sec
	sbc s0Width
	and #%00000011
	tax
	lda sValues,x
	sta s1
	; calc s1 pos
	stx LineTmp
	lda LineXPos
	clc
	adc LineWidth
	sec
	sbc LineTmp
	sta s1Pos
	
	; convert bx to px.
	; ------------------------------
	
	; P0
	lda b0
	asl
	asl
	asl
	asl
	sta p0

	; P1
	lda #0
	sta LineTmp
	lda b0
	rol
	ror LineTmp
	rol
	ror LineTmp
	rol
	ror LineTmp
	rol
	ror LineTmp

	lda #0
	sta p1
	lda b1
	ror
	rol p1
	ror
	rol p1
	ror
	rol p1
	ror
	rol p1
	
	lda LineTmp
	ora p1
	sta p1
	
	; P2
	lda b1
	lsr
	lsr
	lsr
	lsr
	sta LineTmp
	lda b2
	asl
	asl
	asl
	asl
	ora LineTmp
	sta p2
	
	; P3 = P0 #2
	lda b2
	sta p3
	
	; P4 = P1 #2
	lda b3
	ror
	rol p4
	ror
	rol p4
	ror
	rol p4
	ror
	rol p4
	ror
	rol p4
	ror
	rol p4
	ror
	rol p4
	ror
	rol p4
	
	; P5 = P2 #2
	lda b4
	sta p5
	
	rts


	; Values for s depending on LineXPos/LineWidth
	; ------------------------------
sValues
	dc.b %00000000, %10000000, %11000000, %11100000


	
; =========================================================================
; Tables
; =========================================================================

; Script event durations
; ------------------------------

ZoomerScriptDurations
	dc.b 50				; ZOOM_PAUSE_1		= 0
	dc.b ZOOM_FIRST_STAY+32+32	; ZOOM_FIRST_BOX	= 1
	dc.b 75				; ZOOM_PAUSE_2		= 2
	dc.b ZOOM_SECOND_STAY+16+16	; ZOOM_SECOND_BOX	= 3
	dc.b 100			; ZOOM_PAUSE_3		= 4
	dc.b ZOOM_THIRD_STAY+24+24	; ZOOM_THIRD_BOX	= 5
	dc.b 50				; ZOOM_PAUSE_4		= 6
	dc.b 63				; ZOOM_ZOOMING_BOX	= 7
	dc.b 31				; ZOOM_FADE_BACK	= 8
	dc.b 50				; ZOOM_PAUSE_5		= 9


; Parabola for zoom
; ------------------------------

ZoomerParabola
	include zoom_parabola.asm
