; NOtes:
; - AdvanceTopSquares: calc dy: might be +14? (because sink in?)
; - Caution: Code path in "BottomSquares" can end up in .wasteRest and
;   be longer than 76 cycles, causing an extra scanline?



; The kernel is divided into five parts:
; 1) Display top (falling) squares
; 2) Display top logo line under construction
; 3) Display finished logo lines
; 4) Display bottom logo line under construction
; 5) Display bottom (rising) squares
;
; 1):
; - Display TopYPos empty lines. Increase each frame; simulates falling.
; After reaching 14, reset to zero and generate new falling square. If no new
; square needs to be generated, simply let it increase more.
; 14 because we need 2 lines to reposition sprite and 12 to display it.
; Actually, current animation only has 8 lines of gfx, but we want some
; empty space inbewteen squares so no more than 8 can exist at the same
; time.
;
; - Display squares. Ring buffer of top squares is in TopXPos, TopAnimState,
; TopCurrentInc (Counter for when to incease TopXPos) and TopXIncMax (start
; value for TopCurrentInc). TopSquareIndex points into first element in ring
; buffer. Ring buffer terminates with a 0 in TopXPos.
; Stop displaying sprites if end of ring buffer is reached.
; Stop altogether if max number of top lines is reached.
; If fewer than 4 lines are left to display when starting a new square,
; don't display that square (needs 2 lines setup anyway) and mark index
; in TopFallenSquare as a candidate for a completely fallen square.
; This is checked later during square advancement; 255 means no square
; has fallen completely yet.
;
; 2) Display LogoTopPixels asymmetrically.
;
; 3) Display Logo according to TopPixelCurrentLine and BottomPixelCurrentLine.
;
; 4) Display LogoBottomPixels asymmetrically.
;
; 5) Display Squares. Similar to 1), with these differences:
; - BottomYPos denotes how many lines have to be skipped until first
; square gets displayed. Actually BottomYPos-12 lines have to
; skipped. Values 1-11 denote lines to be skipped for first square.
;
; - After displaying all squares, the number of lines left to skip
; (IntroCurrentLine) reveals when to let a new square appear (at 14).




; =========================================================================
; Constants
; =========================================================================

; number of animation states for squares
IN_SQU_MAXANIM_COL	= 16



; =========================================================================
; Start
; =========================================================================

IntroStart

; optional, safe inits
	lda #0
	sta VDELP0
	sta VDELP1
	sta GRP0
	sta GRP1
	sta ENABL
	sta NUSIZ0
	sta NUSIZ0
	sta CTRLPF
	sta COLUBK
	sta ENAM0
	sta ENAM1
	sta ENABL

	; Demo state
	sta IntroNumFrame
	sta IntroDemoState

	sta LogoTopPixels+0
	sta LogoTopPixels+1
	sta LogoTopPixels+2
	sta LogoTopPixels+3
	sta LogoTopPixels+4
	sta LogoBottomPixels+0
	sta LogoBottomPixels+1
	sta LogoBottomPixels+2
	sta LogoBottomPixels+3
	sta LogoBottomPixels+4

	; Top init
	lda #2
	sta TopPixelCurrentLine
	sta TopSendCurrentLine
	lda #1
	sta TopPixelIndex
	sta TopFillIndex
	lda Logo_Top
	sta TopPixelsInLine
	sta TopFillLeftInLine
	lda #14
	sta TopYPos	
	lda #0
	sta TopSquareIndex
	sta TopXPos
	
	; Bottom init
	lda #3
	sta BottomPixelCurrentLine
	sta BottomSendCurrentLine
	lda #1
	sta BottomPixelIndex
	sta BottomFillIndex
	lda Logo_Bottom
	sta BottomPixelsInLine
	sta BottomFillLeftInLine
	ldy BottomLines
	clc			; add #14 to account for sunken square
	adc #14
	sty BottomYPos
	lda #0
	sta BottomSquareIndex
	sta BottomXPos

	jmp IntroIncoming
	
	
; =========================================================================
; Kernel
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

IntroKernel
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; test for end of part
	lda IntroDemoState
	bpl .noEnd
	JMB StarfieldStart
.noEnd
	; Script counter
	inc IntroNumFrame
	lda IntroNumFrame
	ldx IntroDemoState
	cmp IntroScriptDurations,x 
	bne .noNextState
	inc IntroDemoState
	lda #0
	sta IntroNumFrame
.noNextState


IntroIncoming
	jsr IntroWaitForIntim
	lda #0
	sta VBLANK
	
	; necessary init?
	lda #$0e
	sta COLUP0
	lda #$06
	sta COLUP1
	lda #$de
	sta COLUPF
	lda #0
	sta CTRLPF


; -------------------------------------------------------------------------
; Visible area: 228 lines
; -------------------------------------------------------------------------

	sta WSYNC
	lda #$ec
	sta COLUBK
	sta WSYNC
	lda #0
	sta COLUBK

TopSquares SUBROUTINE	
	; 77 lines for full logo, more during construction

	; init line counter
	ldx TopPixelCurrentLine
	inx			; account for -1 (finished state)
	lda TopLines,x
	sta IntroCurrentLine
	; init TopFallenSquare: 255 = no square has completed falling.
	; This is checked later during square advancement.
	lda #255
	sta TopFallenSquare

	; waste TopYPos lines
	ldx TopYPos
.wasteTopY
	sta WSYNC
	dec IntroCurrentLine
	beq .afterTopSquares	; for last square, TopYPos will be >19
	dex
	bpl .wasteTopY
	
	; Display squares
	ldx TopSquareIndex
.displayTopSquares
	; make sure x still points into ring buffer
	txa
	and #$07
	tax

	; if fewer than 4 lines are to display, don't start a new square
	lda IntroCurrentLine
	cmp #4
	bcs .displayNewSquare
	; store current ring buffer index to check later if square has
	; completed its fall and pixel has to be set.
	cmp #3
	bne .noFallenSquare
	stx TopFallenSquare
.noFallenSquare
	jmp .wasteTopRest
.displayNewSquare
	
	; position sprite
	lda TopXPos,x
	beq .wasteTopRest	; 0 = end of ring buffer
	sta WSYNC
	SLEEP 14
	sec
.divide15
	sbc #15
	bcs .divide15
	sta RESP0
	; Do immediate WSYNC b/c we don't know how far to the right we are
	; already, and don't want to lose count of used scanlines.
	sta WSYNC
	eor #$07
	asl
	asl
	asl
	asl
	sta HMP0
	; Set color
	ldy TopAnimState,x
	lda SquareAnimColors,y
	cpy #IN_SQU_MAXANIM_COL
	bcc .noMaxCol
	lda SquareAnimColors+IN_SQU_MAXANIM_COL
.noMaxCol
	sta COLUP0
	; Get ptr to GFX
	tya
	and #$0f
	tay
	lda SquareAnimGfx,y
	tay
	; account for square setup WSYNCs
	dec IntroCurrentLine
	dec IntroCurrentLine
	dec IntroCurrentLine
	; do wsync/hmove
	sta WSYNC
	sta HMOVE
		
	; display square
	lda #12
	sta IntroTmp
.displaySquare
	lda Square_GFX,y
	sta GRP0
	iny
	sta WSYNC
	dec IntroCurrentLine
	beq .afterTopSquares
	dec IntroTmp
	bne .displaySquare
	lda #0
	sta GRP0
	
	; next square
	inx
	jmp .displayTopSquares

	; waste rest
.wasteTopRest
	ldy IntroCurrentLine
	beq .afterTopSquares
.wasteRestLine
	sta WSYNC
	dey
	bne .wasteRestLine
.afterTopSquares
	lda #0
	sta GRP0

	
; -------------------------------------------------------------------------
	
IntroDisplayLogo SUBROUTINE

	; display top buildup line
	lda TopPixelCurrentLine
	bmi .noTopPixels
	ldy #6
.topPixels
	sta WSYNC
	lda #0
	sta PF0
	lda LogoTopPixels+0
	sta PF1
	lda LogoTopPixels+1
	sta PF2
	lda LogoTopPixels+2
	SLEEP 10	; @23+7
	sta PF0
	lda LogoTopPixels+3
	SLEEP 4		; @37+3
	sta PF1
	lda LogoTopPixels+4
	SLEEP 4		; @47+3
	sta PF2
	dey
	bne .topPixels
.noTopPixels

; -------------------------------------------------------------------------
	
	; display finished lines
	ldx TopPixelCurrentLine
.logo_x
	inx
	cpx BottomPixelCurrentLine
	beq .endFinishedLogo
	ldy #6
.logo_y
	sta WSYNC
	lda #0
	sta PF0
	lda Logo_1,x
	sta PF1
	lda Logo_2,x
	sta PF2
	lda Logo_3,x
	SLEEP 7		; @23+7
	sta PF0
	lda Logo_4,x
	SLEEP 3		; @37+3
	sta PF1
	lda Logo_5,x
	SLEEP 3		; @47+3
	sta PF2
	dey
	bne .logo_y
	jmp .logo_x
.endFinishedLogo

	
; -------------------------------------------------------------------------

	; coming in at cycle @68, so do WSYNC at end of loop
	; display bottom buildup
	lda BottomPixelCurrentLine
	cmp #7
	beq .noBottomPixels
	ldy #6
.BottomPixels
	lda #0
	sta PF0
	lda LogoBottomPixels+0
	sta PF1
	lda LogoBottomPixels+1
	sta PF2
	lda LogoBottomPixels+2
	SLEEP 10	; @20+10
	sta PF0
	lda LogoBottomPixels+3
	SLEEP 4		; @36+4
	sta PF1
	lda LogoBottomPixels+4
	SLEEP 4		; @46+4
	sta PF2
	sta WSYNC
	dey
	bne .BottomPixels
.noBottomPixels

	lda #0
	sta PF0
	sta PF1
	sta PF2


; -------------------------------------------------------------------------

BottomSquares SUBROUTINE
	; If BottomYPos >= 12:
	;     display BottomYPos-12 empty lines (or zero), then squares
	; If BottomYPos < 12:
	;    Display only BottomYPos lines of first square
	; If BottomYPos = 0:
	;    Set to 14 again and fill pixel.
	;    Should not happen during display!
	; if after displaying all squares, IntroCurrentLine = 14(?):
	;     send new square
	;
	; Caution: If line is filled completely, sub #6 from BottomYPos
	;   so other squares remain on same y pos


	; Skip BottomYPos lines if needed

	; init line counter
	ldx BottomPixelCurrentLine
	lda BottomLines-3,x	; -3 because first line is #3
	sta IntroCurrentLine
	; init BottomSendSquare: 255 = no square has to be sent.
	; This is checked later during square advancement.
	lda #255
	sta BottomSendSquare
	
	ldy BottomYPos
	cpy #12
	bcs .skipFirstLines
	; BottomYPos equals number of lines to display for first "sinking" square
	sty IntroSquareNumLines
	bcc .afterSkip		; unconditional jmp
	
	; Skip BottomYPos-12 lines b/c first square hasn't reached start yet.
	; Can be zero.
.skipFirstLines
	lda #12			; First square will be displayed completely
	sta IntroSquareNumLines
	; Skip BottomYPos - 12 lines
	tya
	sbc #12			; carry is still set, so no sec needed
	beq .afterSkip
	tay
.skipFirstLoop
	sta WSYNC
	dec IntroCurrentLine
	bmi .afterBottomSquares
	dey
	bne .skipFirstLoop
.afterSkip

	; Display squares
	ldx BottomSquareIndex
.displayBottomSquares
	; make sure x still points into ring buffer
	txa
	and #$07
	tax

	; position sprite
	lda BottomXPos,x
	beq .wasteBottomRest	; 0 = end of ring buffer
	sta WSYNC
	SLEEP 14
	sec
.divide15
	sbc #15
	bcs .divide15
	sta RESP0
	; Do immediate WSYNC b/c we don't know how far to the right we are
	; already, and don't want to lose count of used scanlines.
	sta WSYNC
	eor #$07
	asl
	asl
	asl
	asl
	sta HMP0
	; Set color
	ldy BottomAnimState,x
	lda SquareAnimColors,y
	cpy #IN_SQU_MAXANIM_COL
	bcc .noMaxCol
	lda SquareAnimColors+IN_SQU_MAXANIM_COL
.noMaxCol
	sta COLUP0
	; Get ptr to GFX
	tya
	and #$0f
	tay
	lda SquareAnimGfx,y
	; correct for potential sinking square
	clc
	adc #12
	sec
	sbc IntroSquareNumLines
	tay
	; account for square setup WSYNCs
	dec IntroCurrentLine
	dec IntroCurrentLine
	dec IntroCurrentLine
	; do wsync/hmove
	sta WSYNC
	sta HMOVE
		
	; display square. IntroSquareNumLines is 12 (or maybe less for first)
.displaySquare
	lda Square_GFX,y
	sta GRP0
	iny
	sta WSYNC
	dec IntroCurrentLine
	bmi .afterBottomSquares
	dec IntroSquareNumLines
	bne .displaySquare
	lda #0
	sta GRP0
	; Next square will be 12 lines no matter what
	lda #12
	sta IntroSquareNumLines
	
	; next square
	inx
	jmp .displayBottomSquares

	; waste rest
.wasteBottomRest
	ldy IntroCurrentLine
	bmi .afterBottomSquares
	; send new square at #lines == 14
	cpy #15
	bne .wasteRestLine
	stx BottomSendSquare
.wasteRestLine
	sta WSYNC
	dey
	bne .wasteRestLine
.afterBottomSquares
	lda #$00
	sta GRP0

	WASTE 23

	sta WSYNC
	lda #$ec
	sta COLUBK
	sta WSYNC
	lda #0
	sta COLUBK

	
	
; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

IntroDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	jsr AdvanceTopSquares
	jsr AdvanceBottomSquares
	
	JSB MusicPlayer

	jsr IntroWaitForIntim

	jmp IntroKernel

	
; =========================================================================
; Subroutines
; =========================================================================


; -------------------------------------------------------------------------
; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

IntroWaitForIntim
	lda INTIM
	bne IntroWaitForIntim
	sta WSYNC
	rts



; -------------------------------------------------------------------------
; Advance top squares.
; Do x and anim advancement.
; If number of lines to waste exceed 18, add new falling square if there
; are still squares left.
; If TopFallenSquare is != 255, remove square and paint pixel. If all
; pixels have been painted in a line, advance line.
; -------------------------------------------------------------------------

AdvanceTopSquares SUBROUTINE

	; Advance x positions and anim states
	ldx TopSquareIndex
.advanceSquare
	; make sure x still points into ring buffer
	txa
	and #$07
	tax
	; Test for end of ring buffer
	lda TopXPos,x
	beq .afterAdvance
	
	; Bresenham step
	lda TopBresenError,x
	sec
	sbc TopBresenDx,x
	bpl .noXAdvance
	; move 1px to right
	inc TopXPos,x
	clc
	adc TopBresenDy,x
.noXAdvance
	sta TopBresenError,x

	; advance anim state
	inc TopAnimState,x

	; next
	inx
	bne .advanceSquare	; unconditional jmp
.afterAdvance

; -------------------------------------------------------------------------
	
	; increase TopYPos. If 15 is reached, reset and add new square
	ldy TopYPos
	iny
	cpy #15
	bne .noNewSquare

	; add new square to send.
	ldy TopPixelIndex	; ldy so sty TopYPos at .noNewSquare works
	bne .squaresLeft	;  Still squares left to send
	ldy TopYPos		; no square left: increase TopYPos >19
	iny
	bne .noNewSquare	; unconditional jmp
.squaresLeft
	; Decrease ring buffer index, so new square appears on top.
	ldx TopSquareIndex
	dex
	txa
	and #$07
	sta TopSquareIndex
	tax

	; Calc square movement (Bresenham).
	; Send squares from x=10. If dx > dy, use bigger x accordingly.
	; First, calc dy and starting error
	ldy TopSendCurrentLine
	lda TopLines,y
	sta TopBresenDy,x
	lsr
	sta TopBresenError,x
	; Now dx. PixelPos*4 + 4*4 is target x coordinate, 10 is start.
	ldy TopPixelIndex
	lda Logo_Top,y
	rol			; *4 + 4*4 is target coordinate
	rol
	adc #9			; 4*4 - 10 + visual correction
	; test for dx > dy
	cmp TopBresenDy,x	; test for dx > dy
	bcc .canStartAt10
	; Start x has to be >10, i.e. targetX - dy
	adc #10
	sec
	sbc TopBresenDy,x
	sta TopXPos,x
	lda TopBresenDy,x
	sta TopBresenDx,x
	bne .afterBresen	; unconditional jmp
.canStartAt10
	sta TopBresenDx,x
	lda #6
	sta TopXPos,x
.afterBresen
	
	; init Anim
	lda #0
	sta TopAnimState,x

	; move index to next pixel
	inc TopPixelIndex
	dec TopPixelsInLine
	bne .noNewSendLine
	; new pixel send line
	dec TopSendCurrentLine
	ldy TopPixelIndex
	lda Logo_Top,y
	bpl .noEnd		; 255 means end
	lda #0
	sta TopPixelIndex
	beq .noNewSendLine
.noEnd
	sta TopPixelsInLine
	iny
	sty TopPixelIndex	

.noNewSendLine
	ldy #0
		
.noNewSquare
	sty TopYPos

; -------------------------------------------------------------------------

	; Check for completely fallen square
	ldx TopFallenSquare
	bmi .noCompletedSquare
	lda TopXPos,x
	beq .noCompletedSquare

	; draw pixel
	ldy TopFillIndex
	lda Logo_Top,y
	asl
	tay
	lda Logo_SetPixelData+1,y	; ora mask
	sta IntroTmp
	lda Logo_SetPixelData,y		; index into LogoTopPixel
	tay
	lda LogoTopPixels,y
	ora IntroTmp
	sta LogoTopPixels,y
	
	; advance TopFillIndex
	inc TopFillIndex
	dec TopFillLeftInLine
	bne .noNewFillLine
	; next line
	ldy TopFillIndex
	lda Logo_Top,y
	bpl .noEndOfPart
	sta IntroDemoState		; end of part
.noEndOfPart
	sta TopFillLeftInLine
	iny
	sty TopFillIndex
	dec TopPixelCurrentLine
	; Clear logo line buffer
	lda #0
	sta LogoTopPixels+0
	sta LogoTopPixels+1
	sta LogoTopPixels+2
	sta LogoTopPixels+3
	sta LogoTopPixels+4
.noNewFillLine
		
	; remove square from ring buffer
	lda #0
	sta TopXPos,x

.noCompletedSquare
	rts



; -------------------------------------------------------------------------
; Advance Bottom squares.
; Do x and anim advancement.
; If number of lines to waste after displaying all squares is 14, add new
; falling square if there ; are still squares left.
; If BottomYPos is reaches 0, remove square and paint pixel. If all
; pixels have been painted in a line, advance line.
; -------------------------------------------------------------------------

AdvanceBottomSquares SUBROUTINE

	; Advance x positions and anim states
	ldx BottomSquareIndex
.advanceSquare
	; make sure x still points into ring buffer
	txa
	and #$07
	tax
	; Test for end of ring buffer
	lda BottomXPos,x
	beq .afterAdvance
	
	; Bresenham step
	lda BottomBresenError,x
	sec
	sbc BottomBresenDx,x
	bpl .noXAdvance
	; move 1px to left
	dec BottomXPos,x
	clc
	adc BottomBresenDy,x
.noXAdvance
	sta BottomBresenError,x

	; advance anim state
	inc BottomAnimState,x

	; next
	inx
	bne .advanceSquare	; unconditional jmp
.afterAdvance

; -------------------------------------------------------------------------
	
	; Check if we have to send a new square
	ldx BottomSendSquare
	bmi .noNewSquare	; 255 means no new square
		
	; add new square to send.
	lda BottomPixelIndex
	beq .noNewSquare	; 0 means no squares left to send

	; x is index of first free position (tail) in ring buffer

	; Calc square movement (Bresenham).
	; Send squares from x=150. If dx > dy, use bigger x accordingly.
	; First, calc dy and starting error
	ldy BottomSendCurrentLine
	lda BottomLines-3,y	; -3 because line #3 is the first to fill
	sta BottomBresenDy,x
	lsr
	sta BottomBresenError,x
	; Now dx. PixelPos*4 + 4*4 is target x coordinate, 150 is start.
	ldy BottomPixelIndex
	lda Logo_Bottom,y
	rol			; *4 + 4*4 is target coordinate
	rol
	adc #12			; looks better
	sta IntroTmp
	lda #150
	sec
	sbc IntroTmp
	; test for dx > dy
	cmp BottomBresenDy,x	; test for dx > dy
	bcc .canStartAt150
	; Start x has to be <150, i.e. targetX + dy
	lda IntroTmp
	clc
	adc BottomBresenDy,x
	sbc #4			; looks better
	sta BottomXPos,x
	lda BottomBresenDy,x
	sta BottomBresenDx,x
	bne .afterBresen	; unconditional jmp
.canStartAt150
	sta BottomBresenDx,x
	lda #150
	sta BottomXPos,x
.afterBresen
	
	; init Anim
	lda #0
	sta BottomAnimState,x

	; set next entry to 0
	inx
	txa
	and #$07
	tax
	lda #0
	sta BottomXPos,x
	
	; move index to next pixel
	inc BottomPixelIndex
	dec BottomPixelsInLine
	bne .noNewSendLine
	; new pixel send line
	dec BottomSendCurrentLine
	ldy BottomPixelIndex
	lda Logo_Bottom,y
	bpl .noEnd		; 255 means end
	lda #0
	sta BottomPixelIndex
	beq .noNewSendLine
.noEnd
	sta BottomPixelsInLine
	iny
	sty BottomPixelIndex	

.noNewSendLine
		
.noNewSquare

; -------------------------------------------------------------------------

	; Check for completely fallen square
	dec BottomYPos
	bne .noCompletedSquare
	
	; add 14 again
	lda #14
	;lda BottomYPos
	;clc
	;adc #14
	sta BottomYPos
		
	; Load index and make sure we really have to fill pixel
	lda BottomSquareIndex
	and #$07
	tax
	lda BottomXPos,x
	beq .noCompletedSquare

	; draw pixel
	ldy BottomFillIndex
	lda Logo_Bottom,y
	asl
	tay
	lda Logo_SetPixelData+1,y	; ora mask
	sta IntroTmp
	lda Logo_SetPixelData,y		; index into LogoBottomPixel
	tay
	lda LogoBottomPixels,y
	ora IntroTmp
	sta LogoBottomPixels,y
	
	; advance BottomFillIndex
	inc BottomFillIndex
	dec BottomFillLeftInLine
	bne .noNewFillLine

	; next line
	ldy BottomFillIndex
	lda Logo_Bottom,y
	sta BottomFillLeftInLine
	iny
	sty BottomFillIndex
	inc BottomPixelCurrentLine
	; decrease BottomYPos by 6 because new line has appeared
	lda BottomYPos
	sec
	sbc #6
	sta BottomYPos	
	; Clear logo line buffer
	lda #0
	sta LogoBottomPixels+0
	sta LogoBottomPixels+1
	sta LogoBottomPixels+2
	sta LogoBottomPixels+3
	sta LogoBottomPixels+4
.noNewFillLine
		
	; remove square from ring buffer
	lda #0
	sta BottomXPos,x
	inx
	txa
	and #$07
	sta BottomSquareIndex
	
.noCompletedSquare
	rts

	

; =========================================================================
; Tables
; =========================================================================

; Script event durations
; ------------------------------

IntroScriptDurations
	dc.b 250, 250, 250, 250, 250

; Squares
; ------------------------------



TopLines
	dc.b 78, 78, 84, 90
BottomLines
	dc.b 97, 91, 85, 79, 79


; Index of square gfx (0...5) according to AnimState

SquareAnimGfx
	dc.b 0, 0, 12, 12, 24, 24, 36, 36
	dc.b 48, 48, 60, 60, 72, 72, 0, 0


; Color of square according to AnimState

SquareAnimColors
	dc.b $02, $02, $d0, $d0
	dc.b $d2, $d2, $d4, $d4, $d6, $d6, $d8, $d8
	dc.b $da, $da, $dc, $dc, $de


; Logo
; ------------------------------
	
; Logo playfield graphics

;	ALIGN $100
	
Logo_1
	; 0-7
	dc.b %11111100
	dc.b %11111100
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000

Logo_2
	; 8-15 mirrored
	dc.b %00110011
	dc.b %01110011
	dc.b %11110011
	dc.b %10110011
	dc.b %00110011
	dc.b %00110011
	dc.b %00110011

Logo_3
	; 16-19 mirrored
	dc.b %11000000
	dc.b %11100000
	dc.b %11110000
	dc.b %11010000
	dc.b %11000000
	dc.b %11000000
	dc.b %11000000
	
Logo_4
	; 20-27
	dc.b %00110011
	dc.b %00110011
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000
	dc.b %00110000

Logo_5
	; 28-35 mirrored
	dc.b %00001111
	dc.b %00001111
	dc.b %00000011
	dc.b %00000011
	dc.b %00000011
	dc.b %00000011	
	dc.b %00000011	
	
; ora data to set individual pixels.
; format: index for LogoTopPixels, ora value

Logo_SetPixelData
	dc.b 0, %10000000
	dc.b 0, %01000000
	dc.b 0, %00100000
	dc.b 0, %00010000
	dc.b 0, %00001000
	dc.b 0, %00000100
	dc.b 0, %00000010
	dc.b 0, %00000001
	
	dc.b 1, %00000001
	dc.b 1, %00000010
	dc.b 1, %00000100
	dc.b 1, %00001000
	dc.b 1, %00010000
	dc.b 1, %00100000
	dc.b 1, %01000000
	dc.b 1, %10000000

	dc.b 2, %00010000
	dc.b 2, %00100000
	dc.b 2, %01000000
	dc.b 2, %10000000
	
	dc.b 3, %10000000
	dc.b 3, %01000000
	dc.b 3, %00100000
	dc.b 3, %00010000
	dc.b 3, %00001000
	dc.b 3, %00000100
	dc.b 3, %00000010
	dc.b 3, %00000001

	dc.b 4, %00000001
	dc.b 4, %00000010
	dc.b 4, %00000100
	dc.b 4, %00001000
	; not needed for logo
	;dc.b 4, %00010000
	;dc.b 4, %00100000
	;dc.b 4, %01000000
	;dc.b 4, %10000000


; Pixel data. Format:
; dc.b #PixelsInLine
; dc.b xpos1, xpos2, ..., #PixelsInLine
; Positions are in view order, from left to right
; but upside down (lines #3-1

Logo_Top
	; #3
	dc.b 16
	dc.b 29, 28
	dc.b 23, 22
	dc.b 19, 18, 17, 16
	dc.b 15, 14, 13, 12, 8, 9
	dc.b 3, 2
	
	; #2
	dc.b 22
	dc.b 31, 30, 29, 28
	dc.b 27, 26, 23, 22
	dc.b 19, 18, 17
	dc.b 14, 13, 12, 9, 8
	dc.b 5, 4, 3, 2, 1, 0
			
	; #1
	dc.b 20
	dc.b 31, 30, 29, 28
	dc.b 27, 26, 23, 22
	dc.b 19, 18
	dc.b 13, 12, 9, 8
	dc.b 5, 4, 3, 2, 1, 0
		
	; End
	dc.b 255

	
; Bottom order is top to bottom, i.e. lines #4-7
; and right to left
	
Logo_Bottom
	; #4
	dc.b 14
	dc.b 2, 3
	dc.b 8, 9, 12, 13, 15
	dc.b 16, 18, 19
	dc.b 22, 23
	dc.b 28, 29
	
	; #5
	dc.b 12
	dc.b 2, 3
	dc.b 8, 9, 12, 13
	dc.b 18, 19
	dc.b 22, 23
	dc.b 28, 29
	
	; #6
	dc.b 12
	dc.b 2, 3
	dc.b 8, 9, 12, 13
	dc.b 18, 19
	dc.b 22, 23
	dc.b 28, 29
	
	; #7
	dc.b 12
	dc.b 2, 3
	dc.b 8, 9, 12, 13
	dc.b 18, 19
	dc.b 22, 23
	dc.b 28, 29

	; End
	dc.b 255


; Rotating square

Square_GFX
; 1
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100

; 2
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00000000

; 3
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00011000
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00000000
	dc.b %00000000

; 4
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00011000
	dc.b %00111100
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000

; 5
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00111100
	dc.b %00011000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000

; 6
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00011000
	dc.b %00000000
	dc.b %00000000

; Could be removed; same as #2
; 7
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00000000
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00111100
	dc.b %00000000
