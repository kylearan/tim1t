; Logo colors:
; Background: $90 (003c70)
; Rectangle:  $96 (508cb4)
; Logo:       $ec (d0d0d0)


; =========================================================================
; Constants
; =========================================================================

BEG_MAX_BULGE_IN_COUNT	= 21
BEG_MAX_PRESENTS_COL	= 7
BEG_MAX_SHUTTERS_COL	= 11
BEG_CLUSTER_HEIGHT	= 24
BEG_PLATE_HEIGHT	= 28
BEG_LOGO_HEIGHT		= BEG_CLUSTER_HEIGHT + BEG_PLATE_HEIGHT


; Script state constants
BEG_BULGE_IN		= 0
BEG_SHUTTERS_IN 	= 1
BEG_LOGO_IN		= 2
BEG_PRESENTS_IN		= 3
BEG_PRESENTS_OUT	= 4
BEG_LOGO_OUT		= 5
BEG_SHUTTERS_OUT	= 6
BEG_BULGE_OUT		= 7
BEG_BULGE_NEXT_PART	= 8



; =========================================================================
; Start
; =========================================================================

BeginningStart

	; inits
	; ------------------------------
	
	; gfx
	lda #%00000001		; mirror PF
	sta CTRLPF
	
	lda #$90
	sta COLUBK
	lda #0
	sta COLUPF
	lda #$ec
	sta COLUP0
	sta COLUP1
	
	; 48px routine to Zeropage
	ldx #BegROMDisplay48End - BegROMDisplay48Start - 1
.copyLoop
	lda BegROMDisplay48Start,x
	sta BegZPDisplay48,x
	dex
	bpl .copyLoop
	
	; Demo state
	lda #0
	sta BeginningNumFrame
	lda #BEG_BULGE_IN
	sta BeginningDemoState
	jmp BeginningIncoming

	
	
; =========================================================================
; Kernel
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

BeginningKernel
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc BeginningNumFrame
	lda BeginningNumFrame
	ldx BeginningDemoState
	cmp BeginningScriptDurations,x 
	bne .noNextState
	inc BeginningDemoState
	lda BeginningDemoState
	cmp #BEG_BULGE_NEXT_PART
	bne .noNextPart
	JMB BackZoomerStart
	
.noNextPart
	lda #0
	sta BeginningNumFrame
.noNextState

; -------------------------------------------------------------------------

BeginningIncoming

	jsr BegSetPresentsColor
	jsr BegSetShutterColor
	jsr BegCalcLogoParameters
	
; -------------------------------------------------------------------------

BegInitObjects SUBROUTINE

	; Init objects according to state
	
	lda BeginningDemoState
	cmp #BEG_BULGE_IN
	beq .initBulgeIn
	cmp #BEG_BULGE_OUT
	bne .initLogo

	; bulge out
.initBulgeOut
	lda BeginningScriptDurations+BEG_BULGE_OUT
	sec
	sbc BeginningNumFrame
	bcs .calcBulgeParams
	
	; bulge in
.initBulgeIn
	lda BeginningNumFrame
	
	; bulge in or out
.calcBulgeParams
	lsr
	cmp #BEG_MAX_BULGE_IN_COUNT
	bcc .noGreater
	lda #BEG_MAX_BULGE_IN_COUNT
.noGreater
	sta BulgeHeight

	; init and position objects
	lda #0
	sta VDELP0
	sta VDELP1
	sta NUSIZ0
	sta NUSIZ1
	sta ENABL
	sta COLUP0
	sta COLUP1
	lda #%00000010
	sta ENAM0
	sta ENAM1
	
	lda BulgeHeight
	clc
	adc #6
	ldx #0
	jsr BegPositionObject
	sta WSYNC
	lda #0
	sta HMM0
	lda #156
	sec
	sbc BulgeHeight
	inx
	jsr BegPositionObject


	jmp DisplayBulgeInOut

; -------------------------------------------------------------------------

	; Init for logo and presents
.initLogo	
	lda #1
	sta VDELP0
	sta VDELP1
	lda #0
	sta ENAM0
	sta ENAM1
	sta COLUBK
	lda #%00000011		; three copies close
	sta NUSIZ0
	sta NUSIZ1
	; Init and position 48px sprites at center
	sta WSYNC
	; SLEEP 36
	jsr BeginningRTS
	jsr BeginningRTS
	jsr BeginningRTS
	sta RESP0
	sta RESP1
	lda #NO_MOTION
	sta HMP0
	lda #LEFT_1
	sta HMP1
	sta WSYNC
	sta HMOVE
	lda #%00000010
	sta ENABL
	
	; Bulge in or not?
	lda BeginningDemoState
	cmp #BEG_BULGE_IN
	beq DisplayBulgeInOut
	cmp #BEG_BULGE_OUT
	beq DisplayBulgeInOut
	jmp DisplayStaticBulge


; -------------------------------------------------------------------------
; Visible area: 228 lines
; -------------------------------------------------------------------------

DisplayBulgeInOut SUBROUTINE
	jsr BeginningWaitForIntim
	lda #0
	sta VBLANK
	lda #255
	sta PF0
	sta PF1
	sta PF2

	; upper bulge
	sta WSYNC
	lda #$90
	sta COLUBK
	ldx BulgeHeight
	beq .afterUpper
.upperBulge
	lda bulge_1,x
	sta PF0
	lda bulge_2,x
	sta PF1
	lda bulge_3,x
	sta PF2
	sta WSYNC
	dex
	bpl .upperBulge

.afterUpper
	lda #0
	sta PF0
	sta PF1
	sta PF2

	lda #223
	sec
	sbc BulgeHeight
	sbc BulgeHeight
	tax
.wasteMiddle
	sta WSYNC
	dex
	bne .wasteMiddle

	; lower bulge
	sta WSYNC
	ldx BulgeHeight
	inx
	stx BegTmp
	ldx #0
.lowerBulge
	lda bulge_1,x
	sta PF0
	lda bulge_2,x
	sta PF1
	lda bulge_3,x
	sta PF2
	sta WSYNC
	inx
	cpx BegTmp
	bne .lowerBulge

.afterLower
	jmp BeginningDoVBlank


; -------------------------------------------------------------------------

	ALIGN $100
DisplayStaticBulge SUBROUTINE
	jsr BeginningWaitForIntim
	lda #0
	sta VBLANK

	sta WSYNC
	lda #$90
	sta COLUBK
	ldx #21
.loop
	lda bulge_1,x
	sta PF0
	lda bulge_2,x
	sta PF1
	lda bulge_3,x
	sta PF2
	sta.w RESBL	; @27
	; SLEEP 33
	jsr BeginningRTS
	jsr BeginningRTS
	SLEEP 9
	sta RESBL	; @63
	sta WSYNC
	dex
	bpl .loop
	
	lda #0
	sta PF0
	sta PF1
	sta PF2

	; @15
	; Display upper gap
	lda #32
	clc
	adc PlateDelay
	tax
	lda #$90

	sta.w RESBL	; @27
	ldy #$90
	; SLEEP 31
	jsr BeginningRTS
	jsr BeginningRTS
	SLEEP 7
	sta RESBL	; @63

	; SLEEP 31
	jsr BeginningRTS
	jsr BeginningRTS
	SLEEP 7
	jsr BegDisplayPlateNoNewline

; -------------------------------------------------------------------------

DisplayCluster SUBROUTINE

	; SLEEP 26
	jsr BeginningRTS
	jsr BeginningRTS
	SLEEP 2

	lda CurrentShutterCol
	sta RESBL	; @27
	nop
	sta COLUBK

	; SLEEP 23
	jsr BeginningRTS
	SLEEP 11
	
	lda #$90
	sta COLUBK
	sta RESBL	; @63

	; SLEEP 34
	jsr BeginningRTS
	jsr BeginningRTS
	SLEEP 10

	lda CurrentShutterCol
	sta RESBL	; @27
	nop
	sta COLUBK
	
	; SLEEP 23
	jsr BeginningRTS
	SLEEP 11
	
	lda #$90
	sta COLUBK
	sta RESBL	; @63
	
	; Display rest of upper shutter
	ldx #1
	ldy #$90
	lda CurrentShutterCol
	; SLEEP 24
	jsr BeginningRTS
	jsr BeginningRTS

	jsr BegDisplayPlateNoNewline
	
	; Display upper gap
	ldx #2
	lda #$90
	; SLEEP 19
	jsr BeginningRTS
	SLEEP 7
	
	jsr BegDisplayPlateNoNewline

	; @74
	; Display upper half of plate
	; display plate only, or with parts of Cluster?
	lda ClusterLogoLines
	bne .withCluster
	; Only plate
	lda PlateHeight
	; zero height means no plate at all
	bne .withPlate
	SLEEP 5
	ldx #3
	bne .displayLowerGapNoLogo	; unconditional jmp
	; display at least 1 line of plate
.withPlate
	asl
	tax
	inx
	lda #$96
	SLEEP 4
	jsr BegDisplayPlateNoNewline
	; SLEEP 16
	jsr BeginningRTS
	SLEEP 4
	
	jmp .displayLowerGap
.withCluster
	; Plate + Cluster
	ldx PlateHeight
	lda #$96
	; SLEEP 12
	jsr BeginningRTS
	jsr BegDisplayPlateNoNewline

	; SLEEP 29
	jsr BeginningRTS
	jsr BeginningRTS
	SLEEP 5

	sta RESBL	; @27
	nop
	sta COLUBK
	
	; Display CLUSTER
	lda #$ec
	sta COLUP0
	sta COLUP1
	SLEEP 9
	lda #$90
	jsr BegZPDisplay48
	
	; @74
	lda #0
	sta GRP0
	sta GRP1
	sta GRP0
	sta GRP1
	
	; Display lower half of plate
	ldx PlateHeight
	lda #$96
	ldy #$90
	SLEEP 2
	jsr BegDisplayPlateNoNewline
	
	; Display lower gap
	; SLEEP 19
	jsr BeginningRTS
	SLEEP 7

.displayLowerGap
	ldx #2
.displayLowerGapNoLogo
	lda #$90
	jsr BegDisplayPlateNoNewline
	
	; Display lower shutter
	ldx #3
	lda CurrentShutterCol
	; SLEEP 18
	jsr BeginningRTS
	SLEEP 6

	jsr BegDisplayPlateNoNewline	; @54

	; Display lower gap up to presents
	ldx #25
	lda #$90
	; SLEEP 19
	jsr BeginningRTS
	SLEEP 7

	jsr BegDisplayPlateNoNewline
	
	
; -------------------------------------------------------------------------


DisplayPresents SUBROUTINE

	lda #0
	sta PF0
	sta PF1
	sta PF2
	
	; Prepare ZP
	lda #$90
	sta BegZPDisplay48 + Beg48RectCol - BegROMDisplay48Start + 1
	lda #$90
	sta BegZPDisplay48 + Beg48BgCol - BegROMDisplay48Start + 1

	lda #<presents_1
	sta BegZPDisplay48 + Beg48Ptr1 - BegROMDisplay48Start + 1

	SLEEP 3
	sta RESBL	; @27
	lda #$90
	sta COLUBK

	lda #>presents_1
	sta BegZPDisplay48 + Beg48Ptr1 - BegROMDisplay48Start + 2
	lda #<presents_2
	sta BegZPDisplay48 + Beg48Ptr2 - BegROMDisplay48Start + 1
	lda #>presents_2
	sta BegZPDisplay48 + Beg48Ptr2 - BegROMDisplay48Start + 2
	lda #<presents_3
	sta BegZPDisplay48 + Beg48Ptr3 - BegROMDisplay48Start + 1

	nop
	lda #$90
	sta.w COLUBK
	sta RESBL	; @63

	lda #>presents_3
	sta BegZPDisplay48 + Beg48Ptr3 - BegROMDisplay48Start + 2
	lda #<presents_4
	sta BegZPDisplay48 + Beg48Ptr4 - BegROMDisplay48Start + 1
	lda #>presents_4
	sta BegZPDisplay48 + Beg48Ptr4 - BegROMDisplay48Start + 2
	lda #<presents_5
	sta BegZPDisplay48 + Beg48Ptr5 - BegROMDisplay48Start + 1
	lda #>presents_5
	sta BegZPDisplay48 + Beg48Ptr5 - BegROMDisplay48Start + 2
	lda #<presents_6
	sta BegZPDisplay48 + Beg48Ptr6 - BegROMDisplay48Start + 1
	lda #>presents_6
	sta BegZPDisplay48 + Beg48Ptr6 - BegROMDisplay48Start + 2

	nop
	sta RESBL	; @27
	lda #$90
	sta COLUBK

	lda #19			; presents height
	sta BegZPDisplay48 + Beg48Height - BegROMDisplay48Start + 1
	
	lda CurrentPresentsCol
	sta COLUP0
	sta COLUP1
	SLEEP 3
	lda #$90
	jsr BegZPDisplay48
	
	; @74
	lda #0
	sta GRP0
	sta GRP1
	sta GRP0
	sta GRP1
	
	; Display lower half of gap until lower bulge
	lda #38
	clc
	adc PlateDelay
	tax
	lda #$90
	ldy #$90
	
	SLEEP 2
	sta RESBL	; @27
	; SLEEP 33
	jsr BeginningRTS
	jsr BeginningRTS
	SLEEP 9

	sta RESBL	; @63

	; SLEEP 31
	jsr BeginningRTS
	jsr BeginningRTS
	SLEEP 7

	jsr BegDisplayPlateNoNewline


; -------------------------------------------------------------------------

DisplayBottomBulge SUBROUTINE

	ldx #0
.loop
	lda bulge_1,x
	sta PF0
	lda bulge_2,x
	sta PF1
	lda bulge_3,x
	sta PF2
	
	SLEEP 6
	sta RESBL	; @27
	
	; SLEEP 33
	jsr BeginningRTS
	jsr BeginningRTS
	SLEEP 9

	sta RESBL	; @63

	SLEEP 2
	
	inx
	cpx #22
	bne .loop

	
; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

BeginningDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	JSB MusicPlayer

	jsr BeginningWaitForIntim

	jmp BeginningKernel


	
; =========================================================================
; Subroutines
; =========================================================================

; -------------------------------------------------------------------------
; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

BeginningWaitForIntim SUBROUTINE
	lda INTIM
	bne BeginningWaitForIntim
	sta WSYNC
BeginningRTS
	rts



; -------------------------------------------------------------------------
; Fine-position missile number in x at position in a
; -------------------------------------------------------------------------

BegPositionObject SUBROUTINE
	sta WSYNC
	SLEEP 13
	sec
.divide15
	sbc #15
	bcs .divide15
	sta RESM0,x
	eor #$07
	asl
	asl
	asl
	asl
	sta HMM0,x
	sta WSYNC
	sta HMOVE
	rts
	


; -------------------------------------------------------------------------
; Set colors of shutters according to state
; -------------------------------------------------------------------------

BegSetShutterColor SUBROUTINE

	; Set shutter color	
	ldy #$96
	lda BeginningDemoState
	cmp #BEG_SHUTTERS_IN
	beq .shuttersIn
	cmp #BEG_SHUTTERS_OUT
	bne .noShuttersIn
	; Shutters out
	lda BeginningScriptDurations+BEG_SHUTTERS_OUT
	sec
	sbc BeginningNumFrame
	bcs .calcShuttersCol
	; Shutters in
.shuttersIn
	lda BeginningNumFrame
.calcShuttersCol
	lsr
	lsr
	cmp #BEG_MAX_SHUTTERS_COL
	bcc .noGreater
	lda #BEG_MAX_SHUTTERS_COL
.noGreater
	tax
	ldy ShutterColors,x
.noShuttersIn
	sty CurrentShutterCol
	rts
	
	
	
; -------------------------------------------------------------------------
; Set colors of "presents" according to state
; -------------------------------------------------------------------------

BegSetPresentsColor SUBROUTINE

	; Set presents color	
	ldy #$90
	lda BeginningDemoState
	cmp #BEG_PRESENTS_IN
	beq .presentsIn
	cmp #BEG_PRESENTS_OUT
	bne .noPresentsIn
	; Presents out
	lda BeginningScriptDurations+BEG_PRESENTS_OUT
	sec
	sbc BeginningNumFrame
	bcs .calcPresentsCol
	; Presents in
.presentsIn
	lda BeginningNumFrame
.calcPresentsCol
	lsr
	lsr
	cmp #BEG_MAX_PRESENTS_COL
	bcc .noGreater
	lda #BEG_MAX_PRESENTS_COL
.noGreater
	tax
	ldy PresentColors,x
.noPresentsIn
	sty CurrentPresentsCol
	rts



; -------------------------------------------------------------------------
; Calculate parameters for plate and logo
; -------------------------------------------------------------------------

BegCalcLogoParameters SUBROUTINE

	; Calc plate and logo start+height
	lda #0			; value for state before logo in/out
	ldy BeginningDemoState
	cpy #BEG_LOGO_IN
	beq .logoIn
	cpy #BEG_PRESENTS_IN
	beq .fullLogo
	cpy #BEG_PRESENTS_OUT
	beq .fullLogo
	cpy #BEG_LOGO_OUT
	bne .noGreater2
	; Logo out
	lda BeginningScriptDurations+BEG_LOGO_OUT
	sec
	sbc BeginningNumFrame
	bcs .calcLogoHeight	; unconditional jmp

.fullLogo
	lda #BEG_LOGO_HEIGHT/2
	bne .noGreater2
	
	; Logo in
.logoIn
	lda BeginningNumFrame
.calcLogoHeight
	cmp #BEG_LOGO_HEIGHT/2
	bcc .noGreater2
	lda #BEG_LOGO_HEIGHT/2
.noGreater2
	sta BegTmp		; half number of logo lines to display

	; Show (parts of) Cluster?
	cmp #BEG_PLATE_HEIGHT/2
	bcc .noCluster
	; Calc half number of Cluster lines to show
	sbc #BEG_PLATE_HEIGHT/2
	sta ClusterLogoLines
	; show full plate
	lda #BEG_PLATE_HEIGHT/2
	sta PlateHeight
	jmp .afterClusterCalc

	; show no Cluster lines
.noCluster
	sta PlateHeight
	lda #0
	sta ClusterLogoLines
.afterClusterCalc

	; Plate delay
	lda #BEG_LOGO_HEIGHT/2
	sec
	sbc PlateHeight
	sbc ClusterLogoLines
	sta PlateDelay
	
	; Init ZP values

	; logo height
	lda ClusterLogoLines
	asl			; Height = *2
	tax			; -1 b/c of bpl in loop
	dex
	stx BegZPDisplay48 + Beg48Height - BegROMDisplay48Start + 1
	
	; init logo ptrs
	lda #BEG_CLUSTER_HEIGHT/2
	sec
	sbc ClusterLogoLines
	sta BegTmp
	lda #$96
	sta BegZPDisplay48 + Beg48RectCol - BegROMDisplay48Start + 1
	lda #$90
	sta BegZPDisplay48 + Beg48BgCol - BegROMDisplay48Start + 1
	lda #<Cluster_1
	clc
	adc BegTmp
	sta BegZPDisplay48 + Beg48Ptr1 - BegROMDisplay48Start + 1
	lda #>Cluster_1
	sta BegZPDisplay48 + Beg48Ptr1 - BegROMDisplay48Start + 2
	lda #<Cluster_2
	adc BegTmp
	sta BegZPDisplay48 + Beg48Ptr2 - BegROMDisplay48Start + 1
	lda #>Cluster_2
	sta BegZPDisplay48 + Beg48Ptr2 - BegROMDisplay48Start + 2
	lda #<Cluster_3
	adc BegTmp
	sta BegZPDisplay48 + Beg48Ptr3 - BegROMDisplay48Start + 1
	lda #>Cluster_3
	sta BegZPDisplay48 + Beg48Ptr3 - BegROMDisplay48Start + 2
	lda #<Cluster_4
	adc BegTmp
	sta BegZPDisplay48 + Beg48Ptr4 - BegROMDisplay48Start + 1
	lda #>Cluster_4
	sta BegZPDisplay48 + Beg48Ptr4 - BegROMDisplay48Start + 2
	lda #<Cluster_5
	adc BegTmp
	sta BegZPDisplay48 + Beg48Ptr5 - BegROMDisplay48Start + 1
	lda #>Cluster_5
	sta BegZPDisplay48 + Beg48Ptr5 - BegROMDisplay48Start + 2
	lda #<Cluster_6
	adc BegTmp
	sta BegZPDisplay48 + Beg48Ptr6 - BegROMDisplay48Start + 1
	lda #>Cluster_6
	sta BegZPDisplay48 + Beg48Ptr6 - BegROMDisplay48Start + 2
	rts
	
	
	
; -------------------------------------------------------------------------
; Display x lines with balls, bg color y and plate color a inbetween.
; For no WSYNC/SLEEP 27: Enter Beg...NoNewline @27
; After rts: @74
; -------------------------------------------------------------------------

BegDisplayPlate SUBROUTINE

.loop
	sta WSYNC
	SLEEP 27
	
BegDisplayPlateNoNewline

	sta RESBL	; @27
	nop
	sta COLUBK

	SLEEP 23
	dex
	
	sty COLUBK
	sta RESBL	; @63

	bne .loop
	
	rts



; -------------------------------------------------------------------------
; Display a 48 pixel sprite with a colored background flanked by two balls.
; Enter at cycle @60 for centered sprite. Needs to be in zero page.
; First displays right ball and sets bg color to a.
;
; Returns at cycle @74.
; -------------------------------------------------------------------------

BegROMDisplay48Start

	; enter @60
	
	sta COLUBK		; 3
	sta RESBL		; 3, @63
	SLEEP 3
Beg48Loop

Beg48Height
	ldy #0			; 2
Beg48Ptr1
	lda Cluster_1,y		; 4
	sta GRP0		; 3
Beg48Ptr2
	lda Cluster_2,y		; 4, @2
	sta GRP1		; 3	
Beg48Ptr3
	lda Cluster_3,y		; 4
	sta GRP0		; 3
Beg48Ptr6
	lda Cluster_6,y		; 4
	sta BegZPDisplay48 + Beg48Tmp - BegROMDisplay48Start + 1
				; 3
				
Beg48Ptr5
	lax Cluster_5,y		; 4

	sta RESBL		; 3, @27
Beg48RectCol
	lda #$96		; 2
	sta COLUBK		; 3

Beg48Ptr4
	lda Cluster_4,y		; 4


Beg48Tmp	
	ldy #0			; 2
	
	sta GRP1		; 3
	stx GRP0		; 3
	sty GRP1		; 3
	sta GRP0		; 3
	
Beg48BgCol
	lda #$90		; 2
	dec BegZPDisplay48 + Beg48Height - BegROMDisplay48Start + 1
				; 5
	sta COLUBK		; 3
	sta RESBL		; 3, @63
	
	bpl Beg48Loop		; 3

	rts			; 6
BegROMDisplay48End


	
; =========================================================================
; Tables
; =========================================================================

; Script event durations
; ------------------------------

BeginningScriptDurations
	dc.b 60		; BEG_BULGE_IN		= 0
	dc.b 60		; BEG_SHUTTERS_IN 	= 1
	dc.b 150	; BEG_LOGO_IN		= 2
	dc.b 150	; BEG_PRESENTS_IN	= 3
	dc.b 100	; BEG_PRESENTS_OUT	= 4
	dc.b 100	; BEG_LOGO_OUT		= 5
	dc.b 50		; BEG_SHUTTERS_OUT	= 6
	dc.b 80		; BEG_BULGE_OUT		= 7



; Colors
; ------------------------------

PresentColors
	dc.b $90, $92, $94, $96, $98, $9a, $9c, $ec

ShutterColors
	dc.b $90, $92, $94, $96, $98, $9a, $9c, $9e
	dc.b $9c, $9a, $98, $96


; Graphics
; ------------------------------

	include cluster_logo.asm	
	include presents.asm
	include bulge.asm
