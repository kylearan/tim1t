; =========================================================================
; Zoomer
; =========================================================================

	SEG.U ZOOM_RAM
	ORG $80

; Permanent
; --------------------

ZoomerNumFrame		ds 1	; Frame counter
ZoomerDemoState		ds 1	; Current demo/effect state

; Morpher vars
MorphBoxXLo		ds 1
MorphBoxXHi		ds 1
MorphBoxYLo		ds 1
MorphBoxYHi		ds 1
MorphBoxWidthLo		ds 1
MorphBoxWidthHi		ds 1
MorphBoxHeightLo	ds 1
MorphBoxHeightHi	ds 1

	echo "---- Zoomer: ",($100 - *) , "tmp bytes of RAM left"

; Temporary
; --------------------

ZoomerTmp		ds 1
BZoomerLineCol		ds 1
BZoomerBackCol		ds 1
BoxY			ds 1
BoxHeight		ds 1
BZoomerWaitState	ds 1	; if we are in wait or zoom state

; Line draw vars
LineXPos		ds 1
LineWidth		ds 1
b0			ds 1
b1			ds 1
b2			ds 1
b3			ds 1
b4			ds 1
s0			ds 1
s1			ds 1
s1Pos			ds 1
p0			ds 1
p1			ds 1
p2			ds 1
p3			ds 1
p4			ds 1
p5			ds 1
LineTmp			ds 1
s0Width			ds 1
mask			ds 1

LineXPos_2		ds 1
s0_2			ds 1
s1_2			ds 1
s1Pos_2			ds 1
p0_2			ds 1
p1_2			ds 1
p2_2			ds 1
p3_2			ds 1
p4_2			ds 1
p5_2			ds 1


	echo "---- Zoomer: ",($100 - *) , "permanent bytes of RAM left"



; =========================================================================
; Beginning
; =========================================================================

	SEG.U BEG_RAM
	ORG $80

; Permanent
; --------------------

BeginningNumFrame	ds 1	; Frame counter
BeginningDemoState	ds 1	; Current demo/effect state

BegZPDisplay48		ds BegROMDisplay48End - BegROMDisplay48Start

	echo "---- Beginning: ",($100 - *) , "tmp bytes of RAM left"

; Temporary
; --------------------

BegTmp			ds 1

BulgeHeight		ds 1	; number of lines to display for bulge
CurrentPresentsCol	ds 1
CurrentShutterCol	ds 1
PlateDelay		ds 1	; #lines to delay plate
PlateHeight		ds 1	; half height of plate
ClusterLogoLines	ds 1	; Half number of logo lines to display


	echo "---- Beginning: ",($100 - *) , "permanent bytes of RAM left"

	

; =========================================================================
; Intro
; =========================================================================

	SEG.U IN_RAM
	ORG $80

; Permanent
; --------------------

IntroNumFrame		ds 1	; Frame counter
IntroDemoState		ds 1	; Current demo/effect state

LogoTopPixels		ds 5	; row of pixels being filled (top)
LogoBottomPixels	ds 5	; row of pixels being filled (bottom)

TopPixelIndex		ds 1	; index of next pixel in line to send (0 = end)
TopSendCurrentLine	ds 1	; Current line number of pixel to send
TopPixelCurrentLine	ds 1	; Current line number of pixel to fill
TopPixelsInLine		ds 1	; Number of pixels left in current line top
TopFillIndex		ds 1	; index of next pixel to fill when square reaches end
TopFillLeftInLine	ds 1	; Number of pixels left in current fill line

BottomPixelIndex	ds 1	; index of next pixel in line to send (0 = end)
BottomSendCurrentLine	ds 1	; Current line number of pixel to send
BottomPixelCurrentLine	ds 1	; Current line number of pixels to fill
BottomPixelsInLine	ds 1	; Number of pixels left in current line top
BottomFillIndex		ds 1	; index of next pixel to fill when square reaches end
BottomFillLeftInLine	ds 1	; Number of pixels left in current fill line

TopSquareIndex		ds 1	; index in ring buffer for falling squares
TopYPos			ds 1	; pos of first square
BottomSquareIndex	ds 1	; index in ring buffer for rising squares
BottomYPos		ds 1	; Size of gap until first displayed square. 0-12 means skip some square lines.

TopAnimState		ds 8	; ring buffer of animation state
TopXPos			ds 8	; X pos of square
TopBresenError		ds 8	; Bresenham variables
TopBresenDx		ds 8
TopBresenDy		ds 8
BottomAnimState		ds 8	; ring buffer of animation state
BottomXPos		ds 8	; X pos of square
BottomBresenError	ds 8	; Bresenham variables
BottomBresenDx		ds 8
BottomBresenDy		ds 8

	echo "---- Intro: ",($100 - *) , "tmp bytes of RAM left"

; Temporary
; --------------------

BottomSendSquare	ds 1	; Index of tail of ring buffer if new square has to be sent (255 otherwise)
TopFallenSquare		ds 1	; Index of square that might be a fully fallen one
IntroSquareNumLines	ds 1	; Number of square lines to display for first square
IntroCurrentLine	ds 1
IntroTmp		ds 1

	echo "---- Intro: ",($100 - *) , "permanent bytes of RAM left"


	
; =========================================================================
; Starfield
; =========================================================================

	SEG.U SF_RAM
	ORG $80

; Permanent
; --------------------

NumFrame		ds 1	; Frame counter
DemoState		ds 1	; Current demo/effect state

SFSeed			ds 1
SFSeed2			ds 1
SortingArray		ds SF_NUM_STARS		; for continues sorting
; +2 for dummy y=255 head and tail entries
StarArray		ds SF_NUM_STARS+2	; sanitized, for display
StarXY			ds SF_NUM_STARS
StarZ			ds SF_NUM_STARS
StarScrX		ds SF_NUM_STARS
; +1 for dummy 255 head and tail entry (head and tail point to this same entry)
StarScrY		ds SF_NUM_STARS+1

	echo "---- Starfield: ",($100 - *) , "tmp bytes of RAM left"

; Temporary
; --------------------

CurrentLine		ds 1
CurrentStarIndex	ds 1
CurrentSortingIndex	ds 1
SFTmp			ds 1
SFTmp2			ds 1

	echo "---- Starfield: ",($100 - *) , "permanent bytes of RAM left"



; =========================================================================
; Plasma
; =========================================================================

	SEG.U PLSM_RAM
	ORG $80
	
; Permanent
; --------------------

PlsmNumFrame	ds 1	; Frame counter
PlsmDemoState	ds 1	; Current demo/effect state

	; CellMem must start at $82 for auto-generated plasma row display .asm
CellMem		ds PLASMA_WIDTH*PLASMA_HEIGHT
PlasmaCnt0	ds 1
PlasmaCnt1	ds 1
PaletteOffset	ds 1

	echo "---- Plasma: ",($100 - *) , "tmp bytes of RAM left"

; Temporary
; --------------------

RtSine		ds 1	; Really tmp?
Sine0		ds 1	; Sine0+1: pointer for current plasma kernel
Sine1		ds 1
CurrentCell	ds 1
RowsLeft	ds 1
YPos		ds 1
LineOffset	ds 1
Color		ds 1
PlsmTmp		ds 1

	echo "---- Plasma: ",($100 - *) , "permanent bytes of RAM left"
	
	

; =========================================================================
; Four in One Top (Sine Waves, Raster Splits)
; =========================================================================

	SEG.U FOUR_RAM
	ORG $80

; Permanent
; --------------------

FourNumFrame		ds 1	; Frame counter
FourDemoState		ds 1	; Current demo/effect state

FourPBLine1		ds 1	; Plasma bar counters per frame
FourPBLine2		ds 1

FourRSIndex1		ds 1	; Raster splits parabola index
FourRSIndex2		ds 1	; Raster splits parabola index
FourRSIndex3		ds 1	; Raster splits parabola index
FourRSIndex4		ds 1	; Raster splits parabola index

FourCurrentSWLine1	ds 1
FourCurrentSWLine2	ds 1
FourCurrentHMove0	ds 1
FourCurrentHMove1	ds 1
FourNextHMove0		ds 1
FourNextHMove1		ds 1

FourCurrLineLo		ds 1
FourCurrLineHi		ds 1
FourAddLo		ds 1
FourAddHi		ds 1
FourTwisterDir		ds 1


FOUR_PERM		= (* - $80)

	echo "---- 4in1: ",($100 - *) , "tmp bytes of RAM left"

; Temporary
; --------------------

FourTmp			ds 1
FourTmp2		ds 1
FourKernelPtr		ds 2	; Ptr to current kernel according to state
FourCurrentLine		ds 1
FourBCurrentLine	ds 1
FourPBCurrentLine	ds 1
FourRSPtr1		ds 2	; Raster split color pointers
FourRSPtr2		ds 2
FourRSPtr3		ds 2
FourRSPtr4		ds 2
FourLoopEnd		ds 1
FourFadeState		ds 1

Four48_1		ds 2	; 48 pixel sprite data
Four48_2		ds 2
Four48_3		ds 2
Four48_4		ds 2
Four48_5		ds 2
Four48_6		ds 2

FourLineData		ds 48


	echo "---- 4in1: ",($100 - *) , "permanent bytes of RAM left"



; =========================================================================
; Upscroll
; =========================================================================

	SEG.U UP_RAM
	ORG $80

; Permanent
; --------------------

UpNumFrame		ds 1	; Frame counter
UpDemoState		ds 1	; Current demo/effect state

UpScrollSpeedLo		ds 1
UpScrollSpeedHi		ds 1
UpScrollAddLo		ds 1
UpScrollAddHi		ds 1

UpCharIndex		ds 1	; top-most char (0...)
UpRowIndex		ds 1	; top-most char row (4...0)
UpRowScanline		ds 1	; #scanlines to show for first row (UP_DOTHEIGHT-1...0)
UpPeekIndex		ds 1	; char to peek-ahead for effects

UpFxType		ds 6	; Effect types currently on screen
UpFxColor		ds 6	; Effect colors currently on screnn
UpFxData1		ds 6	; data for effects currently on screen
UpFxData2		ds 6	; data for effects currently on screen

UpLogoXPos		ds 1	; x pos of logo during in/out
UpLogoLetterLock	ds 1	; index of letter already at its final position
UpStartLocking		ds 1	; flag if letters should lock into final positions
UpLogoLetterCounter	ds 7	; current index of letter in sin table


	echo "---- Upscroll: ",($100 - *) , "tmp bytes of RAM left"

; Temporary
; --------------------

UpTmp			ds 1
UpTmp2			ds 1

UpCurrentFx		ds 1	; index of current effect during frame
UpCurrentFxData1	ds 6	; fx data for current effect, for advancing between chars
UpCurrentFxData2	ds 6
UpCurrentChar		ds 1	; current char during frame
UpRowScanCount		ds 1	; #scanlines left to display char row (UP_DOTHEIGHT-1...0)
UpCharBLColor		ds 1	; current char ball color
UpCharBits		ds 5	; current char ENAM0/M1/BL bits
UpCharP0		ds 5	; current char P0
UpCharP1		ds 5	; current char P1
UpHMxTmp		ds 5	; tmp data for fine positioning

UpLogoLetterGap		ds 7	; gap of logo letters during in/out


	echo "---- Upscroll: ",($100 - *) , "permanent bytes of RAM left"



; =========================================================================
; End
; =========================================================================

	SEG.U CHESS_RAM
	ORG $80

; Permanent
; --------------------

ChessNumFrame		ds 1	; Frame counter
ChessDemoState		ds 1	; Current demo/effect state


	echo "---- Chessboards: ",($100 - *) , "tmp bytes of RAM left"

; Temporary
; --------------------

ChessTmp		ds 1
ChessTmp2		ds 1
ChessOldSP		ds 1

ChessCounterLo		ds 1		; current chessboard counter
ChessCounterHi		ds 1
ChessAddLo		ds 1		; chessboard counter speed
ChessAddHi		ds 1
ChessLines		ds 1

ChessLineCounter	ds 1		; count lines during loops
ChessInnerDelta		ds 1
ChessCurrentCol1	ds 1
ChessCurrentCol2	ds 1

ChessBoardBackDist	ds 1
ChessBoardFrontDist	ds 1
ChessBoardYPos		ds 1		; top of gfx
ChessBoardDisplayHeight	ds 1
ChessTextDisplayHeight	ds 1		; height of textbox (max 20)
ChessTextColor		ds 1
ChessTextColor2		ds 1	
ChessBoardAfterLines	ds 1		; lines to waste after gfx
ChessBoardMode		ds 1
ChessLinesToPrepare	ds 1
ChessDeltaBuffer	ds 70		; 2*35 delta values

Chess48_1		ds 2
Chess48_2		ds 2
Chess48_3		ds 2
Chess48_4		ds 2
Chess48_5		ds 2
Chess48_6		ds 2
Chess48Height		ds 1



	echo "---- Chessboards: ",($100 - *) , "permanent bytes of RAM left"



; =========================================================================
; End
; =========================================================================

	SEG.U END_RAM
	ORG $80

; Permanent
; --------------------

EndNumFrame		ds 1	; Frame counter
EndDemoState		ds 1	; Current demo/effect state

EndCounter		ds 1
EndCounterDelayLo	ds 1
EndCounterDelayHi	ds 1
EndCounterSpeedLo	ds 1
EndCounterSpeedHi	ds 1
EndCounterNextDecrease	ds 1
EndPixelSize		ds 1
EndPixelGap		ds 1


	echo "---- End: ",($100 - *) , "tmp bytes of RAM left"

; Temporary
; --------------------

EndTmp			ds 1
EndTmp2			ds 1
EndTmp3			ds 1

EndKernelPtr		ds 2
End48_1			ds 2
End48_2			ds 2
End48_3			ds 2
End48_4			ds 2
End48_5			ds 2
End48_6			ds 2
End48Height		ds 1

EndCounterGfx		ds 6


	echo "---- End: ",($100 - *) , "permanent bytes of RAM left"



; =========================================================================
; End
; =========================================================================

	SEG.U PLAYER_RAM
	ORG $80

PermanentVars		ds 109
TempVars		ds 5
PlayerVars		ds 10
