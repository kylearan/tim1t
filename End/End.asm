; =========================================================================
; Constants
; =========================================================================


; Script state constants

END_COUNTER_IN		= 0
END_TIM1T_INOUT		= 1
END_END_IN		= 2
END_WAITFORZOOM		= 3
END_ZOOM		= 4
END_WAITFOREND		= 5
END_DELAY_RESTART	= 6
END_NEXT_PART		= 7



; =========================================================================
; Start
; =========================================================================

EndStart

	; inits
	; ------------------------------
	
	; gfx
	lda #$00
	sta COLUBK
	sta COLUPF
	sta COLUP0
	sta COLUP1
	sta PF0
	sta PF1
	sta ENAM0
	sta ENAM1
	sta ENABL
	lda #%11111100
	sta PF2
	lda #%00000001		; PF Mirror
	sta CTRLPF
	
	; Demo state
	lda #0
	sta EndNumFrame
	sta EndDemoState
	
	lda #255
	sta EndCounter
	lda #1
	sta EndCounterDelayHi
	lda #0
	sta EndCounterDelayLo
	lda #5
	sta EndCounterSpeedLo
	lda #0
	sta EndCounterSpeedHi
	lda #1
	sta EndCounterNextDecrease
	
	lda #5
	sta EndPixelSize
	lda #1
	sta EndPixelGap

	jmp EndIncoming

	
	
; =========================================================================
; Kernel
; =========================================================================

; -------------------------------------------------------------------------
; VSYNC
; -------------------------------------------------------------------------

EndKernel
	VERTICAL_SYNC

; -------------------------------------------------------------------------
; VBLANK: Normally 45 lines
; -------------------------------------------------------------------------

	lda #45*76/64 + 1
	sta TIM64T
	lda #%00000010
	sta VBLANK

	; Script counter
	inc EndNumFrame
	lda EndNumFrame
	ldx EndDemoState
	cmp EndScriptDurations,x 
	bne .noNextState
	inc EndDemoState
	lda #0
	sta EndNumFrame
	lda EndDemoState
	cmp #END_NEXT_PART
	bne .noNextState
	JMB ZoomerStart

.noNextState

; -------------------------------------------------------------------------

EndIncoming SUBROUTINE

	; check if we need to adjust zoom
	lda EndDemoState
	cmp #END_ZOOM
	bne .noZoom
	lda EndNumFrame
	and #3
	bne .noZoom
	; zoom if not finished
	lda EndPixelSize
	cmp #13
	beq .noZoom
	inc EndPixelSize
	; increase gap?
	cmp #8
	beq .increaseGap
	cmp #11
	bne .noZoom
.increaseGap
	inc EndPixelGap
	
.noZoom
	
	; check if we need to adjust counter speed
	lda EndNumFrame
	bne .noAdjust
	lda EndDemoState
	cmp #END_WAITFORZOOM
	bne .notWaitForZoom
	inc EndCounterSpeedHi
	inc EndCounterSpeedHi
	jmp .noAdjust
.notWaitForZoom
	cmp #END_WAITFOREND
	bne .noAdjust
	lda EndCounterSpeedHi
	clc
	adc #5
	sta EndCounterSpeedHi
	
.noAdjust
	
	; check if we need to process counter
	dec EndCounterNextDecrease
	bne .noCounter
	lda EndCounterDelayHi
	sta EndCounterNextDecrease
	
	; decrease counter speed
	lda EndCounterDelayLo
	clc
	adc EndCounterSpeedLo
	sta EndCounterDelayLo
	lda EndCounterDelayHi
	adc EndCounterSpeedHi
	sta EndCounterDelayHi
	
	; decrease counter
	dec EndCounter
;	lda EndCounter
;	cmp #255
;	bne .noCounter
;	JMB ZoomerStart
	
.noCounter

	; adjust volume
	lda EndDemoState
	cmp #END_DELAY_RESTART
	beq .noVolumeAdjust
	lda #0
	sec
	sbc EndCounter
	lsr
	lsr
	lsr
	lsr
	cmp #15
	bcc .notGreater
	lda #15
.notGreater
	sta PlayerVars

.noVolumeAdjust

	; turn into gfx
	lda EndCounter
	sta EndTmp
	ldy #48
.createCounterLoop
	ror EndTmp
	bcs .carrySet
	ldx EndPixelSize
.cClearPixelLoop
	lsr EndCounterGfx+0
	ror EndCounterGfx+1
	ror EndCounterGfx+2
	ror EndCounterGfx+3
	ror EndCounterGfx+4
	ror EndCounterGfx+5
	dey
	beq .end
	dex
	bne .cClearPixelLoop
	
.gap
	ldx EndPixelGap
.gapLoop
	lsr EndCounterGfx+0
	ror EndCounterGfx+1
	ror EndCounterGfx+2
	ror EndCounterGfx+3
	ror EndCounterGfx+4
	ror EndCounterGfx+5
	dey
	beq .end
	dex
	bne .gapLoop
	beq .createCounterLoop	; unconditional
	
.carrySet
	ldx EndPixelSize
.cSetPixelLoop
	sec
	ror EndCounterGfx+0
	ror EndCounterGfx+1
	ror EndCounterGfx+2
	ror EndCounterGfx+3
	ror EndCounterGfx+4
	ror EndCounterGfx+5
	dey
	beq .end
	dex
	bne .cSetPixelLoop
	beq .gap		; unconditional

.end

	jsr EndWaitForIntim
	lda #0
	sta VBLANK


; -------------------------------------------------------------------------
; Visible area
; -------------------------------------------------------------------------

EndVisible SUBROUTINE

	lda EndDemoState
	cmp #END_DELAY_RESTART
	bne .endPart
	
	WASTE 227
	jmp EndDoVBlank
	
.endPart

	WASTE 45
	
	; Display TIM1T
	; ----------------------------------------

	lda #32*76/64 + 1
	sta TIM64T

EndDisplayTIM1T SUBROUTINE
	; Set TIM1T color
	lda EndDemoState
	cmp #END_COUNTER_IN
	bne .notCounterIn
	; No TIM1T
	lda #0
	jmp .setColor

.notCounterIn

	cmp #END_TIM1T_INOUT
	bne .notTIM1TInOut
	; Fade in TIM1T
	lda EndNumFrame
	lsr
	lsr
	cmp #$0e
	bcc .notYetIn
	lda #$0e
.notYetIn
	jmp .setColor

.notTIM1TInOut

	cmp #END_END_IN
	bne .noEndIn
	; Fade out TIM1T
	lda EndNumFrame
	lsr
	lsr
	cmp #$0e
	bcc .notYetOut
	lda #$0e
.notYetOut
	sta EndTmp
	lda #$0e
	sec
	sbc EndTmp
	jmp .setColor

.noEndIn	
	; no TIM1T
	lda #0

.setColor
	sta COLUP0
	sta COLUP1

	; Display TIM1T

	sta WSYNC
	jsr EndInit48
	lda #<TIM1T_1
	ldx #>TIM1T_1
	ldy #14
	jsr EndPrepare48
	jsr EndDisplay48

.waitTimer
	lda INTIM
	bne .waitTimer
	sta WSYNC


	; Display Counter
	; ----------------------------------------
	
EndDisplayCounter SUBROUTINE

	lda #38*76/64 + 1
	sta TIM64T
	
	; Set counter color
	lda EndDemoState
	cmp #END_COUNTER_IN
	bne .notIn

	; Fade in
	lda EndNumFrame
	lsr
	lsr
	lsr
	cmp #$0e
	bcc .notYetIn
	lda #$0e
.notYetIn
	jmp .setColor
	
.notIn
	lda #$0e
	
.setColor
	sta COLUP0
	sta COLUP1
	sta EndTmp2
	
	sta WSYNC
	
	; Set line color
	lda EndDemoState
	cmp #END_ZOOM
	bne .noFadeLine
	lda EndNumFrame
	lsr
	cmp #$0e
	bcc .notYetOut2
	lda #$0e
.notYetOut2
	sta EndTmp
	lda #$0e
	sec
	sbc EndTmp
	sta EndTmp2
	jmp .displayCounter
	
.noFadeLine
	bcc .displayCounter
	lda #0
	sta EndTmp2
	
.displayCounter
	; Display counter
	; Height: PixelSize - (3 - PixelGap)
	lda EndPixelSize
	asl
	sec
	sbc #2
	clc
	adc EndPixelGap	
	sta End48Height
	
	lda #14
	sec
	sbc EndPixelSize
	sta EndTmp3
	tax
	
.centerCounterLoop
	sta WSYNC
	dex
	bne .centerCounterLoop

	; display line
	sta WSYNC
	lda EndTmp2
	sta COLUPF
	sta WSYNC
	lda #0
	sta COLUPF
	
.displayLoop
	sta WSYNC

	ldy End48Height		; 3
	lda EndCounterGfx	; 3
	sta GRP0		; 3
	lda EndCounterGfx+1	; 3
	sta GRP1		; 3
	lda EndCounterGfx+2	; 3
	sta GRP0		; 3
	lda EndCounterGfx+5	; 3
	sta EndTmp		; 3
	lax EndCounterGfx+4	; 3
	lda EndCounterGfx+3	; 3
	ldy EndTmp		; 3	
	SLEEP 5
	sta GRP1		; @41
	stx GRP0
	sty GRP1
	sta GRP0
	
	dec End48Height
	bne .displayLoop

	lda #0
	sta GRP0
	sta GRP1
	sta GRP0
	sta WSYNC

	; display line

	sta WSYNC
	lda EndTmp2
	sta COLUPF
	sta WSYNC
	lda #0
	sta COLUPF
	
.waitTimer
	lda INTIM
	bne .waitTimer
	sta WSYNC
	

	; Display the end
	; ----------------------------------------

EndDisplayEnd SUBROUTINE

	lda #110*76/64 + 1
	sta TIM64T
	
	; Set End color
	lda EndDemoState
	cmp #END_END_IN
	bne .notEndIn

	; Fade in end
	lda EndNumFrame
	lsr
	lsr
	cmp #$0e
	bcc .notYetIn
	lda #$0e
.notYetIn
	jmp .setColor

.notEndIn

	cmp #END_WAITFORZOOM
	bne .notEndOut
	; Fade out end
	lda EndNumFrame
	lsr
	lsr
	cmp #$0e
	bcc .notYetOut
	lda #$0e
.notYetOut
	sta EndTmp
	lda #$0e
	sec
	sbc EndTmp
	jmp .setColor

.notEndOut
	; no end
	lda #0

.setColor
	sta COLUP0
	sta COLUP1

	; Display end

	sta WSYNC
	jsr EndInit48
	lda #<end_1
	ldx #>end_1
	ldy #11
	jsr EndPrepare48
	jsr EndDisplay48

.waitTimer
	lda INTIM
	bne .waitTimer
	sta WSYNC
	

	
; -------------------------------------------------------------------------
; VBLANK: Normally 36 lines	
; -------------------------------------------------------------------------

EndDoVBlank SUBROUTINE
	lda #%00000010
	sta VBLANK
	lda #36*76/64 + 1
	sta TIM64T
	
	JSB MusicPlayer

	jsr EndWaitForIntim

	jmp EndKernel


	
; =========================================================================
; Subroutines
; =========================================================================

; -------------------------------------------------------------------------
; Wait for INTIM with WSYNC afterwards
; -------------------------------------------------------------------------

EndWaitForIntim SUBROUTINE
	lda INTIM
	bne EndWaitForIntim
	sta WSYNC
EndRTS
	rts



; -------------------------------------------------------------------------
; Position sprites for 48pix display
; -------------------------------------------------------------------------

EndInit48 SUBROUTINE
	sta WSYNC
	; ----------
	lda #0
	sta GRP0
	sta GRP1
	lda #%00000011	; three copies close
	sta NUSIZ0
	sta NUSIZ1
	lda #RIGHT_2
	sta HMP0
	lda #RIGHT_1
	sta HMP1
	lda #1
	sta VDELP0
	sta VDELP1
	nop
	
	sta RESP0	; @36
	sta RESP1

	sta WSYNC
	sta HMOVE

	rts



; -------------------------------------------------------------------------
; Prepare pointers for 48 pixel sprites.
; IN:
; a: lo ptr to first byte
; x: hi ptr to first byte
; y: height of sprite
; -------------------------------------------------------------------------

EndPrepare48 SUBROUTINE
	sta EndTmp
	lda #<End48_1
	sta EndKernelPtr
	lda #>End48_1
	sta EndKernelPtr + 1
	sty EndTmp2
	dey
	sty End48Height	; for display current later
	ldy #0

.loop
	; Store ptr
	lda EndTmp
	sta (EndKernelPtr),y
	iny
	txa
	sta (EndKernelPtr),y
	iny
	; advance ptr
	lda EndTmp
	clc
	adc EndTmp2
	sta EndTmp
	bcc .noHi
	inx
.noHi
	cpy #12
	bne .loop

	rts



; -------------------------------------------------------------------------
; Display centered 48 pixel sprite.
; Pointers are in End48_x
; Height-1 is in End48Height
; -------------------------------------------------------------------------

EndDisplay48 SUBROUTINE

.loop
	ldy End48Height
	lda (End48_1),y

	sta WSYNC

	sta GRP0
	lda (End48_2),y
	sta GRP1
	lda (End48_3),y
	sta GRP0
	lda (End48_6),y
	sta EndTmp
	lax (End48_5),y
	lda (End48_4),y
	ldy EndTmp
	sta.w GRP1		; @41
	stx GRP0
	sty GRP1
	sta GRP0
	dec End48Height
	bpl .loop

	lda #0
	sta GRP0
	sta GRP1
	sta GRP0
	rts


	
; =========================================================================
; Tables
; =========================================================================

; Script event durations
; ------------------------------

END_COUNTER_IN_DUR	= 220
END_TIM1T_INOUT_DUR	= 220
END_END_IN_DUR		= 220

EndScriptDurations
	dc.b END_COUNTER_IN_DUR		; END_COUNTER_IN = 0
	dc.b END_TIM1T_INOUT_DUR	; END_TIM1T_INOUT = 1
	dc.b END_END_IN_DUR		; END_END_IN	= 2
	dc.b 199			; END_WAITFORZOOM = 3
	dc.b 30				; END_ZOOM = 4
	dc.b 255			; END_WAITFOREND = 5
	dc.b 255
	dc.b 255
	

; Gfx
; ------------------------------

	ALIGN $100
	
	include TIM1T_gfx.asm
	include end_gfx.asm

